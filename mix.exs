defmodule TungstenSwallowUmbrella.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      version: "0.1.0",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [plt_add_apps: [:mix]],
      releases: [
        manganese: [
          version: "0.0.1",
          applications: [manganese: :permanent]
        ]
      ]
    ]
  end

  defp deps do
    [
      {:dialyxir, "~> 1.3", only: [:dev], runtime: false},
      {:credo, "~> 1.7.0-rc.1", only: [:dev, :test], runtime: false}
    ]
  end
end
