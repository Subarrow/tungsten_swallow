import Config

config :logger, :console, level: :info

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

config :manganese, ecto_repos: [Manganese.Repo]

import_config "#{Mix.env()}.exs"
