import Config

app_dir = Application.app_dir(:cobalt)
priv_dir = Path.join([app_dir, "priv/ssh"])

if Mix.env() != :test do
  config :esshd,
    enabled: true,
    priv_dir: priv_dir,
    port: 10_022,
    public_key_authenticator: "Cobalt.SSH.KeyHandler",
    access_list: "Cobalt.SSH.AccessList",
    password_authenticator: "Cobalt.SSH.PasswordHandler",
    handler: {Cobalt.SSH.ShellHandler, :on_shell, []},
    # handler: :elixir,
    parallel_login: true
end
