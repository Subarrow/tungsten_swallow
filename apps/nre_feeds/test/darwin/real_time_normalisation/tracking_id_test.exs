defmodule Darwin.RealTimeNormalisation.TrackingIDTest do
  use ExUnit.Case

  doctest NREFeeds.Darwin.Schema.RealTime.TrackingId

  alias NREFeeds.Darwin.Schema.RealTime.TrackingId

  @test_input_1 %{
    _id: :"TDv1:TrackingID",
    anyAttribs: "",
    berth: %{
      "#text": "GB09",
      _id: :"TDv1:FullTDBerthID",
      anyAttribs: "",
      area: "AW"
    },
    correctTrainID: "2N14",
    incorrectTrainID: "2N13"
  }

  @expected_output_1 %TrackingId{
    correct_signalling_id: "2N14",
    incorrect_signalling_id: "2N13",
    td_area_id: "AW",
    td_berth_id: "GB09"
  }

  test "normalises tracking ID message" do
    assert TrackingId.from_xml(@test_input_1) == @expected_output_1
  end
end
