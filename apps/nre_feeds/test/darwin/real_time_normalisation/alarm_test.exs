defmodule Darwin.RealTimeNormalisation.AlarmTest do
  use ExUnit.Case

  doctest NREFeeds.Darwin.Schema.RealTime.Alarm

  alias NREFeeds.Darwin.Schema.RealTime.Alarm
  alias NREFeeds.Darwin.Schema.RealTime
  alias NREFeeds.Darwin.Util.DarwinXml

  @envelope_first ~s(<Pport xmlns="http://www.thalesgroup.com/rtti/PushPort/v16" xmlns:ns11="http://www.thalesgroup.com/rtti/PushPort/Alarms/v1" ts="2022-02-22T22:22:22.222222Z" version="16.0"><uR >)
  @envelope_second ~s(</uR></Pport>)

  @alarm_clear ~s(<alarm><ns11:clear>1</ns11:clear></alarm>)
  @td_area ~s(<alarm><ns11:set id="1"><ns11:tdAreaFail>E1</ns11:tdAreaFail></ns11:set></alarm>)
  @td_all ~s(<alarm><ns11:set id="1"><ns11:tdFeedFail /></ns11:set></alarm>)
  @tyrell_all ~s(<alarm><ns11:set id="1"><ns11:tyrellFeedFail /></ns11:set></alarm>)

  setup_all do
    {:ok,
     schema_model: DarwinXml.load_schema_model!(),
     schema_record_definitions: DarwinXml.load_schema_record_definitions!()}
  end

  test "clear", state do
    assert_alarm(
      @alarm_clear,
      state,
      %NREFeeds.Darwin.Schema.RealTime.Alarm{
        alarm_id: "1",
        alarm_type: :clear,
        td_area: nil
      }
    )
  end

  test "td area", state do
    assert_alarm(@td_area, state, %NREFeeds.Darwin.Schema.RealTime.Alarm{
      alarm_id: "1",
      alarm_type: :td_area,
      td_area: "E1"
    })
  end

  test "td all", state do
    assert_alarm(@td_all, state, %NREFeeds.Darwin.Schema.RealTime.Alarm{
      alarm_id: "1",
      alarm_type: :td,
      td_area: nil
    })
  end

  test "tyrell all", state do
    assert_alarm(@tyrell_all, state, %NREFeeds.Darwin.Schema.RealTime.Alarm{
      alarm_id: "1",
      alarm_type: :tyrell,
      td_area: nil
    })
  end

  defp assert_alarm(inner_xml, state, alarm_obj = %Alarm{}) do
    {:ok, parsed} =
      DarwinXml.parse_darwin_binary(
        @envelope_first <> inner_xml <> @envelope_second,
        state.schema_model,
        state.schema_record_definitions
      )

    assert RealTime.from_xml(parsed) == %NREFeeds.Darwin.Schema.RealTime{
             source: nil,
             request_id: nil,
             source_instance: nil,
             timestamp_darwin: ~U[2022-02-22 22:22:22.222222Z],
             timestamp_stream: nil,
             snapshot?: false,
             type: NREFeeds.Darwin.Schema.RealTime.Alarm,
             messages: [
               alarm_obj
             ]
           }
  end
end
