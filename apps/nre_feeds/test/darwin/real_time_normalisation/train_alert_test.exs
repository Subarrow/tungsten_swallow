defmodule Darwin.RealTimeNormalisation.TrainAlertTest do
  use ExUnit.Case

  doctest NREFeeds.Darwin.Schema.RealTime.TrainAlert

  alias NREFeeds.Darwin.Schema.RealTime.TrainAlert
  alias NREFeeds.Darwin.Schema.RealTime.TrainAlert.Service

  # Observed output with one modification - all but first and last two CRS codes removed

  @test_input_1 %{
    AlertID: "4079",
    AlertServices: %{
      AlertService: [
        %{
          Location: [
            "VIC",
            "XVJ",
            "XUK",
            "DVP"
          ],
          RID: "202301167141954",
          SSD: "2023-01-16",
          UID: "G41954",
          _id: :"TAv1:AlertService",
          anyAttribs: ""
        }
      ],
      _id: :"TAv1:AlertServices",
      anyAttribs: ""
    },
    AlertText: "This train is formed of 4 instead of 8 coaches today",
    AlertType: "Normal",
    Audience: "Customer",
    CopiedFromAlertID: nil,
    CopiedFromSource: nil,
    SendAlertByEmail: false,
    SendAlertBySMS: false,
    SendAlertByTwitter: false,
    Source: "SE",
    _id: :"TAv1:TrainAlert",
    anyAttribs: ""
  }

  # Modified sample

  @test_input_2 %{
    AlertID: "4079",
    AlertServices: %{
      AlertService: nil,
      _id: :"TAv1:AlertServices"
    },
    AlertText: "This train is formed of 4 instead of 8 coaches today",
    AlertType: "Normal",
    Audience: "Customer",
    CopiedFromAlertID: nil,
    CopiedFromSource: nil,
    SendAlertByEmail: false,
    SendAlertBySMS: false,
    SendAlertByTwitter: false,
    Source: "SE",
    _id: :"TAv1:TrainAlert"
  }

  # Expected outputs

  @expected_output_1 %TrainAlert{
    alert_id: "4079",
    audience: :customer,
    clear?: false,
    copied_from_alert_id: nil,
    copied_from_source: nil,
    forced?: false,
    message: "This train is formed of 4 instead of 8 coaches today",
    send_alert_by_email?: false,
    send_alert_by_sms?: false,
    send_alert_by_twitter?: false,
    services: [
      %Service{
        rid: "202301167141954",
        ssd: ~D[2023-01-16],
        stations: [
          "VIC",
          "XVJ",
          "XUK",
          "DVP"
        ],
        uid: "G41954"
      }
    ],
    source: "SE"
  }

  @expected_output_2 %TrainAlert{
    alert_id: "4079",
    audience: :customer,
    clear?: true,
    copied_from_alert_id: nil,
    copied_from_source: nil,
    forced?: false,
    message: "This train is formed of 4 instead of 8 coaches today",
    send_alert_by_email?: false,
    send_alert_by_sms?: false,
    send_alert_by_twitter?: false,
    services: [],
    source: "SE"
  }

  test "handles set message" do
    assert TrainAlert.from_xml(@test_input_1) == @expected_output_1
  end

  test "handles clear message" do
    assert TrainAlert.from_xml(@test_input_2) == @expected_output_2
  end
end
