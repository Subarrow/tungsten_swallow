defmodule NREFeeds.Promotions.Schema.OperatorContact do
  @moduledoc """
  annotation                    (Annotation)             O Annotation
  primary_telephone             (PrimaryTelephoneNumber) O Primary public phone
  alternate_telephone  (AlternatePublicTelephoneNumbers) O Alternate public phone
  (not mapped)       (AlternateInternalTelephoneNumbers) O Staff-only
  fax                                        (FaxNumber) O Fax
  primary_minicom                 (PrimaryMinicomNumber) O Minicom (i.e. terminal)
  alternate_minicom             (AlternateMinicomNumber) O Alternate minicom
  primary_textphone             (PrimaryTextphoneNumber) O Primary textphone
  alternate_textphone         (AlternateTextphoneNumber) O Alternate textphone
  (not mapped)                           (PostalAddress) O Postal address
  email                                   (EmailAddress) O Email address
  alternate_email              (AlternativeEmailAddress) O Alternate email
  url                                              (Url) O URL

  Several fields here are described in RSPS 5050 and in the schema as containing public
  information (e.g. email address) despite this not being the case. Be very careful using
  any of this contact information.

  Internal phone numbers aren't mapped because these are malformed in the file.

  apps/nre_feeds/priv/nre_promotions_v4_schema/nre-common-v4-0.xsd, ContactDetailsStructure
  """

  @enforce_keys [
    :annotation,
    :primary_telephone,
    :alternate_telephone,
    :fax,
    :primary_minicom,
    :alternate_minicom,
    :primary_textphone,
    :alternate_textphone,
    :email,
    :alternate_email,
    :url
  ]
  defstruct @enforce_keys

  @type t :: %__MODULE__{
          annotation: binary() | nil,
          primary_telephone: binary() | nil,
          alternate_telephone: binary() | nil,
          fax: binary() | nil,
          primary_minicom: binary() | nil,
          alternate_minicom: binary() | nil,
          primary_textphone: binary() | nil,
          alternate_textphone: binary() | nil,
          email: binary() | nil,
          alternate_email: binary() | nil,
          url: binary() | nil
        }

  @spec from_xml(
          %{
            _id: :"common:ContactDetailsStructure",
            Annotation: binary() | nil,
            PostalAddress: map() | nil,
            PrimaryTelephoneNumber: binary() | nil,
            AlternatePublicTelephoneNumbers: binary() | nil,
            AlternateInternalTelephoneNumbers: binary() | nil,
            FaxNumber: binary() | nil,
            PrimaryMinicomNumber: binary() | nil,
            AlternateMinicomNumber: binary() | nil,
            PrimaryTextphoneNumber: binary() | nil,
            AlternateTextphoneNumber: binary() | nil,
            EmailAddress: binary() | nil,
            AlternativeEmailAddress: binary() | nil,
            Url: binary() | nil
          }
          | nil
        ) :: t() | nil

  def from_xml(nil), do: nil

  def from_xml(%{
        _id: :"common:ContactDetailsStructure",
        Annotation: annotation_raw,
        PostalAddress: _postal_address,
        PrimaryTelephoneNumber: primary_telephone,
        AlternatePublicTelephoneNumbers: alternate_telephone,
        AlternateInternalTelephoneNumbers: _internal,
        FaxNumber: fax,
        PrimaryMinicomNumber: primary_minicom,
        AlternateMinicomNumber: alternate_minicom,
        PrimaryTextphoneNumber: primary_textphone,
        AlternateTextphoneNumber: alternate_textphone,
        EmailAddress: email,
        AlternativeEmailAddress: alternate_email,
        Url: url
      }) do
    %__MODULE__{
      annotation: annotation_raw,
      primary_telephone: primary_telephone,
      alternate_telephone: alternate_telephone,
      fax: fax,
      primary_minicom: primary_minicom,
      alternate_minicom: alternate_minicom,
      primary_textphone: primary_textphone,
      alternate_textphone: alternate_textphone,
      email: email,
      alternate_email: alternate_email,
      url: url
    }
  end
end
