defmodule NREFeeds.Promotions.Schema.Railcard do
  @moduledoc """
  Railcard as used in promotion

  railcard_id (RailCardId) R Name of railcard (e.g. "Disabled Persons Railcard")
  price            (Price) O Price of promotion/fare with railcard (e.g. "21.10")
  details        (Details) O Details - any details about the use of this railcard

  This structure is sometimes misused, you may find some promotions with a single
  listed railcard, which contains information in the details field on other
  railcards the promotion is valid with.

  apps/nre_feeds/priv/nre_promotions_v4_schema/nre-promotion-v4-0.xsd, PromotionRailCard
  """

  @enforce_keys [:railcard_id, :price, :details]
  defstruct @enforce_keys

  def from_xml(nil), do: nil

  def from_xml(%{
        _id: :PromotionRailCardListStructure,
        PromotionRailCard: railcards
      }) do
    Enum.map(railcards, &from_xml/1)
  end

  def from_xml(%{
        _id: :PromotionRailCardStructure,
        Price: price,
        Details: details,
        RailCardId: railcard_id
      }) do
    %__MODULE__{
      railcard_id: railcard_id,
      price: price,
      details: details
    }
  end
end
