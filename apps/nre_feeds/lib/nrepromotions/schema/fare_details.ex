defmodule NREFeeds.Promotions.Schema.FareDetails do
  @moduledoc """
  price     (Price) O Price (as a string)
  details (Details) O HTML text for details

  apps/nre_feeds/priv/nre_promotions_v4_schema/nre-promotion-v4-0.xsd, PromotionFareDetailsStructure
  """

  @enforce_keys [:price, :details]
  defstruct @enforce_keys

  @type t :: %__MODULE__{price: binary() | nil, details: binary() | nil}

  @spec from_xml(%{
          _id: :PromotionFareDetailsStructure,
          Price: binary() | nil,
          Details: binary() | nil
        }) :: t() | nil

  def from_xml(nil) do
    nil
  end

  def from_xml(%{
        _id: :PromotionFareDetailsStructure,
        Price: nil,
        Details: nil
      }) do
    nil
  end

  def from_xml(%{
        _id: :PromotionFareDetailsStructure,
        Price: price,
        Details: details
      }) do
    %__MODULE__{
      price: price,
      details: details
    }
  end
end
