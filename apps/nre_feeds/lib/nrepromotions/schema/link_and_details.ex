defmodule NREFeeds.Promotions.Schema.LinkAndDetails do
  @moduledoc """
  Note that this will silently accept nil (returning nil), and if both
  details and uri are nil, will also return nil

  uris        (Uri) O Links for further information
  details (Details) O Details (HTML)

  apps/nre_feeds/priv/nre_promotions_v4_schema/nre-promotion-v4-0.xsd, LinkAndDetailsStructure
  """

  @type t :: %__MODULE__{uris: list(binary()), details: binary()}

  @enforce_keys [:uris, :details]
  defstruct @enforce_keys

  @spec from_xml(
          %{id: :LinkAndDetailsStructure, Details: binary() | nil, Uri: [binary()] | nil}
          | nil
        ) :: t() | nil

  def from_xml(nil) do
    nil
  end

  def from_xml(%{
        _id: :LinkAndDetailsStructure,
        Details: nil,
        Uri: nil
      }) do
    nil
  end

  def from_xml(%{
        _id: :LinkAndDetailsStructure,
        Details: details,
        Uri: nil
      }) do
    %__MODULE__{
      details: details,
      uris: []
    }
  end

  def from_xml(%{
        _id: :LinkAndDetailsStructure,
        Details: details,
        Uri: uri
      })
      when is_list(uri) do
    %__MODULE__{
      details: details,
      uris: uri
    }
  end
end
