defmodule NREFeeds.Promotions.Schema.Passengers do
  @moduledoc """
  note                (Note) O Free text notes
  min_adults     (MinAdults) O Minimum number of adults
  max_adults     (MaxAdults) O Maximum number of adults
  min_children (MinChildren) O Min children
  max_children (MaxChildren) O Max children

  apps/nre_feeds/priv/nre_promotions_v4_schema/nre-promotion-v4-0.xsd, PassengerStructure
  RSPS5050 P-01-02 7.2.4.16 (p29)
  """

  @enforce_keys [:note, :min_adults, :max_adults, :min_children, :max_children]
  defstruct @enforce_keys

  @type t :: %__MODULE__{
          note: binary() | nil,
          min_adults: non_neg_integer(),
          max_adults: non_neg_integer(),
          min_children: non_neg_integer(),
          max_children: non_neg_integer()
        }

  @spec from_xml(
          %{
            _id: :PassengerStructure,
            Note: binary() | nil,
            MinAdults: non_neg_integer(),
            MaxAdults: non_neg_integer(),
            MinChildren: non_neg_integer(),
            MaxChildren: non_neg_integer()
          }
          | nil
        ) :: t() | nil

  def from_xml(nil), do: nil

  def from_xml(%{
        _id: :PassengerStructure,
        Note: note,
        MinAdults: min_adults,
        MaxAdults: max_adults,
        MinChildren: min_children,
        MaxChildren: max_children
      }) do
    %__MODULE__{
      note: note,
      min_adults: min_adults,
      max_adults: max_adults,
      min_children: min_children,
      max_children: max_children
    }
  end
end
