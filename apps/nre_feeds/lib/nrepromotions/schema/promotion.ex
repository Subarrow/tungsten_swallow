defmodule NREFeeds.Promotions.Schema.Promotion do
  @moduledoc """
  A promotion as outlined by NRE. A promotion confers some sort of benefit on a
  passenger if they meet certain eligibility criteria. A promotion may correspond
  to a type of ticket, and may allow you to infer some fare zone boundaries for
  non-London city areas (KB stations can be used for London stations).

  Note that promotions XML may be malformed. At time of writing,
  AlternateInternalTelephoneNumbers appears multiple times inside the TocContact
  tag in some entries (despite this contradicting the schema).
  Editing the common schema to set maxOccurs="unbounded" on the
  alt internal tel numbers is one "solution" (eugh), another would be using a
  regular expression to eliminate or properly compound these.

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  (not mapped)           (ChangeHistory) R Who last changed this promotion and when
  promotion_id     (PromotionIdentifier) R Unique identifier, binary, [A-Za-z0-9]{1,32}
  type                            (Type) R Type of promotion
  nearest_stations      (NearestStation) O "The name of the Promotion.", according to the schema. it's not.
                                           Used for promotions near - but not on - the network, nearest stations
                                           to promotion location (e.g. station near airport, ferry terminals, attraction)
  -                (InterchangeStations) O Station interchange for plusbus
  name                   (PromotionName) R The name of the promotion
  summary                      (Summary) R Promotion summary (HTML)
  offer_details           (OfferDetails) O Eligibility details and link to info on conditions
  validity_start        (ValidityPeriod) O Period in which promotion is valid (commencement)
  validity_end          (ValidityPeriod) O Period in which promotion is valid (end)
  (not mapped)      (ValidityDayAndTime) O Days and times on which the promotion is valid
  available_start    (AvailableFromDate) O Date promotion is available from (a promotion might be available before it's valid)
  available_end      (AvailableFromDate) O Date promotion is available from (a promotion might be available before it's valid)
  -                             (Region) O List of applicable BBC region codes (always empty, unsurprisingly)
  area_map                     (AreaMap) O Link with description to map showing area of validity
  -                     (TimetableLinks) O Links to relevant non-rail timetables (PlusBus only, always nil)
  lead_operator_code           (LeadToc) O Lead train operator, self-set or set by RDG, can be nil
  applicable_operators  (ApplicableTocs) R List of applicable TOC codes, or :all
  -                          (Operators) O Single string describing plusbus operators (always nil)
  -                   (ExcludedServices) O Excluded plusbus services (always nil)
  (not mapped)           (ProductPrices) O Pricing information for promotion
  -      (ApplicableOriginStationGroups) O Applicable origin station groups (fare groups)
  applicable_origins (ApplicableOrigins) O CRS codes for stations journey must begin at
  - (ApplicableDestinationStationGroups) O Applicable destination station groups (fare groups)
  applicable_destinations
                (ApplicableDestinations) O CRS codes for stations journey must end at
  -      (ApplicableZoneOfStationGroups) O Applicable station groups for promotion zone
  applicable_zone
              (ApplicableZoneOfStations) O CRS codes for stations within promotion zone
  reversible?               (Reversible) R Is the promotion valid for traveling both ways?
  -                     (PromotionFlows) O flows and exceptions for promotion
  further_information
                    (FurtherInformation) O Links and details for further information
  ticket_validity_conditions
              (TicketValidityConditions) O Link and details for validity conditions
  booking_conditions (BookingConditions) O Link and details for booking conditions
  purchase_details     (PurchaseDetails) O Link and details for purchasing promotion
  passengers                (Passengers) O "How and where to purchase the promotion." no it's not??? why are so many field descriptions duplicated
  railcards         (PromotionRailCards) O Which railcards are applicable to the promotion
  promotion_code         (PromotionCode) O A short promotion code for an "ordinary" promotion (nominally optional, always present in practice)
  nalco                        (NlcCode) O A national location code code (hm), haven't yet checked if this is for cost centres (more likely) or location
  operator_contact          (TocContact) O Train operating company contact information
  internal_information    (InternalInfo) O Internal information about promotion not visible to public
  ticket_name               (TicketName) O Name (shortened fares form) of the relevant ticket, nominally HTML (expect entities for & etc)
  adult_fares               (AdultFares) O Information about promotion adult fares
  child_fares               (ChildFares) O Information about promotion child fares
  family_fares             (FamilyFares) O Information about promotion family fares
  concession_fares     (ConcessionFares) O Information about promotion concession fares
  group_fares               (GroupFares) O Information about promotion group fares
  -                          (DayTicket) O Plusbus day ticket information
  -                       (SeasonTicket) O Plusbus season ticket information
  -                         (ViewableBy) R Who may view the promotion (always "public")
  -                 (DiscountsAvailable) O Available discounts

  priv/nre_promotions_v4_schema/nre-promotion-v4-0.xsd

  See also RSPS5050 P-01-02, p26-30 incl.
  """

  alias NREFeeds.Promotions.Schema.{
    FareDetails,
    InternalInformation,
    LinkAndDetails,
    OperatorContact,
    Passengers,
    Railcard
  }

  @enforce_keys [
    :promotion_id,
    :type,
    :nearest_stations,
    :name,
    :summary,
    :offer_details,
    :validity_start,
    :validity_end,
    :available_start,
    :available_end,
    :area_map,
    :lead_operator_code,
    :applicable_operators,
    :applicable_origins,
    :applicable_destinations,
    :applicable_zone,
    :reversible?,
    :further_information,
    :ticket_validity_conditions,
    :booking_conditions,
    :purchase_details,
    :passengers,
    :railcards,
    :promotion_code,
    :nalco,
    :operator_contact,
    :internal_information,
    :ticket_name,
    :adult_fares,
    :child_fares,
    :family_fares,
    :concession_fares,
    :group_fares
  ]
  defstruct @enforce_keys

  defp tocs(%{
         choice: %{_id: :"PromoApplicableTocsStructure-AllTocs", AllTocs: true},
         _id: :PromoApplicableTocsStructure
       }) do
    :all
  end

  defp tocs(%{
         choice: %{
           _id: :"PromoApplicableTocsStructure/SEQ1",
           TocRef: tocs
         },
         _id: :PromoApplicableTocsStructure
       }) do
    tocs
  end

  defp stations_list(nil), do: nil

  defp stations_list(%{
         _id: :"common:StationListStructure",
         StationRef: stations
       }) do
    stations
  end

  defp nearest_stations(nil), do: nil

  defp nearest_stations(%{
         _id: :NearestStationStructure,
         CrsCode: stations
       }) do
    stations
  end

  defp parse_date(nil), do: nil

  defp parse_date(date) do
    Date.from_iso8601!(date)
  end

  defp half_open_date(nil), do: {nil, nil}

  defp half_open_date(%{
         _id: :"common:HalfOpenDateRangeStructure",
         StartDate: start_date,
         EndDate: end_date
       }) do
    {parse_date(start_date), parse_date(end_date)}
  end

  def from_xml(%{Promotion: promotions}) when is_list(promotions) do
    Enum.map(promotions, &from_xml/1)
  end

  def from_xml(%{
        _id: :PromotionStructure,
        PromotionIdentifier: promotion_id,
        PromotionName: name,
        Type: type,
        NearestStation: nearest_stations_raw,
        InterchangeStations: nil,
        Summary: summary,
        OfferDetails: offer_details_raw,
        ValidityPeriod: validity_period_raw,
        AvailableFromDate: available_from_raw,
        Region: nil,
        AreaMap: area_map_raw,
        LeadToc: lead_operator_code,
        ApplicableTocs: applicable_tocs_raw,
        Operators: nil,
        ExcludedServices: nil,
        ApplicableOriginStationGroups: nil,
        ApplicableOrigins: applicable_origins_raw,
        ApplicableDestinationStationGroups: nil,
        ApplicableDestinations: applicable_destinations_raw,
        ApplicableZoneOfStationGroups: nil,
        ApplicableZoneOfStations: applicable_zone_of_stations_raw,
        Reversible: reversible?,
        PromotionFlows: nil,
        FurtherInformation: further_information_raw,
        TicketValidityConditions: ticket_validity_conditions_raw,
        BookingConditions: booking_conditions_raw,
        PurchaseDetails: purchase_details_raw,
        Passengers: passengers_raw,
        PromotionRailCards: railcards_raw,
        PromotionCode: promotion_code,
        NlcCode: nalco,
        TocContact: operator_contact_raw,
        InternalInfo: internal_info_raw,
        TicketName: ticket_name,
        AdultFares: adult_fares_raw,
        ChildFares: child_fares_raw,
        FamilyFares: family_fares_raw,
        ConcessionFares: concession_fares_raw,
        GroupFares: group_fares_raw,
        DayTicket: nil,
        SeasonTicket: nil,
        ViewableBy: "public",
        DiscountsAvailable: nil
      }) do
    {validity_start, validity_end} = half_open_date(validity_period_raw)
    {available_start, available_end} = half_open_date(available_from_raw)

    %__MODULE__{
      promotion_id: promotion_id,
      type: type,
      nearest_stations: nearest_stations(nearest_stations_raw),
      name: name,
      summary: summary,
      offer_details: LinkAndDetails.from_xml(offer_details_raw),
      validity_start: validity_start,
      validity_end: validity_end,
      available_start: available_start,
      available_end: available_end,
      area_map: LinkAndDetails.from_xml(area_map_raw),
      lead_operator_code: lead_operator_code,
      applicable_operators: tocs(applicable_tocs_raw),
      applicable_origins: stations_list(applicable_origins_raw),
      applicable_destinations: stations_list(applicable_destinations_raw),
      applicable_zone: stations_list(applicable_zone_of_stations_raw),
      reversible?: reversible?,
      further_information: LinkAndDetails.from_xml(further_information_raw),
      ticket_validity_conditions: LinkAndDetails.from_xml(ticket_validity_conditions_raw),
      booking_conditions: LinkAndDetails.from_xml(booking_conditions_raw),
      purchase_details: LinkAndDetails.from_xml(purchase_details_raw),
      passengers: Passengers.from_xml(passengers_raw),
      railcards: Railcard.from_xml(railcards_raw),
      promotion_code: promotion_code,
      nalco: nalco,
      operator_contact: OperatorContact.from_xml(operator_contact_raw),
      internal_information: InternalInformation.from_xml(internal_info_raw),
      ticket_name: ticket_name,
      adult_fares: FareDetails.from_xml(adult_fares_raw),
      child_fares: FareDetails.from_xml(child_fares_raw),
      family_fares: FareDetails.from_xml(family_fares_raw),
      concession_fares: FareDetails.from_xml(concession_fares_raw),
      group_fares: FareDetails.from_xml(group_fares_raw)
    }
  end
end
