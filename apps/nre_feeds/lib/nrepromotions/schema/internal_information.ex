defmodule NREFeeds.Promotions.Schema.InternalInformation do
  @moduledoc """
  issuing_instructions (IssuingInstructions) O
  details              (Details)             O
  """

  @type t :: %__MODULE__{issuing_instructions: binary() | nil, details: binary() | nil}

  @enforce_keys [:issuing_instructions, :details]
  defstruct @enforce_keys

  @spec from_xml(
          %{
            _id: :InternalInfoStructure,
            Details: binary() | nil,
            IssuingInstructions: binary() | nil
          }
          | nil
        ) :: t() | nil

  def from_xml(nil) do
    nil
  end

  def from_xml(%{
        _id: :InternalInfoStructure,
        Details: nil,
        IssuingInstructions: nil
      }) do
    nil
  end

  def from_xml(%{
        _id: :InternalInfoStructure,
        Details: details,
        IssuingInstructions: issuing_instructions
      }) do
    %__MODULE__{
      details: details,
      issuing_instructions: issuing_instructions
    }
  end
end
