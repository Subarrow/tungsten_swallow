defmodule NREFeeds.Promotions.Util.NREPromotionsXML do
  import TungstenSwallow.Util.XmlTools
  import NREFeeds.KnowledgeBase.Util.KnowledgeBaseXML

  defp root_charlist() do
    root_bin() |> :binary.bin_to_list()
  end

  defp root_bin() do
    Application.app_dir(:nre_feeds) <> "/"
  end

  def load_schema_model! do
    {:ok, schema_model} =
      :erlsom.compile_xsd_file(
        root_charlist() ++ ~c"priv/knowledgebase/nre-promotion-v4-0.xsd",
        [
          {:include_dirs, [root_charlist() ++ ~c"priv/knowledgebase/common"]},
          {:include_fun, &include_fun/4}
        ]
      )

    schema_model
  end

  def parse_binary(
        body,
        schema_model,
        schema_record_definitions
      ) do
    case :erlsom.scan(body, schema_model, [{:expand_entities, true}]) do
      {:ok, erlsom_out, _rest} ->
        {:ok, transform_xml(erlsom_out, schema_record_definitions)}

      {:error, error} ->
        {:error, error}
    end
  end

  def parse_binary!(body, schema_model, schema_record_definitions) do
    {:ok, parsed} = parse_binary(body, schema_model, schema_record_definitions)

    parsed
  end

  def load_promotions_record_definitions!() do
    extract_hrl(root_bin() <> "priv/nre_promotions_v4.hrl")
  end
end
