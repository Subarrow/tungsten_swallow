defmodule Mix.Tasks.NREFeeds.WriteKnowledgeBaseErlsomSchemas do
  @moduledoc "
  Write erlsom schemas priv/knowledgebase/nre-*.xsd to priv/nre_*.hrl
  "
  use Mix.Task

  alias NREFeeds.KnowledgeBase.Util.KnowledgeBaseXML

  @from_to [
    {~c"priv/knowledgebase/nre-promotion-v4-0.xsd", ~c"priv/nre_promotions_v4.hrl"},
    {~c"priv/knowledgebase/nre-incident-v5-0.xsd", ~c"priv/nre_incidents_v5.hrl"},
    {~c"priv/knowledgebase/nre-station-v4-0.xsd", ~c"priv/nre_stations_v4.hrl"}
  ]

  @shortdoc "Write Erlsom representation of NRE KB schemas to file"
  def run(_args) do
    Enum.each(@from_to, &write_schema/1)
  end

  defp write_schema({schema_filename, header_filename}) do
    :erlsom.write_xsd_hrl_file(
      schema_filename,
      header_filename,
      [
        {:include_dirs, [~c"priv/knowledgebase/common"]},
        {:include_fun, &KnowledgeBaseXML.include_fun/4}
      ]
    )
    |> inspect
    |> IO.puts()
  end
end
