defmodule Mix.Tasks.NREFeeds.TestParseNREIncidentsFeed do
  @moduledoc "Dumps from NRE incidents feed to stdout"
  use Mix.Task

  alias NREFeeds.Incidents.Util.NREIncidentsXML
  alias NREFeeds.Incidents.Schema.Incident
  alias TungstenSwallow.Util.BackoffManager

  alias NREFeeds.Darwin.MessageHandler
  alias Barytherium.Frame

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(
      label <> ": " <> inspect(value, pretty: true, limit: :infinity, printable_limit: :infinity)
    )
  end

  @shortdoc "Dumps from NRE incidents feed to stdout"
  def run(_) do
    IO.puts("Start")

    schema_record_definitions = NREIncidentsXML.load_incidents_record_definitions!()

    schema_model = NREIncidentsXML.load_schema_model!()

    IO.puts("Read schema definitions")

    relay_host = String.to_charlist(Application.fetch_env!(:manganese, :relay_host))
    relay_port = 61613
    relay_username = Application.fetch_env!(:manganese, :relay_username)
    relay_password = Application.fetch_env!(:manganese, :relay_password)
    incidents_queue = Application.fetch_env!(:manganese, :incidents_queue)

    subscribe_params = [{"x-stream-offset", "last"}, {"ack", "client"}]

    BackoffManager.start_link(%{})

    {:ok, pid} = Task.start_link(__MODULE__, :loop, [schema_record_definitions, schema_model])

    MessageHandler.start_link(%{
      host: relay_host,
      port: relay_port,
      user: relay_username,
      pass: relay_password,
      queue: incidents_queue,
      id: "nre_inc_mix",
      name: NREFeeds.IncidentsIntake,
      subscribe_params: subscribe_params,
      sink: pid,
      decompress?: false
    })

    wait_loop()
  end

  def wait_loop() do
    Process.sleep(10_000)
    wait_loop()
  end

  def loop(schema_record_definitions, schema_model) do
    receive do
      {:"$gen_cast", frames} ->
        frames
        |> Enum.reject(fn %Frame{headers: headers} ->
          Frame.headers_to_map(headers)["INCIDENT_MESSAGE_STATUS"] == "REMOVED"
        end)
        |> Enum.map(&NREIncidentsXML.parse_frame!(&1, schema_model, schema_record_definitions))
        |> Enum.map(&Incident.from_xml/1)
        |> print_inspect("a")
    end

    loop(schema_record_definitions, schema_model)
  end
end
