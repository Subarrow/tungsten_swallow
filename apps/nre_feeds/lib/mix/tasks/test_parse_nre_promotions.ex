defmodule Mix.Tasks.NREFeeds.TestParseNREPromotions do
  @moduledoc "Dumps the given NRE promotions file to stdout"
  use Mix.Task

  alias NREFeeds.Promotions.Util.NREPromotionsXML
  alias NREFeeds.Promotions.Schema.Promotion

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(
      label <> ": " <> inspect(value, pretty: true, limit: :infinity, printable_limit: :infinity)
    )
  end

  @shortdoc "Dumps given Darwin reference file to stdout"
  def run([file_path]) do
    IO.puts("Start")

    schema_record_definitions = NREPromotionsXML.load_promotions_record_definitions!()

    schema_model = NREPromotionsXML.load_schema_model!()

    IO.puts("Read schema definitions")

    file_contents = File.read!(file_path)

    {:ok, promotions_parsed} =
      NREPromotionsXML.parse_binary(file_contents, schema_model, schema_record_definitions)

    Promotion.from_xml(promotions_parsed)
    |> print_inspect("Contents")

    IO.puts(".")
  end
end
