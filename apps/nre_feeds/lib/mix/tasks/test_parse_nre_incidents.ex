defmodule Mix.Tasks.NREFeeds.TestParseNREIncidents do
  @moduledoc "Dumps the given NRE incidents file to stdout"
  use Mix.Task

  alias NREFeeds.Incidents.Util.NREIncidentsXML
  alias NREFeeds.Incidents.Schema.Incident

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(
      label <> ": " <> inspect(value, pretty: true, limit: :infinity, printable_limit: :infinity)
    )
  end

  @shortdoc "Dumps given Darwin reference file to stdout"
  def run([file_path]) do
    IO.puts("Start")

    schema_record_definitions = NREIncidentsXML.load_incidents_record_definitions!()

    schema_model = NREIncidentsXML.load_schema_model!()

    IO.puts("Read schema definitions")

    file_contents = File.read!(file_path)

    {:ok, incidents_parsed} =
      NREIncidentsXML.parse_binary(file_contents, schema_model, schema_record_definitions)

    Incident.from_xml(incidents_parsed) |> print_inspect("Contents")

    IO.puts(".")
  end
end
