defmodule Mix.Tasks.NREFeeds.TestParseReference do
  @moduledoc "Dumps the given Darwin reference file to stdout"
  use Mix.Task

  alias NREFeeds.Darwin.Util.DarwinXml
  alias NREFeeds.Darwin.Schema.Reference

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(
      label <> ": " <> inspect(value, pretty: true, limit: :infinity, printable_limit: :infinity)
    )
  end

  @shortdoc "Dumps given Darwin reference file to stdout"
  def run([file_path]) do
    IO.puts("Start")

    schema_record_definitions = DarwinXml.load_reference_schema_record_definitions!()

    schema_model = DarwinXml.load_reference_schema_model!()

    IO.puts("Read schema definitions")

    file_contents = File.read!(file_path)

    {:ok, refdata_parsed} =
      DarwinXml.parse_darwin_binary(file_contents, schema_model, schema_record_definitions)

    Reference.from_xml(refdata_parsed) |> print_inspect("Contents")

    IO.puts(".")
  end
end
