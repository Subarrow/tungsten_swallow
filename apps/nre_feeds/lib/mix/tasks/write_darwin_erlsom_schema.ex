defmodule Mix.Tasks.NREFeeds.WriteDarwinErlsomSchema do
  @moduledoc "Write Erlson schema in priv/darwin_v16_schema/ to priv/darwin_v16.hrl"
  use Mix.Task

  alias NREFeeds.Darwin.Util.DarwinXml

  @shortdoc "Write Erlsom representation of darwin schema to file"
  def run(_args) do
    :erlsom.write_xsd_hrl_file(
      ~c"priv/darwin_v16_schema/rttiPPTSchema_v16.xsd",
      ~c"priv/darwin_v16.hrl",
      [{:include_dirs, [~c"priv/darwin_v16_schema"]}, {:include_fun, &DarwinXml.include_fun/4}]
    )
    |> inspect
    |> IO.puts()

    :erlsom.write_xsd_hrl_file(
      ~c"priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd",
      ~c"priv/darwin_v16_reference.hrl",
      [{:include_dirs, [~c"priv/darwin_v16_schema"]}]
    )
    |> inspect
    |> IO.puts()

    :erlsom.write_xsd_hrl_file(
      ~c"priv/darwin_v16_schema/rttiCTTSchema_v8.xsd",
      ~c"priv/darwin_v16_timetable.hrl",
      [
        {:include_dirs, [~c"priv/darwin_v16_schema"]},
        {:include_fun, &DarwinXml.timetable_include_fun/4}
      ]
    )
  end
end
