defmodule NREFeeds.DTD.Util.ParseField do
  def parse(:date_le, <<day::binary-size(2), month::binary-size(2), year::binary-size(4)>>) do
    "#{year}-#{month}-#{day}" |> Date.from_iso8601!()
  end
end
