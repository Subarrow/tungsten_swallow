defmodule NREFeeds.DTD.Util.PositionalRecord do
  @moduledoc """
  Macro helpers to define a struct, and its relation to a positional record
  """

  alias NREFeeds.DTD.Util.PositionalRecord

  defmacro field(name, type, length, args \\ []) do
    args =
      Enum.uniq(args)
      |> Enum.map(fn
        {key, _val} when key not in [:trim_r, :null] ->
          raise "Unsupported arg"

        {key, true} ->
          {key, true}
      end)

    quote do
      @fields [{unquote(name), unquote(type), unquote(length), unquote(args)} | @fields]
    end
  end

  defp parse(type, var, len, [{:null, true} | rest]) do
    empty = String.pad_leading("", len, " ")

    quote do
      if unquote(var) == unquote(empty) do
        nil
      else
        unquote(parse(type, var, len, rest))
      end
    end
  end

  defp parse(type, var, len, [{:trim_r, true} | rest]) do
    quote do
      String.trim_trailing(unquote(parse(type, var, len, rest)))
    end
  end

  defp parse(type, var, len, [{:null, false} | rest]) do
    parse(type, var, len, rest)
  end

  defp parse(type, var, len, [{:trim_r, false} | rest]) do
    parse(type, var, len, rest)
  end

  defp parse(:binary, var, _len, []) do
    var
  end

  defp parse(:date_le, var, _len, []) do
    quote do
      parse(:date_le, unquote(var))
    end
  end

  defp parse(:bool_yn, var, len, []) do
    if len == 1 do
      quote do
        unquote(var) == "Y"
      end
    else
      quote do
        Enum.map(String.codepoints(unquote(var)), &(&1 == "Y"))
      end
    end
  end

  defmacro __using__(_) do
    quote do
      import NREFeeds.DTD.Util.ParseField
      import NREFeeds.DTD.Util.PositionalRecord, only: [schema: 1, field: 3, field: 4]

      @before_compile PositionalRecord
      @fields []
    end
  end

  defmacro __before_compile__(%{module: module}) do
    fields = Module.get_attribute(module, :fields) |> Enum.reverse()

    fields_as_args =
      fields
      |> Enum.map(fn {name, _type, length, _args} ->
        quote do
          unquote(Macro.var(name, module)) :: binary - size(unquote(length))
        end
      end)

    fields_keys_only =
      fields
      |> Enum.map(fn {name, _type, _length, _args} ->
        name
      end)

    vars_to_fields =
      fields
      |> Enum.map(fn {name, type, length, args} ->
        {name, parse(type, Macro.var(name, module), length, args)}
      end)

    quote do
      @enforce_keys unquote(fields_keys_only)
      defstruct @enforce_keys

      def from_line(<<unquote_splicing(fields_as_args)>>) do
        %__MODULE__{
          unquote_splicing(vars_to_fields)
        }
      end
    end
  end

  defmacro schema(do: block) do
    quote do
      unquote(block)
    end
  end
end
