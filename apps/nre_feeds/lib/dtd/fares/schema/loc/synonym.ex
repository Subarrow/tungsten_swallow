defmodule NREFeeds.DTD.Fares.Schema.LOC.Synonym do
  use NREFeeds.DTD.Util.PositionalRecord

  schema do
    field(:uic_code, :binary, 7)
    field(:end_date, :date_le, 8)
    field(:start_date, :date_le, 8)
    field(:description, :binary, 16, trim_r: true)
  end
end
