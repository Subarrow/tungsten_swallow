defmodule NREFeeds.DTD.Fares.Schema.LOC.GroupMember do
  use NREFeeds.DTD.Util.PositionalRecord

  schema do
    field(:group_uic_code, :binary, 7)
    field(:end_date, :date_le, 8)
    field(:member_uic_code, :binary, 7)
    field(:member_crs_code, :binary, 3)
  end
end
