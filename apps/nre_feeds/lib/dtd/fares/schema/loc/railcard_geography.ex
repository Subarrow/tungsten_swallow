defmodule NREFeeds.DTD.Fares.Schema.LOC.RailcardGeography do
  use NREFeeds.DTD.Util.PositionalRecord

  schema do
    field(:uic_code, :binary, 7)
    field(:railcard_code, :binary, 3)
    field(:end_date, :date_le, 8)
  end
end
