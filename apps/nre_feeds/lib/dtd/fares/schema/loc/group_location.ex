defmodule NREFeeds.DTD.Fares.Schema.LOC.GroupLocation do
  use NREFeeds.DTD.Util.PositionalRecord

  schema do
    field(:group_uic_code, :binary, 7)
    field(:end_date, :date_le, 8)
    field(:start_date, :date_le, 8)
    field(:quote_date, :date_le, 8)
    field(:description, :binary, 16, trim_r: true)
    field(:ers_country, :binary, 2, null: true)
    field(:ers_code, :binary, 3, null: true)
  end
end
