defmodule NREFeeds.DTD.Fares.Schema.LOC do
  alias NREFeeds.DTD.Fares.Schema.LOC

  def from_line(<<"R", "G", rest::binary>>) do
    LOC.GroupLocation.from_line(rest)
  end

  def from_line(<<"R", "M", rest::binary>>) do
    LOC.GroupMember.from_line(rest)
  end

  def from_line(<<"R", "R", rest::binary>>) do
    LOC.RailcardGeography.from_line(rest)
  end

  def from_line(<<"R", "S", rest::binary>>) do
    LOC.Synonym.from_line(rest)
  end

  def from_line(<<"R", "L", rest::binary>>) do
    LOC.Location.from_line(rest)
  end
end
