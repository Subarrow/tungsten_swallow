defmodule NREFeeds.DTD.Fares.Schema.Trailer do
  @enforce_keys [:generated_date]
  defstruct @enforce_keys

  def from_line(
        <<"/!! End of file (", days::binary-size(2), "/", months::binary-size(2), "/",
          years::binary-size(4), ")">>
      ) do
    %__MODULE__{generated_date: Date.from_iso8601!("#{years}-#{months}-#{days}")}
  end
end
