defmodule NREFeeds.DTD.Fares do
  @moduledoc """

  https://wiki.openraildata.com/index.php?title=Fares_Data
  RSPS5045
  """

  @behaviour TungstenSwallow.Util.StaticFileDefinition

  alias NREFeeds.DTD.Fares.Schema
  alias NREFeeds.DTD.Fares.Schema.LOC

  def from_file_path!(path) do
    path = Path.absname(path)
    file_type = String.split(path, ".") |> List.last()

    {:ok, contents} =
      File.open(path, [:read], fn file_handle ->
        read!(file_handle, %{file_type: file_type})
      end)

    contents
  end

  def parse_line(_, line = <<"/!! End of file (", _rest::binary>>) do
    Schema.Trailer.from_line(line)
  end

  def parse_line(_, <<"/!!", _rest::binary>>) do
    nil
  end

  def parse_line("LOC", str) do
    LOC.from_line(str)
  end

  def get_version(file_handle) do
    case {
      :file.position(file_handle, {:eof, -500}),
      IO.read(file_handle, :eof),
      :file.position(file_handle, :bof)
    } do
      {{:ok, _newpos_1}, lines, {:ok, _newpos_2}} when is_binary(lines) ->
        trailer =
          <<"/!! End of file", _rest::binary>> = lines |> String.split("\n") |> List.last()

        {:ok, trailer}

      {a, b, c} ->
        [a, b, c]
        |> Enum.filter(fn
          {:error, _reason} -> true
          :eof -> true
          _ -> false
        end)
        |> Enum.map(fn
          {:error, reason} -> {:error, reason}
          :eof -> {:error, :eof}
        end)
        |> List.first()
    end
  end

  def read!(file_handle, %{file_type: file_type}) do
    if file_type not in ["LOC"] do
      raise "Not a supported file (note file must retain original extension, e.g. RJFAF648.LOC)"
    end

    IO.read(file_handle, :eof)
    |> String.split("\n")
    |> Enum.map(&parse_line(file_type, &1))
    |> Enum.reject(&is_nil/1)
  end
end
