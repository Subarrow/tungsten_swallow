defmodule NREFeeds.KnowledgeBase.Util.KnowledgeBaseXML do
  @spec include_fun(charlist(), charlist(), [], [charlist()]) ::
          {charlist(), charlist()}

  def include_fun(namespace, schema_location, [], [include_dir | []]) do
    contents = File.read!(:binary.list_to_bin(include_dir ++ ~c"/" ++ schema_location))

    new_namespace =
      case namespace do
        ~c"http://nationalrail.co.uk/xml/common" -> ~c"common"
        ~c"http://www.govtalk.gov.uk/people/AddressAndPersonalDetails" -> ~c"APD"
        ~c"http://www.govtalk.gov.uk/people/bs7666" -> ~c"BS7666"
        ~c"http://www.govtalk.gov.uk/core" -> ~c"core"
      end

    {:binary.bin_to_list(contents), new_namespace}
  end
end
