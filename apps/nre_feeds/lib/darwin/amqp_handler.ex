defmodule NREFeeds.Darwin.AMQPHandler do
  use GenServer
  use AMQP
  require Logger

  alias Barytherium.Frame

  alias TungstenSwallow.Util.BackoffManager

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: opts.name)
  end

  def init(opts = %{name: name}) do
    {interval, _} = BackoffManager.get_interval(name)
    Logger.info("#{name} waiting #{interval}ms to start connection")
    Process.send_after(self(), :connect, interval)
    {:ok, %{opts: opts}}
  end

  defp decompress_frame_body(frame = %Frame{body: body}) do
    %Frame{frame | body: :zlib.gunzip(:binary.bin_to_list(body))}
  end

  defp restructure_headers(headers) do
    headers |> Enum.map(fn {key, _type, value} -> {key, "#{value}"} end)
  end

  def handle_info(
        :connect,
        state = %{
          opts:
            opts = %{
              host: host,
              port: port,
              user: user,
              pass: pass,
              name: name,
              queue: queue,
              sink: _sink
            }
        }
      ) do
    Logger.info("#{name} Begin connection to #{host}:#{port}")
    BackoffManager.notify_connect(name)

    case Connection.open(host: host, port: port, username: user, password: pass) do
      {:ok, connection} ->
        Process.monitor(connection.pid)

        {:ok, channel} = Channel.open(connection)

        :ok = Basic.qos(channel, prefetch_count: 10)
        {:ok, _consumer_tag} = Basic.consume(channel, queue)

        {:noreply,
         Map.merge(state, %{
           network: connection,
           channel: channel,
           opts: %{opts | pass: nil}
         })}

      {:error, error} ->
        Logger.error("#{name} Connection to #{host}:#{port} failed: #{inspect(error)}")
        BackoffManager.notify_failure(name)
        {:stop, :connect_disconnected, state}
    end
  end

  def handle_info(
        {:DOWN, _ref, :process, _pid, _reason},
        state = %{opts: %{name: name, host: host, port: port}}
      ) do
    Logger.error("#{name} Connection to #{host}:#{port} dropped")
    {:stop, :connect_disconnected, state}
  end

  def handle_info(
        {:basic_consume_ok, %{consumer_tag: _consumer_tag}},
        state = %{opts: %{name: name}}
      ) do
    Logger.info("#{name} Consumer OK")
    {:noreply, state}
  end

  def handle_info({:basic_cancel, %{consumer_tag: _consumer_tag}}, state = %{opts: %{name: name}}) do
    Logger.warning("#{name} Broker cancelled (non-requested)")
    {:stop, :normal, state}
  end

  def handle_info(
        {:basic_cancel_ok, %{consumer_tag: _consumer_tag}},
        state = %{opts: %{name: name}}
      ) do
    Logger.info("#{name} Broker cancelled (requested)")
    {:stop, :normal, state}
  end

  def handle_info(
        {:basic_deliver, payload, _meta = %{delivery_tag: delivery_tag, headers: headers}},
        state = %{channel: channel, opts: opts = %{name: _name, sink: sink}}
      ) do
    frame = %Frame{command: :message, body: payload, headers: restructure_headers(headers)}

    frame =
      if Map.get(opts, :decompress?, true) do
        frame |> decompress_frame_body()
      else
        frame
      end

    GenServer.cast(sink, [
      frame
    ])

    Basic.ack(channel, delivery_tag)

    {:noreply, state}
  end
end
