defimpl TungstenSwallow.Util.Linked.LinkedStructs,
  for: NREFeeds.Darwin.Schema.RealTime.DisruptionReason do
  alias NREFeeds.Darwin.Schema.RealTime.DisruptionReason

  import TungstenSwallow.Util.Linked

  def to_linked_map(%DisruptionReason{tiploc: nil, reason_code: reason_code}, _context) do
    %{
      "type" => "DisruptionOutline",
      "reason_code" => reason_code,
      "reason" => ref([:darwin, :disruption])
    }
  end

  def to_linked_map(
        %DisruptionReason{tiploc: tiploc, reason_code: reason_code, near?: true},
        _context
      ) do
    %{
      "type" => "DisruptionOutline",
      "tiploc" => tiploc,
      "reason_code" => reason_code,
      "reason" => ref([:darwin, :disruption]),
      "near" => ref([:darwin, :location, tiploc])
    }
  end

  def to_linked_map(
        %DisruptionReason{tiploc: tiploc, reason_code: reason_code, near?: false},
        _context
      ) do
    %{
      "type" => "DisruptionOutline",
      "tiploc" => tiploc,
      "reason_code" => reason_code,
      "reason" => ref([:darwin, :disruption]),
      "at" => ref([:darwin, :location, tiploc])
    }
  end
end
