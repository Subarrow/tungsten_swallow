alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation

defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: Formation do
  import TungstenSwallow.Util.Linked

  defp coach_to_map(formation = %Formation.Coach{toilet: %Formation.Coach.Toilet{type: "None"}}) do
    coach_to_map(%Formation.Coach{formation | toilet: nil})
  end

  defp coach_to_map(%Formation.Coach{
         coach_class: coach_class,
         coach_number: coach_number,
         toilet: %Formation.Coach.Toilet{type: type, status: status}
       }) do
    %{
      "type" => "FormationCoach",
      "coach_number" => coach_number,
      "coach_class" => coach_class,
      "toilet" => %{
        "type" => "Toilet",
        "toilet_type" => type,
        "toilet_status" => status,
        "is_cancelled" => status == :not_in_service
      }
    }
  end

  defp coach_to_map(%Formation.Coach{
         coach_class: coach_class,
         coach_number: coach_number,
         toilet: nil
       }) do
    %{"type" => "FormationCoach", "coach_number" => coach_number, "coach_class" => coach_class}
  end

  def to_linked_map(
        %Formation{
          formation_id: formation_id,
          coaches: coaches
        },
        %{rid: rid}
      ) do
    %{
      "type" => "Formation",
      "@id" => self_ref([:darwin, :service, rid, :formation, formation_id]),
      "rid" => rid,
      "formation_id" => formation_id,
      "coaches" => Enum.map(coaches, &coach_to_map/1)
    }
  end
end
