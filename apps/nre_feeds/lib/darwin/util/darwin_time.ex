defmodule NREFeeds.Darwin.Util.DarwinTime do
  @moduledoc """
  Various utilities for normalising Darwin times
  """

  alias NREFeeds.Darwin.Data.LocationTimezones

  alias NREFeeds.Darwin.Schema.Types

  @timezones LocationTimezones.location_timezones()

  @type combine_state :: NaiveDateTime.t() | Date.t()

  @type normalised_public_datetimes :: %{
          optional(:arrive) => NaiveDateTime.t(),
          optional(:depart) => NaiveDateTime.t()
        }

  @type normalised_working_datetimes :: %{
          optional(:arrive) => NaiveDateTime.t(),
          optional(:depart) => NaiveDateTime.t(),
          optional(:pass) => NaiveDateTime.t()
        }

  @type normalised_public_times :: %{
          optional(:arrive) => Time.t(),
          optional(:depart) => Time.t()
        }

  @type normalised_working_times :: %{
          optional(:arrive) => Time.t(),
          optional(:depart) => Time.t(),
          optional(:pass) => Time.t()
        }

  @doc """
  Calculates seconds elapsed since midnight for a time, from 0 (00:00:00) until 86399
  (23:59:59)
  """

  @spec seconds_since_midnight(Time.t()) :: 0..86_399

  def seconds_since_midnight(%Time{second: second, minute: minute, hour: hour}) do
    second + minute * 60 + hour * 3600
  end

  def seconds_since_midnight(nil) do
    0
  end

  @doc """
  Subtracts first time from second time, returns 0 if either time is nil
  """

  @spec compare_time_seconds(Time.t() | nil, Time.t() | nil) :: integer()

  def compare_time_seconds(previous_time, current_time)
      when is_nil(previous_time) or is_nil(current_time),
      do: 0

  def compare_time_seconds(previous_time, current_time) do
    seconds_since_midnight(current_time) - seconds_since_midnight(previous_time)
  end

  @doc """
  Compares two nullable times to determine whether the step between them means the second
  time is yesterday (-1), today (0) or tomorrow (+1)

  This is done by subtracting the previous time from the current time, and comparing the
  difference in hours.
  If the difference is less than -6, the date offset will be +1 (crossing midnight forwards);
  If the difference is between -6 and +18, the date offset is assumed to be a regular time
  increase (0).
  If the difference is greater than +18, the offset will be -1 (crossing midnight backwards)
  """

  @spec days_offset(Time.t() | nil, Time.t() | nil) :: integer()

  def days_offset(previous_time, current_time) do
    difference = compare_time_seconds(previous_time, current_time)

    cond do
      difference < -6 * 3600 -> +1
      difference in (-6 * 3600)..(+18 * 3600) -> 0
      difference > +18 * 3600 -> -1
    end
  end

  @doc """
  Pads then parses a Darwin time
  """

  @spec parse_darwin_time!(binary() | nil) :: Time.t() | nil

  def parse_darwin_time!(nil), do: nil

  def parse_darwin_time!(time) when byte_size(time) == 5 do
    parse_darwin_time!(time <> ":00")
  end

  def parse_darwin_time!(time) do
    Time.from_iso8601!(time)
  end

  @doc """
  combine_time is meant to be used for two purposes:
  * Walking a complete schedule to apply correct dates to working/public times
  * Using the corresponding working naive datetime to confer the correct date on an estimated/actual status
    time

  If you're using this for the first (walking a schedule), the first call should have the scheduled start
  date as the first argument, for all subsequent calls, use whatever was last returned in the first
  position of the tuple this function returns.

  If you're using this for the second scenario, you can discard the first element in the tuple

  See `days_offset/2` for more information on the calculations used to determine when the date needs to be
  incremented or decremented
  """

  @spec combine_time!(combine_state(), Time.t() | nil) ::
          {combine_state(), NaiveDateTime.t() | nil}

  def combine_time!(last_datetime, nil) do
    {last_datetime, nil}
  end

  def combine_time!(datetime = %NaiveDateTime{}, time = %Time{}) do
    combine_time_tup!({NaiveDateTime.to_date(datetime), NaiveDateTime.to_time(datetime)}, time)
  end

  def combine_time!(date = %Date{}, time = %Time{}) do
    combine_time_tup!({date, nil}, time)
  end

  @spec combine_time_tup!({Date.t(), Time.t() | nil}, Time.t()) ::
          {last_datetime :: NaiveDateTime.t(), combined_time :: NaiveDateTime.t()}

  defp combine_time_tup!({last_date, last_time}, time = %Time{}) do
    offset_date = Date.add(last_date, days_offset(last_time, time))
    {NaiveDateTime.new!(offset_date, time), NaiveDateTime.new!(offset_date, time)}
  end

  @spec parse_working_times!(map(), option) :: normalised_working_times() when option: :map
  @spec parse_working_times!(map(), option) :: list() when option: :list

  def parse_working_times!(darwin_object, option \\ :map)

  def parse_working_times!(darwin_object, :list) do
    parse_times!(darwin_object, wta: :arrive, wtp: :pass, wtd: :depart)
  end

  def parse_working_times!(darwin_object, :map) do
    parse_working_times!(darwin_object, :list) |> Map.new()
  end

  @spec parse_public_times!(map(), option) :: normalised_public_times() when option: :map
  @spec parse_public_times!(map(), option) :: list() when option: :list

  def parse_public_times!(darwin_object, option \\ :map)

  def parse_public_times!(darwin_object, :list) do
    parse_times!(darwin_object, pta: :arrive, ptd: :depart)
  end

  def parse_public_times!(darwin_object, :map) do
    parse_public_times!(darwin_object, :list) |> Map.new()
  end

  @spec parse_combine_working_times!(map(), combine_state()) ::
          {combine_state(), normalised_working_datetimes()}

  def parse_combine_working_times!(darwin_object, last_datetime) do
    darwin_object |> parse_working_times!(:list) |> combine_times!(last_datetime)
  end

  @spec parse_combine_public_times!(map(), combine_state()) ::
          {combine_state(), normalised_public_datetimes()}

  def parse_combine_public_times!(darwin_object, last_datetime) do
    darwin_object |> parse_public_times!(:list) |> combine_times!(last_datetime)
  end

  @spec parse_times!(map(), [{atom(), atom()}]) :: [
          {atom(), Time.t()}
        ]

  def parse_times!(darwin_object, field_map) do
    Enum.map(field_map, fn {field_name, normalised_name} ->
      {normalised_name, parse_darwin_time!(Map.get(darwin_object, field_name))}
    end)
    |> Enum.filter(fn
      {_, nil} -> false
      {_, _} -> true
    end)
  end

  @spec combine_times!([{atom(), Time.t()}], combine_state()) ::
          {combine_state(), normalised_working_datetimes() | normalised_public_datetimes()}

  def combine_times!([], last_datetime) do
    {last_datetime, %{}}
  end

  def combine_times!(times, last_datetime) do
    times
    |> Enum.reduce({last_datetime, %{}}, fn {normalised_name, time},
                                            {combine_state, accumulator} ->
      {last_datetime, combined_time} = combine_time!(combine_state, time)
      {last_datetime, Map.put(accumulator, normalised_name, combined_time)}
    end)
  end

  @doc """
  Converts three (nullable) working times into a three-integer triplet, formed of the seconds
  since midnight for each time, which can be more easily used for matching
  """
  @spec wt_match(normalised_working_times() | normalised_public_times()) ::
          {integer(), integer(), integer()}

  def wt_match(times) do
    Enum.map([:arrive, :pass, :depart], fn key ->
      Map.get(times, key) |> seconds_since_midnight()
    end)
    |> List.to_tuple()
  end

  @doc """
  Uses location timezone data to adjust a normalised wt-style map to use UTC times
  """

  @spec wt_to_utc!(Types.tiploc(), normalised_working_datetimes() | nil) :: map() | nil

  def wt_to_utc!(_, nil), do: nil

  def wt_to_utc!(tiploc, times) do
    times
    |> Enum.map(fn {key, naive_dt} ->
      {key, to_utc!(tiploc, naive_dt)}
    end)
    |> Map.new()
  end

  @doc """
  Uses location timezone data to adjust a NaiveDateTime to a timezone-aware UTC DateTime

  As a partial fix for Darwin's timezone non-awareness, a day spans from 0200 until 0159 for
  timezone purposes
  """

  @spec to_utc!(Types.tiploc(), NaiveDateTime.t() | nil) :: DateTime.t() | nil

  def to_utc!(_tiploc, nil), do: nil

  def to_utc!(tiploc, naive_dt) do
    timezone = @timezones |> Map.get(tiploc, "Europe/London")

    cond do
      naive_dt.hour >= 2 ->
        DateTime.from_naive!(naive_dt, timezone) |> DateTime.shift_zone!("Etc/UTC")

      naive_dt.hour < 2 ->
        NaiveDateTime.add(naive_dt, -prev_day_mid_offset(naive_dt, timezone), :second)
        |> DateTime.from_naive!("Etc/UTC")
    end
  end

  defp prev_day_mid_offset(naive_dt, timezone) do
    new_dt =
      NaiveDateTime.to_date(naive_dt)
      |> Date.add(-1)
      |> DateTime.new!(Time.new!(12, 0, 0), timezone)

    new_dt.utc_offset + new_dt.std_offset
  end
end
