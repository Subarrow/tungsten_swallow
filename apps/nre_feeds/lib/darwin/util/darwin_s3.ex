defmodule NREFeeds.Darwin.Util.DarwinS3 do
  @moduledoc """
  Functions to retrieve darwin reference data from S3
  """

  alias TungstenSwallow.Util.{S3, XmlTools}

  defp transform_contents({"Contents", %{}, child_elements, ""}) do
    Enum.map(child_elements, fn {key, %{}, _child_elements, text} -> {key, text} end)
    |> Enum.reject(fn entry -> entry == {"Owner", ""} end)
    |> Map.new()
  end

  defp transform_bucket_list({"ListBucketResult", %{}, child_elements, ""}) do
    child_elements
    |> Enum.filter(fn {key, _attributes, _child_elements, _text} -> key == "Contents" end)
    |> Enum.map(&transform_contents/1)
  end

  defp name_fun(name, ~c"http://s3.amazonaws.com/doc/2006-03-01/", _prefix) do
    name
  end

  def list_darwin_files({access, secret}) do
    case S3.authorised_get(
           "https://s3.eu-west-1.amazonaws.com/darwin.xmltimetable/",
           "eu-west-1",
           {access, secret}
         ) do
      {:ok, 200, _headers, body} ->
        {:ok, parsed, _rest} = :erlsom.simple_form(body, nameFun: &name_fun/3)
        {:ok, parsed |> XmlTools.transform_simple_xml() |> transform_bucket_list}

      {:ok, 200, _headers} ->
        {:error, :no_body}

      error = {:error, _term} ->
        error
    end
  end

  def latest_reference_filename({access, secret}) do
    {:ok, file_listing} = list_darwin_files({access, secret})

    %{"Key" => key} =
      file_listing
      |> Enum.filter(fn %{"Key" => key} -> String.contains?(key, "_ref_v3") end)
      |> Enum.sort(fn %{"LastModified" => last_modified_a},
                      %{"LastModified" => last_modified_b} ->
        last_modified_a <= last_modified_b
      end)
      |> List.last()

    key
  end

  def latest_timetable_filename({access, secret}) do
    {:ok, file_listing} = list_darwin_files({access, secret})

    %{"Key" => key} =
      file_listing
      |> Enum.filter(fn %{"Key" => key} -> String.contains?(key, "_v8") end)
      |> Enum.sort(fn %{"LastModified" => last_modified_a},
                      %{"LastModified" => last_modified_b} ->
        last_modified_a <= last_modified_b
      end)
      |> List.last()

    key
  end

  def get_latest_reference_file({access, secret}) do
    target_filename = latest_reference_filename({access, secret})
    get_file(target_filename, {access, secret})
  end

  def get_latest_timetable_file({access, secret}) do
    target_filename = latest_timetable_filename({access, secret})
    get_file(target_filename, {access, secret})
  end

  @spec get_file(binary(), {binary(), binary()}, boolean()) :: {:ok, binary()} | {:error, term()}

  def get_file(filename, {access, secret}, decompress? \\ true) do
    case S3.authorised_get(
           "https://s3.eu-west-1.amazonaws.com/darwin.xmltimetable/#{filename}",
           "eu-west-1",
           {access, secret}
         ) do
      {:ok, 200, _headers, body} ->
        if decompress? do
          {:ok, body |> :zlib.gunzip()}
        else
          {:ok, body}
        end

      {:ok, 200, _headers} ->
        {:error, :no_body}

      error = {:error, _term} ->
        error
    end
  end
end
