defmodule NREFeeds.Darwin.Util.DarwinXml do
  @moduledoc """
  XML parsing utilities specifically useful for Darwin
  """

  alias Barytherium.Frame
  import TungstenSwallow.Util.XmlTools

  defp root_charlist() do
    root_bin() |> :binary.bin_to_list()
  end

  defp root_bin() do
    Application.app_dir(:nre_feeds) <> "/"
  end

  @spec preprocess_ow_xml(binary()) :: binary()
  def preprocess_ow_xml(message_body) do
    message_body
    |> String.replace("\n", "")
    |> String.replace(~r[<ns7:Msg>(.+)</ns7:Msg>]s, fn match ->
      ~c"<ns7:Msg>" ++
        :xmerl_lib.export_text(String.replace(match, ~r"</?ns7:Msg>", "")) ++ ~c"</ns7:Msg>"
    end)
  end

  @spec parse_darwin_frame(Frame.t(), tuple(), any()) ::
          {Elixir.DateTime.t(), binary(), map()}
          | {Elixir.DateTime.t(), binary(), {:error, any()}}

  def parse_darwin_frame(
        _frame = %Frame{headers: list_headers, body: body},
        schema_model,
        schema_record_definitions
      ) do
    headers = Frame.headers_to_map(list_headers)

    timestamp_raw = headers["timestamp"] || headers["CamelMessageTimestamp"]
    {:ok, timestamp} = DateTime.from_unix(trunc(elem(Integer.parse(timestamp_raw), 0) / 1000))

    case parse_darwin_binary(body, schema_model, schema_record_definitions) do
      {:ok, output} -> {timestamp, body, output}
      {:error, error} -> {:error, body, error}
    end
  end

  def parse_darwin_binary(
        body,
        schema_model,
        schema_record_definitions
      ) do
    body_escaped = preprocess_ow_xml(body)

    case :erlsom.scan(body_escaped, schema_model, [{:expand_entities, true}]) do
      {:ok, erlsom_out, _rest} ->
        {:ok, transform_xml(erlsom_out, schema_record_definitions)}

      {:error, error} ->
        {:error, error}
    end
  end

  @spec include_fun(charlist(), charlist(), [], [charlist()]) :: {charlist(), charlist()}

  def include_fun(namespace, schema_location, [], [include_dir | []]) do
    contents = File.read!(:binary.list_to_bin(include_dir ++ ~c"/" ++ schema_location))

    new_namespace =
      case namespace do
        ~c"http://thalesgroup.com/RTTI/PushPortStatus/root_1" -> ~c"PPS"
        ~c"http://www.thalesgroup.com/rtti/PushPort/CommonTypes/" ++ rest -> ~c"CT" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/Schedules/" ++ rest -> ~c"SC" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/Forecasts/" ++ rest -> ~c"TS" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/TrainOrder/" ++ rest -> ~c"TO" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/StationMessages/" ++ rest -> ~c"SM" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/TrainAlerts/" ++ rest -> ~c"TA" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/TDData/" ++ rest -> ~c"TD" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/Alarms/" ++ rest -> ~c"AL" ++ rest
        ~c"http://www.thalesgroup.com/rtti/PushPort/Formations/" ++ rest -> ~c"FO" ++ rest
      end

    {:binary.bin_to_list(contents), new_namespace}
  end

  @spec timetable_include_fun(charlist(), charlist(), [], [charlist()]) ::
          {charlist(), charlist()}

  def timetable_include_fun(namespace, schema_location, [], [include_dir | []]) do
    contents = File.read!(:binary.list_to_bin(include_dir ++ ~c"/" ++ schema_location))

    new_namespace =
      case namespace do
        ~c"http://www.thalesgroup.com/rtti/PushPort/CommonTypes/" ++ rest -> ~c"CT" ++ rest
      end

    {:binary.bin_to_list(contents), new_namespace}
  end

  def load_schema_model! do
    {:ok, schema_model} =
      :erlsom.compile_xsd_file(
        root_charlist() ++ ~c"priv/darwin_v16_schema/rttiPPTSchema_v16.xsd",
        [
          {:include_dirs, [root_charlist() ++ ~c"priv/darwin_v16_schema"]},
          {:include_fun, &include_fun/4}
        ]
      )

    schema_model
  end

  def load_reference_schema_model! do
    {:ok, schema_model} =
      :erlsom.compile_xsd_file(
        root_charlist() ++ ~c"priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd",
        [
          {:include_dirs, [root_charlist() ++ ~c"priv/darwin_v16_schema"]}
        ]
      )

    schema_model
  end

  def load_timetable_schema_model! do
    {:ok, schema_model} =
      :erlsom.compile_xsd_file(
        root_charlist() ++ ~c"priv/darwin_v16_schema/rttiCTTSchema_v8.xsd",
        [
          {:include_dirs, [root_charlist() ++ ~c"priv/darwin_v16_schema"]}
        ]
      )

    schema_model
  end

  def load_schema_record_definitions!() do
    extract_hrl(root_bin() <> "priv/darwin_v16.hrl")
  end

  def load_reference_schema_record_definitions!() do
    extract_hrl(root_bin() <> "priv/darwin_v16_reference.hrl")
  end

  def load_timetable_record_definitions!() do
    extract_hrl(root_bin() <> "/priv/darwin_v16_timetable.hrl")
  end
end
