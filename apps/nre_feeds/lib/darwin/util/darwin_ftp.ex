defmodule NREFeeds.Darwin.Util.DarwinFTP do
  @moduledoc """
  Utils for Darwin FTP
  """
  def connect!(hostname, user, pass) do
    {:ok, conn} = :ftp.open(:erlang.binary_to_list(hostname))

    :ftp.user(
      conn,
      :erlang.binary_to_list(user),
      :erlang.binary_to_list(pass),
      :erlang.binary_to_list(user)
    )

    :ftp.type(conn, :binary)

    conn
  end

  def with_conn!(hostname, user, pass, func) do
    conn = connect!(hostname, user, pass)
    return = func.(conn)
    :ftp.close(conn)
    return
  end

  def bare_list!(conn, dir) do
    {:ok, listing} = :ftp.nlist(conn, dir |> :erlang.binary_to_list())
    :string.tokens(listing, ~c"\r\n")
  end

  def detail_list!(conn, dir) do
    {:ok, listing} = :ftp.ls(conn, dir |> :erlang.binary_to_list())
    :string.tokens(listing, ~c"\r\n")
  end

  def list_snapshot!(conn) do
    bare_list!(conn, "/snapshot")
  end

  def list_pushport!(conn) do
    bare_list!(conn, "/pushport")
  end

  def list_snapshot_detail!(conn) do
    detail_list!(conn, "/snapshot") |> Enum.map(&:binary.list_to_bin/1)
  end

  def list_pushport_detail!(conn) do
    detail_list!(conn, "/pushport") |> Enum.map(&:binary.list_to_bin/1)
  end

  def get_file!(conn, filename) do
    {:ok, contents} = :ftp.recv_bin(conn, filename |> :erlang.binary_to_list())
    contents
  end

  def get_snapshot_lines!(conn) do
    contents = get_file!(conn, "/snapshot/snapshot.gz") |> :zlib.gunzip()

    Regex.scan(~r[<Pport.+</Pport>]msU, contents) |> List.flatten()
  end
end
