defmodule NREFeeds.Darwin.Util.DarwinFieldNorm do
  @moduledoc """
  Utilities for normalising darwin fields (other than times)
  """

  @doc """
  For parsing nullable integers, will raise if Integer.parse returns error or if the remainder is non-empty
  """

  @spec parse_nullable_integer!(binary() | nil | integer()) :: integer() | nil

  def parse_nullable_integer!(value) when is_integer(value), do: value

  def parse_nullable_integer!(nil), do: nil

  def parse_nullable_integer!(value) do
    case Integer.parse(value) do
      :error -> raise "Integer.parse returned :error (value was #{inspect(value)})"
      {parsed_int, ""} -> parsed_int
      {_, _} -> raise "Integer.parse returned non-empty remainder"
    end
  end
end
