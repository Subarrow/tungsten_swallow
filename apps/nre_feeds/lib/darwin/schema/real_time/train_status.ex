defmodule NREFeeds.Darwin.Schema.RealTime.TrainStatus do
  @moduledoc """
  Train status indicates live running information for services, such as estimated/actual times, platforms, whether a service is delayed,
  length at each location, and a cause for delays.

  (priv/darwin_v16_schema/rttiPPTForecasts_v3.xsd, TS)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * rid (rid)                               R         - RTTI unique Train ID, unique for this service on this day
  * uid (uid)                               R         - Network Rail schedule UID, only unique when combined with SSD
  * ssd (ssd)                               R         - Day schedule starts on
  * reverse_formation? (isReverseFormation) O (false) - "Indicates whether a train that divides is working with portions in reverse to their normal formation"
  * delay_reason               (LateReason) O         - Indicates a cause for train delay. Note that a train may be delayed without an attributed cause.
    See the time fields in DarwinTrainStatusLocation/DarwinTrainStatusTime for more information.
  * locations              (tns:TSLocation) R         - mapped to a list of DarwinTrainStatusLocation.

  Note that TrainStatus locations are provided as and when updates are available, in principle the list can be empty
  """

  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus.Location
  alias NREFeeds.Darwin.Schema.RealTime.DisruptionReason
  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          rid: Types.rid(),
          uid: Types.uid(),
          ssd: Elixir.Date.t(),
          reverse_formation?: boolean(),
          delay_reason: DisruptionReason | nil,
          locations: [Location.t()]
        }

  @enforce_keys [:rid, :uid, :ssd, :reverse_formation?, :delay_reason, :locations]
  defstruct [:rid, :uid, :ssd, {:reverse_formation?, false}, :delay_reason, :locations]

  @spec from_xml(map()) :: t()

  def from_xml(
        _train_status = %{
          _id: :"TSv3:TS",
          rid: rid,
          uid: uid,
          ssd: ssd,
          isReverseFormation: reverse_formation_raw,
          LateReason: delay_reason_raw,
          Location: locations_raw
        }
      ) do
    %__MODULE__{
      rid: rid,
      uid: uid,
      ssd: ssd,
      reverse_formation?: reverse_formation_raw || false,
      delay_reason: DisruptionReason.from_xml(delay_reason_raw),
      locations: Enum.map(locations_raw || [], &Location.from_xml/1)
    }
  end
end
