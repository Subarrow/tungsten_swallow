defmodule NREFeeds.Darwin.Schema.RealTime.StationMessage do
  @moduledoc """
  (priv/darwin_v16_schema/rttiPPTStationMessages_v1.xsd, StationMessage)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * stations     (Station) R         - List of CRS codes this message applies to
  * message          (Msg) R         - pseudo-HTML message (only usable tags are p and a (with mandatory href))
  * message_id        (id) R         - An integer in the schema, but put back to string here
  * category         (cat) R         - Category of the message, in practice most are "Train" or "Station", mapped
  * severity         (sev) R         - 0..3, 0 is 'situation normal', 3 is presumably 'apocalypse', I've never seen it
  * suppressed? (suppress) O (false) - If set, train running information should be suppressed at this station
  * clear?    (Non-darwin) R         - Set to true if the station list is empty
  """

  import NREFeeds.Darwin.Util.DarwinFieldNorm
  alias NREFeeds.Darwin.Schema.Types

  @type message_severity :: 0..3
  @type message_category ::
          :train | :station | :connections | :system | :misc | :prior_trains | :prior_other

  @type t :: %__MODULE__{
          stations: [Types.crs()],
          message: binary(),
          message_id: binary(),
          category: message_category(),
          severity: message_severity(),
          suppressed?: boolean(),
          clear?: boolean()
        }

  @enforce_keys [:stations, :message, :message_id, :category, :severity, :suppressed?, :clear?]
  defstruct [:stations, :message, :message_id, :category, :severity, :suppressed?, :clear?]

  defp normalise_category(category_bin) do
    case category_bin do
      "Train" -> :train
      "Station" -> :station
      "Connections" -> :connections
      "System" -> :system
      "Misc" -> :misc
      "PriorTrains" -> :prior_trains
      "PriorOther" -> :prior_other
    end
  end

  defp normalise_stations(nil) do
    []
  end

  defp normalise_stations(stations) do
    Enum.map(stations, & &1.crs)
  end

  def from_xml(%{
        _id: :"SMv1:StationMessage",
        Station: stations_raw,
        Msg: %{choice: message_raw},
        id: message_id_raw,
        cat: category_raw,
        sev: severity_raw,
        suppress: suppressed_raw
      }) do
    %__MODULE__{
      stations: normalise_stations(stations_raw),
      message: Enum.join(message_raw, ""),
      message_id: Integer.to_string(message_id_raw),
      category: normalise_category(category_raw),
      severity: parse_nullable_integer!(severity_raw),
      suppressed?: suppressed_raw || false,
      clear?: is_nil(stations_raw)
    }
  end
end
