defmodule NREFeeds.Darwin.Schema.RealTime.DisruptionReason do
  @moduledoc """
  Representation of Darwin disruption reason. This is used for either delays or cancellations

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * reason_code (#text, i.e. within tags) R - Reason code, e.g. "653" could mean either:
    * This train has been delayed by a supermarket trolley on the track
    * This train has been cancelled because of a supermarket trolley on the track
    All codes are usable for cancellation or delay, and text forms for both are defined in the Darwin
    reference data for each code.

  * near?    (near) O (false) - If "near" is set, this disruption is "near" to the location referenced in the tiploc field
  * tiploc (tiploc) O         - The code of the location causing the disruption. As with near, often not set.

  If reason_code is set but there's no tiploc, the delay/cancellation has no location information:
  "This train has been delayed by a supermarket trolley on the track"

  If the reason code is set, and the tiploc is set (we'll assume CATERHM), but *not* near,
  "This train has been delayed by a supermarket trolley on the track [at Caterham]"

  If the reason code is set, and near is true
  "This train has been delayed by a supermarket trolley on the track [near Caterham]"

  """

  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{reason_code: binary(), near?: boolean(), tiploc: Types.tiploc()}

  @enforce_keys [:reason_code]
  defstruct [:reason_code, {:near?, false}, {:tiploc, nil}]

  @spec from_xml(map() | nil) :: t() | nil

  def from_xml(nil), do: nil

  def from_xml(%{"#text": reason_code, near: near_raw, tiploc: tiploc}) do
    %__MODULE__{
      reason_code: reason_code,
      near?: near_raw || false,
      tiploc: tiploc
    }
  end
end
