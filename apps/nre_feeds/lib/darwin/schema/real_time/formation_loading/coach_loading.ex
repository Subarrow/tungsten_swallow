defmodule NREFeeds.Darwin.Schema.RealTime.FormationLoading.CoachLoading do
  @moduledoc """
    Loading (i.e. occupancy) for an individual carriage

    (priv/darwin_v16_schema/rttiPPTFormations_v1.xsd, CoachLoadingData)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    coach_number    (coachNumber) R - Alphanumeric coach number
    loading               (#text) R - 0..100 percentage occupancy
    source                  (src) O - Source of information
    source_instance     (srcInst) O - Code for source instance
  """

  alias NREFeeds.Darwin.Util.DarwinFieldNorm

  @type t :: %__MODULE__{
          coach_number: binary(),
          loading: 0 | pos_integer(),
          source: binary(),
          source_instance: binary()
        }

  @enforce_keys [:coach_number, :loading, :source, :source_instance]
  defstruct [:coach_number, :loading, :source, :source_instance]

  def from_xml(%{
        "#text": loading,
        coachNumber: coach_number,
        src: source,
        srcInst: source_instance
      }) do
    %__MODULE__{
      loading: DarwinFieldNorm.parse_nullable_integer!(loading),
      coach_number: coach_number,
      source: source,
      source_instance: source_instance
    }
  end
end
