defmodule NREFeeds.Darwin.Schema.RealTime.Association do
  @moduledoc """
    An association links two services together at a given location

    (priv/darwin_v16_schema/rttiPPTSchedules_v2.xsd, Association)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    * main              (main) R         - "The through, previous working or link-to service"
    * associated       (assoc) R         - "The starting, terminating, subsequent working or link-from service"
    * category      (category) R         - Category of association, see below
    * cancelled? (isCancelled) O (false) - The association is cancelled and will no longer happen
    * deleted?     (isDeleted) O (false) - The association has been deleted

    Categories:
    * NP - The vehicle running the main service will run the associated service next (the overwhelming majority)
    * JJ - The train running the associated service will terminate and be joined to the main service, which continues
    * VV - The train running the main service will detach coaches or units, which will form the associated service
    * LK - Linked services. A 'linked' service has no defined meaning, in practice used to express replacement
      services, and not routinely used

    Note again that JJ is somewhat counterintuitive, it's "main is joined by associated", not "main joins associated",
    as the distinction is between through service and non-through, instead of the more obvious subject-object
    approach.

    For this reason, JJ is substituted with JN, with associated and main reversed.

    Also note that a single service can have multiple associations, and it can have multiple associations at one
    location. The Caledonian Sleeper is a good example of this, the main schedule from Euston to Inverness has two
    divide associations at Edinburgh, with the associated services going from Edinburgh to Fort William and Aberdeen
    respectively.

    Since a schedule may cross the same location multiple times, the schedule working times are included in main and
    associated to help match the tiploc to the correct location on each schedule.

    A survey of a few months of partial data from Darwin (2022-11-16..2023-01-15) suggests that, of approximately
    421313 collected associations:
    * NP: 415511
    * JJ:   2794
    * VV:   2825
    * LK:    183
      * SN (Southern):     5 - mostly used to associate trains with replacement trains (only one links to a bus replacement)
      * SW (SW Railway): 178 - mostly used to associate passenger services with bus replacement services (though there is one EE-EE)
  """

  alias NREFeeds.Darwin.Schema.RealTime.Misc.ServiceRIDWithCircularTimes
  alias NREFeeds.Darwin.Schema.Types

  @type association_category :: binary()

  @type t :: %__MODULE__{
          tiploc: Types.tiploc(),
          main: any(),
          associated: any(),
          category: association_category(),
          cancelled?: boolean(),
          deleted?: boolean()
        }

  @enforce_keys [:tiploc, :main, :associated, :category, :cancelled?, :deleted?]
  defstruct [:tiploc, :main, :associated, :category, {:cancelled?, false}, {:deleted?, false}]

  def from_xml(
        _association = %{
          tiploc: tiploc,
          main: main_raw,
          assoc: associated_raw,
          category: "JJ",
          isCancelled: cancelled_raw,
          isDeleted: deleted_raw
        }
      ) do
    %__MODULE__{
      tiploc: tiploc,
      associated: ServiceRIDWithCircularTimes.from_xml(main_raw),
      main: ServiceRIDWithCircularTimes.from_xml(associated_raw),
      category: "JN",
      cancelled?: cancelled_raw || false,
      deleted?: deleted_raw || false
    }
  end

  def from_xml(
        _association = %{
          tiploc: tiploc,
          main: main_raw,
          assoc: associated_raw,
          category: category,
          isCancelled: cancelled_raw,
          isDeleted: deleted_raw
        }
      ) do
    %__MODULE__{
      tiploc: tiploc,
      main: ServiceRIDWithCircularTimes.from_xml(main_raw),
      associated: ServiceRIDWithCircularTimes.from_xml(associated_raw),
      category: category,
      cancelled?: cancelled_raw || false,
      deleted?: deleted_raw || false
    }
  end
end
