defmodule NREFeeds.Darwin.Schema.RealTime.DeactivatedSchedule do
  @moduledoc """
  "Notification that a train schedule is now deactivated in Darwin."

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * rid (rid) R - RTTI unique service ID
  """

  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{rid: Types.rid()}

  @enforce_keys [:rid]
  defstruct [:rid]

  @spec from_xml(%{_id: atom(), rid: binary()}) :: t()

  def from_xml(%{_id: :"SCv2:DeactivatedSchedule", rid: rid}) do
    %__MODULE__{
      rid: rid
    }
  end
end
