defmodule NREFeeds.Darwin.Schema.RealTime.TimetableIdentifier do
  @moduledoc """

  apps/nre_feeds/priv/darwin_v16_schema/rttiPPTSchema_v16.xsd ()

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  timetable_identifier (#text)             R     Timetable identifier, 14 chars per schema, e.g. "20230408020501" (2023-04-08, 02:05:01)
  timetable_filename (ttfile)              O " " Generated timetable filename (e.g. 20230415020501_v4.xml.gz)
  timetable_reference_filename (ttreffile) O " " Generated timetable reference filename (e.g. 20230415020501_ref_v1.xml.gz)

  Note that either the timetable filename or timetable reference filename will be supplied, but not both. The other field will be present
  but blanked in the underlying XML (usually a single space), here these are nulled.

  TimetableFilenameType in CT says a filename is "The name of a timetable file that can be downloaded via FTP",
  but this is (at least from the pov of NROD users) no longer accurate, you must retrieve the file via S3.

  This type is constrained between 1 and 128 characters, explaining (although not really justifying) the empty space.

  Note that one message will be issued per file, e.g. for the timetable, you would have separate messages for:
  * 20230415020501_v4.xml.gz
  * 20230415020501_v5.xml.gz

  Similarly to when you collect the timetable/reference file, it's important to filter on the correct version.
  """

  @enforce_keys [:timetable_identifier, :timetable_filename, :timetable_reference_filename]
  defstruct @enforce_keys

  @type t :: %__MODULE__{
          timetable_identifier: binary(),
          timetable_filename: binary() | nil,
          timetable_reference_filename: binary() | nil
        }

  defp normalise_filename(" "), do: nil

  defp normalise_filename(filename) when is_binary(filename), do: filename

  def from_xml(%{
        "#text": timetable_identifier,
        _id: :"Pport/TimeTableId",
        ttfile: timetable_filename_raw,
        ttreffile: timetable_reference_filename_raw
      }) do
    %__MODULE__{
      timetable_identifier: timetable_identifier,
      timetable_filename: normalise_filename(timetable_filename_raw),
      timetable_reference_filename: normalise_filename(timetable_reference_filename_raw)
    }
  end
end
