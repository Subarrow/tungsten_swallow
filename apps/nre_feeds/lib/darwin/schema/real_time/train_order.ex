defmodule NREFeeds.Darwin.Schema.RealTime.TrainOrder do
  @moduledoc """
  (priv/darwin_v16_schema/rttiPPTTrainOrder_v1.xsd, TrainOrder)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * tiploc (tiploc)       R TIPLOC for this location. May be reported incorrectly against schedule TIPLOCs.
  * crs (crs)             R CRS for this location, will be accurate
  * platform (platform)   R Alphanumeric platform number
  * trains (set/first)    O |
  * trains (set/second)   O | 1..3 train services, listed in the order they'll arrive
  * trains (set/third)    O |
  * clear? (clear)        O set element was substituted with clear element, which clears the order

  Note that a train can either be a combination of a RID with 'circular' times for schedule matching, or
  for trains not in Darwin, the only information provided will be the train ID (or 'headcode').

  If a TrainOrder element is clearing the order for a given platform, trains will be an empty list

  These are represented in the trains list either as a ServiceRIDWithCircularTimes, or as a simple binary,
  depending on which is appropriate.
  """

  alias NREFeeds.Darwin.Schema.RealTime.Misc.ServiceRIDWithCircularTimes
  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          tiploc: Types.tiploc(),
          crs: Types.crs(),
          platform: Types.platform(),
          trains: [ServiceRIDWithCircularTimes.t() | binary()],
          clear?: boolean()
        }

  @enforce_keys [:tiploc, :crs, :platform, :trains, :clear?]
  defstruct [:tiploc, :crs, :platform, :trains, :clear?]

  defp normalise_train(train = %{choice: %{_id: :"TOv1:TrainOrderItem/rid"}}) do
    ServiceRIDWithCircularTimes.from_xml(train)
  end

  defp normalise_train(%{
         _id: :"TOv1:TrainOrderItem",
         choice: %{_id: :"TOv1:TrainOrderItem-trainID", trainID: train_id}
       }) do
    train_id
  end

  def from_xml(%{
        _id: :"TOv1:TrainOrder",
        choice: "",
        crs: crs,
        tiploc: tiploc,
        platform: platform
      }) do
    # This is how 'clear' manifests with the current parsing setup
    %__MODULE__{
      tiploc: tiploc,
      crs: crs,
      platform: platform,
      trains: [],
      clear?: true
    }
  end

  def from_xml(
        train_order = %{
          _id: :"TOv1:TrainOrder",
          choice: %{first: first, "TOv1:TrainOrderData/SEQ1": nil}
        }
      ) do
    from_xml(train_order, [first])
  end

  def from_xml(
        train_order = %{
          _id: :"TOv1:TrainOrder",
          choice: %{first: first, "TOv1:TrainOrderData/SEQ1": %{second: second, third: nil}}
        }
      ) do
    from_xml(train_order, [first, second])
  end

  def from_xml(
        train_order = %{
          _id: :"TOv1:TrainOrder",
          choice: %{first: first, "TOv1:TrainOrderData/SEQ1": %{second: second, third: third}}
        }
      ) do
    from_xml(train_order, [first, second, third])
  end

  def from_xml(%{_id: :"TOv1:TrainOrder", crs: crs, tiploc: tiploc, platform: platform}, trains) do
    %__MODULE__{
      tiploc: tiploc,
      crs: crs,
      platform: platform,
      trains: Enum.map(trains, &normalise_train/1),
      clear?: false
    }
  end
end
