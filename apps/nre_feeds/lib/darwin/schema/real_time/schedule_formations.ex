defmodule NREFeeds.Darwin.Schema.RealTime.ScheduleFormations do
  @moduledoc """
    List of formations for a given service

    (priv/darwin_v16_schema/rttiPPTFormations_v2.xsd, ScheduleFormations)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    * rid (rid)                R - Service RID
    * formations   (formation) O - all formations for service
    * clear?      (non-darwin) R - set if formations is empty

    Semantics for clearing isn't entirely clear, but an empty list is allowed and seems like the likely
    way. The way this type is described in the schema suggests that this is a complete update, i.e. any
    information omitted which was present in a previous ScheduleFormations should be discarded.
  """

  alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation
  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{rid: Types.rid(), formations: [Formation.t()], clear?: boolean()}

  @enforce_keys [:rid, :formations, :clear?]
  defstruct [:rid, :formations, :clear?]

  def from_xml(%{rid: rid, formation: formations_raw}) do
    %__MODULE__{
      rid: rid,
      formations: Enum.map(formations_raw || [], &Formation.from_xml/1),
      clear?: is_nil(formations_raw)
    }
  end
end
