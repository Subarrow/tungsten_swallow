defmodule NREFeeds.Darwin.Schema.RealTime.Misc.ServiceRIDWithCircularTimes do
  @moduledoc """
  Represents either a service within an association, or a train with a provided RID within a TrainOrder

  (priv/darwin_v16_schema/rttiPPTSchedules_v2.xsd, AssocService)
  (priv/darwin_v16_schema/rttiPPTTrainOrder_v1.xsd, TrainOrderItem)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * rid                   (rid) R - Train RID [AssocService]
  * rid                 (#text) R - Train RID [TrainOrderItem/rid]
  * working_times*        (wta) O - Working time arrival
  * working_times*        (wtd) O - Working time departure
  * working_times*        (wtp) O - Working time pass
  * (not mapped)          (pta) O - Public time arrival
  * (not mapped)          (ptd) O - Public time departure
  * wt_match       (non-darwin) R - Composed from working times, used for matching working time to schedule
  """

  alias NREFeeds.Darwin.Schema.Types
  alias NREFeeds.Darwin.Util.DarwinTime

  @type t :: %__MODULE__{
          rid: Types.rid(),
          working_times: DarwinTime.normalised_working_times(),
          wt_match: any()
        }

  @enforce_keys [:rid, :working_times, :wt_match]
  defstruct [:rid, :working_times, :wt_match]

  def from_xml(association_service = %{_id: :"SCv2:AssocService", rid: rid}) do
    working_times = DarwinTime.parse_working_times!(association_service)

    %__MODULE__{
      rid: rid,
      working_times: working_times,
      wt_match: DarwinTime.wt_match(working_times)
    }
  end

  def from_xml(%{
        _id: :"TOv1:TrainOrderItem",
        choice: train_order_service = %{_id: :"TOv1:TrainOrderItem/rid", "#text": rid}
      }) do
    working_times = DarwinTime.parse_working_times!(train_order_service)

    %__MODULE__{
      rid: rid,
      working_times: working_times,
      wt_match: DarwinTime.wt_match(working_times)
    }
  end
end
