defmodule NREFeeds.Darwin.Schema.RealTime.TrainStatus.Platform do
  @moduledoc """
  Mapping for PlatformData tag, used only within train status.

  (priv/darwin_v16_schema/rttiPPTForecasts_v3.xsd, PlatformData)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * platform    (#text)      O          - Platform or route at this location
  * confirmed?  (conf)       O (false)  - True if platform is confirmed
  * source      (platsrc)    O ("P")    - P, A, or M depending on whether the platform is Planned, Automatic, or Manual
  * suppressed* (platsup)    O (false)  - Platform is suppressed and must not be displayed
  * suppressed* (cisPlatsup) O (false)  - Suppression was applied by CIS at this location

  See the typedoc for platform for caveats on platform numbers

  Note that platsup and cisPlatsup have been merged into a single field, called suppressed.

  Note that the NRE licence requires you to not display suppressed platforms to end users. Platforms are often suppressed
  at terminal stations to allow for unobstructed cleaning and replenishment of trains, for crowd management, and to avoid
  passengers waiting at the wrong platform when the platform is likely to change.
  """

  alias NREFeeds.Darwin.Schema.Types

  @typedoc """
  Whether the platform information was provided automatically (:automatic), manually (:manual), or was pulled from the schedule (:planned)
  """
  @type platform_source :: :planned | :automatic | :manual

  @typedoc """
  Whether the platform is unsuppressed (false), suppressed via darwin (:darwin), or suppressed by the local CIS (:cis)
  """
  @type platform_suppression :: :darwin | :cis | false

  @type t :: %__MODULE__{
          platform: Types.platform(),
          confirmed?: boolean(),
          source: platform_source() | nil,
          suppressed: platform_suppression()
        }

  defstruct [:platform, {:confirmed?, false}, :source, :suppressed]

  @spec from_xml(map() | nil) :: t() | nil

  def from_xml(nil), do: nil

  def from_xml(%{
        _id: :"TSv3:PlatformData",
        "#text": platform,
        conf: confirmed_raw,
        platsrc: platform_source_raw,
        platsup: platsup_raw,
        cisPlatsup: cis_platsup_raw
      }) do
    platsup = platsup_raw || false
    cis_platsup = cis_platsup_raw || false

    %__MODULE__{
      platform: platform,
      confirmed?: confirmed_raw || false,
      source:
        case platform_source_raw do
          "P" -> :planned
          "A" -> :automatic
          "M" -> :manual
          nil -> nil
        end,
      suppressed:
        case {platsup, cis_platsup} do
          {true, false} -> :darwin
          {true, true} -> :cis
          {false, false} -> false
        end
    }
  end
end
