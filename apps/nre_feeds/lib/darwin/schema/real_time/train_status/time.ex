defmodule NREFeeds.Darwin.Schema.RealTime.TrainStatus.Time do
  @moduledoc """

  Expected or actual time for a service

  (priv/darwin_v16_schema/rttiPPTForecasts_v3.xsd, TSTimeData)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * estimated_time                (et) O         - Estimated time, based on public times for stops which have them (i.e. passenger stops),
    uses working schedule for operational/passing locations. Supposedly only set if there's a corresponding activity, but passes don't
    have activities and are usually estimated.
  * working_estimated_time       (wet) O         - Estimated time based on working schedule, supposedly only set for public stops when
    either it differs from et, or where there's an operational activity but no public activity. In reality, the difference between
    public-working estimates appears to be either the main or only factor
  * actual_time                   (at) O         - Actual time
  * actual_time_removed?   (atRemoved) O (false) - Set if an actual time was removed and substituted with the estimated time. Only set once, when it actually happens
  * actual_time_class        (atClass) O         - "The class of the actual time.", no examples or values are given
  * estimated_minimum          (etmin) O         - Manually applied lower limit to estimate (i.e. the estimate will never be before this time)
  * estimated_time_unknown?(etUnknown) O (false) - Indicates that an unknown delay forecast has been set. I don't entirely understand what this means.
  * delayed?                 (delayed) O (false) - This service is indefinitely delayed, departure boards and LDB will display "Delayed" instead of estimated times
  * source                       (src) O         - Source of forecast or actual time, generally "TD", "TRUST, "Darwin" or "CIS"
  * source_instance          (srcInst) O         - Four-character code indicating The RTTI code of the CIS instance if the source is a CIS. This shouldn't be populated
  .                                                if source isn't CIS but for some reason can be a truncated version of the time class. This value is accordingly
  .                                                forced to nil if source is not CIS
  """

  import NREFeeds.Darwin.Util.DarwinTime

  @type t :: %__MODULE__{
          estimated_time: Elixir.Time.t() | nil,
          working_estimated_time: Elixir.Time.t() | nil,
          actual_time: Elixir.Time.t() | nil,
          actual_time_removed?: boolean(),
          actual_time_class: binary() | nil,
          estimated_minimum: Elixir.Time.t() | nil,
          estimated_time_unknown?: boolean(),
          delayed?: boolean(),
          source: binary() | nil,
          source_instance: binary() | nil
        }

  defstruct [
    :estimated_time,
    :working_estimated_time,
    :actual_time,
    :actual_time_removed?,
    :actual_time_class,
    :estimated_minimum,
    :estimated_time_unknown?,
    :delayed?,
    :source,
    :source_instance
  ]

  @spec from_xml(map() | nil) :: t() | nil

  def from_xml(nil), do: nil

  def from_xml(%{
        _id: :"TSv3:TSTimeData",
        et: estimated_time_raw,
        wet: working_estimated_time_raw,
        at: actual_time_raw,
        atRemoved: actual_time_removed_raw,
        atClass: actual_time_class,
        etmin: estimated_time_minimum,
        etUnknown: estimated_time_unknown_raw,
        delayed: delayed_raw,
        src: source,
        srcInst: source_instance
      }) do
    %__MODULE__{
      estimated_time: parse_darwin_time!(estimated_time_raw),
      working_estimated_time: parse_darwin_time!(working_estimated_time_raw),
      actual_time: parse_darwin_time!(actual_time_raw),
      actual_time_removed?: actual_time_removed_raw || false,
      actual_time_class: actual_time_class,
      estimated_minimum: parse_darwin_time!(estimated_time_minimum),
      estimated_time_unknown?: estimated_time_unknown_raw || false,
      delayed?: delayed_raw || false,
      source: source,
      source_instance: if(source == "CIS", do: source_instance, else: nil)
    }
  end
end
