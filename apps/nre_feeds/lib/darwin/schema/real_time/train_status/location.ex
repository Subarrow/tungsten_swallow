defmodule NREFeeds.Darwin.Schema.RealTime.TrainStatus.Location do
  @moduledoc """

  (priv/darwin_v16_schema/rttiPPTForecasts_v3.xsd, TSLocation)


  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * tiploc        (tpl)         R -         "Train in Position Location", Darwin's standard location code
  * arrive        (arr)         O -         Forecast or actual data for arrival (DarwinTrainStatusTime)
  * depart        (dep)         O -         Forecast or actual data for departure (DarwinTrainStatusTime)
  * pass          (pass)        O -         Forecast or actual data for pass (DarwinTrainStatusTime)
  * platform      (plat)        O -         Forecast or actual platform for station (Maps to DarwinTrainStatusPlatform)
  * suppressed?   (suppr)       O (false) - Set if train is suppressed (and should not be displayed) at this location
  * length        (length)      O ("0")   - Train length, nilled instead of being put to 0 as default
  * detach_front? (detachFront) O (false) - True if carriages being detached from the train at this location are being detached from the front
  * wta           (wta)         O -         Working time arrival
  * wtd           (wtd)         O -         Working time departure
  * wtp           (wtp)         O -         Working time pass
  * (not mapped)  (pta)         O -         Public (i.e. timetable) arrival
  * (not mapped)  (ptd)         O -         Public (i.e. timetable) departure
  * wt_match                      -         Short-form combination of working times for use in schedule matching

  Working times are provided for the sake of mapping this status to the correct schedule location on circular routes (i.e. where the train will
  cross the same location twice), such as on the Cathcart loop.

  Note that all times are parsed, but without the context of an existing schedule, they can't be given dates or normalised for timezone.

  Note that the NRE licence requires that you apply suppression, both here, and on the field within the platform struct. If this service is
  suppressed, it shouldn't be shown to passengers, if the platform is suppressed, then the platform should not be shown.

  """

  alias NREFeeds.Darwin.Util.DarwinTime
  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus
  alias NREFeeds.Darwin.Schema.Types

  import NREFeeds.Darwin.Util.DarwinTime
  import NREFeeds.Darwin.Util.DarwinFieldNorm

  @type t :: %__MODULE__{
          tiploc: Types.tiploc(),
          arrive: TrainStatus.Time.t() | nil,
          depart: TrainStatus.Time.t() | nil,
          pass: TrainStatus.Time.t() | nil,
          platform: TrainStatus.Platform.t() | nil,
          suppressed?: boolean(),
          length: pos_integer() | nil,
          detach_front?: boolean(),
          working_times: DarwinTime.normalised_working_times(),
          wt_match: binary()
        }

  @enforce_keys [:tiploc, :suppressed?, :detach_front?, :wt_match]
  defstruct [
    :tiploc,
    :arrive,
    :depart,
    :pass,
    :platform,
    {:suppressed?, false},
    :length,
    {:detach_front?, false},
    :working_times,
    :wt_match
  ]

  def from_xml(
        ts_location = %{
          _id: :"TSv3:TSLocation",
          tpl: tiploc,
          arr: arrive_raw,
          dep: depart_raw,
          pass: pass_raw,
          plat: platform_raw,
          suppr: suppressed_raw,
          length: length_raw,
          detachFront: detach_front_raw
        }
      ) do
    working_times = parse_working_times!(ts_location)

    %__MODULE__{
      tiploc: tiploc,
      arrive: TrainStatus.Time.from_xml(arrive_raw),
      depart: TrainStatus.Time.from_xml(depart_raw),
      pass: TrainStatus.Time.from_xml(pass_raw),
      platform: TrainStatus.Platform.from_xml(platform_raw),
      suppressed?: suppressed_raw || false,
      length: parse_nullable_integer!(length_raw),
      detach_front?: detach_front_raw || false,
      working_times: working_times,
      wt_match: wt_match(working_times)
    }
  end

  @doc """
  Use the working datetimes from a corresponding schedule location to determine the correct datetimes for this status

  Actual time is favoured first, then working estimate (if set), then estimate.

  Note that if location has a status time set which *isn't* in the corresponding working times,
  it'll simply be ignored and set to nil in the output

  The return is a map, {:arrive|:pass|:depart => {datetime | nil, %Location.Time{} | nil}}

  """

  @spec get_datetimes(DarwinTime.normalised_working_datetimes(), __MODULE__.t()) ::
          map()

  def get_datetimes(working_times, location = %__MODULE__{}) do
    Enum.map([:arrive, :pass, :depart], fn key ->
      base_wt = Map.get(working_times, key)
      location_time = Map.get(location, key)
      status_time = favoured_time(location_time)

      if is_nil(base_wt) do
        {key, {nil, location_time}}
      else
        {_ldt, combined} = combine_time!(base_wt, status_time)
        {key, {combined, location_time}}
      end
    end)
    |> Map.new()
  end

  defp favoured_time(nil), do: nil

  defp favoured_time(%TrainStatus.Time{
         estimated_time: estimated,
         actual_time: actual,
         working_estimated_time: working_estimated
       }) do
    actual || working_estimated || estimated
  end
end
