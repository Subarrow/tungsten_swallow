defmodule NREFeeds.Darwin.Schema.RealTime.TrainAlert do
  @moduledoc """

  Alerts applying to specific services at specific locations

  (priv/darwin_v16_schema/rttiPPTTrainAlerts_v1.xsd, TrainAlert)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * alert_id                          (AlertID) R - Unique identifier for the alert
  * services                    (AlertServices) R - RIDs/UIDs/SSDs for services this applies to
  * send_alert_by_sms?         (SendAlertBySMS) R - If true, this alert can be sent by SMS
  * send_alert_by_email?     (SendAlertByEmail) R - If true, this alert can be sent by email
  * send_alert_by_twitter? (SendAlertByTwitter) R - If true, this alert can be sent by twitter
  * source                             (Source) R - Alert sender, TOC code or "NRCC"
  * message                         (AlertText) R - Plain-text alert message
  * audience                         (Audience) R - Intended audience, either "Customer", "Staff", or "Operations"
  * forced?                         (AlertType) R - Either "Normal" (sent within timeframe) or "Forced" (sent immediately)
  * copied_from_alert_id    (CopiedFromAlertID) O - If this alert is copied from another, this is that alert's ID
  * copied_from_source       (CopiedFromSource) O - If this alert is copied from another, this is that alert's source
  * clear?                      (not in Darwin) R - Set if the services list is empty.

  Virtually every field in this file says 'TODO' in the documentation tags in the schema, I can only
  assume that Thales were too busy selling bombs or whatever else it is they do to fill it in.

  The NROD wiki describes some of these fields, but the semantics for clearing an alert are a little unclear. The guess
  I've made here is that this is done in the same way as for station messages (i.e. repeating an alert ID with no
  targets)

  In practice it seems to be mostly SE which uses train alerts routinely

  This is one of the stranger types in the schema, the camel casing is different, most boolean fields are required with
  no defaults.
  """

  alias NREFeeds.Darwin.Schema.RealTime.TrainAlert.Service

  @type alert_audience :: :customer | :staff | :operations

  @type t :: %__MODULE__{
          alert_id: binary(),
          services: [Service.t()],
          send_alert_by_sms?: boolean(),
          send_alert_by_email?: boolean(),
          send_alert_by_twitter?: boolean(),
          source: binary(),
          message: binary(),
          audience: alert_audience(),
          forced?: boolean(),
          copied_from_alert_id: binary() | nil,
          copied_from_source: binary() | nil,
          clear?: boolean()
        }

  @enforce_keys [
    :alert_id,
    :services,
    :send_alert_by_sms?,
    :send_alert_by_email?,
    :send_alert_by_twitter?,
    :source,
    :message,
    :audience,
    :forced?,
    :copied_from_alert_id,
    :copied_from_source,
    :clear?
  ]
  defstruct [
    :alert_id,
    :services,
    :send_alert_by_sms?,
    :send_alert_by_email?,
    :send_alert_by_twitter?,
    :source,
    :message,
    :audience,
    :forced?,
    :copied_from_alert_id,
    :copied_from_source,
    :clear?
  ]

  defp normalise_audience(audience) do
    case audience do
      "Customer" -> :customer
      "Staff" -> :staff
      "Operations" -> :operations
    end
  end

  defp normalise_services(nil) do
    []
  end

  defp normalise_services(services) do
    Enum.map(services, &Service.from_xml(&1))
  end

  def from_xml(%{
        AlertID: alert_id,
        AlertServices: %{AlertService: services_raw},
        SendAlertBySMS: send_alert_by_sms?,
        SendAlertByEmail: send_alert_by_email?,
        SendAlertByTwitter: send_alert_by_twitter?,
        Source: source,
        AlertText: message,
        Audience: audience_raw,
        AlertType: alert_type_raw,
        CopiedFromAlertID: copied_from_alert_id,
        CopiedFromSource: copied_from_source
      }) do
    %__MODULE__{
      alert_id: alert_id,
      services: normalise_services(services_raw),
      send_alert_by_sms?: send_alert_by_sms?,
      send_alert_by_email?: send_alert_by_email?,
      send_alert_by_twitter?: send_alert_by_twitter?,
      source: source,
      message: message,
      audience: normalise_audience(audience_raw),
      forced?: alert_type_raw == "Forced",
      copied_from_alert_id: copied_from_alert_id,
      copied_from_source: copied_from_source,
      clear?: is_nil(services_raw)
    }
  end
end

defmodule NREFeeds.Darwin.Schema.RealTime.TrainAlert.Service do
  @moduledoc """
  (priv/darwin_v16_schema/rttiPPTTrainAlerts_v1.xsd, AlertService)


  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * stations (Location) R List of station CRS codes, either to display at? or which the message applies at? I'm unsure
  * rid           (RID) R Service RID
  * ssd           (SSD) R Service schedule start date
  * uid           (UID) R Service UID

  Note that the locations list can never be empty, at least per the schema

  The CRS codes included here are odd, there's no effort to check whether they're stations, it appears to just check for
  a corresponding CRS code for every single tiploc, which means that oddities like junctions with CRS codes are included
  """

  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          stations: [Types.crs()],
          rid: Types.rid(),
          ssd: Date.t(),
          uid: Types.uid()
        }

  @enforce_keys [:rid, :ssd, :uid, :stations]
  defstruct [:rid, :ssd, :uid, :stations]

  def from_xml(%{Location: stations, RID: rid, SSD: ssd, UID: uid}) do
    %__MODULE__{
      stations: stations,
      rid: rid,
      ssd: Elixir.Date.from_iso8601!(ssd),
      uid: uid
    }
  end
end
