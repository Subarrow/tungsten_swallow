defmodule NREFeeds.Darwin.Schema.RealTime.FormationLoading do
  @moduledoc """
    List of loadings (i.e. occupancy) for carriages in a given formation at a given location.

    (priv/darwin_v16_schema/rttiPPTFormations_v1.xsd, Loading)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    * rid                  (rid) R - Service RID
    * formation_id         (fid) R - Formation ID
    * tiploc               (tpl) R - The tiploc for the location the loading was measured at
    * working_times*       (wta) O - Working timetable arrival
    * working_times*       (wtd) O - Working timetable departure
    * working_times*       (wtp) O - Working timetable pass
    * (not mapped)         (pta) O - Public arrival
    * (not mapped)         (ptd) O - Public departure
    * wt_match      (non-darwin) R - working times compiled for easier matching
    * loading          (loading) O - List of carriage loadings for this location
    * clear?        (non-darwin) R - set to true if the list of carriages is empty

    working_times and wt_match are provided to match with the correct location on a schedule

    Formation loading is a complete update, any omitted carriages from previous messages should be
    cleared, if the entire carriage list is empty, all carriages are cleared
  """

  alias NREFeeds.Darwin.Schema
  alias NREFeeds.Darwin.Util
  alias Util.DarwinTime
  alias Schema.Types
  alias Schema.RealTime.FormationLoading.CoachLoading

  @type t :: %__MODULE__{
          rid: Types.rid(),
          formation_id: binary(),
          tiploc: Types.tiploc(),
          working_times: DarwinTime.normalised_working_times(),
          wt_match: tuple(),
          loading: [CoachLoading.t()],
          clear?: boolean()
        }

  @enforce_keys [:rid, :formation_id, :tiploc, :working_times, :wt_match, :loading, :clear?]
  defstruct [:rid, :formation_id, :tiploc, :working_times, :wt_match, :loading, :clear?]

  def from_xml(
        formation_loading = %{rid: rid, fid: formation_id, tpl: tiploc, loading: loading_raw}
      ) do
    working_times = DarwinTime.parse_working_times!(formation_loading)

    %__MODULE__{
      rid: rid,
      formation_id: formation_id,
      tiploc: tiploc,
      working_times: working_times,
      wt_match: DarwinTime.wt_match(working_times),
      loading: Enum.map(loading_raw || [], &CoachLoading.from_xml/1),
      clear?: is_nil(loading_raw)
    }
  end
end
