defmodule NREFeeds.Darwin.Schema.RealTime.Schedule.Location do
  @moduledoc """
  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  Note that this struct represents the tags:
  * OR (Origin)
  * OPOR (Operational Origin)
  * IP (Intermediate point)
  * OPIP (Operational intermediate point)
  * PP (Passing point)
  * DT (Destination)
  * OPDT (Operational destination)

  These tags are represented as an atom and a boolean, e.g. for :"SCv3:OR", you'd get a type of
  :origin and operational? would be false

  * type          (non-darwin) R
  * operational?  (non-darwin) R

  These allow somewhat different combinations of fields, but for the sake of expediency, we've flattened
  them into one struct. All of these have some fields in common (SchedLocAttributes), these are:

  * tiploc (tpl) R - "Train In Position Location", the basic unit of location code in Darwin

  * activity (act) O ("  ") - Train activities are two-character codes stuck together in a
    string. e.g. "T RM" would be "Stops to Take Up and Set Down passengers" (T ), and
    "Stops for reversing move or driver changes ends" (RM). In the schedule, this is a fixed length,
    Darwin truncates it (but preserves the multiple-of-two nature)

  * planned_activity (planAct) O - The previous planned activity, if it differs to the current one

  * cancelled? (can) O (false) - This location is cancelled

  * formation_id (fid) O - Formation ID which corresponds to this location. While present on PP per schema,
    it's seemingly not usually set (though, you can safely assume that the train is not changing length and
    facilities while in motion!)

  Fields which only apply to calling points (CallPtAttributes), specifically OR, IP, and DT (and *not* the
  operational variants):

  * public_times.arrive (pta) O - Publicly scheduled arrival time (i.e. this is the time which appears in published timetables)
  * public_times.depart (ptd) O - Publicly scheduled departure time

  * average_loading (avgLoading) O - Average loading (i.e. crowdedness) of the entire train at this calling
    point. This is based on long-term estimates, rather than real-time data, and is a percentage.

  Fields which apply to all except origins (OP, OPOR):
  * route_delay (rdelay) O (0) - A delay value implied by changes to a service's route. The value is added to the
    forecast lateness of the service at the previous location when determining expected lateness at this location

  Fields applying only to OR and IP:
  * false_destination (fd) O - tiploc for a false destination which will be indicated by the CIS at this location for this service.
    This is often done to de-emphasise the destination of slow trains where express alternatives are available (e.g. the Leeds-York
    via Harrogate route will be indicated as being to Poppleton at Leeds, and Burley Park at York), or on the Cathcart loop, where
    it's done to avoid the confusing possibility of displaying a service from Glasgow Central to Glasgow Central (There's a
    Waterloo-Waterloo service which comes to mind which also does this)

  Fields applying only to PP:
  * working_times.pass (wtp) R - Working time pass

  Fields applying to all except PP:
  * working_times.arrive (wta) - Working time arrival, R on IP, OPIP, DT, OPDT, O on OR and OPOR
  * working_times.depart (wtd) - Working time departure, R on OR, OPOR, IP, OPIP, O on DT and OPDT

  Note that public_times is nil for operational stops, and that either or both arrive and depart may be nil at any stop.

  Note that working_times will contain either pass or arrive/depart, but never both. Note that working_times is processed for date but
  *not* for timezone

  Activity codes are described in BPLAN and on the NROD wiki.
  https://wiki.openraildata.com//index.php?title=Activity_codes

  """

  alias NREFeeds.Darwin.Schema.Types
  alias NREFeeds.Darwin.Util.DarwinTime
  import NREFeeds.Darwin.Util.DarwinTime
  import NREFeeds.Darwin.Util.DarwinFieldNorm

  @type schedule_location_type :: :origin | :destination | :intermediate | :pass

  @type t :: %__MODULE__{
          type: schedule_location_type(),
          operational?: boolean(),
          tiploc: Types.tiploc(),
          activity: Types.activity(),
          planned_activity: Types.activity() | nil,
          cancelled?: boolean(),
          formation_id: Types.formation_id(),
          public_times: DarwinTime.normalised_public_times(),
          average_loading: 0..100 | nil,
          route_delay: integer() | nil,
          false_destination: Types.tiploc() | nil,
          working_times: DarwinTime.normalised_working_times(),
          index: 0 | pos_integer(),
          wt_match: any()
        }

  @enforce_keys [
    :type,
    :operational?,
    :tiploc,
    :activity,
    :cancelled?,
    :route_delay,
    :working_times,
    :index,
    :wt_match
  ]
  defstruct [
    :type,
    :operational?,
    :tiploc,
    {:activity, "  "},
    :planned_activity,
    {:cancelled?, false},
    :formation_id,
    :public_times,
    :average_loading,
    {:route_delay, 0},
    :false_destination,
    :working_times,
    :index,
    :wt_match
  ]

  @spec from_xml([%{atom() => any()}], Date.t()) :: [t()]

  def from_xml(schedule_locations, ssd = %Date{}) do
    from_xml(schedule_locations, [], 0, ssd)
  end

  defp from_xml([], accumulator, _idx, _), do: Enum.reverse(accumulator)

  defp from_xml(
         [
           schedule_location = %{
             _id: xml_tag,
             tpl: tiploc,
             act: activity,
             planAct: planned_activity,
             can: cancelled?,
             fid: formation_id
           }
           | rest
         ],
         accumulator,
         idx,
         last_datetime
       ) do
    {last_datetime, working_datetimes} =
      parse_combine_working_times!(schedule_location, last_datetime)

    working_times = parse_working_times!(schedule_location)

    {_last_datetime, public_times} = parse_combine_public_times!(schedule_location, last_datetime)

    average_loading = parse_nullable_integer!(schedule_location[:avgLoading])
    false_destination = schedule_location[:fd]

    route_delay =
      if(Map.has_key?(schedule_location, :rdelay),
        do: parse_nullable_integer!(schedule_location.rdelay) || 0,
        else: nil
      )

    sl_struct = %__MODULE__{
      type:
        cond do
          xml_tag in [:"SCv3:OR", :"SCv3:OPOR"] -> :origin
          xml_tag in [:"SCv3:DT", :"SCv3:OPDT"] -> :destination
          xml_tag in [:"SCv3:IP", :"SCv3:OPIP"] -> :intermediate
          xml_tag == :"SCv3:PP" -> :pass
        end,
      operational?: xml_tag in [:"SCv3:OPOR", :"SCv3:OPIP", :"SCv3:OPDT"],
      tiploc: tiploc,
      activity: activity,
      planned_activity: planned_activity,
      cancelled?: cancelled? || false,
      formation_id: formation_id,
      public_times: public_times,
      average_loading: average_loading,
      route_delay: route_delay,
      false_destination: false_destination,
      working_times: working_datetimes,
      index: idx,
      wt_match: wt_match(working_times)
    }

    from_xml(rest, [sl_struct | accumulator], idx + 1, last_datetime)
  end
end
