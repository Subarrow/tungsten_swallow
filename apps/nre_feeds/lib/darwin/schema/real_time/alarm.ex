defmodule NREFeeds.Darwin.Schema.RealTime.Alarm do
  @moduledoc """
  An alarm indicates upstream discontinuity of a TD area,
  the entirety of TD, or the entirety of Tyrell

  (priv/darwin_v16_schema/rttiPPTAlarms_v1.xsd)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * alarm_id              (id) R ID of alarm
  * alarm_type                 R :td, :tyrell, :td_area, :clear
  * td_area (tdAreaFail→#text) O failed TD area if alarm_type is :td_area (nil otherwise)

  Note that clear is supplied here as a type rather than a flag, since none of the alarm information
  is included in clears
  """

  @type alarm_type :: :td | :tyrell | :td_area | :clear

  @type t :: %__MODULE__{
          alarm_id: binary(),
          alarm_type: alarm_type(),
          td_area: binary() | nil
        }

  @enforce_keys [:alarm_id, :alarm_type, :td_area]
  defstruct @enforce_keys

  def from_xml(%{
        _id: :"ALv1:RTTIAlarm",
        choice: %{
          _id: :"ALv1:RTTIAlarmData",
          choice: %{
            _id: :"ALv1:RTTIAlarmData-tdAreaFail",
            tdAreaFail: td_area
          },
          id: alarm_id
        }
      }) do
    %__MODULE__{
      alarm_id: alarm_id,
      alarm_type: :td_area,
      td_area: td_area
    }
  end

  def from_xml(%{
        _id: :"ALv1:RTTIAlarm",
        choice: %{_id: :"ALv1:RTTIAlarm-clear", clear: alarm_id}
      }) do
    %__MODULE__{
      alarm_id: alarm_id,
      alarm_type: :clear,
      td_area: nil
    }
  end

  def from_xml(%{
        _id: :"ALv1:RTTIAlarm",
        choice: %{
          _id: :"ALv1:RTTIAlarmData",
          choice: %{_id: :"ALv1:RTTIAlarmData_tyrellFeedFail", tyrellFeedFail: ""},
          id: alarm_id
        }
      }) do
    %__MODULE__{
      alarm_id: alarm_id,
      alarm_type: :tyrell,
      td_area: nil
    }
  end

  def from_xml(%{
        _id: :"ALv1:RTTIAlarm",
        choice: %{
          _id: :"ALv1:RTTIAlarmData",
          choice: %{_id: :"ALv1:RTTIAlarmData_tdFeedFail", tdFeedFail: ""},
          id: alarm_id
        }
      }) do
    %__MODULE__{
      alarm_id: alarm_id,
      alarm_type: :td,
      td_area: nil
    }
  end
end
