defmodule NREFeeds.Darwin.Schema.RealTime.Schedule do
  @moduledoc """
  Normalised representation of a Darwin schedule, representing the planned running of a train. These are issued
  when trains are activated, when trains are partly or completely cancelled, or when any other change needs to
  be made to any of its fields. These messages are complete; schedules are never part-updated like train
  statuses

  (priv/darwin_v16_schema/rttiPPTSchedules_v3.xsd, Schedule)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * rid                    (rid) R         - RTTI unique Train ID, unique for this service on this day. All-numeric but should be handled
    as a string.
  * ssd                    (ssd) R         - Scheduled Start Date (i.e. the date which applies to the first time in this schedule)
  * uid                    (uid) R         - Network Rail Schedule UID, usually a letter and five digits. Only unique when combined with ssd.
  * rsid                  (rsid) O         - Retail Service ID, not always exposed, 6-8 chars when it is
  * signalling_id      (trainId) R         - Four-character identifier used in railway signalling

  * operator               (toc) R         - Two-letter ATOC code identifying the operator (e.g. LE for Greater Anglia). Note that Darwin will
    occasionally copy operator codes which are not in the Darwin reference data (e.g. LS)

  * status              (status) O ("P")   - Service status, can be omitted in feed and defaults to P (Permanent WTT rail) if absent
  * category          (trainCat) O ("OO")  - Service category, can be omitted in feed, is assumed "OO" (non-express ML rail) if so
  * passenger?  (isPassengerSvc) O (true)  - false if the category is a non-passenger movement (e.g. EE - Empty coaching stock)
  * active?           (isActive) O (true)  - Indicates if service is "active in Darwin".
  * deleted?           (deleted) O (false) - For services which either have to be purged at short notice, or were input by mistake.
    Deleted services should at least be suppressed, if not removed entirely.
  * charter?         (isCharter) O (false) - Set if the train's a charter, but likely not very reliably.
  * cancel_reason (cancelReason) O - does not necessarily indicate that the entire service is cancelled,
    this can be used to justify part-cancellation (i.e. some stations struck out) too
  * locations - List of DarwinScheduleLocation structs, every schedule must have at least two for obvious reasons

  Status and Category are defined in BPLAN, and are also described on the NROD wiki:
  https://wiki.openraildata.com/index.php/CIF_Codes
  """

  alias NREFeeds.Darwin.Schema.RealTime.Schedule
  alias NREFeeds.Darwin.Schema.RealTime.DisruptionReason
  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          rid: Types.rid(),
          ssd: Elixir.Date.t(),
          uid: Types.uid(),
          rsid: Types.rsid() | nil,
          signalling_id: Types.signalling_id(),
          operator: binary(),
          status: binary(),
          category: binary(),
          passenger?: boolean(),
          active?: boolean(),
          deleted?: boolean(),
          charter?: boolean,
          cancel_reason: DisruptionReason.t() | nil,
          locations: [Schedule.Location.t()]
        }

  @enforce_keys [
    :rid,
    :ssd,
    :uid,
    :rsid,
    :signalling_id,
    :operator,
    :status,
    :category,
    :passenger?,
    :active?,
    :deleted?,
    :charter?,
    :cancel_reason,
    :locations
  ]
  defstruct [
    :rid,
    :ssd,
    :uid,
    {:rsid, nil},
    :signalling_id,
    :operator,
    {:status, "P"},
    {:category, "OO"},
    {:passenger?, true},
    {:active?, true},
    {:deleted?, false},
    {:charter?, false},
    {:cancel_reason, nil},
    :locations
  ]

  defp def_when_nil_or_empty(thing, default) do
    case thing do
      "" -> default
      nil -> default
      _ -> thing
    end
  end

  @doc """
  Eurostar train signalling IDs directly reference their train numbers, used in public information

  Since signalling IDs must have a letter in the second position, ES trains use I and O, which
  correspond to 1 and 0 in the train number. UK headcode 1O34 is for ES train number 1034, headcode
  1I12 is for train number 1112

  This will return the train number of a schedule, if it's a Eurostar train, and follows the I/O
  scheme, nil otherwise
  """

  @spec get_train_number(t()) :: binary() | nil

  def get_train_number(%Schedule{operator: "ES", signalling_id: <<f::1-binary, "O", s::binary>>}) do
    f <> "0" <> s
  end

  def get_train_number(%Schedule{operator: "ES", signalling_id: <<f::1-binary, "I", s::binary>>}) do
    f <> "1" <> s
  end

  def get_train_number(_) do
    nil
  end

  @spec from_xml(map()) :: t()

  def from_xml(
        _schedule = %{
          _id: :"SCv3:Schedule",
          rid: rid,
          ssd: ssd,
          uid: uid,
          rsid: rsid,
          trainId: signalling_id,
          toc: operator,
          status: status,
          trainCat: category,
          isPassengerSvc: passenger?,
          isActive: active?,
          deleted: deleted?,
          isCharter: charter?,
          cancelReason: cancel_reason,
          choice: schedule_locations
        }
      ) do
    ssd_parsed = Elixir.Date.from_iso8601!(ssd)

    %__MODULE__{
      rid: rid,
      ssd: ssd_parsed,
      uid: uid,
      rsid: rsid,
      signalling_id: signalling_id,
      operator: operator,
      status: def_when_nil_or_empty(status, "P"),
      category: def_when_nil_or_empty(category, "OO"),
      passenger?: passenger? !== false,
      active?: active? !== false,
      deleted?: deleted? || false,
      charter?: charter? || false,
      cancel_reason: DisruptionReason.from_xml(cancel_reason),
      locations: Schedule.Location.from_xml(schedule_locations, ssd_parsed)
    }
  end
end
