defmodule NREFeeds.Darwin.Schema.RealTime.TrackingId do
  @moduledoc """
  Corrections to tracked trains in berths.

  (priv/darwin_v16_schema/rttiPPTTDData_v1.xsd, TrackingID)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * td_area_id                    (berth/area) R TD area ID where the train was reported to be
  * td_berth_id                  (berth/#text) R TD berth ID where the train was reported to be
  * incorrect_signalling_id (incorrectTrainID) R The train ID (headcode) which was incorrectly reported
  * correct_signalling_id     (correctTrainID) R The correct train ID

  There's no guarantee that the incorrectly reported train will still be in the reported berth.

  Note that this type is of extremely limited usefulness, because Darwin provides no contextual
  information it can be easily matched with. It's included for completeness.
  """

  alias NREFeeds.Darwin.Schema.Types

  @typedoc """
  Four character alphanumeric TD berth identifier, identifies a berth
  within a TD area
  (priv/darwin_v16_schema/rttiPPTCommonTypes_v1.xsd, TDBerthIDType)
  """
  @type td_berth :: binary()

  @type t :: %__MODULE__{
          td_area_id: Types.td_area(),
          td_berth_id: td_berth(),
          incorrect_signalling_id: Types.signalling_id(),
          correct_signalling_id: Types.signalling_id()
        }

  @enforce_keys [:td_area_id, :td_berth_id, :incorrect_signalling_id, :correct_signalling_id]
  defstruct [:td_area_id, :td_berth_id, :incorrect_signalling_id, :correct_signalling_id]

  def from_xml(%{
        _id: :"TDv1:TrackingID",
        berth: %{
          "#text": td_berth_id,
          area: td_area_id
        },
        correctTrainID: correct_signalling_id,
        incorrectTrainID: incorrect_signalling_id
      }) do
    %__MODULE__{
      td_area_id: td_area_id,
      td_berth_id: td_berth_id,
      incorrect_signalling_id: incorrect_signalling_id,
      correct_signalling_id: correct_signalling_id
    }
  end
end
