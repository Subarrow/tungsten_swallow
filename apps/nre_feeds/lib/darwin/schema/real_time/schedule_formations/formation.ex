defmodule NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation do
  @moduledoc """
    Single formation for a service

    (priv/darwin_v16_schema/rttiPPTFormations_v2.xsd, Formation)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    * formation_id (fid)        R - Formation ID
    * source (src)              O - Formation data source
    * source_instance (srcInst) O - Supposedly a four-character code, similar to the CIS source for TS
    * coaches (coaches)         R - List of coaches for this formation
  """

  alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation.Coach

  @type t :: %__MODULE__{
          formation_id: binary(),
          source: binary() | nil,
          source_instance: binary() | nil,
          coaches: [Coach.t()]
        }

  @enforce_keys [:formation_id, :source, :source_instance, :coaches]
  defstruct [:formation_id, :source, :source_instance, :coaches]

  def from_xml(%{
        fid: formation_id,
        src: source,
        srcInst: source_instance,
        coaches: %{coach: coaches_raw}
      }) do
    %__MODULE__{
      formation_id: formation_id,
      source: source,
      source_instance: source_instance,
      coaches: Enum.map(coaches_raw, &Coach.from_xml/1)
    }
  end
end
