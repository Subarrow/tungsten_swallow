defmodule NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation.Coach do
  @moduledoc """
    Single formation for a service

    (priv/darwin_v16_schema/rttiPPTFormations_v2.xsd, CoachData)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    * coach_number    (coachNumber) R               - Alphanumeric coach number
    * coach_class      (coachClass) O               - Class of carriage seating

  """

  alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation.Coach.Toilet

  @type t :: %__MODULE__{
          coach_number: binary(),
          coach_class: binary() | nil,
          toilet: Toilet.t() | nil
        }

  @enforce_keys [:coach_number, :coach_class, :toilet]
  defstruct [:coach_number, :coach_class, :toilet]

  def from_xml(%{coachNumber: coach_number, coachClass: coach_class, toilet: toilet_raw}) do
    %__MODULE__{
      coach_number: coach_number,
      coach_class: coach_class,
      toilet: Toilet.from_xml(toilet_raw)
    }
  end
end
