defmodule NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation.Coach.Toilet do
  @moduledoc """

    (priv/darwin_v16_schema/rttiPPTCommonTypes_v4.xsd, ToiletAvailabilityType)

    * toilet/type    (#text) O ("Unknown")   - "Unknown", "None", "Standard" or "Accessible" (not exhaustive)
    * toilet/status (status) O ("InService") - Enum: "Unknown", "InService" or "NotInService" (mapped to atoms)
  """

  @type toilet_status :: :in_service | :not_in_service | :unknown
  @type t :: %__MODULE__{type: binary(), status: binary()}

  @enforce_keys [:type, :status]
  defstruct [:type, :status]

  @spec toilet_status_atom(binary()) :: toilet_status()

  def toilet_status_atom(status) do
    case status do
      nil -> :in_service
      "InService" -> :in_service
      "Unknown" -> :unknown
      "NotInService" -> :not_in_service
    end
  end

  def from_xml(nil), do: nil

  def from_xml(%{"#text": type, status: status_raw}) do
    %__MODULE__{type: type || "Unknown", status: toilet_status_atom(status_raw)}
  end
end
