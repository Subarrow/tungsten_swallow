defmodule NREFeeds.Darwin.Schema.RealTime do
  @moduledoc """

    (priv/darwin_v16_schema/rttiPPTSchema_v16.xsd, uR, sR, DataResponse)

    Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
    brackets if any.

    * source                    (updateOrigin) O "CIS", "TRUST", "TD", "Darwin", "Tyrell", non-exhaustive
    * request_id                   (requestID) O 1-16 character long alphanumeric (plus _-) request ID
    * source_instance          (requestSource) O Request source, schema says it's usually a CIS code (i.e. four-digit alphanumeric)
    * timestamp_darwin                    (ts) R Schema says "local timestamp", but the timestamp is formatted as if it's UTC
    * timestamp_stream            (non-darwin) O UTC timestamp from the RabbitMQ stream which relayed the message, if provided
    * snapshot?                   (non-darwin) O True if tag is sR instead of uR
    * (not mapped)                   (version) R Fixed to "16.0"

    * type                        (non-darwin) R
    * messages                    (non-darwin) R
    *                               (schedule) O see NREFeeds.Darwin.Schema.RealTime.Schedule
    *                            (deactivated) O see NREFeeds.Darwin.Schema.RealTime.Deactivated
    *                            (association) O see NREFeeds.Darwin.Schema.RealTime.Association
    *                     (scheduleFormations) O see NREFeeds.Darwin.Schema.RealTime.ScheduleFormations
    *                                     (TS) O see NREFeeds.Darwin.Schema.RealTime.TrainStatus
    *                       (formationLoading) O see NREFeeds.Darwin.Schema.RealTime.FormationLoading
    *                                     (OW) O see NREFeeds.Darwin.Schema.RealTime.StationMessage
    *                             (trainAlert) O see NREFeeds.Darwin.Schema.RealTime.TrainAlert
    *                             (trainOrder) O see NREFeeds.Darwin.Schema.RealTime.TrainOrder
    *                             (trackingID) O see NREFeeds.Darwin.Schema.RealTime.TrackingID
    *                                  (alarm) O see NREFeeds.Darwin.Schema.RealTime.Alarm

    In principle, per schema, an update could contain any combination of these, in practice this is never done and there's only
    ever one. The fact that the type (e.g. SF for schedule formations) is also expressed in the frame header suggests this will
    never be done in the future.

    Note that some message types never have source_instance and/or request_id set, and note that these fields are not provided
    at all with timetable IDs
  """

  require Logger

  alias NREFeeds.Darwin.Schema.RealTime
  alias NREFeeds.Darwin.Util.DarwinXml

  @type t :: %__MODULE__{
          source: binary() | nil,
          request_id: binary() | nil,
          source_instance: binary() | nil,
          timestamp_darwin: Elixir.DateTime.t(),
          timestamp_stream: Elixir.DateTime.t() | nil,
          snapshot?: boolean(),
          type:
            RealTime.Schedule
            | RealTime.DeactivatedSchedule
            | RealTime.Association
            | RealTime.ScheduleFormations
            | RealTime.TrainStatus
            | RealTime.FormationLoading
            | RealTime.StationMessage
            | RealTime.TrainAlert
            | RealTime.TrainOrder
            | RealTime.TrackingId
            | RealTime.Alarm,
          messages:
            [RealTime.Schedule.t()]
            | [RealTime.DeactivatedSchedule.t()]
            | [RealTime.Association.t()]
            | [RealTime.ScheduleFormations.t()]
            | [RealTime.TrainStatus.t()]
            | [RealTime.FormationLoading.t()]
            | [RealTime.StationMessage.t()]
            | [RealTime.TrainAlert.t()]
            | [RealTime.TrainOrder.t()]
            | [RealTime.TrackingId.t()]
            | [RealTime.TimetableIdentifier.t()]
            | [RealTime.Alarm.t()]
        }

  @enforce_keys [
    :source,
    :request_id,
    :source_instance,
    :timestamp_darwin,
    :timestamp_stream,
    :snapshot?,
    :type,
    :messages
  ]

  defstruct [
    :source,
    :request_id,
    :source_instance,
    :timestamp_darwin,
    :timestamp_stream,
    :snapshot?,
    :type,
    :messages
  ]

  defp force_misec_precision(dt = %DateTime{microsecond: {mi_sec, _precision}}) do
    %DateTime{dt | microsecond: {mi_sec, 6}}
  end

  def from_xml({stream_ts, _original_body, parsed}) do
    from_xml(parsed, stream_ts)
  end

  def from_xml(element, timestamp_stream \\ nil)

  def from_xml(
        %{
          _id: :Pport,
          choice: element = %{_id: :"Pport/TimeTableId"},
          ts: timestamp_raw,
          version: "16.0"
        },
        timestamp_stream
      ) do
    {:ok, timestamp_darwin, _} = DateTime.from_iso8601(timestamp_raw)

    %__MODULE__{
      timestamp_darwin: force_misec_precision(timestamp_darwin),
      timestamp_stream: timestamp_stream,
      snapshot?: false,
      source: nil,
      request_id: nil,
      source_instance: nil,
      type: RealTime.TimetableIdentifier,
      messages: [RealTime.TimetableIdentifier.from_xml(element)]
    }
  end

  def from_xml(
        %{
          _id: :Pport,
          ts: timestamp_raw,
          version: "16.0",
          choice: %{
            _id: element_id,
            schedule: schedules_raw,
            deactivated: deactivated_raw,
            association: associations_raw,
            scheduleFormations: schedule_formations_raw,
            TS: train_statuses_raw,
            formationLoading: formation_loadings_raw,
            OW: station_messages_raw,
            trainAlert: train_alerts_raw,
            trainOrder: train_orders_raw,
            trackingID: tracking_ids_raw,
            alarm: alarms_raw,
            updateOrigin: update_origin,
            requestID: request_id,
            requestSource: request_source
          }
        },
        timestamp_stream
      )
      when element_id == :"Pport/uR" or element_id == :DataResponse do
    {:ok, timestamp_darwin, _} = DateTime.from_iso8601(timestamp_raw)

    [{message_type, messages}] =
      [
        {RealTime.Schedule, schedules_raw},
        {RealTime.DeactivatedSchedule, deactivated_raw},
        {RealTime.Association, associations_raw},
        {RealTime.ScheduleFormations, schedule_formations_raw},
        {RealTime.TrainStatus, train_statuses_raw},
        {RealTime.FormationLoading, formation_loadings_raw},
        {RealTime.StationMessage, station_messages_raw},
        {RealTime.TrainAlert, train_alerts_raw},
        {RealTime.TrainOrder, train_orders_raw},
        {RealTime.TrackingId, tracking_ids_raw},
        {RealTime.Alarm, alarms_raw}
      ]
      |> Enum.reject(fn {_module, entries} -> is_nil(entries) end)
      |> Enum.map(fn
        {module, entries} -> {module, Enum.map(entries, &apply(module, :from_xml, [&1]))}
      end)

    %__MODULE__{
      timestamp_darwin: force_misec_precision(timestamp_darwin),
      timestamp_stream: timestamp_stream,
      snapshot?: element_id == :DataResponse,
      source: update_origin,
      request_id: request_id,
      source_instance: request_source,
      type: message_type,
      messages: messages
    }
  end

  def from_xml(pport = %{_id: :Pport, choice: choice = %{_id: :DataResponse}}, feed_ts) do
    from_xml(
      %{
        pport
        | choice:
            Map.merge(choice, %{updateOrigin: "Snapshot", requestID: nil, requestSource: nil})
      },
      feed_ts
    )
  end

  def from_darwin_frames(frames, schema_model, schema_record_definitions) do
    frames
    |> Enum.filter(&(&1.command == :message))
    |> Enum.map(&DarwinXml.parse_darwin_frame(&1, schema_model, schema_record_definitions))
    |> Enum.filter(fn
      {_ts, original, {:error, error}} ->
        Logger.warning(
          "Failed to parse message, returned #{inspect(error)}, message was #{original}"
        )

        false

      {_ts, _original,
       %{
         _id: :Pport,
         choice: %{
           _id: id
         }
       }}
      when id == :"Pport/uR" or id == :"Pport/sR" or id == :DataResponse ->
        true

      {_ts, _original, parsed} ->
        Logger.info("Filtered non-update/snapshot message: #{inspect(parsed)}")
        false
    end)
    |> Enum.map(&from_xml/1)
  end
end
