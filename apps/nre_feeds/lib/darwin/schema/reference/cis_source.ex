defmodule NREFeeds.Darwin.Schema.Reference.CISSource do
  @moduledoc """
  (priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd, CISSource)

  * code (code) R Four-character CIS code
  * name (name) R CIS name

  Note that this data includes CIS codes or names which appear to be obsolete (At time of
  writing, AM06 is listed as "First Capital Connect", defunct in 2014) not all CIS codes
  actually referenced by Darwin are included in the reference data either.

  Known examples of codes used in Darwin but missing in this reference data:
  * XRRC (Presumably "Crossrail route control" from name and usage)
  * NA01 (Unsure, no clear geographical/operator correlation)
  """

  @type t :: %__MODULE__{code: binary(), name: binary()}

  @enforce_keys [:code, :name]
  defstruct [:code, :name]

  def from_xml(%{_id: :CISSource, code: code, name: name}) do
    %__MODULE__{code: code, name: name}
  end
end
