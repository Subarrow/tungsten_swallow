defmodule NREFeeds.Darwin.Schema.Reference.ViaOutline do
  @moduledoc """
    (priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd, Via)

    * at_crs                     (at) R For a train on the departure board of this station
    * destination_tiploc       (dest) R ... which has this destination
    * intermediate_tiplocs     (loc1) R ... which after at_crs will subsequently call here
    * intermediate_tiplocs     (loc2) O ... and then call here (if not nil)
    * text                  (viatext) R ... display this via text

    This might work better with an example:
    * at_crs:                   "ZFD"     (Farringdon London)
    * destination_tiploc:       "STALBCY" (St Albans)
    * intermediate_tiplocs[0]:  "HKBG"    (Hackbridge)
    * intermediate_tiplocs[1]:  "WIMBLDN" (Wimbledon)
    * text                      "via Hackbridge & Wimbledon"

    So for a train on the departure boards at Farringdon, with a destination of St Albans,
    if the train will call at Hackbridge and then Wimbledon, its destination should be
    shown as "St Albans via Hackbridge & Wimbledon"

    The via text could also be constructed from loc1 and loc2 for most of these, the text
    is mostly provided for convenience.

    Note that false destinations are not used to determine the via text, only true
    destinations, but that via text is still displayed with the false destination.
  """

  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          at_crs: Types.crs(),
          destination_tiploc: Types.tiploc(),
          intermediate_tiplocs: [Types.tiploc()],
          text: binary()
        }

  @enforce_keys [:at_crs, :destination_tiploc, :intermediate_tiplocs, :text]
  defstruct [:at_crs, :destination_tiploc, :intermediate_tiplocs, :text]

  def from_xml(%{
        _id: :Via,
        at: at_crs,
        dest: destination_tiploc,
        loc1: intermediate_tiploc_1,
        loc2: intermediate_tiploc_2,
        viatext: text
      }) do
    if is_nil(intermediate_tiploc_2) do
      %__MODULE__{
        at_crs: at_crs,
        destination_tiploc: destination_tiploc,
        intermediate_tiplocs: [intermediate_tiploc_1],
        text: text
      }
    else
      %__MODULE__{
        at_crs: at_crs,
        destination_tiploc: destination_tiploc,
        intermediate_tiplocs: [intermediate_tiploc_1, intermediate_tiploc_2],
        text: text
      }
    end
  end
end
