defmodule NREFeeds.Darwin.Schema.Reference.Operator do
  @moduledoc """

  (priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd, TocRef)

  * operator     (toc) R Two-letter operator code
  * name     (tocname) R Name of operator
  * url          (url) O URL, technically optional but always present

  URL will be set to http://www.nationalrail.co.uk/tocs_maps/tocs/TrainOperators.aspx if NRE
  doesn't have a dedicated map for the operator. This is not specified behaviour, and so is not
  used to nil this field.

  Note that this is not an exhaustive list, Darwin will leak forward operator codes specified in
  the schedule regardless of their presence in the reference data.

  Some operators in here are not mainline operators, e.g.
  * LT - London Underground (from old name "London Transport")
  * NY - North Yorkshire Moors railway, a heritage railway

  TOC codes can be a little opaque at times, because unless the boundaries of rail franchises are
  significantly altered, a code will usually be retained, hence why LNER has GR (GNER), and
  Greater Anglia has LE ("London Eastern", a previous train operator holding company).

  Network Rail is included as "RT" (from "Railtrack") in its role as a station manager.

  ZF is used as a generic operator to apply as the manager for ferry terminals, and ZB is
  similarly used for bus stops

  Some operators included in the reference data are defunct, Heathrow Connect (HC) ceased in 2018.
  """

  @type t :: %__MODULE__{operator: binary(), name: binary(), url: binary()}

  @enforce_keys [:operator, :name, :url]
  defstruct [:operator, :name, :url]

  def from_xml(%{
        _id: :TocRef,
        toc: operator,
        tocname: name,
        url: url
      }) do
    %__MODULE__{
      operator: operator,
      name: name,
      url: url
    }
  end
end
