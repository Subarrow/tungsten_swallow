defmodule NREFeeds.Darwin.Schema.Reference.Location do
  @moduledoc """

  (priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd, LocationRef)

  * tiploc       (tpl) R TIPLOC for this location, unique per record
  * crs          (crs) O CRS code for location, non-unique
  * operator     (toc) O Operator which manages this location, if location is a station
  * name     (locname) R Name of the location

  Note that the operator can be RT ("Railtrack") to indicate that Network Rail manages a
  station

  While TIPLOCs are the primary key here, names are per-CRS, not per-TIPLOC, which means
  that e.g. the distinction between Glasgow Central High Level vs Glasgow Central Low Level
  doesn't exist in these names.

  Darwin does not define names for most locations, but since the name field is mandatory,
  if Darwin doesn't want to offer a name for the location, the name will be set to the
  TIPLOC. This behaviour is fairly undesirable, since a tiploc is not a 'name' in any normal
  sense.

  So, the behaviour here is that if the operator is nil (to safeguard against the
  possibility of an as-of-yet non-existent all-caps station name which matches the tiploc),
  and the tiploc and name are identical, the name field will be set to nil.

  This doesn't mean a location has no name at all, simply that National Rail have elected
  not to include this name in the reference data.

  Network Rail data, specifically BPLAN (or possibly the train planning network model?) can
  be used as a source for additional or differentiated names.

  https://wiki.openraildata.com/index.php?title=Reference_Data
  """

  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          tiploc: Types.tiploc(),
          crs: Types.crs() | nil,
          operator: binary() | nil,
          name: binary() | nil
        }

  @enforce_keys [:tiploc, :crs, :operator, :name]
  defstruct [:tiploc, :crs, :operator, :name]

  def from_xml(%{
        _id: :LocationRef,
        tpl: tiploc,
        crs: crs,
        toc: operator,
        locname: name_raw
      }) do
    if is_nil(operator) and name_raw == tiploc do
      %__MODULE__{
        tiploc: tiploc,
        crs: crs,
        operator: operator,
        name: nil
      }
    else
      %__MODULE__{
        tiploc: tiploc,
        crs: crs,
        operator: operator,
        name: name_raw
      }
    end
  end
end
