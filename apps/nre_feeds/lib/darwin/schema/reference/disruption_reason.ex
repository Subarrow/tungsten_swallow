defmodule NREFeeds.Darwin.Schema.Reference.DisruptionReason do
  @moduledoc """

  (priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd, Reason)

  * code              (code) R The code which identifies this disruption reason
  * reason_text (reasontext) R The text which describes this disruption reason

  Currently, all codes map to equivalent cancel and delay reasons, e.g. "138" is
  either "This train has been delayed by an obstruction on the line" or
  "This train has been cancelled by an obstruction on the line" depending on
  which list you consult.

  Some reasons are more precise ("cattle on the railway"), others are more vague
  ("animals on the railway"), exactly which one will be used in any given
  circumstance is a decision made by those inputting the information.

  """

  @type t :: %__MODULE__{code: binary(), reason_text: binary()}

  @enforce_keys [:code, :reason_text]
  defstruct [:code, :reason_text]

  def from_xml(%{
        _id: :Reason,
        code: code,
        reasontext: reason_text
      }) do
    %__MODULE__{
      code: code,
      reason_text: reason_text
    }
  end
end
