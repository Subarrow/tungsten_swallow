defmodule NREFeeds.Darwin.Schema.Reference do
  @moduledoc """
  (priv/darwin_v16_schema/rttiCTTReferenceSchema_v3.xsd, PportTimetableRef)

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  * timetable_id           (timetableId) R 14 character timetable reference ID
  * locations              (LocationRef) O Location codes (see LocationReference)
  * operators                   (TocRef) O TOC codes (see OperatorReference)
  * delay_reasons   (LateRunningReasons) O Delay reasons (DisruptionRreasonReference)
  * cancel_reasons (CancellationReasons) O Cancel reasons (DisruptionRreasonReference)
  * via_outlines                   (Via) O Via text to be displayed with destinations
  * cis_sources              (CISSource) O CIS source codes

  All of these are theoretically optional but since these are not incremental updates, they should
  always be present
  """

  alias NREFeeds.Darwin.Schema.Reference
  alias Reference.Location
  alias Reference.Operator
  alias Reference.DisruptionReason
  alias Reference.ViaOutline
  alias Reference.CISSource

  defstruct [
    :timetable_id,
    :locations,
    :operators,
    :delay_reasons,
    :cancel_reasons,
    :via_outlines,
    :cis_sources
  ]

  def from_xml(%{
        _id: :PportTimetableRef,
        timetableId: timetable_id,
        LocationRef: locations_raw,
        TocRef: operators_raw,
        LateRunningReasons: %{Reason: delay_reasons_raw},
        CancellationReasons: %{Reason: cancel_reasons_raw},
        Via: via_raw,
        CISSource: cis_sources_raw
      }) do
    %__MODULE__{
      timetable_id: timetable_id,
      locations: Enum.map(locations_raw, &Location.from_xml/1),
      operators: Enum.map(operators_raw, &Operator.from_xml/1),
      delay_reasons: Enum.map(delay_reasons_raw, &DisruptionReason.from_xml/1),
      cancel_reasons: Enum.map(cancel_reasons_raw, &DisruptionReason.from_xml/1),
      via_outlines: Enum.map(via_raw, &ViaOutline.from_xml/1),
      cis_sources: Enum.map(cis_sources_raw, &CISSource.from_xml/1)
    }
  end
end
