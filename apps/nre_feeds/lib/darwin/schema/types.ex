defmodule NREFeeds.Darwin.Schema.Types do
  @moduledoc """
  Various Darwin types which aren't structs
  """

  @typedoc """
  A tiploc, or "Train In Position LOCation" is a 4..7 character long all-caps alphanumeric location code
  which identifies locations essential for rail scheduling and routing, such as stations, junctions,
  sidings, depots, and so on. These codes are taken from British Rail (and later Network Rail's) rail
  scheduling system.

  This type is for strippped tiplocs (e.g. "WACW" instad of "WACW   "), the norm in Darwin.

  Some tiplocs represent ferry terminals only served by ships which are/were in the WTT, some tiplocs are
  bus stops only served by current/former buses, but there's also rail stations which have ferry services
  scheduled against the same location code, and then there's stations with regularly scheduled buses.
  Of course, rail replacement buses are scheduled against the station tiploc too, even at stations
  without regularly scheduled buses.

  Some ferry tiplocs are located abroad (e.g. Belfast or Hoek van Holland), as are some rail tiplocs,
  thanks to Eurostar.

  Some rail locations are on railways not controlled by Network Rail (or otherwise part of the National Rail
  network), but coverage is generally limited to locations which are origins or destinations. London
  Underground trains running partly over NR infrastructure (Richmond-Upminster, Watford-Euston) are given NR
  schedules covering all the locations they traverse on NR parts, but only destinations or
  origins in the LT controlled parts. The same principle applies to Eurostar services.

  Note that one "station" may have multiple tiplocs, for example "London St Pancras", with one National Rail
  CRS code (STP) has the tiplocs
  * STPXBOX (Low Level     - Thameslink Services)
  * STPX    (High Level    - Midland main line)
  * STPANCI (CTRL services - both Eurostar international and Southeastern domestic)

  St Pancras also has STPADOM, meant to differentiate domestic CTRL facilities, but never put into use.

  Defunct locations are often not removed. Sinfin North (SINFINN) no longer exists, and has had no services
  since Darwin was introduced. Darwin reference data also maps defunct tiplocs to station CRS codes, e.g.
  WAT has five tiplocs mapped, all but one of which is defunct.

  (priv/darwin_v16_schema/rttiPPTCommonTypes_v1.xsd, TiplocType)
  """
  @type tiploc :: binary()

  @typedoc """
    The CRS (originally "Computerised Reservation System") code is a three-letter code assigned to each
    station, bus stop, ferry terminal, and to some other locations.

    These codes are derived from BR/Network Rail data (CORPUS) which maintains one "3-alpha" code per
    TIPLOC, but with rare exceptions (detailed below), National Rail doesn't do this. For each distinct
    named station, even if the station has multiple tiplocs, it picks one code, and uses it for the entire
    station.

    Take St Pancras International, for example. It's four stations in a trenchcoat, spread over three tiplocs,
    but to National Rail, this is one station with one name, and it assigns it just one CRS code, STP.

    * STPANCI - 3-alpha: SPX (CTRL services - international and domestic high speed)
    * STPXBOX - 3-alpha: SPL (Low level     - Thameslink)
    * STPX    - 3-alpha: STP (High level    - Midland main line)

    Knowledgebase has a very partial exception, with two entries which don't respect this rule:
    * SPX  - Intended to offer a separate overview of facilities at St Pancras International
    * ASI  - Intended to offer a separate overview of facilities at Ashford International

    Neither entry is maintained, and neither code maps to any tiploc in Darwin's reference data.
  """
  @type crs :: binary()

  @typedoc """
  Four-character code which uniquely identifies this train within a specific area to a rail signaller.
  Alphanumeric and all-capital, taking the form of one number, one letter, two numbers (e.g. "2U31")

  These are used by TD and TRUST, Network Rail real-time systems which feed Darwin, and are sometimes used
  by non-signalling rail staff as shorthand for services. These are sometimes called "headcodes", as they
  were once carried on the front of trains.

  Ferries are given 0S00 (S being ship) in the schedule and buses are 0B00.

  Signalling IDs do carry some meaning, and rail enthusiasts often make reference to them, but for our
  purposes, they don't express anything which isn't covered by the category code.

  Eurostar signalling IDs are interesting because they're designed to be matched to the train numbers in
  use, by replacing the 'O' or 'I' with a 0 or 1 respectively (e.g. 9I26 refers to train 9126, 9O18
  refers to the train 9018, both are public information)

  (priv/darwin_v16_schema/rttiPPTCommonTypes_v1.xsd, TrainIdType)
  """
  @type signalling_id :: binary()

  @typedoc """
  A RID (or "RTTI [Real Time Train Information] Train ID") is a 15-character (schema says 16 character
  maximum) identifier for a given service on a given day in Darwin, applying to trains, buses, and ferries
  in the schedule.

  RIDs are currently numeric, but this should not be relied upon, the schema only specifies their length.
  Their construction is also known, but again, must not be relied upon, it's not specified. If you
  need the SSD and UID for a service, these are already provided.

  RIDs are currently shorthand for a pairing of the UID and SSD for a service

  So, for the RID 202301147621947, we can separate it as follows:
  * 2023 01 14 (2023-01-14, the schedule start date)
  * 76 (numeric representation of "L" in ASCII)
  * 21947 (numeric portion of UID)

  So 202301147621947 expresses the UID L21947 and SSD 2023-01-14

  (priv/darwin_v16_schema/rttiPPTCommonTypes_v1.xsd, RIDType)
  """
  @type rid :: binary()

  @typedoc """
  An ID for a formation. Currently formed of the RID, with a three digit suffix separated by a hyphen. This
  means that formation IDs are globally unique. For example, a train with three formations might have the
  formation IDs:

  * 202301147621947-001
  * 202301147621947-002
  * 202301147621947-003

  Note that new formations with new IDs might be created to replace old ones. A service having a formation
  means nothing unless it's referenced in a schedule location.
  """
  @type formation_id :: binary()

  @typedoc """
  Retail Service Identifier, six to eight characters long, alphanumeric, and sometimes displayed on
  outward-facing displays on intercity trains. This isn't always provided for all services in Darwin,
  and you should not assume the RSID will be physically displayed for any service.

  The schema suggests that if six characters, the RSID only contains the 'portion' element, and not
  the train operator code

  e.g. a RSID could be "SW812800", or "812800"

  (priv/darwin_v16_schema/rttiPPTCommonTypes_v2.xsd, RSIDType)
  """
  @type rsid :: binary()

  @typedoc """
  Unique six-character identifier for a rail schedule in the train planning system. Note that this
  is unique to the schedule and not the service, and must be combined with the SSD to become unique for
  the service on a given day.

  UIDs in Darwin consist of a letter, then five numbers, e.g. "L21947". Any UID beginning with an "O"
  originates from Darwin proper, rather than TPS.

  Darwin doesn't use the VSTP feed, and doesn't use VSTP format (a space and five numbers). VSTP schedules
  have to be input into Darwin separately, and will have the "O" prefix.

  (priv/darwin_v16_schema/rttiPPTCommonTypes_v1.xsd, UIDType)
  """
  @type uid :: binary()

  @typedoc """
  Platform numbers are a maximum of three characters long, and are alphanumeric

  There are some things you need to know about platform numbers:
  * Platform numbers are alphanumeric, and one to three characters long, you might see EXM, UF, 0, 13R, or B
  * Platform numbers are used for locations which are not stations, and to express things which are not "platforms" in the
    usual sense, e.g. sidings, the route through a junction, or a line through a station the train isn't stopping at.
  * Platforms at stations may be subdivided, this commonly takes letter suffixes (e.g. A, B, etc), but
    Glasgow Central (High level) has F and R (for "Front" and "Rear"), and Aberdeen has N and S ("North" and "South").
  * Some stations with only a single platform indicate direction in the platform field
    (e.g. at LYMPSTC Lympstone Commando, platforms are either EXE - towards Exeter or EXM - towards Exmouth)
  * There is not any published dataset which reliably sets out exactly which tiplocs have which platforms
  """
  @type platform :: binary()

  @typedoc """
  Two character alphanumeric TD area identifier

  https://wiki.openraildata.com/index.php?title=List_of_Train_Describers

  (priv/darwin_v16_schema/rttiPPTCommonTypes_v1.xsd, TDAreaIDType)
  """
  @type td_area :: binary()

  @typedoc """
  Activities a service performs at a location. This will always be an even number of characters long, since each activity is
  two wide, but will always be shortened, unlike TPS extracts.

  Some examples!

  * "T X "   - the service stops to pick up and set down passengers (T), and also to let another train pass (X)
  * "TBK OR" - train begins (TB), passenger count point (K), locomotive attached to rear (OR)
  * ""       - the train does nothing notable
  * "  "     - the train does nothing notable

  For passenger purposes, the codes you particularly need to care about are:
  * "TB" - Train begins (always present at origin)
  * "TF" - Train finishes (always present at destination)
  * "T " - Train stops to set down and pick up passengers
  * "D " - Stops to set down passengers (but not pick up)
  * "U " - Stops to pick up passengers (but not set down)
  * "R " - Train stops "as required"; a passenger at the station wishing to board should clearly indicate this to the driver
           by adopting the same procedure used to catch a bus, a passenger on the train wishing to alight must tell the guard.
  * "N " - Stop not advertised

  Note that origins will always have TB (but will not have T or U), and destinations will always have TF (but not T or D).
  A schedule may have multiple TF and TB if an origin/destination is cancelled and substituted, but you should use schedule
  location types for determining origins and destinations within a schedule in any event.

  Some activities (KC, KE, KF, KS) relate to ticket inspection, are unlikely to correspond with how ticket checks are
  actually conducted, and so shouldn't be considered sensitive.

  The actual activities are defined in BPLAN (see `BPLAN.Data.Activity`).
  """
  @type activity :: binary()
end
