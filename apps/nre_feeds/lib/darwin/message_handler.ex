defmodule NREFeeds.Darwin.MessageHandler do
  use GenServer
  require Logger
  alias Barytherium.Frame
  alias Barytherium.Network
  alias Barytherium.Network.Sender

  alias TungstenSwallow.Util.BackoffManager

  defp decompress_frame_body(frame = %Frame{body: body}) do
    %Frame{frame | body: :zlib.gunzip(:binary.bin_to_list(body))}
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %{client: nil, opts: opts}, name: opts.name)
  end

  def init(state) do
    BackoffManager.notify_connect(state.opts.id)
    {interval, _} = BackoffManager.get_interval(state.opts.id)
    Logger.info("#{state.opts.id} waiting before attempting (re)connection #{interval}ms")
    Process.send_after(self(), :connect, interval)

    state =
      Map.merge(state, %{
        sequence: 0
      })

    {:ok, state}
  end

  def handle_info(:connect, state = %{opts: %{host: host, port: port}}) do
    {:ok, network_pid} =
      Network.start_link(self(), host, port,
        receiver_options: [heartbeat_receive_allowance: 4000]
      )

    {:noreply, Map.put(state, :network_pid, network_pid)}
  end

  def handle_cast(
        {:barytherium, :disconnect, reason},
        state = %{opts: %{host: host, port: port, id: relay_id}}
      ) do
    Logger.error("Connection to #{host}:#{port} disconnected for reason #{reason}")
    BackoffManager.notify_failure(relay_id)
    {:stop, :connect_disconnected, state}
  end

  def handle_cast(
        {:barytherium, :connect, {:error, error}},
        state = %{opts: %{host: host, port: port, id: relay_id}}
      ) do
    Logger.error("Connection to #{host}:#{port} failed, error: #{error}")
    BackoffManager.notify_failure(relay_id)
    {:stop, :connect_disconnected, state}
  end

  def handle_cast(
        {:barytherium, :connect, {:ok, sender_pid}},
        state = %{
          opts:
            opts = %{
              host: host,
              port: port,
              user: user,
              pass: pass,
              id: relay_id
            }
        }
      ) do
    Logger.info("Connection to #{host}:#{port} succeeded, remote end has picked up")

    Sender.write(sender_pid, [
      %Frame{
        command: :connect,
        headers: [
          {"accept-version", "1.2"},
          {"host", "/"},
          {"heart-beat", "7000,7000"},
          {"login", user},
          {"passcode", pass},
          {"client-id", "#{user}-#{relay_id}"}
        ]
      }
    ])

    {:noreply, Map.merge(state, %{sender_pid: sender_pid, opts: %{opts | user: nil, pass: nil}})}
  end

  def handle_cast(
        {:barytherium, :frames, {[frame = %Frame{command: :connected}], sender_pid}},
        state = %{opts: opts = %{queue: queue, id: relay_id}}
      ) do
    Logger.info("Received connected frame: " <> inspect(frame, binaries: :as_strings))

    Sender.write(sender_pid, [
      %Barytherium.Frame{
        command: :subscribe,
        headers:
          (opts[:subscribe_params] || []) ++
            [
              {"id", "0"},
              {"destination", queue},
              {"ack", "client"}
            ]
      }
    ])

    BackoffManager.notify_success(relay_id)

    {:noreply, state}
  end

  def handle_cast(
        {:barytherium, :frames, {frames, sender_pid}},
        state = %{opts: opts = %{sink: sink}}
      ) do
    cond do
      length(frames) > 120 ->
        Logger.error("Received >120 frames in one cast, quitting")
        {:stop, :frame_bounding, state}

      Map.get(opts, :decompress?, true) ->
        frames = frames |> Enum.map(&decompress_frame_body/1)
        GenServer.cast(sink, frames)

        acknowledge_frame(List.last(frames), sender_pid)
        {:noreply, state}

      true ->
        GenServer.cast(sink, frames)

        acknowledge_frame(List.last(frames), sender_pid)
        {:noreply, state}
    end
  end

  def acknowledge_frame(frame, sender_pid) do
    headers = Frame.headers_to_map(frame.headers)

    Sender.write(
      sender_pid,
      [%Frame{command: :ack, headers: [{"id", Map.get(headers, "ack")}]}]
    )
  end
end
