defmodule NREFeeds.Darwin.Data.LocationNames do
  @location_names %{
    "BRUXMID" => %{
      "nl-be" => "Brussel-Zuid",
      "fr-be" => "Bruxelles-Midi"
    },
    "DUNDETB" => %{
      "en-gb" => "Dundee",
      "gd-gb" => "Dùn Dè"
    }
  }

  def location_names do
    @location_names
  end
end
