defmodule NREFeeds.Darwin.Data.LocationTimezones do
  @moduledoc """
  Determinations for location timezones.

  Note that some locations are defunct, others exist in data despite there being no
  likelihood of services being put in place.


  """

  @location_timezones %{
    ####################
    # Eurostar / other rail
    ####################

    # BE Bruxelles Midi
    "BRUXMID" => "Europe/Brussels",
    # BE Oostende
    "OOSTEND" => "Europe/Brussels",

    # LU Bettembourg
    "BETTMBG" => "Europe/Luxembourg",

    # CZ Velim Test Track
    "VELIMTT" => "Europe/Prague",

    # DE Bochum Hbf
    "BOCHUM" => "Europe/Berlin",
    # DE Bonn Hbf
    "BONNHBF" => "Europe/Berlin",
    # DE Dortmund Hbf
    "DORTMND" => "Europe/Berlin",
    # DE Duisburg Hbf
    "DUISBRG" => "Europe/Berlin",
    # DE Dusseldorf Hbf
    "DUSSDRF" => "Europe/Berlin",
    # DE Essen Hbf
    "ESSNHBF" => "Europe/Berlin",
    # DE Frankfurt Main Hbf
    "FRNKFRT" => "Europe/Berlin",
    # DE Koblenz Hbf
    "KOBLENZ" => "Europe/Berlin",
    # DE Koln (Köln) Hbf
    "KOLNHBF" => "Europe/Berlin",
    # DE Mainz Hbf
    "MINZHBF" => "Europe/Berlin",

    # FR Amiens
    "AMIENS" => "Europe/Paris",
    # FR Avignon C M T
    "AVIGCMT" => "Europe/Paris",
    # FR Avignon Ville
    "AVIGVIL" => "Europe/Paris",
    # FR Bordeaux St. Jean
    "BORDSTJ" => "Europe/Paris",
    # FR Bourg St. Maurice
    "BORGSTM" => "Europe/Paris",
    # FR Calais Frethun
    "CALAFGB" => "Europe/Paris",
    # FR Calais Frethun (Tunnel)
    "CALAFT" => "Europe/Paris",
    # FR Calais Frethun Gare Vgrs
    "CALAFGV" => "Europe/Paris",
    # FR Cerbere (Cerbère)
    "CERBERE" => "Europe/Paris",
    # FR Chambery-Challes-les-Eaux
    "CHAMCLE" => "Europe/Paris",
    # FR Charleville-Mezieres (Charleville-Mézières)
    "CLVMEZR" => "Europe/Paris",
    # FR Dax
    "DAXX" => "Europe/Paris",
    # FR Dieppe
    "DIEPPE" => "Europe/Paris",
    # FR Forbach
    "FORBACH" => "Europe/Paris",
    # FR Hendaye
    "HNDAYE" => "Europe/Paris",
    # FR Hondeghem
    "HONDEGM" => "Europe/Paris",
    # FR Lille
    "LILLE" => "Europe/Paris",
    # FR Marne-Le-Vallee
    "MARNELV" => "Europe/Paris",
    # FR Metz
    "METZ" => "Europe/Paris",
    # FR Marseille St Charles
    "MRSLSTC" => "Europe/Paris",
    # FR Nimes (Nîmes)
    "NIMES" => "Europe/Paris",
    # FR Narbonne
    "NRBONNE" => "Europe/Paris",
    # FR Paris Le Landy Depot
    "PARILLA" => "Europe/Paris",
    # FR Paris St Lazare
    "PARISLZ" => "Europe/Paris",
    # FR Paris Gare du Nord
    "PARISND" => "Europe/Paris",
    # FR Poitiers
    "POITIER" => "Europe/Paris",
    # FR Rouen DD (unsure what DD stands for)
    "ROUENDD" => "Europe/Paris",
    # FR Lille Flandres
    "LILLEFL" => "Europe/Paris",

    # NL Amsterdam CS
    "AMSTDAM" => "Europe/Amsterdam",
    # NL Roosendaal CS
    "ROSNDAL" => "Europe/Amsterdam",
    # NL Den Haag HS (Den Haag CS)
    "DNHAAG" => "Europe/Amsterdam",
    # NL Rotterdam
    "ROTTCS" => "Europe/Amsterdam",

    # GB Channel Tunnel boundary (CTRL)
    "CHUNCTR" => "Europe/London",
    # GB BR/Eurotunnel Ops Boundary
    "CHUNEOB" => "Europe/London",
    # GB Dollands Moor West Jn
    "DOLMWJN" => "Europe/London",
    # GB Westenhanger Crossovers
    "WENHX" => "Europe/London",
    # GB Dollands Moor T.P.L.
    "DOLMEST" => "Europe/London",
    # GB Saltwood Junction
    "SLWDJN" => "Europe/London",
    # GB Dollands Moor Sidings
    "DOLMORS" => "Europe/London",
    # GB Eurotunnel Headshunt
    "EUROHS" => "Europe/London",

    # GB-NIR Belfast Central
    "BLFSTCL" => "Europe/Belfast",
    # GB-NIR Portrush
    "PRSH" => "Europe/Belfast",
    # GB-NIR Londonderry (Derry)
    "LDRY" => "Europe/Belfast",

    # IE "Dun Loaghaire Malin", actually Dun Laoghaire (Mallin)
    "DUNLHRM" => "Europe/Dublin",
    # IE Arklow
    "ARKW" => "Europe/Dublin",
    # IE Wicklow
    "WKLW" => "Europe/Dublin",
    # IE Waterford (Plunkett, Phluincéid)
    "WATERFD" => "Europe/Dublin",
    # IE Bray
    "BRAY" => "Europe/Dublin",
    # IE Galway
    "GALWAY" => "Europe/Dublin",
    # IE Cork
    "CORK" => "Europe/Dublin",
    # IE Dublin Connolly
    "DUBLINC" => "Europe/Dublin",
    # IE Mallow
    "MALLOW" => "Europe/Dublin",
    # IE Limerick
    "LIMERIC" => "Europe/Dublin",
    # IE Wexford
    "WEXFDOH" => "Europe/Dublin",
    # IE Dublin Heuston
    "DUBLINH" => "Europe/Dublin",
    # IE Clonmel
    "CLONMEL" => "Europe/Dublin",
    # IE Enniscorthy
    "ENISCTY" => "Europe/Dublin",
    # IE Killarney
    "KILARNY" => "Europe/Dublin",
    # IE Tralee
    "TRALEE" => "Europe/Dublin",
    # IE Limerick Junction
    "LIMERCJ" => "Europe/Dublin",
    # IE Carrick-on-Suir
    "CARICOS" => "Europe/Dublin",

    ####################
    # Ferry - Ireland
    ####################
    # Belfast Donegall Quay (GB-NIR)
    "BLFSTDQ" => "Europe/Belfast",
    # Belfast port (GB-NIR)
    "BLFSTPT" => "Europe/Belfast",

    # Cork Ringaskiddy Ferryport
    "CORKRNG" => "Europe/Dublin",

    # Dublin Port-Stena (IE)
    "DUBLFST" => "Europe/Dublin",
    # Rosslare Harbour - Calafort Ros Láir (IE)
    "ROSLARE" => "Europe/Dublin",
    # Dun Laoghaire (IE)
    "DUNLGHR" => "Europe/Dublin",
    # Dublin Ferryport (IE)
    "DUBLINF" => "Europe/Dublin",

    ####################
    # Ferry (UK)
    ####################

    # Birkenhead 12 Quays (Manchester) (GB-ENG)
    "BRKNIOM" => "Europe/London",
    # Fishguard Harbour (GB-WLS)
    "FGDHBR" => "Europe/London",
    # Holyhead (GB-WLS)
    "HLYH" => "Europe/London",
    # Heysham Harbour (GB-ENG)
    "HEYSHBR" => "Europe/London",
    # Liverpool Landing Stage (GB-ENG)
    "LVRPLSG" => "Europe/London",
    # Pembroke Ferry Terminal (GB-WLS)
    "PEMBFTM" => "Europe/London",
    # Penzance Quay (GB-ENG)
    "PNZQUAY" => "Europe/London",
    # Preston C.S. (GB-ENG)
    "PRSTNCS" => "Europe/London",
    # Harwich International
    "PRKSTON" => "Europe/London",
    # St. Mary's Quay (GB-ENG, Isles of Scilly)
    "STMAQUY" => "Europe/London",
    # Yarmouth (I.O.W.) (GB-ENG)
    "YMTHIOW" => "Europe/London",

    # Cairnryan (Loch Ryan Port) (GB-SCT)
    "CNRYNP" => "Europe/London",
    # Canna (GB-SCT)
    "CANNA" => "Europe/London",
    # Castlebay (GB-SCT)
    "CSBY" => "Europe/London",
    # Coll (GB-SCT)
    "COLL" => "Europe/London",
    # Colonsay (GB-SCT)
    "COLONSY" => "Europe/London",
    # Eigg (GB-SCT)
    "EIGG" => "Europe/London",
    # Lochmaddy (GB-SCT)
    "LCHMADY" => "Europe/London",
    # Lochboisdale (GB-SCT)
    "LCHBSDL" => "Europe/London",
    # Mallaig (GB-SCT)
    "MLAIG" => "Europe/London",
    # Muck (GB-SCT)
    "MUCK" => "Europe/London",
    # Oban (GB-SCT)
    "OBAN" => "Europe/London",
    # Rùm (Rhum) (GB-SCT)
    "RHUM" => "Europe/London",
    # Stromness (GB-SCT)
    "SNSS" => "Europe/London",
    # Scrabster (GB-SCT)
    "SCRBSTR" => "Europe/London",
    # Stornoway (GB-SCT)
    "STWY" => "Europe/London",
    # Tarbert (GB-SCT)
    "TARBERT" => "Europe/London",
    # Tiree (GB-SCT)
    "TIREE" => "Europe/London",
    # Ullapool (GB-SCT)
    "ULLAPOL" => "Europe/London",
    # Uig (GB-SCT)
    "UIGG" => "Europe/London",
    ####################
    # Ferry (Other)
    ####################
    # Hoek Van Holland (NL)
    "HOEKVHL" => "Europe/Amsterdam",
    # Douglas (Isle of Man) (IM)
    "DOUGLAS" => "Europe/London"
  }

  def location_timezones do
    @location_timezones
  end
end
