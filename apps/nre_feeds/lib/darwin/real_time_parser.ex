defmodule NREFeeds.Darwin.RealTimeParser do
  use GenServer

  alias NREFeeds.Darwin.Schema.RealTime

  @task_chunk_size 8

  def start_link(
        schema_record_definitions: schema_record_definitions,
        schema_model: schema_model,
        sink: sink,
        link_opts: link_opts
      ) do
    GenServer.start_link(
      __MODULE__,
      %{
        schema_record_definitions: schema_record_definitions,
        schema_model: schema_model,
        sink: sink
      },
      link_opts
    )
  end

  def init(state) do
    {:ok, state}
  end

  def submit_realtime_frames(frames, parser_pid) do
    GenServer.cast(parser_pid, frames)
  end

  def handle_cast(
        frames,
        state = %{
          schema_record_definitions: schema_record_definitions,
          schema_model: schema_model,
          sink: sink
        }
      ) do
    frames_processed =
      frames
      |> Enum.chunk_every(@task_chunk_size)
      |> Enum.map(
        &Task.async(RealTime, :from_darwin_frames, [&1, schema_model, schema_record_definitions])
      )
      |> Enum.map(&Task.await/1)
      |> List.flatten()

    GenServer.cast(sink, frames_processed)

    {:noreply, state}
  end
end
