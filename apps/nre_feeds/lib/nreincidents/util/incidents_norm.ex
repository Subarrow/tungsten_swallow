defmodule NREFeeds.Incidents.Util.IncidentsNorm do
  @moduledoc """
  Utilities for normalising fields found in the NRE incidents feed
  """

  @spec default_nil_or_empty(thing :: any() | nil, default :: any()) :: any()
  def default_nil_or_empty(thing, default) do
    case thing do
      "" -> default
      nil -> default
      _ -> thing
    end
  end

  @spec parse_nullable_time(binary() | nil) :: DateTime.t()

  def parse_nullable_time(nil) do
    nil
  end

  def parse_nullable_time(time) do
    {:ok, parsed, _offset} = DateTime.from_iso8601(time)

    parsed
  end
end
