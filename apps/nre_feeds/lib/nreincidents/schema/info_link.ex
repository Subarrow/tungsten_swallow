defmodule NREFeeds.Incidents.Schema.InfoLink do
  @moduledoc """

  Struct field name (Schema field name), O denotes 'optional' in schema, R is 'required'.

  uri           (Uri) R URI
  label       (Label) O Label for link

  priv/nre_incidents_v5_schema/nre-incident-v5-0.xsd.xml, InfoLinks, InfoLink, InfoLinkStructure
  """

  @enforce_keys [:uri, :label]
  defstruct @enforce_keys

  @type t :: %__MODULE__{
          uri: binary(),
          label: binary() | nil
        }

  @type xml_info_links :: %{InfoLink: list(), _id: :"PtIncidentStructure/InfoLinks"}
  @type xml_info_link :: %{
          Label: binary() | nil,
          Uri: binary(),
          _id: :InfoLinkStructure
        }

  @spec from_xml(input :: xml_info_link | xml_info_links) :: [xml_info_links] | xml_info_link

  def from_xml(%{
        InfoLink: info_links,
        _id: :"PtIncidentStructure/InfoLinks"
      }) do
    info_links |> Enum.map(&from_xml/1)
  end

  def from_xml(%{
        Label: label,
        Uri: uri,
        _id: :InfoLinkStructure
      }) do
    %__MODULE__{
      label: label,
      uri: uri
    }
  end
end
