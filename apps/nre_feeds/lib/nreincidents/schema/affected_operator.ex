defmodule NREFeeds.Incidents.Schema.AffectedOperator do
  @moduledoc """
  Struct field name (Schema field name), O denotes 'optional' in schema, R is 'required'.

  operator_code       (OperatorRef) R ATOC code for operator
  name               (OperatorName) O Name for operator

  Keep in mind that KB ATOC operator codes and Darwin operator codes/names don't align 1:1

  priv/nre_incidents_v5_schema/nre-incident-v5-0.xsd.xml, AffectedOperatorStructure
  """

  @type t :: %__MODULE__{
          operator_code: binary(),
          name: binary() | nil
        }

  @enforce_keys [:operator_code, :name]
  defstruct @enforce_keys

  def from_xml(%{
        AffectedOperator: affected_operators,
        _id: :"AffectsStructure/Operators"
      }) do
    from_xml(affected_operators)
  end

  def from_xml(operators) when is_list(operators) do
    operators |> Enum.map(&from_xml/1)
  end

  def from_xml(nil) do
    []
  end

  def from_xml(%{
        OperatorName: name,
        OperatorRef: operator_code,
        _id: :AffectedOperatorStructure
      }) do
    %__MODULE__{
      operator_code: operator_code,
      name: name
    }
  end
end
