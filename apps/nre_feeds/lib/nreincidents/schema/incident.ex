defmodule NREFeeds.Incidents.Schema.Incident do
  @moduledoc """

  Struct field name (Darwin field name), O denotes 'optional' in schema, R is 'required', default in
  brackets if any.

  creation_time           (CreationTime) R          Datetime created
  last_changed_user      (ChangeHistory) R          The username of the person who most recently changed this. See below for note.
  last_changed_time      (ChangeHistory) R          The time the incident was last changed at
  last_changed_toc       (ChangeHistory) O          The TOC affiliation of the person who last changed this incident (may be nil, even for people who are affiliated)
  participant           (ParticipantRef) O          Documentation is ambiguous on this, may refer to organisation issuing incident (undocumented in RSPS5050)
  incident_number       (IncidentNumber) R          Schema is unclear on uniqueness, RSPS5050 says this alone is unique within KB
  version                      (Version) O          Optional version, if a version is larger than a previous one, it takes precedence (similar to darwin_ts)
  twitter_hashtag               (Source) O          Documentation suggests source should express origin of information. It does not, it expresses a twitter hashtag and nothing else.
                   (OuterValidityPeriod) O          Undocumented in schema, "not used" per RSPS5050 (P-01-02)
  validity_start        (ValidityPeriod) R          (HalfOpenTimestampRangeStructure→StartTime) Commencement (Always present). RSPS5050 suggests there can be multiple validity pairs
  .                                                     and the structure returned from parsing seems to bear this out, in practice I can't find any examples of this happening.
  validity_end          (ValidityPeriod) R          (HalfOpenTimestampRangeStructure→EndTime) End time (may be nil)
  planned?                     (Planned) O (false)  Set to true if this is planned disruption (e.g. engineering, but not usually set for industrial action)
  summary                      (Summary) R          Short plain summary, e.g. "Disruption through Worthing"
  description              (Description) R          Full HTML description of incident
  info_links                 (InfoLinks) O          Relevant hyperlinks
  affects_operators            (Affects) O          Operators affected (list)
  affects_routes               (Affects) O          Routes affected (HTML string)
  cleared?             (ClearedIncident) O (false)  Incident is cleared and no longer applies
  priority            (IncidentPriority) O          Inverse of OW severity, here 0 is most severe, 2 is least severe*
  p0_summary                 (P0Summary) O          Undocumented, purpose is unclear (also undocumented in RSPS5050)*

  Note that last_changed_user references the username of the person who most recently changed the entry, these usernames are based on the actual names of rail staff (e.g. John Smith
  would be JSmith), and this field is not exposed on the public NRE website. I strongly recommend you exclude these names from anything passenger-facing.

  Some (all?) NRCC staff have NRCC in their names (JSmith_NRCC)

  The TOC codes referenced here don't align 1:1 with the TOC codes used by Darwin (e.g. LNWR/WMT share code LM in Darwin but are LN and WM in KB), but do align with the rest of KB

  Note that priority and p0_summary are never used in practice

  ** All datetimes include TZ offsets and are therefore converted to UTC **

  priv/nre_incidents_v5_schema/nre-incident-v5-0.xsd.xml
  """

  alias NREFeeds.Incidents.Schema.{AffectedOperator, InfoLink}

  import NREFeeds.Incidents.Util.IncidentsNorm

  @type t :: %__MODULE__{
          creation_time: DateTime.t(),
          last_changed_user: binary(),
          last_changed_time: DateTime.t(),
          last_changed_toc: binary() | nil,
          participant: binary() | nil,
          incident_number: binary(),
          version: pos_integer() | nil,
          twitter_hashtag: binary(),
          validity_start: DateTime.t(),
          validity_end: DateTime.t() | nil,
          planned?: boolean(),
          summary: binary(),
          description: binary(),
          info_links: [InfoLink.t()],
          affects_operators: [AffectedOperator.t()],
          affects_routes: binary() | nil,
          cleared?: boolean(),
          priority: 0 | 1 | 2 | nil,
          p0_summary: binary()
        }

  @enforce_keys [
    :creation_time,
    :last_changed_user,
    :last_changed_time,
    :last_changed_toc,
    :participant,
    :incident_number,
    :version,
    :twitter_hashtag,
    :validity_start,
    :validity_end,
    :planned?,
    :summary,
    :description,
    :info_links,
    :affects_operators,
    :affects_routes,
    :cleared?,
    :priority,
    :p0_summary
  ]

  defstruct @enforce_keys

  def extract_affects(%{
        Operators: operators,
        RoutesAffected: routes_affected_raw,
        _id: :AffectsStructure
      }) do
    {AffectedOperator.from_xml(operators), routes_affected_raw}
  end

  def extract_affects(nil) do
    {nil, nil}
  end

  def extract_twitter_hashtag(%{TwitterHashtag: hashtag, _id: :SourceStructure}) do
    hashtag
  end

  def extract_twitter_hashtag(nil) do
    nil
  end

  def from_xml(%{_id: :Incidents, PtIncident: incidents}) do
    incidents |> Enum.map(&from_xml/1)
  end

  def from_xml(%{
        _id: :PtIncidentStructure,
        CreationTime: creation_time_raw,
        ChangeHistory: %{
          ChangedBy: %{
            "#text": changed_by,
            _id: :"common:ChangeHistoryStructure/ChangedBy",
            tocAffiliation: changed_by_toc_affiliation
          },
          LastChangedDate: changed_date_raw,
          _id: :"common:ChangeHistoryStructure"
        },
        ParticipantRef: participant_ref_raw,
        IncidentNumber: incident_number,
        Version: version,
        Source: twitter_hashtag_raw,
        OuterValidityPeriod: _outer_validity_raw,
        ValidityPeriod: [
          %{
            StartTime: start_time_raw,
            EndTime: end_time_raw,
            _id: :"PtIncidentStructure/ValidityPeriod"
          }
        ],
        Planned: planned_raw,
        Summary: summary_raw,
        Description: description_raw,
        InfoLinks: info_links_raw,
        Affects: affects_raw,
        ClearedIncident: cleared_raw,
        IncidentPriority: priority_raw,
        P0Summary: p0_summary_raw
      }) do
    {affects_operators, affects_routes} = extract_affects(affects_raw)

    %__MODULE__{
      creation_time: parse_nullable_time(creation_time_raw),
      last_changed_user: changed_by,
      last_changed_time: parse_nullable_time(changed_date_raw),
      last_changed_toc: changed_by_toc_affiliation,
      participant: participant_ref_raw,
      incident_number: incident_number,
      version: version,
      twitter_hashtag: extract_twitter_hashtag(twitter_hashtag_raw),
      validity_start: parse_nullable_time(start_time_raw),
      validity_end: parse_nullable_time(end_time_raw),
      planned?: planned_raw === true,
      summary: String.trim(summary_raw),
      description: String.trim(description_raw),
      info_links: InfoLink.from_xml(info_links_raw),
      affects_operators: affects_operators,
      affects_routes: affects_routes,
      cleared?: cleared_raw === true,
      priority: priority_raw,
      p0_summary: p0_summary_raw
    }
  end
end
