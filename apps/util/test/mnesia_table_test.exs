defmodule Util.MnesiaTableTest.TableModuleA do
  alias TungstenSwallow.Util.MnesiaTable
  use MnesiaTable

  @table_name :test_table_a

  fields(:a, [:b, :c, :d])
end

defmodule Util.MnesiaTableTest.TableModuleB do
  alias TungstenSwallow.Util.MnesiaTable
  use MnesiaTable

  @table_name :test_table_b

  fields([:a, :b, :c, :d], [:a, :b, :e, :f])
end

defmodule Util.MnesiaTableTest do
  use ExUnit.Case

  doctest TungstenSwallow.Util.MnesiaTable

  alias Util.MnesiaTableTest.{TableModuleA, TableModuleB}

  test "fields in correct order (table A - compound key)" do
    assert TableModuleA.fields() == [:b, :c, :d]
  end

  test "correctly creates struct from record (table A - no compound key)" do
    assert TableModuleA.from_record({:test_table_a, 10, 20, 30, 40}) == %TableModuleA{
             a: 10,
             b: 20,
             c: 30,
             d: 40
           }
  end

  test "correctly creates struct from truncated list record (table A - no compound key)" do
    assert TableModuleA.from_record([10, 20, 30, 40]) == %TableModuleA{a: 10, b: 20, c: 30, d: 40}
  end

  test "fields in correct order (table B - compound key)" do
    assert TableModuleB.fields() == [:a, :b, :e, :f]
  end

  test "correctly creates struct from record (table B - compound key)" do
    assert TableModuleB.from_record({:test_table_b, {10, 20, 30, 40}, 10, 20, 50, 60}) ==
             %TableModuleB{a: 10, b: 20, c: 30, d: 40, e: 50, f: 60}
  end

  test "correctly creates struct from truncated list record (table B - compound key)" do
    assert TableModuleB.from_record([{10, 20, 30, 40}, 10, 20, 50, 60]) == %TableModuleB{
             a: 10,
             b: 20,
             c: 30,
             d: 40,
             e: 50,
             f: 60
           }
  end
end
