defmodule Util.S3Test do
  use ExUnit.Case

  doctest TungstenSwallow.Util.S3

  alias TungstenSwallow.Util.S3

  test "forms modified example canonical request" do
    assert "e155673fa5bcd4b855a77a15b98fce3d10f286f93a203d6d98d2eb51f885f9b7" ==
             S3.get_canonical_request(
               "https://examplebucket.s3.amazonaws.com/test.txt",
               "20130524T000000Z"
             )
             |> S3.sha_256_lc()
  end

  test "forms example string-to-sign" do
    assert "AWS4-HMAC-SHA256\n20130524T000000Z\n20130524/us-east-1/s3/aws4_request\n7344ae5b7ee6c3e7e6b0fe0640412a37625d1fbfff95c48bbb2dc43964946972" ==
             S3.concatenate_string_to_sign(
               "20130524T000000Z",
               S3.scope_only("20130524", "us-east-1", "s3"),
               "7344ae5b7ee6c3e7e6b0fe0640412a37625d1fbfff95c48bbb2dc43964946972"
             )
  end

  test "forms example signature" do
    assert "f0e8bdb87c964420e857bd35b5d6ed310bd44f0170aba48dd91039c6036bdb41" ==
             S3.sign_string_to_sign(
               "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY",
               "20130524",
               "us-east-1",
               "s3",
               "AWS4-HMAC-SHA256\n20130524T000000Z\n20130524/us-east-1/s3/aws4_request\n7344ae5b7ee6c3e7e6b0fe0640412a37625d1fbfff95c48bbb2dc43964946972"
             )
  end
end
