defmodule TungstenSwallow.Util.XmlTools do
  @moduledoc """
  XML tools which are generally useful, not just for a specific feed
  """

  @spec transform_xml(tuple(), map()) :: map()

  def transform_xml(
        {{_, ~c"http://www.w3.org/2001/XMLSchema-instance"}, _},
        _
      ),
      do: nil

  def transform_xml(record, record_map) when is_tuple(record) do
    record_id = elem(record, 0)
    record = Tuple.to_list(record)
    record_names = Map.get(record_map, record_id)

    Map.new(
      Enum.map(Enum.zip([:_id] ++ record_names, record), fn {key, value} ->
        {key, transform_xml(value, record_map)}
      end)
    )
  end

  def transform_xml(record, record_map) when is_list(record) do
    if Enum.all?(record, &Kernel.is_integer/1) do
      List.to_string(record)
    else
      Enum.map(record, fn element -> transform_xml(element, record_map) end)
    end
  end

  def transform_xml(:undefined, _record_map) do
    nil
  end

  def transform_xml(record, _record_map) do
    record
  end

  @spec transform_simple_xml({charlist(), [{charlist(), charlist()}], [charlist() | tuple()]}) ::
          {binary(), map(), list(), binary()}

  def transform_simple_xml({tag_name, attributes, children}) do
    children_elements = children |> Enum.filter(&is_tuple/1)
    children_text = children |> Enum.filter(&is_list/1)

    {List.to_string(tag_name),
     Enum.map(attributes, fn {k, v} -> {:binary.list_to_bin(k), :binary.list_to_bin(v)} end)
     |> Map.new(), Enum.map(children_elements, &transform_simple_xml/1),
     IO.iodata_to_binary(children_text)}
  end

  @spec extract_hrl(binary()) :: map()
  def extract_hrl(path) do
    Record.extract_all(from: path)
    |> Enum.map(fn {key, value} ->
      {key, elem(Enum.unzip(value), 0)}
    end)
    |> Map.new()
  end
end
