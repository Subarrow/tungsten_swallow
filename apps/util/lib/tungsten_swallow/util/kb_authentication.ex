defmodule TungstenSwallow.Util.KBAuthentication do
  @spec login(binary(), binary()) ::
          {:ok, map()} | {:error, :unexpected_response | :hackney, any()}

  def login(username, password) do
    headers = [
      {"Content-Type", "x-www-form-urlencoded"},
      {"Accept", "application/json"}
    ]

    payload = {:form, [{"username", username}, {"password", password}]}

    case :hackney.post("https://opendata.nationalrail.co.uk/authenticate", headers, payload, [
           :with_body
         ]) do
      {:ok, 200, _headers, body} ->
        out = {:ok, _message_body} = Jason.decode(body)
        out

      response = {:ok, _, _headers, _body} ->
        {:error, :unexpected_response, response}

      {:error, hackney_error} ->
        {:error, :hackney, hackney_error}
    end
  end

  @type url :: binary()
  @type token :: binary()

  @spec authorised_get(url(), token()) ::
          {:ok, binary()} | {:error, :unexpected_response | :hackney, any()}

  def authorised_get(url, token) do
    headers = [
      {"X-Auth-Token", token}
    ]

    case :hackney.get(url, headers, ~c"", [:with_body]) do
      {:ok, 200, _headers, body} ->
        {:ok, body}

      response = {:ok, _, _headers, _body} ->
        {:error, :unexpected_response, response}

      {:error, hackney_error} ->
        {:error, :hackney, hackney_error}
    end
  end
end
