defprotocol TungstenSwallow.Util.Linked.LinkedStructs do
  @spec to_linked_map(t :: struct(), any) :: map()

  @doc """
  Converts the struct to a "linked map"
  """

  def to_linked_map(t, u \\ %{})
end
