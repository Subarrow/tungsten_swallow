defmodule TungstenSwallow.Util.S3 do
  @moduledoc """
  Very limited implementation of the authentication needed to use Amazon's S3.

  Note that this only supports S3, cannot sign additional headers, and does not
  support the signatures necessary for chunked file uploads.

  Relevant S3 documentation:
  * https://docs.aws.amazon.com/AmazonS3/latest/userguide/RESTAPI.html
  * https://docs.aws.amazon.com/AmazonS3/latest/API/API_ListObjects.html
  * https://docs.aws.amazon.com/AmazonS3/latest/API/API_GetObject.html

  Relevant signature documentation:
  * https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html
  * https://docs.aws.amazon.com/general/latest/gr/create-signed-request.html
  * https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-auth-using-authorization-header.html
  * https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-authenticating-requests.html

  Useful examples:
  * https://www.aloneguid.uk/posts/2021/02/aws-s3-auth-raw/
  * https://github.com/tedder/requests-aws4auth
  """

  @empty_sha256 "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
  @signed_headers "host;x-amz-content-sha256;x-amz-date"

  @typedoc """
  A simple binary URL containing protocol, host, path, and optionally request
  parameters. Note that request parameters *must* be in alphabetical order.

  Valid examples:
  * `"https://subdomain.example.com/file.txt"`
  * `"https://example.com/?a=1&b=2"`

  Technically valid:
  * `"https://example/"`

  Invalid:
  * `"https://example.com"` - no final `"/"`
  * `"https://example.com/?b=1&a=2"` - request parameters not ordered
  """
  @type url :: binary()

  @typedoc """
  ISO 8601 datetime stripped of hyphens, in UTC format, with no microseconds part

  e.g. `"20130524T000000Z"`
  """
  @type iso8601_datetime_bare :: binary()

  @typedoc """
  ISO 8601 date stripped of hyphens

  (e.g. `"20130524"`)
  """
  @type iso8601_date_bare :: binary()

  @typedoc """
  Lowercase hexadecimal representation of a sha256 hash

  e.g. `"e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"`
  """
  @type sha256hex :: binary()

  @typedoc """
  Amazon access key ID, e.g. `"AKIAIOSFODNN7EXAMPLE"`
  """
  @type amazon_access :: binary()

  @typedoc """
  Amazon access secret, e.g. `"wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"`
  """

  @type amazon_secret :: binary()

  @typedoc """
  A binary formed of <request_date>/<region>/<service>/aws4_request

  * Request date is in the stripped ISO 8601 form (e.g. `"20230207"`)
  * Region is the AWS region (e.g. `"eu-west-1"`)
  * Service is the AWS service (e.g. `"s3"`)

  e.g. `"20230207/eu-west-1/s3/aws4_request"`
  """

  @type amazon_scope :: binary()

  @typedoc """
  A binary formed of `<access>/<request_date>/<region>/<service>/aws4_request`

  * Access is the Amazon access key (e.g. `"AKIAIOSFODNN7EXAMPLE"`)
  * Request date is in the stripped ISO 8601 form (e.g. `"20230207"`)
  * Region is the AWS region (e.g. `"eu-west-1"`)
  * Service is the AWS service (e.g. `"s3"`)

  e.g. `"AKIAIOSFODNN7EXAMPLE/20230207/eu-west-1/s3/aws4_request"`

  """
  @type amazon_credential_scope :: binary()

  @typedoc """
  One of the many services Jeff Bezos offers, all lowercase (e.g. "s3")
  """
  @type amazon_service :: binary()

  @typedoc """
  An AWS region, e.g. eu-west-1, all lowercase
  """
  @type amazon_region :: binary()

  @typedoc """
  A binary formed of `AWS4-HMAC-SHA256\n<request_datetime>\n<scope>\n<hashed_canonical>`

  * AWS4-HMAC-SHA256 is the signature type
  * The request datetime is the stripped UTC ISO 8601 form (e.g. 20230207T105520Z)
  * The scope is a binary structured as detailed in the type for `amazon_scope()`
  * hashed_canonical is the lowercase hex representation of the sha256 checksum for the canonical request

  e.g. `"AWS4-HMAC-SHA256\n20230207T105954Z\n20230207/eu-west-1/s3/aws4_request\n943e39826aa72d05395f0ada97039ef91855c6383b61ebe16b18530cfbbbbdcd"`
  """
  @type amazon_string_to_sign :: binary()

  @typedoc """
  A 'canonical request' is formed of: <method>\n<canonical_uri>\n<canonical_query_string>\n<canonical_headers>\n<signed_headers>\n<body_hash>

  * Method is the HTTP method, cased as it would appear in the request (e.g. `"GET"`)
  * The canonical URI is everything after the host in the URI, but not including query parameters
    * A request for `"https://example.com/file.txt"` would have a canonical URI of `"/file.txt"`
    * A request for `"https://example.com"` should have a canonical URI of `"/"`
    * A request for `"https://example.com/file?foo=1"` should have a canonical URI of `"/file"`
  * The canonical query string is everything after (but not including the ? in a URI). Parameters should be URL-encoded, and keys should be
    alphabetically sorted
    * A request for `"https://example.com/file.txt"` has a query string of `""`
    * A request for `"https://example.com/file.txt?a=1&b=2"` has a query string of `"a=1&b=2"`
  * Canonical headers are a selection of headers for the request. Only host and any headers with keys beginning with `"x-amz-"` are mandatory.
    All header keys are lower-cased, are separated from their values with a colon (with no spacing), and each header (including the final header
    are terminated with a linebreak. All canonical headers are ordered alphabetically by their keys.
  * Signed headers is a semicolon delimited list of the header keys included in the canonical headers (e.g. `"host;x-amz-content-sha256;x-amz-date"`).
    Alphabetical order is again mandatory.
  * Body hash is a hash of the body of the request. If the request has no body, the hash should be
    `"e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"` (sha256 sum for an empty string)

  `"GET\n/darwin.xmltimetable/\n\nhost:s3.eu-west-1.amazonaws.com\nx-amz-content-sha256:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\nx-amz-date:20230207T115110Z\n\nhost;x-amz-content-sha256;x-amz-date\ne3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"`
  """
  @type amazon_canonical_request :: binary()

  @spec credential_scope(amazon_access(), iso8601_date_bare(), amazon_region(), amazon_service()) ::
          amazon_credential_scope()

  def credential_scope(access, date, region, service = "s3") do
    "#{access}/#{date}/#{region}/#{service}/aws4_request"
  end

  @spec scope_only(iso8601_date_bare(), amazon_region(), amazon_service()) :: amazon_scope()

  def scope_only(date, region, service = "s3") do
    "#{date}/#{region}/#{service}/aws4_request"
  end

  @spec bezos_datetime() :: iso8601_datetime_bare()

  def bezos_datetime do
    DateTime.to_iso8601(%{DateTime.utc_now() | microsecond: {0, 0}}, :basic)
  end

  @spec bezos_date() :: iso8601_date_bare()

  def bezos_date do
    DateTime.utc_now() |> Date.to_iso8601(:basic)
  end

  @spec sha_256_lc(binary() | charlist()) :: sha256hex()

  def sha_256_lc(input) do
    :crypto.hash(:sha256, input) |> Base.encode16(case: :lower)
  end

  @spec sha_256_hmac(binary(), binary()) :: binary()

  def sha_256_hmac(key, data) do
    :crypto.mac(:hmac, :sha256, key, data)
  end

  @spec authorised_get(url(), amazon_region(), {amazon_access(), amazon_secret()}) :: any()

  def authorised_get(url, region, {access, secret}) do
    headers = authorise(url, region, {access, secret})
    :hackney.get(url, headers, ~c"", [:with_body])
  end

  @spec decompose_url(url()) :: map()

  def decompose_url(url) do
    %{"host" => _host, "path" => _canonical_uri, "query" => _canonical_query_string} =
      Regex.named_captures(~r{https://(?<host>[^/]*)(?<path>[^?]*)(\?(?<query>.*))?}, url)
  end

  @spec authorise(url(), amazon_region(), {amazon_access(), amazon_secret()}) :: [
          {binary(), binary()}
        ]

  def authorise(url, region, {access, secret}) do
    request_datetime = bezos_datetime()
    request_date = bezos_date()

    credential = credential_scope(access, request_date, region, "s3")

    canonical_request_hash = get_canonical_request(url, request_datetime) |> sha_256_lc()

    string_to_sign_out =
      concatenate_string_to_sign(
        request_datetime,
        scope_only(request_date, region, "s3"),
        canonical_request_hash
      )

    signature = sign_string_to_sign(secret, request_date, region, "s3", string_to_sign_out)

    [
      {"x-amz-content-sha256", @empty_sha256},
      {"x-amz-date", request_datetime},
      {"Authorization",
       "AWS4-HMAC-SHA256 Credential=#{credential},SignedHeaders=#{@signed_headers},Signature=#{signature}"}
    ]
  end

  @spec get_canonical_request(url(), iso8601_datetime_bare()) :: amazon_canonical_request()

  def get_canonical_request(url, request_datetime) do
    %{"host" => host, "path" => canonical_uri, "query" => canonical_query_string} =
      decompose_url(url)

    [
      "GET",
      canonical_uri,
      canonical_query_string,
      [
        "host:#{host}\n",
        "x-amz-content-sha256:#{@empty_sha256}\n",
        "x-amz-date:#{request_datetime}\n"
      ],
      @signed_headers,
      @empty_sha256
    ]
    |> Enum.intersperse("\n")
    |> IO.iodata_to_binary()
  end

  @spec concatenate_string_to_sign(iso8601_datetime_bare(), amazon_scope(), sha256hex()) ::
          amazon_string_to_sign()

  def concatenate_string_to_sign(request_datetime, credential_scope, hashed_canonical) do
    "AWS4-HMAC-SHA256\n#{request_datetime}\n#{credential_scope}\n#{hashed_canonical}"
  end

  @spec sign_string_to_sign(
          amazon_secret(),
          iso8601_date_bare(),
          amazon_region(),
          amazon_service(),
          amazon_string_to_sign()
        ) :: sha256hex()

  def sign_string_to_sign(u_secret, u_date, u_region, u_service = "s3", u_sts) do
    u_signing = "aws4_request"

    ("AWS4" <> u_secret)
    |> sha_256_hmac(u_date)
    |> sha_256_hmac(u_region)
    |> sha_256_hmac(u_service)
    |> sha_256_hmac(u_signing)
    |> sha_256_hmac(u_sts)
    |> Base.encode16(case: :lower)
  end
end
