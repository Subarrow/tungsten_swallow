defmodule TungstenSwallow.Util.Linked do
  def resolve_references(thing, opts) do
    opts = Keyword.validate!(opts, [:prefix, inline: %{}])
    _resolve_references(thing, opts, MapSet.new())
  end

  defp _resolve_references(thing, opts, hierarchy) when is_list(thing) do
    thing |> Enum.map(&_resolve_references(&1, opts, hierarchy))
  end

  defp _resolve_references(thing, opts, hierarchy) when is_map(thing) do
    hierarchy_with_self =
      case thing["@id"] do
        nil -> hierarchy
        {:self, id} -> MapSet.put(hierarchy, id)
      end

    thing
    |> Enum.map(fn
      {key, value} -> {key, _resolve_references(value, opts, hierarchy_with_self)}
    end)
    |> Map.new()
  end

  defp _resolve_references({reference_type, path_list}, opts, hierarchy) do
    inline = Keyword.get(opts, :inline)
    prefix = Keyword.fetch!(opts, :prefix)

    dereferenced_above? = MapSet.member?(hierarchy, path_list)

    case {reference_type, Map.get(inline, path_list), dereferenced_above?} do
      {:reference, _, true} ->
        prefix <> Enum.join(path_list, "/")

      {:reference, nil, false} ->
        prefix <> Enum.join(path_list, "/")

      {:reference, obj, false} ->
        _resolve_references(obj, opts, hierarchy)

      {:inline_or_nil, nil, false} ->
        nil

      {:inline_or_nil, obj, false} ->
        _resolve_references(obj, opts, hierarchy)

      {:self, _, _} ->
        prefix <> Enum.join(path_list, "/")

      {:inline, obj, false} when not is_nil(obj) ->
        _resolve_references(obj, opts, hierarchy)

      _ ->
        raise "Could not resolve #{inspect({reference_type, path_list})}, under: #{inspect(hierarchy)}"
    end
  end

  defp _resolve_references(thing, _opts, _hierarchy) do
    thing
  end

  def extract_id(obj = %{"@id" => {:self, id}}), do: {id, obj}

  def inline(thing) do
    {:inline, thing}
  end

  def ref(thing) do
    {:reference, thing}
  end

  def self_ref(thing) do
    {:self, thing}
  end

  def inline_or_nil(thing) do
    {:inline_or_nil, thing}
  end
end
