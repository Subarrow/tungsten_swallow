defmodule TungstenSwallow.Util.MnesiaTable do
  @moduledoc """
  Macro helpers to define a struct, and its mapping to a mnesia table.
  """

  alias TungstenSwallow.Util.MnesiaTable

  defmacro __using__(_) do
    quote do
      import TungstenSwallow.Util.MnesiaTable, only: [fields: 2]

      @before_compile MnesiaTable
    end
  end

  @doc """

  Declare primary keys and other columns, creates struct.

  Struct keys are taken from primary key and secondary columns, if primary_key
  is a list, all atoms within it will be part of the struct, but will be
  enclosed in a tuple.

  If you're using a compound primary key, note that the names in it match the
  secondary columns.

  So, if you invoke this with ([:a, :b], [:b, :c]), your struct will contain
  keys :a, :b, :c, and will be matched as {:table_name, {a, b}, b, c}.

  """

  @spec fields(
          primary_key :: atom() | nonempty_list(atom()),
          secondary_columns :: nonempty_list(atom())
        ) :: ast :: any()

  defmacro fields(primary_key, secondary_columns) do
    pkey_list = if is_list(primary_key), do: primary_key, else: [primary_key]

    struct_fields = (pkey_list ++ secondary_columns) |> Enum.uniq()

    quote do
      @primary_key unquote(primary_key)
      @primary_key_list unquote(pkey_list)
      @fields unquote(secondary_columns)

      @enforce_keys unquote(struct_fields)
      defstruct unquote(struct_fields)
    end
  end

  @spec primary_key_pattern_quoted(primary_keys :: nonempty_list(atom()), module :: atom()) ::
          ast :: any()

  defp primary_key_pattern_quoted([primary_key], module) do
    Macro.var(primary_key, module)
  end

  defp primary_key_pattern_quoted(primary_key_list, module) do
    pk_as_vars = primary_key_list |> Enum.map(&Macro.var(&1, module))

    quote do
      {unquote_splicing(pk_as_vars)}
    end
  end

  @spec __before_compile__(%{any() => any(), module: atom()}) :: ast :: any()

  @doc false
  defmacro __before_compile__(%{module: module}) do
    if not Module.has_attribute?(module, :fields) do
      raise "#{module}: fields not declared, use macro fields/2 to declare fields"
    end

    if not Module.has_attribute?(module, :table_name) do
      raise "#{module}: attribute @table_name not declared"
    end

    table_name = Module.get_attribute(module, :table_name)

    primary_key_list = Module.get_attribute(module, :primary_key_list)
    fields = Module.get_attribute(module, :fields)

    # For matching in function definitions (doesn't include pkey)
    fields_only_quoted = fields |> Enum.map(&Macro.var(&1, module))

    # For mapping struct values to variables (includes pkey)
    vars_to_fields =
      (primary_key_list ++ fields) |> Enum.uniq() |> Enum.map(&{&1, Macro.var(&1, module)})

    # Entire match (including pkey)
    full_match = [nil | fields] |> Enum.map(fn _ -> :_ end)

    primary_key_quoted = primary_key_pattern_quoted(primary_key_list, module)

    quote do
      def table_name do
        @table_name
      end

      def primary_key do
        @primary_key
      end

      def fields do
        @fields
      end

      def from_record(
            _record =
              {unquote(table_name), unquote(primary_key_quoted),
               unquote_splicing(fields_only_quoted)}
          ) do
        %__MODULE__{
          unquote_splicing(vars_to_fields)
        }
      end

      def from_record(
            _record = [unquote(primary_key_quoted), unquote_splicing(fields_only_quoted)]
          ) do
        %__MODULE__{
          unquote_splicing(vars_to_fields)
        }
      end

      def from_records(records) do
        records |> Enum.map(&from_record/1)
      end

      def store_row(%__MODULE__{unquote_splicing(vars_to_fields)}) do
        :mnesia.write(
          {@table_name, unquote(primary_key_quoted), unquote_splicing(fields_only_quoted)}
        )
      end

      def store_rows(rows) do
        rows |> Enum.map(&store_row/1)
      end

      def delete_row(%__MODULE__{unquote_splicing(vars_to_fields)}) do
        delete_row(unquote(primary_key_quoted))
      end

      def delete_row(primary_key) do
        :mnesia.delete({@table_name, primary_key})
      end

      def delete_rows(rows) do
        rows |> Enum.map(&delete_row/1)
      end

      def match_all do
        :mnesia.match_object({@table_name, unquote_splicing(full_match)}) |> from_records()
      end

      def dirty_match_all do
        :mnesia.dirty_match_object({@table_name, unquote_splicing(full_match)}) |> from_records()
      end

      def read_rows(primary_key) do
        :mnesia.read({@table_name, primary_key}) |> from_records()
      end
    end
  end
end
