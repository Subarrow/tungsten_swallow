defmodule TungstenSwallow.Util.StaticFileDefinition do
  @moduledoc """

  """

  @type version_string :: binary()

  @doc """
  Gets a version string for the given file handle.
  Should work without the pointer needing to be in a specific position
  Pointer must be reset to beginning of file, regardless of where version information may be.
  """
  @callback get_version(file_handle :: IO.device()) :: {:ok, version_string :: version_string()} | {:error, reason :: any()}

  @doc """
  Reads the entire contents of the file
  """
  @callback read!(file_handle :: IO.device(), options :: map()) :: [struct()]
end
