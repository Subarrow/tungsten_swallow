defmodule Manganese.MixProject do
  use Mix.Project

  def project do
    [
      app: :manganese,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [plt_add_apps: [:mix]]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Manganese.Application, []}
    ]
  end

  defp deps do
    [
      {:util, in_umbrella: true},
      {:bplan, in_umbrella: true},
      {:nre_feeds, in_umbrella: true},
      {:manganese_swallow_orm,
       git: "https://codeberg.org/Subarrow/manganese_swallow_orm_ex.git",
       tag: "0fbf33cc5fba0a38e6727761347e4c1f4db01e1b"},
      {:ecto_sql, "~> 3.10.1"},
      {:postgrex, "~> 0.17.1"},
      {:flow, "~> 1.2.4"}
    ]
  end
end
