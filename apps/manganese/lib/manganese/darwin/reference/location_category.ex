defmodule Manganese.Darwin.Reference.LocationCategory do
  @moduledoc """
  Categorising locations doesn't have to be easy or fun, and it's not!

  Note that this is extremely patchy with only Darwin data, you really need BPLAN for this
  to work well.

  There's a lot of cues you might think are useful here that really aren't, in particular, in Darwin names,
  there's something of a tendency for locations to end up with "(Bus)" in their name after they're deemed to have
  outlived their usefulness, even things which weren't bus stops in the first place, or which are still in use,
  albeit objects which were never part of the mainline rail network (Some Metrolink and T&W stations have fallen
  victim to this). Same goes for the ZB pseudo-operator.


  Initial logic and category letters are based on prior work in the unfortunate IronSwallow
  https://codeberg.org/Subarrow/IronSwallow/src/branch/master/ironswallow/store/reference/category.py

  * D - Siding (si*d*ing) - a place set aside for the purpose of storage of rolling stock, without any substantial maintenance/cleaning facilities
  * F - Ferry terminal
  * G - Ground frame
  * I - Signal (s*i*gnal)
  * J - Junction
  * L - Loop
  * M - Station (off-network, e.g. North Yorkshire Moors railway, LT, TW metro)
  * O - Tunnel (O because... it looks a bit like a tunnel mouth)
  * Q - Freight reception (Q is a reference to schedule category for "runs as required")
  * R - Level crossing (c*r*ossing)
  * S - Station (mainline)
  * T - Depot (depo*t* or alternatively the first letter of T&RSMD/TMD), applies to any location set aside for maintenance (including light maintenance) and cleaning of rolling stock
  * U - Station yet to open
  * V - Viaduct
  * X - Crossover (X is a cross, merry xovermas)
  * Z - Defunct - Physical location no longer exists, has lost semantic distinction, or has been in BPLAN for a long time and has no network links
  """
  alias NREFeeds.Darwin.Schema.Reference.Location
  alias ManganeseORM.Table.BPLANLocation

  @force_category %{
    # For some inexplicable reason Westerton Sig YH350 has a CRS assigned in Darwin
    "WEST530" => "I",
    # Yoker Stabling & Cleaning - I guess this makes a bit more sense but still not really
    "YOKERCS" => "T",
    # Eastleigh TRSMD
    "ELGHTMD" => "T",
    # Ilford EMUD,
    "ILFEMUD" => "T",
    # Selhurst TRSMD
    "SLHRSTD" => "T",
    # Tilgate Sidings Entry/exit
    "THBDTGE" => "D",
    # Sinfin North (opened 1976, rail service withdrawn 1993, replacement taxi withdrawn 1998, land disposed 2017, despite this is called "Private Charter" in Darwin)
    "SINFINN" => "Z",
    # Llantrisant - former name of Pontyclun, rebuilt in 1992
    "LLTRSNT" => "Z",
    # Old Oak Common HSTD, closed 2018
    "OLDOHST" => "Z",
    # Godley East
    "GODLEYE" => "Z",
    # West Street (Glasgow Subway), unroutable
    "WSTSTRT" => "M",
    # York Siemens Trans Systems
    "YORKTPE" => "T",
    # Leeds (Whitehall) - Temporary station put in place by RT from 1999 to 2002 as a replacement for Leeds for some early morning/late night and local services during works
    "WHRDWRD" => "Z",
    #######################
    # LT no operator
    #######################
    "NTWD" => "M",
    "PADTLUL" => "M",
    "MORGTLT" => "M",

    #######################
    # Manchester metrolink unroutable
    #######################
    "BRKLNDS" => "M",
    "BSTBARN" => "M",
    "CRUMPSL" => "M",
    "MNCRANC" => "M",
    "MNCRBWY" => "M",
    "MNCRCBK" => "M",
    "MNCRECC" => "M",
    "MNCREQY" => "M",
    "MNCRHBC" => "M",
    "MNCRLDW" => "M",
    "MNCRLWY" => "M",
    "MNCRPOM" => "M",
    "MNCRWST" => "M",
    "MNCRSQY" => "M",
    "TIMPRLY" => "M",
    "BOWKERV" => "M",
    "DNRD" => "M",
    "HTPK" => "M",
    "WHFD" => "M",
    #######################
    # Manchester metrolink defunct
    #######################
    "WDLNDSR" => "Z",
    #######################
    # LT unroutable
    #######################
    "BECNTRE" => "M",
    "ALDGTLT" => "M",
    "ROYALO" => "M",
    "WRKALT" => "M",
    "MDVLLT" => "M",
    "MOORPK" => "M",
    "BNDSLUL" => "M",
    "ACTONTN" => "M",
    "WATFLUL" => "M",
    "SWIMBDN" => "M",
    #######################
    # Nottingham tram unroutable
    #######################
    "NTNGPKK" => "M",
    #######################
    # Heritage
    #######################
    # Grosmont (North Yorkshire Moors Railway)
    "GROSNYM" => "M",
    # Rhiwfron (Rheilffordd Cwm Rheidol - Vale of Rheidol Railway)
    "RHWF" => "M",
    # Nantyronen (Rheilffordd Cwm Rheidol)
    "NNTYRNN" => "M",
    # Aberffrwd (Rheilffordd Cwm Rheidol)
    "ABRFRWD" => "M",
    # Rheidol Falls (Rheilffordd Cwm Rheidol)
    "RHDLFLS" => "M",
    # Devil's Bridge (Rheilffordd Cwm Rheidol)
    "DEVLSBG" => "M",
    # Capel Bangor (Rheilffordd Cwm Rheidol)
    "CAPLBAN" => "M",
    # Glanrafon (Rheilffordd Cwm Rheidol)
    "GLANRFN" => "M",
    # Dufftown (Keith & Dufftown)
    "DUFTOWN" => "M",
    # Shottle (Ecclesbourne Valley Railway)
    "SHOTTLE" => "M",
    # Churston (T&D)
    "CHUSTON" => "M"
  }

  @unopened_stations [
    # Pill station (work commences 2024, same project as Portishead)
    "PILL",
    # Portishead station (work commences 2024, same project as Pill)
    "PRTSHD",
    # Elland station (work commences 2024, expected reopening 2025)
    "ELLAND",
    # White rose station (2023, delayed)
    "WHTRSE",
    # Newsham (Northumberland Line project, 2024)
    "NSHM",
    # Seaton Delaval (Northumberland Line project, 2024)
    "STDL",
    # Northumberland Park NE (Northumberland Line project, 2024)
    "NBRLPK",
    # Blyth Bebside (Northumberland Line project, 2024)
    "BYBEB",
    # Bedlington (Northumberland Line project, 2024)
    "BDLNTN",
    # Ashington (Northumberland Line project, 2024)
    "ASHGTN",
    # Cambridge South (construction started 2023, completion expected 2025)
    "CAMBSTH",
    # Chelmsford Beaulieu Park (construction start exp 2023, opening 2025)
    "CHLMBPK"
  ]

  def get_category(
        _location = %Location{crs: crs, tiploc: tiploc, name: name, operator: operator},
        nil,
        []
      ) do
    cond do
      # Category is forced
      Map.has_key?(@force_category, tiploc) ->
        Map.get(@force_category, tiploc)

      # Only pure ferry terminals are ZF
      operator == "ZF" ->
        "F"

      # T&W metro, Sheffield tram, North Yorkshire Moors, West Somerset, Private Charter, Swanage
      operator in ["TW", "SJ", "NY", "ZM", "PC", "SP"] ->
        "M"

      not is_nil(crs) and not is_nil(name) and not is_nil(operator) and
          operator not in ["ZB", "LT"] ->
        "S"

      # Any tiploc which ends with three numbers is a signal
      Regex.match?(~r/\w{4}\d{3}/, tiploc) ->
        "I"

      # Buses! Some actual bus stops created for actual buses actually end in "BUS"
      Regex.match?(~r/BUS$/, tiploc) and operator == "ZB" ->
        "B"

      # This is done down here so that an "unopened" station is flicked to mainline when enough info is filled in with ref data
      tiploc in @unopened_stations ->
        "U"

      true ->
        nil
    end
  end

  def get_category(
        location = %Location{
          crs: _darwin_crs,
          tiploc: tiploc,
          name: darwin_name,
          operator: _operator
        },
        %BPLANLocation{tiploc: tiploc, name: bplan_name, start_date: start_date},
        network_links
      ) do
    darwin_only_cat = get_category(location, nil, [])
    bplan_name_stripped = bplan_name |> String.upcase() |> String.replace(~r/[&.]/, "")

    cond do
      # Darwin-only location categorisations should always be correct, so defer to them
      not is_nil(darwin_only_cat) ->
        darwin_only_cat

      # Signals
      Regex.match?(~r/(SIGNAL \d+|SIG \d+)$/, bplan_name_stripped) ->
        "I"

      # Unroutable, no name in Darwin, and has the earliest possible start date in BPLAN
      length(network_links) == 0 and is_nil(darwin_name) and start_date == ~D[1995-01-01] ->
        "Z"

      # T&RSMD/TRSMD - "Traction (and) Rolling Stock Maintenance Depot
      # TMD - "Traction Maintenance Depot"
      # EMUD - "Electric Multiple Unit Depot"
      # DMUD - "Diesel Multiple Unit Depot"
      Regex.match?(~r/(TRSMD|TMD|EMUD|DMUD)$/, bplan_name_stripped) ->
        "T"

      # All abbreviations for "Junction"
      # Some stations are named "junction", checking that darwin name is nil is a hedge against this somehow causing problems (no I don't know how, shh)
      Regex.match?(~r/(JN|JCN|JCT|JUNCTION)$/, bplan_name_stripped) and is_nil(darwin_name) ->
        "J"

      # Loop
      Regex.match?(~r/LOOP$/, bplan_name_stripped) ->
        "L"

      # Crossovers
      Regex.match?(~r/(CROSSOVERS?|XOVER)$/, bplan_name_stripped) ->
        "X"

      # Viaducts
      Regex.match?(~r/VIADUCT$/, bplan_name_stripped) ->
        "V"

      # Tunnel
      Regex.match?(~r/TUNNEL$/, bplan_name_stripped) ->
        "O"

      # LC/L Xing/Level Crossing - "Level Crossing"
      # AHB - "Automatic Half Barriers"
      Regex.match?(~r/(LC|L XING|LEVEL CROSSING|LEVEL XG|AHB)$/, bplan_name_stripped) ->
        "R"

      # Sdg/sdgs/sidings - "sidings"
      Regex.match?(~r/(SDG|SDGS|SIDINGS?)$/, bplan_name_stripped) ->
        "D"

      true ->
        nil
    end
  end
end
