defmodule Manganese.Darwin.SnapshotSink do
  use GenStage

  alias Manganese.Darwin.Store

  def start_link(args) do
    GenStage.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(state) do
    {:consumer, state, subscribe_to: [Manganese.Darwin.SnapshotParser]}
  end

  def handle_events(events, _from, state) do
    Store.store_realtime_messages(events)

    {:noreply, [], state}
  end
end
