defmodule Manganese.Darwin.Diff do
  require Logger

  use GenServer

  alias NREFeeds.Darwin.Schema.RealTime

  import Manganese.Util.DiffUtil

  # Anything where all are replaced at once
  @straight_compare [
    :darwin_association,
    :darwin_formation_loading,
    :darwin_formation,
    :darwin_schedule,
    :darwin_schedule_location,
    :darwin_station_message,
    :darwin_station_message_station,
    :darwin_train_alert,
    :darwin_train_alert_service_station,
    :darwin_train_order,
    :darwin_train_status
  ]

  # Anything where only some are replaced at once
  @wonky_compare [
    :darwin_train_status_location,
    :darwin_time
  ]

  @pk_map %{
    darwin_time: :time_id,
    darwin_association: [:main_fq_wt_match, :assoc_fq_wt_match],
    darwin_formation_loading: :fq_wt_match,
    darwin_formation: :formation_id,
    darwin_schedule: :rid,
    darwin_schedule_location: :fq_wt_match,
    darwin_station_message: :message_id,
    darwin_station_message_station: [:message_id, :crs],
    darwin_train_alert: :alert_id,
    darwin_train_alert_service_station: [:alert_id, :crs, :rid],
    darwin_train_order: [:crs, :platform],
    darwin_train_status: :rid,
    darwin_train_status_location: :fq_wt_match
  }
  @pk_map_types Map.keys(@pk_map)

  def start_link(link_opts) do
    GenServer.start_link(__MODULE__, nil, link_opts)
  end

  def init(_) do
    {:ok, %{}}
  end

  def handle_cast({rt = %RealTime{}, differences}, state) do
    new_differences = Enum.map(differences, &diff/1) |> List.flatten()

    GenServer.cast(Manganese.Darwin.Distributor, {strip_real_time(rt), new_differences})

    {:noreply, state}
  end

  @spec diff({atom(), [{atom(), list()}], map() | nil}) :: list()

  def diff({_type, _, nil}) do
    []
  end

  def diff({_type, prefetched, new_thing}) do
    prefetched
    |> Enum.map(fn
      {prefetched_key, prefetched_values} ->
        case Map.fetch!(new_thing, prefetched_key) do
          {0, []} ->
            []

          {_n, new_values} ->
            compare(prefetched_key, strip_all(prefetched_values), strip_all(new_values))
        end
    end)
    |> List.flatten()
  end

  @spec compare(atom(), [map()], [map()]) :: list()

  def compare(type, prefetched_values, new_values)
      when type in @pk_map_types do
    prefetched = list_to_map(type, prefetched_values)
    new = list_to_map(type, new_values)

    prefetched_keys = Map.keys(prefetched)
    new_keys = Map.keys(new)

    created_keys = new_keys -- prefetched_keys
    deleted_keys = prefetched_keys -- new_keys
    updated_keys = (prefetched_keys -- created_keys) -- deleted_keys

    created = created_keys |> Enum.map(&{type, :create, &1, Map.get(new, &1), %{}})

    deleted =
      cond do
        type in @straight_compare ->
          deleted_keys |> Enum.map(&{type, :delete, &1, Map.fetch!(prefetched, &1), %{}})

        type in @wonky_compare ->
          []
      end

    updated =
      updated_keys
      |> Enum.map(
        &{type, :update, &1, Map.fetch!(prefetched, &1),
         map_diff(Map.fetch!(prefetched, &1), Map.fetch!(new, &1))}
      )
      |> Enum.reject(fn
        {_type, :update, _pk, _original, difference} when map_size(difference) == 0 -> true
        _ -> false
      end)

    [created, updated, deleted]
  end

  def compare(type, _, _) do
    Logger.warning("Couldn't compare rows for type #{type}")
    []
  end

  @spec strip_all([map()] | nil) :: list()

  def strip_all(nil) do
    []
  end

  def strip_all(maps) do
    maps |> Enum.map(&strip_one/1)
  end

  @spec strip_one(map()) :: map()

  def strip_one(a_map) do
    a_map
    |> Map.reject(fn {key, value} ->
      key in [
        :source,
        :cis_source,
        :darwin_request_id,
        :cis_source,
        :inserted_at,
        :darwin_ts,
        :updated_at,
        :__struct__,
        :__meta__
      ] or is_struct(value, Ecto.Association.NotLoaded)
    end)
  end

  @spec list_to_map(atom(), list()) :: map()

  def list_to_map(type, list) do
    pks = Map.fetch!(@pk_map, type)

    list |> Map.new(fn row -> {fetch_multi!(row, pks), row} end)
  end

  def strip_real_time(%RealTime{
        source: source,
        source_instance: source_instance,
        request_id: request_id,
        timestamp_darwin: darwin_ts
      }) do
    %{
      source: source,
      cis_source: source_instance,
      darwin_request_id: request_id,
      darwin_ts: darwin_ts
    }
  end
end
