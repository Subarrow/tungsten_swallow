defmodule Manganese.Darwin.Prefetch do
  import Ecto.Query, only: [from: 2]

  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  alias Manganese.Util.StoreUtil

  def prefetch(%RealTime.Association{
        tiploc: tiploc,
        category: _category,
        cancelled?: _cancelled?,
        deleted?: _deleted?,
        main: %RealTime.Misc.ServiceRIDWithCircularTimes{rid: main_rid, wt_match: main_wt_match},
        associated: %RealTime.Misc.ServiceRIDWithCircularTimes{
          rid: assoc_rid,
          wt_match: assoc_wt_match
        }
      }) do
    main_fq_wt_match = StoreUtil.fq_wt_match(main_rid, tiploc, main_wt_match)
    assoc_fq_wt_match = StoreUtil.fq_wt_match(assoc_rid, tiploc, assoc_wt_match)

    [
      darwin_association:
        from(da in Table.DarwinAssociation,
          where:
            da.main_fq_wt_match == ^main_fq_wt_match and
              da.assoc_fq_wt_match == ^assoc_fq_wt_match
        )
        |> Repo.all()
    ]
  end

  def prefetch(%RealTime.FormationLoading{tiploc: tiploc, rid: rid, wt_match: wt_match}) do
    [
      darwin_formation_loading:
        from(fl in Table.DarwinFormationLoading,
          where: fl.fq_wt_match == ^StoreUtil.fq_wt_match(rid, tiploc, wt_match)
        )
        |> Repo.all()
    ]
  end

  def prefetch(%RealTime.ScheduleFormations{rid: rid}) do
    [
      darwin_formation: from(sf in Table.DarwinFormation, where: sf.rid == ^rid) |> Repo.all()
    ]
  end

  def prefetch(%RealTime.Schedule{rid: rid}) do
    [
      darwin_time: Repo.all(from(dt in Table.DarwinTime, where: dt.rid == ^rid)),
      darwin_schedule_location:
        Repo.all(
          from(dsl in Table.DarwinScheduleLocation,
            where: dsl.rid == ^rid,
            order_by: {:asc, dsl.index}
          )
        ),
      darwin_schedule: Repo.all(from(ds in Table.DarwinSchedule, where: ds.rid == ^rid))
    ]
  end

  def prefetch(%RealTime.StationMessage{message_id: message_id}) do
    [
      darwin_station_message_station:
        from(sms in Table.DarwinStationMessageStation, where: sms.message_id == ^message_id)
        |> Repo.all(),
      darwin_station_message:
        from(sm in Table.DarwinStationMessage, where: sm.message_id == ^message_id)
        |> Repo.all()
    ]
  end

  def prefetch(%RealTime.TrainAlert{alert_id: alert_id}) do
    [
      darwin_train_alert_service_station:
        from(tass in Table.DarwinTrainAlertServiceStation, where: tass.alert_id == ^alert_id)
        |> Repo.all(),
      darwin_train_alert:
        from(ta in Table.DarwinTrainAlert, where: ta.alert_id == ^alert_id)
        |> Repo.all()
    ]
  end

  def prefetch(%RealTime.TrainOrder{crs: crs, platform: platform}) do
    [
      darwin_train_order:
        from(to in Table.DarwinTrainOrder, where: to.crs == ^crs and to.platform == ^platform)
        |> Repo.all()
    ]
  end

  def prefetch(%RealTime.TrainStatus{rid: rid}) do
    [
      darwin_time: Repo.all(from(dt in Table.DarwinTime, where: dt.rid == ^rid)),
      darwin_train_status_location:
        Repo.all(from(dtsl in Table.DarwinTrainStatusLocation, where: dtsl.rid == ^rid)),
      darwin_train_status: Repo.all(from(dts in Table.DarwinTrainStatus, where: dts.rid == ^rid))
    ]
  end
end
