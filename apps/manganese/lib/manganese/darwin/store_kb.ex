defmodule Manganese.Darwin.StoreKB do
  @moduledoc """
  Polls for, retrieves, and stores KB reference data
  """

  @poll_interval 3_600_000

  use GenServer

  alias NREFeeds.Promotions.Schema.Promotion
  alias NREFeeds.Promotions.Util.NREPromotionsXML
  alias NREFeeds.Incidents.Util.NREIncidentsXML
  alias TungstenSwallow.Util.KBAuthentication

  alias ManganeseORM.Table

  alias Manganese.Repo

  def init(
        state = %{
          schema_info: schema_info,
          kb_credentials: kb_credentials
        }
      ) do
    fetch_and_store(schema_info, kb_credentials)

    Process.send_after(self(), :poll, @poll_interval)

    {:ok, state}
  end

  def start_link(
        kb_credentials: kb_credentials,
        link_opts: link_opts
      ) do
    schema_info = %{
      promotions:
        {NREPromotionsXML.load_schema_model!(),
         NREPromotionsXML.load_promotions_record_definitions!()},
      incidents: {
        NREIncidentsXML.load_schema_model!(),
        NREIncidentsXML.load_incidents_record_definitions!()
      }
    }

    GenServer.start_link(
      __MODULE__,
      %{
        schema_info: schema_info,
        kb_credentials: kb_credentials
      },
      link_opts
    )
  end

  def handle_info(
        :poll,
        state = %{
          schema_info: schema_info,
          kb_credentials: kb_credentials
        }
      ) do
    fetch_and_store(schema_info, kb_credentials)

    Process.send_after(self(), :poll, @poll_interval)
    {:noreply, state}
  end

  def fetch_and_store(schema_info, kb_credentials) do
    {user, pass} = kb_credentials

    {:ok,
     %{
       "roles" => %{
         "ROLE_KB_API" => true
       },
       "token" => token
     }} = KBAuthentication.login(user, pass)

    {:ok, file_contents} =
      KBAuthentication.authorised_get(
        "https://opendata.nationalrail.co.uk/api/staticfeeds/4.0/promotions-publics",
        token
      )

    {schema_model, schema_record_definitions} = schema_info.promotions

    {:ok, refdata_parsed} =
      NREPromotionsXML.parse_binary(file_contents, schema_model, schema_record_definitions)

    Promotion.from_xml(refdata_parsed) |> store_promotions()
  end

  def extract_link_details(thing) do
    case thing do
      %{
        uris: urls,
        details: description
      } ->
        {urls, description}

      _ ->
        {nil, nil}
    end
  end

  def store_promotions(promotions) do
    now = DateTime.utc_now() |> DateTime.truncate(:second)

    promotions
    |> Enum.map(fn %Promotion{
                     promotion_id: promotion_id,
                     type: type,
                     name: name,
                     summary: summary,
                     offer_details: offer_details,
                     further_information: further_information,
                     validity_start: validity_start,
                     validity_end: validity_end,
                     ticket_name: ticket_name,
                     promotion_code: promotion_code,
                     lead_operator_code: lead_operator_code,
                     applicable_operators: applicable_operators,
                     nearest_stations: nearest_stations,
                     applicable_origins: applicable_origins,
                     applicable_destinations: applicable_destinations,
                     applicable_zone: applicable_zone
                   } ->
      {further_information_urls, further_information_description} =
        extract_link_details(further_information)

      {offer_urls, offer_description} = extract_link_details(offer_details)

      applicable_operators = if applicable_operators == :all, do: nil, else: applicable_operators

      %{
        promotion_id: promotion_id,
        type: type,
        name: name,
        summary: summary,
        offer_urls: offer_urls,
        offer_description: offer_description,
        further_information_urls: further_information_urls,
        further_information_description: further_information_description,
        validity_start: validity_start,
        validity_end: validity_end,
        ticket_name: ticket_name,
        promotion_code: promotion_code,
        lead_operator_code: lead_operator_code,
        applicable_operators: applicable_operators,
        nearest_stations: nearest_stations,
        applicable_origins: applicable_origins,
        applicable_destinations: applicable_destinations,
        applicable_zone: applicable_zone,
        inserted_at: now,
        updated_at: now
      }
    end)
    |> Enum.chunk_every(2500)
    |> Enum.each(fn batch ->
      {_count, _new_rows} =
        Repo.insert_all(Table.KBPromotion, batch,
          on_conflict: {:replace_all_except, [:inserted_at]},
          conflict_target: :promotion_id,
          returning: true
        )
    end)
  end
end
