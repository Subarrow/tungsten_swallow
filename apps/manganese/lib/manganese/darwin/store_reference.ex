defmodule Manganese.Darwin.StoreReference do
  @moduledoc """
  Polls for, retrieves, and stores reference data
  """

  @poll_interval 3_600_000

  @zone_promotion_codes [
    # Greater manchester (TfGM)
    "FCW",
    "GDP",
    "T7F",
    # Liverpool (Merseytravel)
    "UCY",
    # S Yorks (Sheffield area) (Travel South Yorkshire)
    "S9Z",
    "HOY",
    # Tyne and Wear (Nexus)
    "MDD",
    # West Midlands (TfWM)
    "WM1",
    # West Yorks (Leeds area) (WY Metro)
    "YX0",
    "DWY",
    "DWT",
    # Glasgow region (Strathclyde Partnership for Transport)
    "DSX",
    "GPD"
  ]

  use GenServer

  import Ecto.Query, only: [from: 2]

  alias BPLAN
  alias BPLAN.Data.{Category, Status}

  alias NREFeeds.Darwin
  alias Darwin.Data.{LocationNames, LocationTimezones}
  alias Darwin.Schema.Reference
  alias Darwin.Util.{DarwinS3, DarwinXml}

  alias Manganese.Darwin.Reference.LocationCategory

  alias ManganeseORM.Table
  alias Manganese.Repo

  def init(
        state = %{
          schema_record_definitions: schema_record_definitions,
          schema_model: schema_model,
          s3_credentials: s3_credentials
        }
      ) do
    fetch_and_store(schema_record_definitions, schema_model, s3_credentials)

    Process.send_after(self(), :poll, @poll_interval)

    {:ok, state}
  end

  def start_link(
        schema_record_definitions: schema_record_definitions,
        schema_model: schema_model,
        s3_credentials: s3_credentials,
        link_opts: link_opts
      ) do
    GenServer.start_link(
      __MODULE__,
      %{
        schema_record_definitions: schema_record_definitions,
        schema_model: schema_model,
        s3_credentials: s3_credentials
      },
      link_opts
    )
  end

  def handle_info(
        :poll,
        state = %{
          schema_record_definitions: schema_record_definitions,
          schema_model: schema_model,
          s3_credentials: s3_credentials
        }
      ) do
    fetch_and_store(schema_record_definitions, schema_model, s3_credentials)

    Process.send_after(self(), :poll, @poll_interval)
    {:noreply, state}
  end

  def fetch_and_store(schema_record_definitions, schema_model, s3_credentials) do
    store_bplan()

    {:ok, file_contents} = DarwinS3.get_latest_reference_file(s3_credentials)

    {:ok, refdata_parsed} =
      DarwinXml.parse_darwin_binary(file_contents, schema_model, schema_record_definitions)

    reference = Reference.from_xml(refdata_parsed)

    reference.locations |> store_locations()
    reference.cancel_reasons |> Enum.map(&store(:cancel, &1))
    reference.delay_reasons |> Enum.map(&store(:delay, &1))
    reference.cis_sources |> Enum.map(&store/1)
    reference.operators |> Enum.map(&store/1)
  end

  def store_locations(locations) do
    now = DateTime.utc_now() |> DateTime.truncate(:second)
    timezones = LocationTimezones.location_timezones()

    # This could be done more simply with a subquery in theory, but I can't quite work out how with ecto
    # update darwin_location set zones=array(select promotion_code from kb_promotion where applicable_zone @> ARRAY[crs]);
    pte_zones =
      Repo.all(
        from(kbp in Table.KBPromotion,
          where: not is_nil(kbp.applicable_zone) and kbp.promotion_code in @zone_promotion_codes,
          select: [kbp.promotion_code, kbp.applicable_zone]
        )
      )
      |> Enum.map(fn [promotion_code, applicable_zone] ->
        Enum.map(applicable_zone, &{&1, [promotion_code]}) |> Map.new()
      end)
      |> Enum.reduce(fn element, acc -> Map.merge(element, acc, fn _k, v1, v2 -> v1 ++ v2 end) end)

    locations
    |> Enum.map(
      fn reference_location = %Reference.Location{
           tiploc: tiploc,
           crs: crs,
           operator: operator,
           name: name
         } ->
        time_zone = Map.get(timezones, tiploc, "Europe/London")
        _localised_names = Map.get(LocationNames.location_names(), tiploc, %{})

        %{
          tiploc: tiploc,
          crs: crs,
          operator: operator,
          name: name,
          category:
            LocationCategory.get_category(
              reference_location,
              Repo.one(from(bpl in Table.BPLANLocation, where: bpl.tiploc == ^tiploc)),
              Repo.all(
                from(bpl in Table.BPLANNetworkLink,
                  where: bpl.origin_tiploc == ^tiploc or bpl.destination_tiploc == ^tiploc
                )
              )
            ),
          time_zone: time_zone,
          mandatory:
            from(bpl in Table.BPLANLocation,
              where: bpl.tiploc == ^tiploc and bpl.timing_point_type != "optional",
              select: bpl.timing_point_type
            ),
          bplan_name:
            from(bpl in Table.BPLANLocation, where: bpl.tiploc == ^tiploc, select: bpl.name),
          zones: Map.get(pte_zones, crs, []),
          inserted_at: now,
          updated_at: now
        }
      end
    )
    |> Enum.chunk_every(2500)
    |> Enum.each(fn batch ->
      {_count, _new_rows} =
        Repo.insert_all(Table.DarwinLocation, batch,
          on_conflict: {:replace_all_except, [:inserted_at]},
          conflict_target: :tiploc,
          returning: true
        )
    end)
  end

  def store(%Reference.CISSource{code: code, name: name}) do
    now = DateTime.utc_now()

    {:ok, _new_row} =
      Repo.insert(
        Table.DarwinCISSource.changeset_full(%Table.DarwinCISSource{}, %{
          cis_source: code,
          name: name,
          inserted_at: now,
          updated_at: now
        }),
        on_conflict: {:replace_all_except, [:inserted_at]},
        conflict_target: :cis_source,
        returning: true
      )
  end

  def store(%Reference.Operator{operator: operator, name: name, url: url}) do
    now = DateTime.utc_now()

    {:ok, _new_row} =
      Repo.insert(
        Table.DarwinOperator.changeset_full(%Table.DarwinOperator{}, %{
          operator_code: operator,
          name: name,
          url: url,
          inserted_at: now,
          updated_at: now
        }),
        on_conflict: {:replace_all_except, [:inserted_at]},
        conflict_target: :operator_code,
        returning: true
      )
  end

  def store(type, %Reference.DisruptionReason{code: code_raw, reason_text: reason_text}) do
    now = DateTime.utc_now()

    code =
      case type do
        :cancel -> "C" <> code_raw
        :delay -> "D" <> code_raw
      end

    {:ok, _new_row} =
      Repo.insert(
        Table.DarwinDisruptionReason.changeset_full(%Table.DarwinDisruptionReason{}, %{
          reason_code: code,
          text: reason_text,
          inserted_at: now,
          updated_at: now
        }),
        on_conflict: {:replace_all_except, [:inserted_at]},
        conflict_target: :reason_code,
        returning: true
      )
  end

  def store_bplan do
    Category.get() |> Enum.map(&store_bplan/1)
    Status.get() |> Enum.map(&store_bplan/1)
  end

  def store_bplan(%Category{code: code, description: text}) do
    now = DateTime.utc_now()

    {:ok, _new_row} =
      Repo.insert(
        Table.BPLANCategory.changeset_full(%Table.BPLANCategory{}, %{
          category_code: code,
          text: text,
          inserted_at: now,
          updated_at: now
        }),
        on_conflict: {:replace_all_except, [:inserted_at]},
        conflict_target: :category_code,
        returning: true
      )
  end

  def store_bplan(%BPLAN.Data.Status{code: code, description: text}) do
    now = DateTime.utc_now()

    {:ok, _new_row} =
      Repo.insert(
        Table.BPLANStatus.changeset_full(%Table.BPLANStatus{}, %{
          status_code: code,
          text: text,
          inserted_at: now,
          updated_at: now
        }),
        on_conflict: {:replace_all_except, [:inserted_at]},
        conflict_target: :status_code,
        returning: true
      )
  end
end
