defmodule Manganese.Darwin.Store.Association do
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  import Manganese.Util.StoreUtil

  require Logger

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.Association{
          tiploc: tiploc,
          category: category,
          cancelled?: cancelled?,
          deleted?: deleted?,
          main: %RealTime.Misc.ServiceRIDWithCircularTimes{rid: main_rid, wt_match: main_wt_match},
          associated: %RealTime.Misc.ServiceRIDWithCircularTimes{
            rid: assoc_rid,
            wt_match: assoc_wt_match
          }
        }
      ) do
    if schedule_exists?(main_rid) and schedule_exists?(assoc_rid) do
      main_sched_locs =
        Table.DarwinScheduleLocation.schedule_locations_by_fq_wt_match(Repo, main_rid)

      assoc_sched_locs =
        Table.DarwinScheduleLocation.schedule_locations_by_fq_wt_match(Repo, assoc_rid)

      main_fq_wt_match = fq_wt_match(main_rid, tiploc, main_wt_match)
      assoc_fq_wt_match = fq_wt_match(assoc_rid, tiploc, assoc_wt_match)

      unless main_sched_locs == %{} or assoc_sched_locs == %{} do
        if Map.has_key?(main_sched_locs, main_fq_wt_match) and
             Map.has_key?(assoc_sched_locs, assoc_fq_wt_match) do
          association_res =
            Repo.insert_all(
              Table.DarwinAssociation,
              [
                %{
                  main_fq_wt_match: main_fq_wt_match,
                  assoc_fq_wt_match: assoc_fq_wt_match,
                  main_rid: main_rid,
                  assoc_rid: assoc_rid,
                  tiploc: tiploc,
                  category: category,
                  is_cancelled: cancelled?,
                  is_deleted: deleted?,
                  inserted_at: now,
                  updated_at: now,
                  source: source,
                  cis_source: source_instance,
                  darwin_request_id: request_id,
                  darwin_ts: darwin_ts
                }
              ],
              on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinAssociation),
              conflict_target: [:main_fq_wt_match, :assoc_fq_wt_match, :tiploc],
              returning: true
            )

          %{
            darwin_association: association_res
          }
        else
          Logger.warning(
            "Schedules #{main_rid} (main) and #{assoc_rid} (assoc) exist, but either/both #{main_fq_wt_match} (m) or #{assoc_fq_wt_match} (a) do not (c. #{category}, canc. #{cancelled?}, del. #{deleted?})"
          )

          nil
        end
      end
    end
  end
end
