defmodule Manganese.Darwin.Store.TrainOrder do
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.TrainOrder{
          crs: crs,
          platform: platform,
          trains: trains_raw
        }
      ) do
    trains =
      trains_raw
      |> Enum.map(fn
        %RealTime.Misc.ServiceRIDWithCircularTimes{rid: rid} -> rid
        headcode -> headcode
      end)

    train_order_res =
      Repo.insert_all(
        Table.DarwinTrainOrder,
        [
          %{
            crs: crs,
            platform: platform,
            trains: trains,
            source: source,
            cis_source: source_instance,
            darwin_request_id: request_id,
            darwin_ts: darwin_ts,
            inserted_at: now,
            updated_at: now
          }
        ],
        on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinTrainOrder),
        conflict_target: [:crs, :platform],
        returning: true
      )

    %{darwin_train_order: train_order_res}
  end
end
