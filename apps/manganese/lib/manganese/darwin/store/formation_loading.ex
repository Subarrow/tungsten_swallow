defmodule Manganese.Darwin.Store.FormationLoading do
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  import Manganese.Util.StoreUtil

  require Logger

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.FormationLoading{
          rid: rid,
          formation_id: formation_id,
          tiploc: tiploc,
          wt_match: wt_match,
          loading: loading_raw
        }
      ) do
    sched_locations = Table.DarwinScheduleLocation.schedule_locations_by_fq_wt_match(Repo, rid)
    fq_wt_match = fq_wt_match(rid, tiploc, wt_match)

    case Map.get(sched_locations, fq_wt_match) do
      nil ->
        Logger.warning(
          "Schedule #{rid} exists but location #{fq_wt_match} does not (loading for formation #{formation_id})"
        )

        nil

      _ ->
        {coach_numbers, coach_loads} =
          loading_raw
          |> Enum.map(fn %RealTime.FormationLoading.CoachLoading{
                           coach_number: coach_number,
                           loading: coach_load,
                           source: nil,
                           source_instance: nil
                         } ->
            {coach_number, coach_load}
          end)
          |> Enum.unzip()

        formation_loading_res =
          Repo.insert_all(
            Table.DarwinFormationLoading,
            [
              %{
                fq_wt_match: fq_wt_match,
                rid: rid,
                tiploc: tiploc,
                formation_id: formation_id,
                coach_numbers: coach_numbers,
                coach_loading: coach_loads,
                minimum_loading: Enum.min(coach_loads),
                maximum_loading: Enum.max(coach_loads),
                average_loading: div(Enum.sum(coach_loads), length(coach_loads)),
                length: length(coach_loads),
                source: source,
                cis_source: source_instance,
                darwin_request_id: request_id,
                darwin_ts: darwin_ts,
                inserted_at: now,
                updated_at: now
              }
            ],
            on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinFormationLoading),
            conflict_target: [:fq_wt_match],
            returning: true
          )

        %{
          darwin_formation_loading: formation_loading_res
        }
    end
  end
end
