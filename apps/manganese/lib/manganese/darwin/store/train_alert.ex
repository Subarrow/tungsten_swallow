defmodule Manganese.Darwin.Store.TrainAlert do
  import Ecto.Query, only: [from: 2]
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.TrainAlert{
          alert_id: alert_id,
          services: services_raw,
          send_alert_by_sms?: can_send_by_sms,
          send_alert_by_email?: can_send_by_email,
          send_alert_by_twitter?: can_send_by_twitter,
          source: alert_source,
          message: message,
          audience: audience,
          forced?: is_forced,
          copied_from_alert_id: copied_alert_id,
          copied_from_source: copied_alert_source
        }
      ) do
    train_alert_res =
      Repo.insert_all(
        Table.DarwinTrainAlert,
        [
          %{
            alert_id: alert_id,
            message: message,
            can_send_by_sms: can_send_by_sms,
            can_send_by_email: can_send_by_email,
            can_send_by_twitter: can_send_by_twitter,
            alert_source: alert_source,
            audience: Atom.to_string(audience),
            is_forced: is_forced,
            copied_alert_id: copied_alert_id,
            copied_alert_source: copied_alert_source,
            source: source,
            cis_source: source_instance,
            darwin_request_id: request_id,
            darwin_ts: darwin_ts,
            inserted_at: now,
            updated_at: now
          }
        ],
        on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinTrainAlert),
        conflict_target: [:alert_id],
        returning: true
      )

    train_alert_service_station_res =
      Repo.insert_all(
        Table.DarwinTrainAlertServiceStation,
        services_raw
        |> Enum.map(fn %RealTime.TrainAlert.Service{
                         rid: rid,
                         stations: stations
                       } ->
          Enum.map(stations, &{rid, &1})
        end)
        |> List.flatten()
        |> Enum.map(fn {rid, crs} ->
          %{
            alert_id: alert_id,
            crs: crs,
            rid: rid,
            darwin_ts: darwin_ts,
            inserted_at: now,
            updated_at: now
          }
        end),
        on_conflict:
          on_conflict_all_update_fields_no_rollback(Table.DarwinTrainAlertServiceStation),
        conflict_target: [:alert_id, :crs, :rid]
      )

    {_count, nil} =
      from(tas in Table.DarwinTrainAlertServiceStation,
        where: tas.alert_id == ^alert_id and tas.darwin_ts < ^darwin_ts
      )
      |> Repo.delete_all()

    %{
      darwin_train_alert_service_station: train_alert_service_station_res,
      darwin_train_alert: train_alert_res
    }
  end
end
