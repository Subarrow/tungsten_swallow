defmodule Manganese.Darwin.Store.Schedule do
  import Ecto.Query, only: [from: 2]
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  alias NREFeeds.Darwin.Util.DarwinTime
  alias Manganese.Util.StoreUtil

  defp type_to_string(type) do
    case type do
      :origin -> "O"
      :destination -> "D"
      :pass -> "P"
      :intermediate -> "I"
    end
  end

  defp event_to_string(event) do
    case event do
      :arrive -> "A"
      :pass -> "P"
      :depart -> "D"
    end
  end

  defp expand_cancel_reason(%RealTime.DisruptionReason{
         reason_code: reason_code,
         near?: near?,
         tiploc: tiploc
       }) do
    {"C" <> reason_code, near?, tiploc}
  end

  defp expand_cancel_reason(_), do: {nil, nil, nil}

  defp sched_fq_wt_match(rid, tiploc, wt_match, activity) do
    StoreUtil.fq_wt_match(rid, tiploc, block_out_wt_match(wt_match, activity))
  end

  defp block_out_wt_match({arrive, _pass, _depart}, "TF") do
    {arrive, 0, 0}
  end

  defp block_out_wt_match({_arrive, _pass, depart}, "TB") do
    {0, 0, depart}
  end

  defp block_out_wt_match(wt_match, _activity) do
    wt_match
  end

  def store(
        now,
        real_time = %RealTime{
          timestamp_darwin: darwin_ts
        },
        schedule = %RealTime.Schedule{
          rid: rid
        }
      ) do
    case store_schedule(now, real_time, schedule) do
      {0, []} ->
        nil

      sched_res = {1, [_new_sched_row]} ->
        sched_loc_res =
          {_count, _new_sched_loc_rows} =
          store_schedule_locations(now, real_time, schedule)

        time_res = store_times(now, real_time, schedule)

        {_count, nil} =
          from(sl in Table.DarwinScheduleLocation,
            where: sl.rid == ^rid and sl.darwin_ts != ^darwin_ts
          )
          |> Repo.delete_all()

        :ets.insert(:rid_lu, {rid, nil})

        %{
          darwin_schedule: sched_res,
          darwin_schedule_location: sched_loc_res,
          darwin_time: time_res
        }
    end
  end

  def store_schedule(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        schedule = %RealTime.Schedule{
          rid: rid,
          ssd: ssd,
          uid: uid,
          rsid: rsid,
          signalling_id: signalling_id,
          status: status_code,
          category: category_code,
          operator: operator_code,
          passenger?: is_passenger,
          deleted?: is_deleted,
          charter?: is_charter,
          active?: is_active,
          cancel_reason: cancel_reason_raw
        }
      ) do
    {cancel_reason_code, cancel_reason_is_near, cancel_reason_tiploc} =
      expand_cancel_reason(cancel_reason_raw)

    Repo.insert_all(
      Table.DarwinSchedule,
      [
        %{
          rid: rid,
          ssd: ssd,
          uid: uid,
          rsid: rsid,
          signalling_id: signalling_id,
          train_number: RealTime.Schedule.get_train_number(schedule),
          status_code: status_code,
          category_code: category_code,
          operator_code: operator_code,
          is_passenger: is_passenger,
          is_active: is_active,
          is_deleted: is_deleted,
          is_charter: is_charter,
          cancel_reason_code: cancel_reason_code,
          cancel_reason_is_near: cancel_reason_is_near,
          cancel_reason_tiploc: cancel_reason_tiploc,
          inserted_at: now,
          updated_at: now,
          source: source,
          cis_source: source_instance,
          darwin_request_id: request_id,
          darwin_ts: darwin_ts
        }
      ],
      on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinSchedule),
      conflict_target: :rid,
      returning: true
    )
  end

  def store_schedule_locations(
        now,
        %RealTime{
          source: source,
          source_instance: source_instance,
          request_id: request_id,
          timestamp_darwin: darwin_ts
        },
        %RealTime.Schedule{rid: rid, locations: schedule_locations}
      ) do
    Repo.insert_all(
      Table.DarwinScheduleLocation,
      schedule_locations
      |> Enum.map(fn %RealTime.Schedule.Location{
                       type: type,
                       tiploc: tiploc,
                       wt_match: wt_match,
                       index: index,
                       activity: activity,
                       planned_activity: planned_activity,
                       cancelled?: cancelled?,
                       operational?: operational?,
                       formation_id: formation_id,
                       average_loading: average_loading,
                       route_delay: route_delay,
                       false_destination: false_destination,
                       public_times: public_times,
                       working_times: working_times
                     } ->
        public_times_utc = DarwinTime.wt_to_utc!(tiploc, public_times)
        working_times_utc = DarwinTime.wt_to_utc!(tiploc, working_times)

        %{
          fq_wt_match: sched_fq_wt_match(rid, tiploc, wt_match, activity),
          rid: rid,
          index: index,
          type: type_to_string(type),
          tiploc: tiploc,
          activity: activity,
          planned_activity: planned_activity,
          is_cancelled: cancelled?,
          is_operational: operational?,
          formation_id: formation_id,
          average_loading: average_loading,
          route_delay: route_delay,
          false_destination: false_destination,
          public_arrive: public_times_utc[:arrive],
          public_depart: public_times_utc[:depart],
          working_arrive: working_times_utc[:arrive],
          working_pass: working_times_utc[:pass],
          working_depart: working_times_utc[:depart],
          naive_arrive: working_times[:arrive],
          naive_pass: working_times[:pass],
          naive_depart: working_times[:depart],
          inserted_at: now,
          updated_at: now,
          source: source,
          cis_source: source_instance,
          darwin_request_id: request_id,
          darwin_ts: darwin_ts
        }
      end),
      on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinScheduleLocation),
      conflict_target: :fq_wt_match,
      returning: true
    )
  end

  def store_times(
        now,
        %RealTime{source: source, source_instance: source_instance, timestamp_darwin: darwin_ts},
        %RealTime.Schedule{rid: rid, locations: schedule_locations}
      ) do
    Repo.insert_all(
      Table.DarwinTime,
      schedule_locations
      |> Enum.map(fn %RealTime.Schedule.Location{
                       tiploc: tiploc,
                       wt_match: wt_match,
                       working_times: working_times,
                       activity: activity
                     } ->
        [
          arrive: working_times[:arrive],
          pass: working_times[:pass],
          depart: working_times[:depart]
        ]
        |> Enum.map(fn
          {_event, nil} ->
            []

          {event, naive_time} ->
            event_str =
              event_to_string(event)

            fq_wt_match = sched_fq_wt_match(rid, tiploc, wt_match, activity)
            utc_time = DarwinTime.to_utc!(tiploc, naive_time)

            %{
              time_id: fq_wt_match <> "_W" <> event_str <> "_SC",
              fq_wt_match: fq_wt_match,
              rid: rid,
              tiploc: tiploc,
              timetable: "W",
              event: event_str,
              type: "S",
              # original_time: original_time,
              time: utc_time,
              naive_time: naive_time,
              source: source,
              cis_source: source_instance,
              inserted_at: now,
              updated_at: now,
              darwin_ts: darwin_ts
            }
        end)
      end)
      |> List.flatten(),
      on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinTime),
      conflict_target: :time_id,
      returning: true
    )
  end
end
