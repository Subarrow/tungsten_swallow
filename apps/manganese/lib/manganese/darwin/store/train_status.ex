defmodule Manganese.Darwin.Store.TrainStatus do
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  alias NREFeeds.Darwin.Util.DarwinTime
  import Manganese.Util.StoreUtil

  require Logger

  defp event_to_str(event) do
    case event do
      :arrive -> "A"
      :depart -> "D"
      :pass -> "P"
    end
  end

  defp platform_source_to_string(platform_source) do
    case platform_source do
      :automatic -> "A"
      :manual -> "M"
      :planned -> "P"
      nil -> nil
    end
  end

  defp platform_suppression_to_string(platform_suppression) do
    case platform_suppression do
      false -> nil
      :cis -> "CIS"
      :darwin -> "Darwin"
    end
  end

  defp expand_delay_reason(%RealTime.DisruptionReason{
         reason_code: reason_code,
         near?: near?,
         tiploc: tiploc
       }) do
    {"D" <> reason_code, near?, tiploc}
  end

  defp expand_delay_reason(_), do: {nil, nil, nil}

  def store(
        now,
        real_time = %RealTime{
          timestamp_darwin: _darwin_ts,
          request_id: _request_id,
          source_instance: _source_instance,
          source: _source
        },
        train_status = %RealTime.TrainStatus{
          rid: rid,
          uid: _uid,
          ssd: _ssd,
          reverse_formation?: _reverse_formation?,
          delay_reason: _delay_reason_raw,
          locations: _locations
        }
      ) do
    schedule_locations = Table.DarwinScheduleLocation.schedule_locations_by_fq_wt_match(Repo, rid)

    case store_train_status(now, real_time, train_status) do
      {0, []} ->
        nil

      train_status_res = {_, _} ->
        train_status_loc_res =
          store_train_status_locations(schedule_locations, now, real_time, train_status)

        # times
        times_res = schedule_locations |> store_times(now, real_time, train_status)

        %{
          darwin_train_status: train_status_res,
          darwin_train_status_location: train_status_loc_res,
          darwin_time: times_res
        }
    end
  end

  def store_train_status(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.TrainStatus{
          rid: rid,
          uid: _uid,
          ssd: _ssd,
          reverse_formation?: reverse_formation?,
          delay_reason: delay_reason_raw
        }
      ) do
    {delay_reason_code, delay_reason_is_near, delay_reason_tiploc} =
      expand_delay_reason(delay_reason_raw)

    Repo.insert_all(
      Table.DarwinTrainStatus,
      [
        %{
          rid: rid,
          is_reverse_formation: reverse_formation?,
          delay_reason_code: delay_reason_code,
          delay_reason_is_near: delay_reason_is_near,
          delay_reason_tiploc: delay_reason_tiploc,
          inserted_at: now,
          updated_at: now,
          source: source,
          cis_source: source_instance,
          darwin_request_id: request_id,
          darwin_ts: darwin_ts
        }
      ],
      on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinTrainStatus),
      conflict_target: :rid,
      returning: true
    )
  end

  def store_train_status_locations(
        schedule_locations,
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.TrainStatus{
          rid: rid,
          uid: _uid,
          ssd: _ssd,
          locations: locations
        }
      ) do
    Repo.insert_all(
      Table.DarwinTrainStatusLocation,
      locations
      |> Enum.map(
        fn ts_loc = %RealTime.TrainStatus.Location{tiploc: tiploc, wt_match: wt_match} ->
          {Map.get(schedule_locations, fq_wt_match(rid, tiploc, wt_match)), ts_loc}
        end
      )
      |> Enum.map(fn
        {%Table.DarwinScheduleLocation{
           fq_wt_match: fq_wt_match
         },
         %RealTime.TrainStatus.Location{
           tiploc: tiploc,
           wt_match: _wt_match,
           suppressed?: is_suppressed,
           length: length,
           platform: platform,
           detach_front?: is_front_detaching
         }} ->
          Map.merge(
            case platform do
              %RealTime.TrainStatus.Platform{
                platform: platform_text,
                suppressed: platform_suppression,
                confirmed?: platform_confirmed?,
                source: platform_source
              } ->
                %{
                  platform: platform_text,
                  platform_is_confirmed: platform_confirmed?,
                  platform_source: platform_source_to_string(platform_source),
                  platform_suppression: platform_suppression_to_string(platform_suppression)
                }

              nil ->
                %{
                  platform: nil,
                  platform_is_confirmed: false,
                  platform_source: nil,
                  platform_suppression: nil
                }
            end,
            %{
              fq_wt_match: fq_wt_match,
              rid: rid,
              tiploc: tiploc,
              is_suppressed: is_suppressed,
              length: length,
              is_front_detaching: is_front_detaching,
              inserted_at: now,
              updated_at: now,
              darwin_ts: darwin_ts,
              cis_source: source_instance,
              source: source,
              darwin_request_id: request_id
            }
          )

        {nil,
         %RealTime.TrainStatus.Location{
           tiploc: tiploc,
           wt_match: wt_match
         }} ->
          candidates =
            schedule_locations
            |> Map.filter(fn {_key, %Table.DarwinScheduleLocation{tiploc: v_tiploc}} ->
              tiploc == v_tiploc
            end)
            |> Map.keys()
            |> Enum.join("; ")

          Logger.warning(
            "Schedule #{rid} exists but location #{fq_wt_match(rid, tiploc, wt_match)} does not (candidates: #{candidates})"
          )

          []
      end)
      |> List.flatten(),
      on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinTrainStatusLocation),
      conflict_target: :fq_wt_match,
      returning: true
    )
  end

  def store_times(
        schedule_locations,
        now,
        %RealTime{
          timestamp_darwin: darwin_ts
        },
        %RealTime.TrainStatus{
          rid: rid,
          uid: _uid,
          ssd: _ssd,
          locations: locations
        }
      ) do
    Repo.insert_all(
      Table.DarwinTime,
      locations
      |> Enum.map(
        fn ts_loc = %RealTime.TrainStatus.Location{tiploc: tiploc, wt_match: wt_match} ->
          {Map.get(schedule_locations, fq_wt_match(rid, tiploc, wt_match)), ts_loc}
        end
      )
      |> Enum.map(fn
        {%Table.DarwinScheduleLocation{
           fq_wt_match: fq_wt_match,
           naive_arrive: wt_arr,
           naive_pass: wt_pass,
           naive_depart: wt_dep
         },
         %RealTime.TrainStatus.Location{
           tiploc: tiploc,
           wt_match: _wt_match,
           arrive: arrive,
           pass: pass,
           depart: depart
         }} ->
          [arrive: {arrive, wt_arr}, pass: {pass, wt_pass}, depart: {depart, wt_dep}]
          |> Enum.map(fn
            {_event, {nil, _}} ->
              []

            {event,
             {%RealTime.TrainStatus.Time{
                working_estimated_time: wt_et,
                estimated_time: pt_et,
                actual_time: wt_at,
                estimated_minimum: et_min_raw,
                delayed?: is_delayed,
                estimated_time_unknown?: is_unknown_delay,
                actual_time_class: at_class,
                source: source,
                source_instance: source_instance
              }, schedule_naive_time}} ->
              event_str = event_to_str(event)

              {type, original_time} =
                [{"A", wt_at}, {"E", wt_et}, {"E", pt_et}]
                |> Enum.filter(fn {_type, original_time} -> !is_nil(original_time) end)
                |> List.first({nil, nil})

              {et_min, utc_time, naive_time} =
                if not is_nil(original_time) and not is_nil(schedule_naive_time) do
                  {_ldt, naive_time} =
                    DarwinTime.combine_time!(schedule_naive_time, original_time)

                  {_ldt, naive_min_time} =
                    DarwinTime.combine_time!(schedule_naive_time, et_min_raw)

                  utc_time = DarwinTime.to_utc!(tiploc, naive_time)
                  utc_min_time = DarwinTime.to_utc!(tiploc, naive_min_time)

                  {utc_min_time, utc_time, naive_time}
                else
                  {nil, nil, nil}
                end

              %{
                time_id: fq_wt_match <> "_W" <> event_str <> "_RT",
                fq_wt_match: fq_wt_match,
                rid: rid,
                tiploc: tiploc,
                timetable: "W",
                event: event_str,
                type: type,
                original_time: original_time,
                minimum: et_min,
                time: utc_time,
                naive_time: naive_time,
                is_delayed: is_delayed,
                is_unknown_delay: is_unknown_delay,
                actual_time_class: at_class,
                source: source,
                cis_source: source_instance,
                darwin_ts: darwin_ts,
                updated_at: now,
                inserted_at: now
              }
          end)

        {nil, _} ->
          []
      end)
      |> List.flatten(),
      on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinTime),
      conflict_target: :time_id,
      returning: true
    )
  end
end
