defmodule Manganese.Darwin.Store.ScheduleFormations do
  import Ecto.Query, only: [from: 2]
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.ScheduleFormations{
          rid: rid,
          formations: formations
        }
      ) do
    formation_res =
      Repo.insert_all(
        Table.DarwinFormation,
        Enum.map(formations, fn %RealTime.ScheduleFormations.Formation{
                                  formation_id: formation_id,
                                  coaches: coaches_raw,
                                  source_instance: nil,
                                  source: nil
                                } ->
          coaches =
            coaches_raw
            |> Enum.map(fn %RealTime.ScheduleFormations.Formation.Coach{
                             coach_number: coach_number,
                             coach_class: coach_class,
                             toilet: toilet
                           } ->
              {unit_letter, _number_in_unit} =
                case Regex.run(~r/([A-Z]+)([0-9]+)|(.+)/i, coach_number, capture: :all_but_first) do
                  [unit_letter, number_in_unit] -> {unit_letter, number_in_unit}
                  ["", "", _coach_number] -> {nil, nil}
                end

              {toilet_status, toilet_type} =
                case toilet do
                  %RealTime.ScheduleFormations.Formation.Coach.Toilet{
                    status: toilet_status_raw,
                    type: toilet_type
                  } ->
                    {Atom.to_string(toilet_status_raw), toilet_type}

                  nil ->
                    {nil, nil}
                end

              %{
                "coach_number" => coach_number,
                "coach_class" => coach_class,
                "unit_letter" => unit_letter,
                "toilet_status" => toilet_status,
                "toilet_type" => toilet_type
              }
            end)

          unit_length = coaches |> Enum.group_by(& &1["unit_letter"]) |> Map.keys() |> length()

          has_toilet =
            coaches
            |> Enum.map(
              &((&1["toilet_type"] != "Accessible" or &1["toilet_type"] == "Standard") and
                  &1["toilet_status"] == "in_service")
            )
            |> Enum.any?()

          %{
            rid: rid,
            formation_id: formation_id,
            length: length(coaches),
            unit_length: unit_length,
            coaches: coaches,
            has_toilet: has_toilet,
            source: source,
            cis_source: source_instance,
            darwin_request_id: request_id,
            darwin_ts: darwin_ts,
            inserted_at: now,
            updated_at: now
          }
        end),
        on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinFormation),
        conflict_target: [:formation_id],
        returning: true
      )

    {_count, nil} =
      from(df in Table.DarwinFormation,
        where: df.rid == ^rid and df.darwin_ts < ^darwin_ts
      )
      |> Repo.delete_all()

    %{darwin_formation: formation_res}
  end
end
