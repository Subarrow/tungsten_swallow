defmodule Manganese.Darwin.Store.StationMessage do
  import Ecto.Query, only: [from: 2]
  import Manganese.Util.ManganesePostgres, only: [on_conflict_all_update_fields_no_rollback: 1]
  alias NREFeeds.Darwin.Schema.RealTime
  alias ManganeseORM.Table

  alias Manganese.Repo

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts,
          request_id: request_id,
          source_instance: source_instance,
          source: source
        },
        %RealTime.StationMessage{
          message_id: message_id,
          message: message,
          stations: stations,
          severity: severity,
          category: category,
          suppressed?: suppressed?
        }
      ) do
    station_message_res =
      Repo.insert_all(
        Table.DarwinStationMessage,
        [
          %{
            message_id: message_id,
            message: message,
            category: Atom.to_string(category),
            severity: severity,
            is_suppressed: suppressed?,
            incident_url: nil,
            source: source,
            cis_source: source_instance,
            darwin_request_id: request_id,
            darwin_ts: darwin_ts,
            inserted_at: now,
            updated_at: now
          }
        ],
        on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinStationMessage),
        conflict_target: [:message_id],
        returning: true
      )

    station_message_station_res =
      Repo.insert_all(
        Table.DarwinStationMessageStation,
        stations
        |> Enum.map(fn crs ->
          %{
            message_id: message_id,
            crs: crs,
            darwin_ts: darwin_ts,
            inserted_at: now,
            updated_at: now
          }
        end),
        on_conflict: on_conflict_all_update_fields_no_rollback(Table.DarwinStationMessageStation),
        conflict_target: [:message_id, :crs]
      )

    {_count, nil} =
      from(sms in Table.DarwinStationMessageStation,
        where: sms.message_id == ^message_id and sms.darwin_ts < ^darwin_ts
      )
      |> Repo.delete_all()

    %{
      darwin_station_message: station_message_res,
      darwin_station_message_station: station_message_station_res
    }
  end
end
