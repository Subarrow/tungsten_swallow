defmodule Manganese.Darwin.SnapshotParser do
  use GenStage

  alias NREFeeds.Darwin.Util.DarwinXml
  alias NREFeeds.Darwin.Schema.RealTime

  def start_link(args) do
    GenStage.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(state) do
    {:producer_consumer, state, subscribe_to: [Manganese.Darwin.SnapshotSource]}
  end

  def handle_events(
        events,
        _from,
        state = %{
          schema_model: schema_model,
          schema_record_definitions: schema_record_definitions
        }
      ) do
    parsed =
      events
      |> Stream.map(fn event ->
        {:ok, parsed_event} =
          DarwinXml.parse_darwin_binary(event, schema_model, schema_record_definitions)

        parsed_event
      end)
      |> Stream.map(&RealTime.from_xml(&1, 0))
      |> Enum.to_list()

    {:noreply, parsed, state}
  end
end
