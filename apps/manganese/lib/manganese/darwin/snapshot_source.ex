defmodule Manganese.Darwin.SnapshotSource do
  use GenStage
  require Logger

  alias NREFeeds.Darwin.Util.DarwinFTP
  import Ecto.Query, only: [from: 2]

  alias ManganeseORM.Table.DarwinSnapshotVersion
  alias Manganese.Repo

  def start_link(args) do
    GenStage.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(args) do
    Process.send_after(self(), :pull, 0)
    {:producer, args, buffer_size: 100_000}
  end

  def handle_info(:pull, state) do
    host = Application.get_env(:manganese, :ftp_hostname)
    user = Application.get_env(:manganese, :ftp_username)
    pass = Application.get_env(:manganese, :ftp_password)

    now_sec = DateTime.utc_now() |> DateTime.truncate(:second)

    {_ls_line, snapshot_lines} =
      DarwinFTP.with_conn!(host, user, pass, fn conn ->
        [ls_line] = DarwinFTP.list_snapshot_detail!(conn)

        case from(ssv in DarwinSnapshotVersion, where: ssv.ftp_line == ^ls_line) |> Repo.all() do
          [%DarwinSnapshotVersion{started_at: started_at}] when not is_nil(started_at) ->
            {ls_line, []}

          _ ->
            Repo.insert_all(
              DarwinSnapshotVersion,
              [
                %{
                  ftp_line: ls_line,
                  started_at: now_sec,
                  inserted_at: now_sec,
                  updated_at: now_sec
                }
              ],
              conflict_target: :ftp_line,
              on_conflict: {:replace_all_except, [:inserted_at]}
            )

            {ls_line, DarwinFTP.get_snapshot_lines!(conn)}
        end
      end)

    Logger.info("Fetched #{length(snapshot_lines)} messages from base snapshot")

    # 14_400_000 - 1000*60*60*4 (i.e. four hours)
    Process.send_after(self(), :pull, 14_400_000)

    {:noreply, snapshot_lines, state}
  end

  def handle_demand(_count, state) do
    {:noreply, [], state}
  end
end
