defmodule Manganese.Darwin.Distributor do
  use GenServer

  def start_link({serial, link_opts}) do
    GenServer.start_link(__MODULE__, serial, link_opts)
  end

  def init(serial) do
    {:ok, serial}
  end

  def handle_cast(diff, serials) do
    framed_serials =
      serials
      |> Enum.map(fn serial ->
        {[{"serial", Atom.to_string(serial)}], serialise(serial, diff)}
      end)

    GenServer.cast(Manganese.Darwin.Diff.AMQPSink, framed_serials)

    {:noreply, serials}
  end

  def serialise(:ettb_elx, message) do
    :erlang.term_to_binary(message)
  end
end
