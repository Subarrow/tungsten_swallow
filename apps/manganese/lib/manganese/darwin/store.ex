defmodule Manganese.Darwin.Store do
  use GenServer

  require Logger

  import Ecto.Query, only: [from: 2]
  import Manganese.Util.StoreUtil

  alias ManganeseORM.Table
  alias Manganese.Repo

  alias NREFeeds.Darwin.Schema.RealTime
  alias NREFeeds.Darwin.Schema.RealTime.DeactivatedSchedule
  alias NREFeeds.Darwin.Schema.RealTime.Schedule
  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus
  alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations
  alias NREFeeds.Darwin.Schema.RealTime.FormationLoading
  alias NREFeeds.Darwin.Schema.RealTime.Association
  alias NREFeeds.Darwin.Schema.RealTime.StationMessage
  alias NREFeeds.Darwin.Schema.RealTime.TrainAlert
  alias NREFeeds.Darwin.Schema.RealTime.TrainOrder

  alias Manganese.Darwin.Store
  alias Manganese.Darwin.Prefetch

  def start_link(link_opts) do
    GenServer.start_link(__MODULE__, nil, link_opts)
  end

  def init(_) do
    :rid_lu = :ets.new(:rid_lu, [:named_table, :public])

    :ets.insert(
      :rid_lu,
      Table.DarwinSchedule.all_schedule_rids(Repo)
    )

    {:ok, %{}}
  end

  def submit_realtime_messages(messages, store_pid) do
    GenServer.cast(store_pid, messages)
  end

  def handle_cast(real_time, state) do
    store_realtime_messages(real_time)

    {:noreply, state}
  end

  @spec store_realtime_messages([RealTime.t()] | []) :: nil

  def store_realtime_messages([]) do
  end

  def store_realtime_messages([real_time = %RealTime{messages: messages} | rest]) do
    now = DateTime.utc_now() |> DateTime.truncate(:second)

    differences =
      messages
      |> Enum.filter(fn
        %Schedule{} -> true
        %{rid: rid} -> schedule_exists?(rid)
        _ -> true
      end)
      |> Enum.map(fn
        message ->
          Repo.transaction(fn _repo ->
            Repo.query!("SET CONSTRAINTS ALL DEFERRED;")
            store(now, real_time, message)
          end)
      end)
      |> Enum.filter(fn
        {:ok, nil} ->
          false

        {:ok, _etc} ->
          true

        _ ->
          false
      end)
      |> Enum.map(fn {:ok, etc} -> etc end)

    GenServer.cast(Manganese.Darwin.Diff, {real_time, differences})

    store_realtime_messages(rest)
  end

  def store(
        now,
        real_time = %RealTime{},
        schedule = %RealTime.Schedule{}
      ) do
    {:darwin_schedule, Prefetch.prefetch(schedule),
     Store.Schedule.store(now, real_time, schedule)}
  end

  def store(
        now,
        %RealTime{
          timestamp_darwin: darwin_ts
        },
        %DeactivatedSchedule{rid: rid}
      ) do
    from(schedule in Table.DarwinSchedule,
      where: schedule.rid == ^rid and schedule.darwin_ts <= ^darwin_ts,
      select: schedule.rid
    )
    |> Repo.update_all(set: [is_active: false, updated_at: now, darwin_ts: darwin_ts])

    {:darwin_schedule, [], %{}}
  end

  def store(now, real_time = %RealTime{}, train_status = %TrainStatus{}) do
    {:darwin_train_status, Prefetch.prefetch(train_status),
     Store.TrainStatus.store(now, real_time, train_status)}
  end

  def store(now, real_time = %RealTime{}, association = %Association{}) do
    {:darwin_association, Prefetch.prefetch(association),
     Store.Association.store(now, real_time, association)}
  end

  def store(now, real_time = %RealTime{}, station_message = %StationMessage{}) do
    {:darwin_station_message, Prefetch.prefetch(station_message),
     Store.StationMessage.store(now, real_time, station_message)}
  end

  def store(now, real_time = %RealTime{}, train_alert = %TrainAlert{}) do
    {:darwin_train_alert, Prefetch.prefetch(train_alert),
     Store.TrainAlert.store(now, real_time, train_alert)}
  end

  def store(now, real_time = %RealTime{}, train_order = %TrainOrder{}) do
    {:darwin_train_order, Prefetch.prefetch(train_order),
     Store.TrainOrder.store(now, real_time, train_order)}
  end

  def store(now, real_time = %RealTime{}, formation_loading = %FormationLoading{}) do
    {:darwin_formation_loading, Prefetch.prefetch(formation_loading),
     Store.FormationLoading.store(now, real_time, formation_loading)}
  end

  def store(now, real_time = %RealTime{}, schedule_formations = %ScheduleFormations{}) do
    {:darwin_formation, Prefetch.prefetch(schedule_formations),
     Store.ScheduleFormations.store(now, real_time, schedule_formations)}
  end

  def store(_, _, _) do
  end
end
