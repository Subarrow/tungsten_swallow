defmodule Manganese.Darwin.Cleaner do
  @moduledoc """
  Cleans up old schedules
  """

  use GenServer
  require Logger

  import Ecto.Query, only: [from: 2]

  alias Manganese.Repo

  @old_threshold 15
  @targets_limit 100
  # 20 seconds
  @interval_active 20_000
  # 10 min
  @interval_inactive 600_000

  def start_link(link_opts \\ []) do
    GenServer.start_link(
      __MODULE__,
      %{},
      link_opts
    )
  end

  def init(state) do
    Process.send_after(self(), :clean, 1_000)
    {:ok, state}
  end

  def handle_info(:clean, state) do
    case clean() do
      {0, nil} ->
        Process.send_after(self(), :clean, @interval_inactive)

      {_count, nil} ->
        Process.send_after(self(), :clean, @interval_active)
    end

    {:noreply, state}
  end

  def clean() do
    threshold_date = Date.utc_today() |> Date.add(-@old_threshold)

    select_q =
      from(s in "darwin_schedule",
        where: s.ssd < ^threshold_date,
        select: s.rid,
        limit: @targets_limit
      )

    delete_q = from(s in "darwin_schedule", where: s.rid in subquery(select_q))

    Repo.delete_all(delete_q)
  end
end
