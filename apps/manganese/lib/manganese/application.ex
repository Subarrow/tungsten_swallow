defmodule Manganese.Application do
  @moduledoc false

  require Logger

  use Application

  alias NREFeeds.Darwin.Util.DarwinXml

  def start_diff_sup(nil), do: nil

  def start_diff_sup(%{
        serial: diff_serial,
        amqp: %{
          host: diff_host,
          port: diff_port,
          user: diff_user,
          pass: diff_pass,
          destination: diff_dest
        }
      }) do
    children = [
      {Manganese.Util.AMQPSink,
       %{
         host: diff_host,
         port: diff_port,
         user: diff_user,
         pass: diff_pass,
         destination: diff_dest,
         id: "darwin_diff_amqp_sink",
         name: Manganese.Darwin.Diff.AMQPSink
       }},
      {Manganese.Darwin.Distributor, {diff_serial, name: Manganese.Darwin.Distributor}},
      {Manganese.Darwin.Diff, [name: Manganese.Darwin.Diff]}
    ]

    opts = [strategy: :rest_for_one, name: Manganese.Diff.Supervisor]
    {:ok, _} = Supervisor.start_link(children, opts)
  end

  def start_main_sup() do
    children = [
      Manganese.Repo,
      {TungstenSwallow.Util.BackoffManager, opts: %{}}
    ]

    opts = [strategy: :one_for_one, name: Manganese.Main.Supervisor]
    {:ok, _} = Supervisor.start_link(children, opts)
  end

  def start_darwin_sup() do
    schema_record_definitions = DarwinXml.load_schema_record_definitions!()

    schema_model = DarwinXml.load_schema_model!()

    ref_schema_record_definitions = DarwinXml.load_reference_schema_record_definitions!()

    ref_schema_model = DarwinXml.load_reference_schema_model!()

    relay_host = String.to_charlist(Application.fetch_env!(:manganese, :relay_host))
    relay_port = Application.fetch_env!(:manganese, :relay_port)
    relay_username = Application.fetch_env!(:manganese, :relay_username)
    relay_password = Application.fetch_env!(:manganese, :relay_password)
    relay_queue = Application.fetch_env!(:manganese, :relay_queue)

    s3_credentials = Application.fetch_env!(:manganese, :s3_credentials)

    kb_credentials = Application.fetch_env!(:manganese, :kb_credentials)

    bplan_path = Application.get_env(:manganese, :bplan_path)
    dtd_loc_path = Application.get_env(:manganese, :dtd_loc_path)

    subscribe_params =
      case Application.fetch_env!(:manganese, :stream_mode) do
        :first -> [{"x-stream-offset", "first"}]
        :last -> [{"x-stream-offset", "last"}]
      end

    children = [
      {Manganese.Darwin.StoreKB,
       kb_credentials: kb_credentials, link_opts: [name: Manganese.KB.StoreKB]},
      {Manganese.Darwin.StoreReference,
       [
         schema_record_definitions: ref_schema_record_definitions,
         schema_model: ref_schema_model,
         s3_credentials: s3_credentials,
         link_opts: [name: Manganese.Darwin.StoreReference]
       ]},
      {Manganese.Darwin.Store, [name: Manganese.Darwin.Store]},
      {NREFeeds.Darwin.RealTimeParser,
       [
         schema_record_definitions: schema_record_definitions,
         schema_model: schema_model,
         sink: Manganese.Darwin.Store,
         link_opts: [name: Manganese.Darwin.RealTimeParser]
       ]},
      Supervisor.child_spec(
        {NREFeeds.Darwin.MessageHandler,
         %{
           host: relay_host,
           port: relay_port,
           user: relay_username,
           pass: relay_password,
           queue: relay_queue,
           id: "darwin_intake",
           name: Manganese.DarwinIntake,
           subscribe_params: subscribe_params,
           sink: Manganese.Darwin.RealTimeParser
         }},
        id: :darwin_intake
      ),
      Manganese.Darwin.Cleaner,
      {Manganese.Darwin.SnapshotSource, %{link: [name: Manganese.DarwinSnapshotSource]}},
      {Manganese.Darwin.SnapshotParser,
       %{schema_model: schema_model, schema_record_definitions: schema_record_definitions}},
      {Manganese.Darwin.SnapshotSink, %{}},
      Supervisor.child_spec(
        {Manganese.Static.StaticFileSource,
         %{
           module: BPLAN,
           file_path: bplan_path,
           link: [name: Manganese.BPLAN.BPLANSource],
           args: %{no_timing_link?: true}
         }},
        id: Manganese.BPLAN.BPLANSource
      ),
      {Manganese.Static.BPLAN.BPLANSink, %{}},
      Supervisor.child_spec(
        {Manganese.Static.StaticFileSource,
         %{
           module: NREFeeds.DTD.Fares,
           file_path: dtd_loc_path,
           link: [name: Manganese.DTD.DTDSource],
           args: %{file_type: "LOC"}
         }},
        id: Manganese.DTD.DTDSource
      ),
      {Manganese.Static.DTD.DTDSink, %{}}
    ]

    opts = [strategy: :one_for_one, name: Manganese.Supervisor]
    {:ok, _} = Supervisor.start_link(children, opts)
  end

  @impl true
  def start(_type, _args) do
    diff_config = Application.get_env(:manganese, :diff)

    start_main_sup()
    start_diff_sup(diff_config)
    start_darwin_sup()
  end
end
