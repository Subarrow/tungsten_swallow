defmodule Manganese.Util.DiffUtil do
  @spec fetch_multi!(map(), atom() | [atom()]) :: any() | [any()]

  def fetch_multi!(map, key) when is_atom(key) do
    fetch_multi!(map, [key])
  end

  def fetch_multi!(map, keys) when is_list(keys) do
    case Enum.map(keys, &Map.fetch!(map, &1)) do
      [single] -> single
      many -> many
    end
  end

  @spec map_diff(map(), map()) :: map()

  def map_diff(map_a, map_b) do
    Map.reject(map_b, fn {key, value_b} -> Map.fetch!(map_a, key) == value_b end)
  end
end
