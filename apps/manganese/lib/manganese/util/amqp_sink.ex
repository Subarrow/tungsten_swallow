defmodule Manganese.Util.AMQPSink do
  use GenServer
  require Logger

  alias AMQP.{Channel, Connection}

  alias TungstenSwallow.Util.BackoffManager

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: opts.name)
  end

  def init(opts = %{name: name}) do
    {interval, _} = BackoffManager.get_interval(name)
    Logger.info("#{name} waiting #{interval}ms to start connection")
    Process.send_after(self(), :connect, interval)
    {:ok, %{opts: opts}}
  end

  def handle_info(
        :connect,
        state = %{
          opts:
            opts = %{
              host: host,
              port: port,
              user: user,
              pass: pass,
              name: name
            }
        }
      ) do
    Logger.info("#{name} Begin connection to #{host}:#{port}")
    BackoffManager.notify_connect(name)

    case Connection.open(host: host, port: port, username: user, password: pass) do
      {:ok, connection} ->
        Process.monitor(connection.pid)

        {:ok, channel} = Channel.open(connection)

        {:noreply,
         Map.merge(state, %{
           network: connection,
           channel: channel,
           opts: %{opts | pass: nil}
         })}

      {:error, error} ->
        Logger.error("#{name} Connection to #{host}:#{port} failed: #{inspect(error)}")
        BackoffManager.notify_failure(name)
        {:stop, :connect_disconnected, state}
    end
  end

  def handle_info(
        {:DOWN, _ref, :process, _pid, _reason},
        state = %{opts: %{name: name, host: host, port: port}}
      ) do
    Logger.error("#{name} Connection to #{host}:#{port} dropped")
    {:stop, :connect_disconnected, state}
  end

  def handle_cast(frames, state = %{channel: channel, opts: %{destination: destination}}) do
    Enum.each(frames, &send_frame(&1, channel, destination))

    {:noreply, state}
  end

  def handle_cast(frames, state) do
    Logger.warning("Discarded #{length(frames)} messages")

    {:noreply, state}
  end

  def send_frame({headers, body}, channel, destination) do
    AMQP.Basic.publish(channel, destination, "", body,
      headers: headers,
      mandatory: true
    )
  end
end
