defmodule Manganese.Util.ManganesePostgres do
  import Ecto.Query, only: [update: 3, where: 3]

  defmacro on_conflict_all_update_fields_no_rollback(module) do
    module_expanded = Macro.expand(module, __CALLER__)

    fields_done =
      (module_expanded.__schema__(:fields) --
         [:inserted_at])
      |> Enum.map(fn field -> module_expanded.__schema__(:field_source, field) end)
      |> Enum.map(fn f ->
        {f, quote(do: fragment(unquote("EXCLUDED.#{f}")))}
      end)

    quote do
      update(
        where(unquote(module_expanded), [x], fragment("EXCLUDED.darwin_ts") > x.darwin_ts),
        [x],
        set: [unquote_splicing(fields_done)]
      )
    end
  end
end
