defmodule Manganese.Util.StoreUtil do
  def wt_match_str({wtm_a, wtm_b, wtm_c}) do
    "#{wtm_a}_#{wtm_b}_#{wtm_c}"
  end

  def fq_wt_match(
        _rid = <<ssd::8-binary, uid_prefix::2-binary, uid_body::5-binary>>,
        tiploc,
        wt_match
      ) do
    {uid_prefix_parsed, ""} = Integer.parse(uid_prefix)

    ssd <>
      "_" <> <<uid_prefix_parsed>> <> uid_body <> "_" <> tiploc <> "_" <> wt_match_str(wt_match)
  end

  def schedule_exists?(rid) do
    case :ets.lookup(:rid_lu, rid) do
      [] -> false
      _ -> true
    end
  end
end
