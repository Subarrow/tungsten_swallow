defmodule Manganese.Static.BPLAN.BPLANSink do
  use GenStage

  alias BPLAN.Schema.Location
  alias BPLAN.Schema.NetworkLink

  alias ManganeseORM.Table

  alias Manganese.Repo

  def start_link(args) do
    GenStage.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(state) do
    {:consumer, state, subscribe_to: [Manganese.BPLAN.BPLANSource]}
  end

  def handle_events(events, _from, state) do
    Enum.each(events, &do_store/1)

    {:noreply, [], state}
  end

  defp atom_to_string(nil), do: nil
  defp atom_to_string(atom), do: Atom.to_string(atom)

  def do_store(%NetworkLink{
        origin_tiploc: origin_tiploc,
        destination_tiploc: destination_tiploc,
        running_line_code: running_line_code,
        running_line_description: running_line_description,
        start_date: start_date,
        end_date: end_date,
        initial_direction: initial_direction,
        final_direction: final_direction,
        distance: distance,
        doo_passenger?: doo_passenger?,
        doo_non_passenger?: doo_non_passenger?,
        radio_electric_token_block?: radio_electric_token_block?,
        zone: zone,
        reversible_line: reversible_line,
        power_supply_type: power_supply_type,
        route_availability: route_availability,
        maximum_train_length: maximum_train_length
      }) do
    now_sec = DateTime.utc_now() |> DateTime.truncate(:second)

    Repo.insert_all(
      Table.BPLANNetworkLink,
      [
        %{
          origin_tiploc: origin_tiploc,
          destination_tiploc: destination_tiploc,
          running_line_code: running_line_code,
          running_line_description: running_line_description,
          start_date: start_date,
          end_date: end_date,
          initial_direction: atom_to_string(initial_direction),
          final_direction: atom_to_string(final_direction),
          distance: distance,
          is_doo_passenger: doo_passenger?,
          is_doo_non_passenger: doo_non_passenger?,
          is_radio_electric_token_block: radio_electric_token_block?,
          zone: zone,
          reversible_line: atom_to_string(reversible_line),
          power_supply_type: power_supply_type,
          route_availability: route_availability,
          maximum_train_length: maximum_train_length,
          inserted_at: now_sec,
          updated_at: now_sec
        }
      ],
      conflict_target: [:origin_tiploc, :destination_tiploc, :running_line_code],
      on_conflict: {:replace_all_except, [:inserted_at]}
    )
  end

  def do_store(%Location{
        tiploc: tiploc,
        name: name,
        start_date: start_date,
        end_date: end_date,
        os_easting: _os_easting,
        os_northing: _os_northing,
        timing_point_type: timing_point_type,
        zone: _zone,
        stanox: stanox,
        off_network?: off_network?,
        force_lpb: force_lpb
      }) do
    now_sec = DateTime.utc_now() |> DateTime.truncate(:second)

    Repo.insert_all(
      Table.BPLANLocation,
      [
        %{
          tiploc: tiploc,
          name: name,
          start_date: start_date,
          end_date: end_date,
          timing_point_type: Atom.to_string(timing_point_type),
          stanox: stanox,
          is_off_network: off_network?,
          force_lpb: force_lpb,
          inserted_at: now_sec,
          updated_at: now_sec
        }
      ],
      conflict_target: :tiploc,
      on_conflict: {:replace_all_except, [:inserted_at]}
    )
  end

  def do_store(_), do: nil
end
