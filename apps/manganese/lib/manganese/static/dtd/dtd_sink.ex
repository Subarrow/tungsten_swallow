defmodule Manganese.Static.DTD.DTDSink do
  use GenStage

  alias NREFeeds.DTD.Fares.Schema.LOC

  alias ManganeseORM.Table

  alias Manganese.Repo

  def start_link(args) do
    GenStage.start_link(__MODULE__, args, name: __MODULE__)
  end

  def init(state) do
    {:consumer, state, subscribe_to: [Manganese.DTD.DTDSource]}
  end

  def handle_events(events, _from, state) do
    Enum.each(events, &do_store/1)

    {:noreply, [], state}
  end

  def do_store(loc = %LOC.Location{}) do
    now_sec = DateTime.utc_now() |> DateTime.truncate(:second)

    formed = loc |> Map.from_struct() |> Map.merge(%{inserted_at: now_sec, updated_at: now_sec})

    Repo.insert_all(
      Table.DTDFaresLocation,
      [
        formed
      ],
      conflict_target: [:uic_code, :end_date],
      on_conflict: {:replace_all_except, [:inserted_at]}
    )
  end

  def do_store(_), do: nil
end
