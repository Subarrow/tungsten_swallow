defmodule Manganese.Static.StaticFileSource do
  use GenStage
  require Logger

  import Ecto.Query, only: [from: 2]

  alias Manganese.Repo

  alias ManganeseORM.Table.FeedVersion

  def start_link(args = %{file_path: _file_path, module: _module, link: link_opts}) do
    GenStage.start_link(__MODULE__, args, link_opts)
  end

  def init(args) do
    Process.send_after(self(), :pull, 0)
    {:producer, args, buffer_size: 300_000}
  end

  def handle_info(:pull, state = %{file_path: file_path, module: module}) do
    cond do
      # Path is unset, nowt we can do
      is_nil(file_path) ->
        Logger.debug("#{module}: can't poll for new file, path unset")
        {:noreply, [], state}

      # Doesn't exist. That means they tried, so let's warn them
      not File.exists?(file_path) ->
        Logger.warning(
          "File does not exist:  #{inspect(file_path)}. Can't ingest #{module} type feed."
        )

        {:noreply, [], state}

      # File should exist, let's give it a whirl
      true ->
        {:ok, lines} =
          File.open(file_path, [:read], fn file_handle ->
            pull(file_handle, module, state[:args] || %{})
          end)

        {:noreply, lines, state}
    end
  end

  defp pull(file_handle, module, args) do
    now_sec = DateTime.utc_now() |> DateTime.truncate(:second)

    {:ok, version_str_raw} = module.get_version(file_handle)
    version_str = String.replace("#{module}::", "Elixir.", "") <> version_str_raw

    pre_existing_header =
      from(fv in FeedVersion, where: fv.version == ^version_str) |> Repo.one()

    if is_nil(pre_existing_header) do
      Logger.info("#{module} static file version not ingested yet, will start: #{version_str}")

      Repo.insert_all(
        FeedVersion,
        [
          %{
            feed_name: "#{module}",
            version: version_str,
            started_at: now_sec,
            inserted_at: now_sec,
            updated_at: now_sec
          }
        ],
        conflict_target: :version,
        on_conflict: {:replace_all_except, [:inserted_at]}
      )

      module.read!(file_handle, args)
    else
      Logger.info("#{module} static file already version #{version_str}, won't update")
      []
    end
  end

  def handle_demand(_count, state) do
    {:noreply, [], state}
  end
end
