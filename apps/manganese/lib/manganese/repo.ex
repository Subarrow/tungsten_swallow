defmodule Manganese.Repo do
  use Ecto.Repo,
    otp_app: :manganese,
    adapter: Ecto.Adapters.Postgres
end
