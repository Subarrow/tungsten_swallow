defmodule Manganese.Repo.Migrations.AddDeferrableToConstraints do
  use Ecto.Migration

  def up do
    # darwin_schedule_location
    execute "ALTER TABLE darwin_schedule_location ALTER CONSTRAINT darwin_schedule_location_rid_fkey DEFERRABLE INITIALLY DEFERRED"
    execute "ALTER TABLE darwin_schedule_location ALTER CONSTRAINT darwin_schedule_location_tiploc_fkey DEFERRABLE INITIALLY DEFERRED"
    execute "ALTER TABLE darwin_schedule_location ALTER CONSTRAINT darwin_schedule_location_false_destination_fkey DEFERRABLE INITIALLY DEFERRED"
    # darwin_train_status_location
    execute "ALTER TABLE darwin_train_status_location ALTER CONSTRAINT darwin_train_status_location_rid_fkey DEFERRABLE INITIALLY DEFERRED"
    execute "ALTER TABLE darwin_train_status_location ALTER CONSTRAINT darwin_train_status_location_tiploc_fkey DEFERRABLE INITIALLY DEFERRED"
    # darwin_time
    execute "ALTER TABLE darwin_time ALTER CONSTRAINT darwin_time_rid_fkey DEFERRABLE INITIALLY DEFERRED"
    execute "ALTER TABLE darwin_time ALTER CONSTRAINT darwin_time_tiploc_fkey DEFERRABLE INITIALLY DEFERRED"
  end

  def down do
    # darwin_schedule_location
    execute "ALTER TABLE darwin_schedule_location ALTER CONSTRAINT darwin_schedule_location_rid_fkey NOT DEFERRABLE"
    execute "ALTER TABLE darwin_schedule_location ALTER CONSTRAINT darwin_schedule_location_tiploc_fkey NOT DEFERRABLE"
    execute "ALTER TABLE darwin_schedule_location ALTER CONSTRAINT darwin_schedule_location_false_destination_fkey NOT DEFERRABLE"
    # darwin_train_status_location
    execute "ALTER TABLE darwin_train_status_location ALTER CONSTRAINT darwin_train_status_location_rid_fkey NOT DEFERRABLE"
    execute "ALTER TABLE darwin_train_status_location ALTER CONSTRAINT darwin_train_status_location_tiploc_fkey NOT DEFERRABLE"
    # darwin_time
    execute "ALTER TABLE darwin_time ALTER CONSTRAINT darwin_time_rid_fkey NOT DEFERRABLE"
    execute "ALTER TABLE darwin_time ALTER CONSTRAINT darwin_time_tiploc_fkey NOT DEFERRABLE"
  end
end
