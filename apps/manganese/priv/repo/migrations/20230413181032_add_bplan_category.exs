defmodule Manganese.Repo.Migrations.AddBPLANCategory do
  use Ecto.Migration

  def up do
    create table("bplan_category", primary_key: false) do
      add :category_code, :string, null: false, primary_key: true
      add :text, :string, null: false
      timestamps()
    end
  end

  def down do
    drop table("bplan_category")
  end
end
