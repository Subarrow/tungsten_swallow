defmodule Manganese.Repo.Migrations.AddKbPromotionTable do
  use Ecto.Migration

  def up do
    create table("kb_promotion", primary_key: false) do
      add :promotion_id, :string, null: false, primary_key: true
      add :type, :string, null: false
      add :name, :string, null: false
      add :summary, :text, null: false

      add :offer_urls, {:array, :string}, null: true
      add :offer_description, :text, null: true
      add :further_information_urls, {:array, :string}, null: true
      add :further_information_description, :text, null: true

      add :validity_start, :date, null: true
      add :validity_end, :date, null: true

      add :ticket_name, :string, null: true
      add :promotion_code, :string, null: true

      add :lead_operator_code, :string, null: true
      add :applicable_operators, {:array, :string}, null: true

      add :nearest_stations, {:array, :string}, null: true
      add :applicable_origins, {:array, :string}, null: true
      add :applicable_destinations, {:array, :string}, null: true
      add :applicable_zone, {:array, :string}, null: true

      timestamps()
    end

    create index("kb_promotion", :updated_at)
    create index("kb_promotion", :promotion_code)
    create index("kb_promotion", :nearest_stations, using: "GIN")
    create index("kb_promotion", :applicable_origins, using: "GIN")
    create index("kb_promotion", :applicable_destinations, using: "GIN")
    create index("kb_promotion", :applicable_zone, using: "GIN")
  end

  def down do
    drop table("kb_promotion")
  end
end
