defmodule Manganese.Repo.Migrations.DarwinMandatoryLocationField do
  use Ecto.Migration

  def up do
    alter table "darwin_location" do
      add :mandatory, :string, null: true
    end
  end

  def down do
    alter table "darwin_location" do
      remove :mandatory, :string, null: true
    end
  end
end
