defmodule Manganese.Repo.Migrations.AddSnapshotVersionTable do
  use Ecto.Migration

  def up do
    create table("darwin_snapshot_version", primary_key: false) do
      add :ftp_line, :string, null: false, primary_key: true
      add :started_at, :timestamp, null: true
      add :finished_at, :timestamp, null: true
      add :remarks, :text, null: true
      timestamps()
    end
  end

  def down do
    drop table("darwin_snapshot_version")
  end
end
