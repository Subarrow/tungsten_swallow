defmodule Manganese.Repo.Migrations.AddDarwinAssociationTable do
  use Ecto.Migration

  def up do
    create table("darwin_association", primary_key: false) do
      add :main_fq_wt_match, references("darwin_schedule_location", column: :fq_wt_match, type: :string, on_delete: :delete_all), null: false, primary_key: true
      add :assoc_fq_wt_match, references("darwin_schedule_location", column: :fq_wt_match, type: :string, on_delete: :delete_all), null: false, primary_key: true

      add :main_rid, references("darwin_schedule", column: :rid, type: :string, on_delete: :delete_all), null: false
      add :assoc_rid, references("darwin_schedule", column: :rid, type: :string, on_delete: :delete_all), null: false

      add :tiploc, references("darwin_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false, primary_key: true

      add :category, :string, null: false
      add :is_cancelled, :boolean, null: false, default: false
      add :is_deleted, :boolean, null: false, default: false

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_association")
  end
end
