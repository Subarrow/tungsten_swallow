defmodule Manganese.Repo.Migrations.AddLocationTable do
  use Ecto.Migration

  def up do
    create table("darwin_location", primary_key: false) do
      add :tiploc, :string, primary_key: true
      add :crs, :string, null: true
      add :operator, :string, null: true
      add :name, :string, null: true
      add :category, :string, null: true
      add :time_zone, :string, null: false, default: "Europe/London"
      timestamps()
    end
  end

  def down do
    drop table("darwin_location")
  end
end
