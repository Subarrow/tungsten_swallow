defmodule Manganese.Repo.Migrations.AddDarwinTimeTable do
  use Ecto.Migration

  def up do
    create table("darwin_time", primary_key: false) do
      add :time_id, :string, null: false, primary_key: true
      add :fq_wt_match, references(:darwin_schedule_location, column: :fq_wt_match, type: :string, on_delete: :delete_all), null: false
      add :rid, references(:darwin_schedule, column: :rid, type: :string, on_delete: :delete_all), null: false
      add :tiploc, references(:darwin_location, column: :tiploc, type: :string, on_delete: :delete_all), null: false
      add :timetable, :string, null: false
      add :event, :string, null: false
      add :type, :string, null: true

      add :minimum, :timestamp, null: true
      add :time, :timestamp, null: true

      add :original_time, :time, null: true
      add :naive_time, :timestamp, null: true

      add :is_delayed, :boolean, null: false, default: false
      add :is_unknown_delay, :boolean, null: false, default: false

      add :actual_time_class, :string, null: true
      add :source, :string, null: true
      add :cis_source, :string, null: true

      add :darwin_ts, :timestamp, size: 6, null: false
      timestamps()
    end
  end

  def down do
    drop table("darwin_time")
  end
end
