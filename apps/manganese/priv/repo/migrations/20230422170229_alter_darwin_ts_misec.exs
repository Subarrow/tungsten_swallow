defmodule Manganese.Repo.Migrations.AlterDarwinTSMiSec do
  use Ecto.Migration

  def up do
    alter table("darwin_schedule") do
      modify :darwin_ts, :timestamp, size: 6
      add :darwin_request_id, :string, null: true
      add :cis_source, :string
      add :source, :string
    end
    alter table("darwin_schedule_location") do
      modify :darwin_ts, :timestamp, size: 6
      add :darwin_request_id, :string, null: true
      add :cis_source, :string
      add :source, :string
    end
  end

  def down do
    alter table("darwin_schedule") do
      modify :darwin_ts, :timestamp, size: 0
      remove :darwin_request_id
      remove :cis_source
      remove :source
    end
    alter table("darwin_schedule_location") do
      modify :darwin_ts, :timestamp, size: 0
      remove :darwin_request_id
      remove :cis_source
      remove :source
    end
  end
end
