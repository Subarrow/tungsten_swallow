defmodule Manganese.Repo.Migrations.BplanVersionToFeedVersion do
  use Ecto.Migration

  import Ecto.Query

  def up do
    alter table("bplan_version") do
      add :feed_name, :string, null: false, default: "BPLAN"
      add :feed_date, :date, null: true
    end

    rename table("bplan_version"), :header_line, to: :version
    rename table("bplan_version"), to: table("feed_version")

    alter table("feed_version") do
      modify :feed_name, :string, null: false
    end

    execute("UPDATE feed_version SET version='BPLAN::' || version WHERE version NOT ILIKE 'BPLAN::%';")
  end

  def down do
    from(fv in "feed_version", where: fv.feed_name != "BPLAN") |> Manganese.Repo.delete_all()

    rename table("feed_version"), to: table("bplan_version")
    rename table("bplan_version"), :version, to: :header_line

    alter table("bplan_version") do
      remove :feed_name
      remove :feed_date
    end
  end
end
