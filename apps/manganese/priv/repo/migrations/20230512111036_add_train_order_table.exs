defmodule Manganese.Repo.Migrations.AddTrainOrderTable do
  use Ecto.Migration

  def up do
    create table("darwin_train_order", primary_key: false) do
      add :crs, :string, null: false, primary_key: true
      add :platform, :string, null: false, primary_key: true
      add :trains, {:array, :string}, null: false

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_train_order")
  end
end
