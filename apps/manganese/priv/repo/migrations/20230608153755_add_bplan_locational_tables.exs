defmodule Manganese.Repo.Migrations.AddBPLANLocationalTables do
  use Ecto.Migration

  def up do
    create table("bplan_location", primary_key: false) do
      add :tiploc, :string, primary_key: true
      add :name, :string, null: false
      add :timing_point_type, :string, null: false
      add :stanox, :string, null: true
      add :is_off_network, :boolean, null: false
      add :force_lpb, :string, null: true
      timestamps()
    end

    create index("bplan_location", :stanox)
    create index("bplan_location", :timing_point_type)
    create index("bplan_location", :name)

    create table("bplan_timing_link", primary_key: false) do
      add :origin_tiploc, references("bplan_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false, primary_key: true
      add :destination_tiploc, references("bplan_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false, primary_key: true
      add :running_line_code, :string, null: true, primary_key: true
      add :traction_type, :string, null: false, primary_key: true
      add :trailing_load, :string, null: true, primary_key: true
      add :speed, :smallint, null: true, primary_key: true
      add :route_allowance, :string, null: true, primary_key: true
      add :entry_speed, :integer, null: true, primary_key: true
      add :exit_speed, :integer, null: true, primary_key: true
      add :start_date, :date, null: false, primary_key: true
      add :end_date, :date, null: true, primary_key: true
      add :running_time, :integer, null: false
      add :description, :string, null: true
      timestamps()
    end

    create table("bplan_network_link", primary_key: false) do
      add :origin_tiploc, references("bplan_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false
      add :destination_tiploc, references("bplan_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false
      add :running_line_code, :string, null: true
      add :running_line_description, :string, null: true
      add :start_date, :date, null: false
      add :end_date, :date, null: true
      add :initial_direction, :string, null: false
      add :final_direction, :string, null: true
      add :distance, :integer, null: true
      add :is_doo_passenger, :boolean, null: false
      add :is_doo_non_passenger, :boolean, null: false
      add :is_radio_electric_token_block, :boolean, null: false
      add :zone, :string, null: false
      add :reversible_line, :string, null: false
      add :power_supply_type, :string, null: true
      add :route_availability, :string, null: true
      add :maximum_train_length, :integer, null: true

      timestamps()
    end

    create unique_index("bplan_network_link", [:origin_tiploc, :destination_tiploc, :running_line_code])
    create index("bplan_network_link", :origin_tiploc)
    create index("bplan_network_link", :destination_tiploc)
    create index("bplan_network_link", :running_line_code)
    create index("bplan_network_link", :reversible_line)
    create index("bplan_network_link", :route_availability)

    create table("bplan_version", primary_key: false) do
      add :header_line, :string, null: false, primary_key: true
      add :started_at, :utc_datetime, null: true
      add :finished_at, :utc_datetime, null: true
      add :remarks, :text, null: true
      timestamps()
    end
  end

  def down do
    drop index("bplan_location", :stanox)
    drop index("bplan_location", :timing_point_type)
    drop index("bplan_location", :name)

    drop unique_index("bplan_network_link", [:origin_tiploc, :destination_tiploc, :running_line_code])
    drop index("bplan_network_link", :origin_tiploc)
    drop index("bplan_network_link", :destination_tiploc)
    drop index("bplan_network_link", :running_line_code)
    drop index("bplan_network_link", :reversible_line)
    drop index("bplan_network_link", :route_availability)

    drop table("bplan_version")
    drop table("bplan_timing_link")
    drop table("bplan_network_link")
    drop table("bplan_location")
  end
end
