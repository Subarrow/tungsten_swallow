defmodule Manganese.Repo.Migrations.AddBplanLocationDateFields do
  use Ecto.Migration

  def up do
    alter table "bplan_location" do
      add :start_date, :date, null: true
      add :end_date, :date, null: true
    end
  end

  def down do
    alter table "bplan_location" do
      remove :start_date
      remove :end_date
    end
  end
end
