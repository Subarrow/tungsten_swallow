defmodule Manganese.Repo.Migrations.AddScheduleTable do
  use Ecto.Migration

  def up do
    create table("darwin_schedule", primary_key: false) do
      add :rid, :string, null: false, primary_key: true
      add :ssd, :date, null: false
      add :uid, :string, null: false
      add :rsid, :string, null: true
      add :signalling_id, :string, null: false
      add :train_number, :string, null: true
      add :status_code, references(:bplan_status, column: :status_code, type: :string), null: false
      add :category_code, references(:bplan_category, column: :category_code, type: :string), null: false
      add :operator_code, :string, null: true
      add :is_passenger, :boolean, null: false
      add :is_active, :boolean, null: false, default: false
      add :is_deleted, :boolean, null: false, default: false
      add :is_charter, :boolean, null: false, default: false
      add :cancel_reason_code, references(:darwin_disruption_reason, column: :reason_code, type: :string), null: true
      add :cancel_reason_is_near, :boolean, null: true
      add :cancel_reason_tiploc, references(:darwin_location, column: :tiploc, type: :string), null: true
      add :darwin_ts, :timestamp, null: false
      timestamps()
    end
  end

  def down do
    drop table("darwin_schedule")
  end
end
