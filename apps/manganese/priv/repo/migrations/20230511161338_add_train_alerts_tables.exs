defmodule Manganese.Repo.Migrations.AddTrainAlertsTables do
  use Ecto.Migration

  def up do
    create table("darwin_train_alert", primary_key: false) do
      add :alert_id, :string, null: false, primary_key: true
      add :message, :text, null: false

      add :can_send_by_sms, :boolean, null: false
      add :can_send_by_email, :boolean, null: false
      add :can_send_by_twitter, :boolean, null: false

      add :alert_source, :string, null: false
      add :audience, :string, null: false

      add :is_forced, :boolean, null: false

      add :copied_alert_id, :string, null: true
      add :copied_alert_source, :string, null: true

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end

    create table("darwin_train_alert_service_station", primary_key: false) do
      add :alert_id, references("darwin_train_alert", column: :alert_id, type: :string, on_delete: :delete_all), primary_key: true
      add :crs, :string, nullable: false, primary_key: true
      add :rid, :string, nullable: false, primary_key: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_train_alert")
    drop table("darwin_train_alert_service_station")
  end
end
