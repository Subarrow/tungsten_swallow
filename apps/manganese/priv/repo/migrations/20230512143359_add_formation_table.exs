defmodule Manganese.Repo.Migrations.AddFormationTable do
  use Ecto.Migration

  def up do
    create table("darwin_formation", primary_key: false) do
      add :formation_id, :string, null: false, primary_key: true
      add :rid, references("darwin_schedule", column: :rid, type: :string, on_delete: :delete_all), null: false

      add :length, :integer, null: false
      add :unit_length, :integer, null: true

      add :coaches, {:array, :jsonb}, null: false
      add :has_toilet, :boolean, null: false

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_formation")
  end
end
