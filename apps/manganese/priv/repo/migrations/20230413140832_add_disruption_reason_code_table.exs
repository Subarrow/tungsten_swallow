defmodule Manganese.Repo.Migrations.AddDarwinDisruptionReasonTable do
  use Ecto.Migration

  def up do
    create table("darwin_disruption_reason", primary_key: false) do
      add :reason_code, :string, null: false, primary_key: true
      add :text, :string, null: false
      timestamps()
    end
  end

  def down do
    drop table("darwin_disruption_reason")
  end
end
