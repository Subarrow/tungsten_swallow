defmodule Manganese.Repo.Migrations.AddDarwinTrainStatusLocationTable do
  use Ecto.Migration

  def up do
    create table("darwin_train_status_location", primary_key: false) do
      add :fq_wt_match, references("darwin_schedule_location", column: :fq_wt_match, type: :string, on_delete: :delete_all), null: false, primary_key: true
      add :rid, references("darwin_schedule", column: :rid, type: :string, on_delete: :delete_all), null: false
      add :tiploc, references("darwin_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false
      add :is_suppressed, :boolean, default: false, null: false
      add :length, :integer, null: true
      add :is_front_detaching, :boolean, default: :false, null: false

      add :platform, :string, null: true
      add :platform_is_confirmed, :boolean, default: false, null: false
      add :platform_source, :string, null: true
      add :platform_suppression, :string, null: true

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_train_status_location")
  end
end
