defmodule Manganese.Repo.Migrations.AddDarwinScheduleLocationTable do
  use Ecto.Migration

  def up do
    create table("darwin_schedule_location", primary_key: false) do
      add :fq_wt_match, :string, primary_key: true
      add :rid, references(:darwin_schedule, column: :rid, type: :string), null: false
      add :index, :integer, null: false
      add :type, :char, null: false
      add :is_operational, :boolean, null: false, default: false
      add :tiploc, references(:darwin_location, column: :tiploc, type: :string)

      add :public_arrive, :timestamp, null: true
      add :public_depart, :timestamp, null: true
      add :working_arrive, :timestamp, null: true
      add :working_pass, :timestamp, null: true
      add :working_depart, :timestamp, null: true

      add :naive_arrive, :timestamp, null: true
      add :naive_pass, :timestamp, null: true
      add :naive_depart, :timestamp, null: true

      add :activity, :string, null: true
      add :planned_activity, :string, null: true
      add :is_cancelled, :boolean, null: false, default: false
      add :false_destination, references(:darwin_location, column: :tiploc, type: :string), null: true
      add :formation_id, :string, null: true
      add :average_loading, :smallint, null: true
      add :route_delay, :integer, null: true
      add :darwin_ts, :timestamp, null: false
      timestamps()
    end
  end

  def down do
    drop table("darwin_schedule_location")
  end
end
