defmodule Manganese.Repo.Migrations.AddZonesColToDarwinLocation do
  use Ecto.Migration

  def up do
    alter table "darwin_location" do
      add :zones, {:array, :string}, null: false, default: []
    end
  end

  def down do
    alter table "darwin_location" do
      remove :zones
    end
  end
end
