defmodule Manganese.Repo.Migrations.AddCisSourceTable do
  use Ecto.Migration

  def up do
    create table("darwin_cis_source", primary_key: false) do
      add :cis_source, :string, null: false, primary_key: true
      add :name, :string, null: false
      timestamps()
    end
  end

  def down do
    drop table("darwin_cis_source")
  end
end
