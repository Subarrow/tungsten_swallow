defmodule Manganese.Repo.Migrations.AddBPLANStatus do
  use Ecto.Migration

  def up do
    create table("bplan_status", primary_key: false) do
      add :status_code, :string, null: false, primary_key: true
      add :text, :string, null: false
      timestamps()
    end
  end

  def down do
    drop table("bplan_status")
  end
end
