defmodule Manganese.Repo.Migrations.AddOperatorCodeTable do
  use Ecto.Migration

  def up do
    create table("darwin_operator", primary_key: false) do
      add :operator_code, :string, null: false, primary_key: true
      add :name, :string, null: false
      add :url, :string, null: false
      add :category, :string, null: true, default: nil
      timestamps()
    end
  end

  def down do
    drop table("darwin_operator")
  end
end
