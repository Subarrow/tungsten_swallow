defmodule Manganese.Repo.Migrations.AddMissingIndexes do
  use Ecto.Migration

  @indexes [
    {"darwin_location", [:crs, :operator, :name, :category, :time_zone]},
    {"darwin_operator", [:category]},
    {"darwin_schedule",
      [:rid, :ssd, :uid, :rsid, :signalling_id, :train_number, :status_code,
      :category_code, :operator_code, :is_passenger, :is_active,
      :is_deleted, :is_charter, :cancel_reason_code, :cancel_reason_is_near,
      :cancel_reason_tiploc]
    },
    {"darwin_schedule_location",
      [:rid, :index, :type, :is_operational, :tiploc,
      :public_arrive, :public_depart,
      :working_arrive, :working_pass, :working_depart]
    },
    {"darwin_train_status",
      [:delay_reason_code, :delay_reason_is_near, :delay_reason_tiploc]
    },
    {"darwin_time",
      [:fq_wt_match, :rid, :tiploc, :timetable, :event, :type,
      :minimum, :time]
    },
    {"darwin_train_status_location",
      [:fq_wt_match, :rid, :tiploc, :length, :platform,
      :platform_is_confirmed]
    },
    {"darwin_association",
      [:main_fq_wt_match, :assoc_fq_wt_match, :main_rid, :assoc_rid, :tiploc,
      :category, :is_cancelled, :is_deleted]
    },
    {"darwin_station_message",
      [:category, :severity, :is_suppressed, :incident_url]
    },
    {"darwin_station_message_station",
      [:message_id, :crs]},
    {"darwin_train_alert",
      [:message, :alert_source, :audience, :copied_alert_id,
      :copied_alert_source]},
    {"darwin_train_alert_service_station", [:alert_id, :crs, :rid]},
    {"darwin_train_order", [:crs, :platform]},
    {"darwin_formation_loading", [:rid, :tiploc, :formation_id, :length]},
    {"darwin_formation", [:rid, :length, :unit_length, :has_toilet]}
  ]

  defp up_index(table_name, index_fields) do
    Enum.each(index_fields, fn idx_name ->
      create index(table_name, [idx_name]) end)
  end

  defp down_index(table_name, index_fields) do
    Enum.each(index_fields, fn idx_name ->
      drop index(table_name, [idx_name]) end)
  end

  def up do
    Enum.each(@indexes, fn {tabname, columns} -> up_index(tabname, columns) end)
  end

  def down do
    Enum.each(@indexes, fn {tabname, columns} -> down_index(tabname, columns) end)
  end
end
