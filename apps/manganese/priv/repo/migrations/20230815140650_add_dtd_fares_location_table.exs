defmodule Manganese.Repo.Migrations.AddDtdFaresLocationTable do
  use Ecto.Migration

  @table_name "dtd_fares_location"

  def up do
    create table(@table_name, primary_key: false) do
      add :uic_code, :string
      add :end_date, :date
      add :start_date, :date
      add :quote_date, :date
      add :admin_area_code, :string
      add :nlc_code, :string
      add :description, :string
      add :crs_code, :string, null: true
      add :resv_code, :string
      add :ers_country, :string, null: true
      add :ers_code, :string, null: true
      add :fare_group, :string
      add :county, :string
      add :pte_zone, :string, null: true
      add :zone_no, :string, null: true
      add :zone_ind, :string, null: true
      add :region, :string
      add :hierarchy, :string, null: true
      add :cc_desc_out, :string
      add :cc_desc_retn, :string
      add :atb_desc_out, :string
      add :atb_desc_retn, :string
      add :special_facilities, :string
      add :lul_direction_ind, :string
      add :lul_uts_mode, :string, null: true
      add :lul_zone_1_6, {:array, :boolean}, null: true
      add :lul_uts_london_station, :string
      add :uts_code, :string
      add :uts_a_code, :string
      add :uts_ptr_bias, :string
      add :uts_offset, :string
      add :uts_north, :string
      add :uts_east, :string
      add :uts_south, :string
      add :uts_west, :string
      timestamps()
    end

    create index(@table_name, :uic_code)
    create index(@table_name, :end_date)
    create index(@table_name, :start_date)
    create index(@table_name, :quote_date)
    create index(@table_name, :nlc_code)
    create index(@table_name, :resv_code)
    create index(@table_name, :ers_country)
    create index(@table_name, :ers_code)
    create index(@table_name, :fare_group)
    create index(@table_name, :county)

    create index(@table_name, :updated_at)

    create unique_index(@table_name, [:uic_code, :end_date])
  end

  def down do
    drop table(@table_name)
  end
end
