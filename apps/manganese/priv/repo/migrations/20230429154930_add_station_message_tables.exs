defmodule Manganese.Repo.Migrations.AddStationMessageTables do
  use Ecto.Migration

  def up do
    create table("darwin_station_message", primary_key: false) do
      add :message_id, :string, null: false, primary_key: true
      add :message, :text, null: false
      add :category, :string, null: false
      add :severity, :smallint, null: false
      add :is_suppressed, :boolean, null: false, default: false

      add :incident_url, :string, null: true

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end

    create table("darwin_station_message_station", primary_key: false) do
      add :message_id, references("darwin_station_message", column: :message_id, type: :string, on_delete: :delete_all), primary_key: true
      add :crs, :string, nullable: false, primary_key: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_station_message_station")
    drop table("darwin_station_message")
  end
end
