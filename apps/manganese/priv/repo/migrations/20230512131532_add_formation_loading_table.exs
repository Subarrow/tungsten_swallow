defmodule Manganese.Repo.Migrations.AddFormationLoadingTable do
  use Ecto.Migration

  def up do
    create table("darwin_formation_loading", primary_key: false) do
      add :fq_wt_match, references("darwin_schedule_location", column: :fq_wt_match, type: :string, on_delete: :delete_all), null: false, primary_key: true
      add :rid, references("darwin_schedule", column: :rid, type: :string, on_delete: :delete_all), null: false
      add :tiploc, references("darwin_location", column: :tiploc, type: :string, on_delete: :delete_all), null: false
      add :formation_id, :string, null: false
      add :coach_numbers, {:array, :string}, null: false
      add :coach_loading, {:array, :integer}, null: false

      add :minimum_loading, :integer, null: false
      add :maximum_loading, :integer, null: false
      add :average_loading, :integer, null: false

      add :length, :integer, null: false

      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false

      timestamps()
    end
  end

  def down do
    drop table("darwin_formation_loading")
  end
end
