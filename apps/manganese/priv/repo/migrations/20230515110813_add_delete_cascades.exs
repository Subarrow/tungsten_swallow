defmodule Manganese.Repo.Migrations.AddDeleteCascades do
  use Ecto.Migration

  def up do
    execute """
      ALTER TABLE darwin_train_status
      DROP CONSTRAINT darwin_train_status_rid_fkey,
      ADD CONSTRAINT darwin_train_status_rid_fkey
        FOREIGN KEY (rid)
        REFERENCES darwin_schedule (rid)
        ON DELETE CASCADE;
      """

      execute """
      ALTER TABLE darwin_schedule_location
      DROP CONSTRAINT darwin_schedule_location_rid_fkey,
      ADD CONSTRAINT darwin_schedule_location_rid_fkey
        FOREIGN KEY (rid)
        REFERENCES darwin_schedule (rid)
        ON DELETE CASCADE;
      """
  end

  def down do
    execute """
    ALTER TABLE darwin_train_status
    DROP CONSTRAINT darwin_train_status_rid_fkey,
    ADD CONSTRAINT darwin_train_status_rid_fkey
      FOREIGN KEY (rid)
      REFERENCES darwin_schedule (rid);
    """

    execute """
      ALTER TABLE darwin_schedule_location
      DROP CONSTRAINT darwin_schedule_location_rid_fkey,
      ADD CONSTRAINT darwin_schedule_location_rid_fkey
        FOREIGN KEY (rid)
        REFERENCES darwin_schedule (rid);
      """
  end
end
