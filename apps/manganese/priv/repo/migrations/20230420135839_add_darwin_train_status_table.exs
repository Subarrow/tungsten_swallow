defmodule Manganese.Repo.Migrations.AddDarwinTrainStatusTable do
  use Ecto.Migration

  def up do
    create table("darwin_train_status", primary_key: false) do
      add :rid, references(:darwin_schedule, column: :rid, type: :string), null: false, primary_key: true
      add :is_reverse_formation, :boolean, null: false, default: false
      add :delay_reason_code, references(:darwin_disruption_reason, column: :reason_code, type: :string), null: true
      add :delay_reason_is_near, :boolean, null: true
      add :delay_reason_tiploc, references(:darwin_location, column: :tiploc, type: :string), null: true
      add :source, :string, null: true
      add :cis_source, :string, null: true
      add :darwin_request_id, :string, null: true
      add :darwin_ts, :timestamp, size: 6, null: false
      timestamps()
    end
  end

  def down do
    drop table("darwin_train_status")
  end
end
