defmodule Manganese.Repo.Migrations.AddBplanNameFieldToDarwinLocation do
  use Ecto.Migration

  def up do
    alter table "darwin_location" do
      add :bplan_name, :string, null: true
    end
  end

  def down do
    alter table "darwin_location" do
      remove :bplan_name, :string, null: true
    end
  end
end
