defmodule Cobalt.MixProject do
  use Mix.Project

  def project do
    [
      app: :cobalt,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [plt_add_apps: [:mix]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :xmerl, :mnesia, :plug_cowboy, :ftp],
      mod: if(Mix.env() == :test, do: [], else: {Cobalt.Application, []})
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:barytherium, "~> 0.6.2"},
      {:tzdata, "~> 1.1"},
      {:erlsom, "~> 1.5"},
      {:esshd, "~> 0.2.1"},
      {:hackney, "~> 1.18.1"},
      {:plug_cowboy, "~> 2.6.0"},
      {:jason, "~> 1.4.0"},
      {:dialyxir, "~> 1.3", only: [:dev], runtime: false},
      {:credo, "~> 1.7.0-rc.1", only: [:dev, :test], runtime: false},
      {:bplan, in_umbrella: true},
      {:nre_feeds, in_umbrella: true},
      {:util, in_umbrella: true}
    ]
  end
end
