defmodule Cobalt.SSH.ShellHandler do
  use Sshd.ShellHandler

  alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations.Formation
  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus
  alias NREFeeds.Darwin.Schema.RealTime.FormationLoading

  alias Cobalt.Darwin.Store.Struct.ScheduleLocation
  alias Cobalt.Darwin.Store.Struct.ScheduleOutline
  alias Cobalt.Darwin.Store.Struct.StatusActualTime
  alias Cobalt.Darwin.Store.Struct.StatusEstimatedTime

  alias Cobalt.Darwin.Store.Table
  alias Cobalt.Darwin.Store.Table.DarwinRefLocation

  alias Cobalt.Darwin.Board.{Location, Service}

  def on_shell(username, _pubkey, address, port) do
    :ok = IO.puts("Hello, #{username}.")
    loop(%{username: username, address: address, port: port, counter: 0})
  end

  def on_connect(username, ip, port, _method) do
    Logger.info("SSH shell for #{inspect(username)} from #{inspect(ip)}:#{port}.")
  end

  def on_disconnect(username, ip, port) do
    Logger.debug(fn ->
      "Disconnecting SSH shell for #{username} from #{inspect(ip)}:#{port}"
    end)
  end

  defp loop(state = %{username: username, counter: counter}) do
    case IO.gets(:stdio, "#{username} (#{counter}) > ") do
      {:error, :interrupted} ->
        Logger.info("Interrupted, quitting.")

      line_in ->
        :ok = Logger.info("Line in: #{inspect(line_in)}")

        compose_board(String.trim(List.to_string(line_in)))

        loop(%{state | counter: counter + 1})
    end
  end

  defp pad_centre(string, count, padding \\ [" "], precedence \\ :leading)

  defp pad_centre(string, count, padding, :leading) do
    string
    |> String.pad_trailing(count - div(String.length(string), 2), padding)
    |> String.pad_leading(count, padding)
  end

  defp pad_centre(string, count, padding, :trailing) do
    string
    |> String.pad_leading(count - div(String.length(string), 2), padding)
    |> String.pad_trailing(count, padding)
  end

  def compose_service(in_line) do
    {:atomic, service} = :mnesia.transaction(fn -> get_service(in_line) end)

    render_non_board_service(service) |> IO.puts()
  end

  def compose_board(in_line) do
    # let's assume it's a TIPLOC for now lol
    ut_now = %{DateTime.utc_now() | second: 0} |> DateTime.to_unix()

    {:atomic, services} =
      :mnesia.transaction(fn ->
        get_tiploc_departures(in_line, ut_now..(ut_now + 3600 * 2))
      end)

    Enum.map_join(services, "\n\n", &render_board_service/1) |> IO.puts()
  end

  def render_non_board_service(
        service = %Service{
          schedule: %ScheduleOutline{},
          locations: locations
        }
      ) do
    render_service_header(service) <>
      "\n\n" <> Enum.map_join(locations, "\n", &render_non_board_service_location/1)
  end

  def render_non_board_service_location(
        board_location = %Location{
          tiploc: tiploc,
          status: status,
          schedule_location: %ScheduleLocation{
            cancelled?: cancelled?,
            working_times_utc: working_times_utc,
            activity: activity,
            type: type
          },
          reference: %DarwinRefLocation{crs: _crs, names: {:darwin, darwin_name, _names_local}},
          formation_loading: formation_loading,
          formation: formation,
          status_times_utc: status_times_utc
        },
        suppress_times \\ false,
        suppress_formation_loading \\ false,
        suppress_false_destination \\ false
      ) do
    status_stuff = get_status_stuff(status)

    case type do
      :origin -> "O"
      :destination -> "D"
      _ -> "~"
    end <>
      " " <>
      (if suppress_times do
         " "
       else
         working_times_utc |> format_times() |> Enum.join(" ")
       end
       |> pad_centre(11)) <>
      " " <>
      cond do
        cancelled? ->
          "(cancelled) "

        suppress_times ->
          "            "

        true ->
          Enum.join(format_status_times(status_times_utc), "")
          |> pad_centre(12, [" "], :trailing)
      end <>
      String.pad_trailing(activity || "", 4, " ") <>
      status_stuff.platform <>
      String.pad_trailing(tiploc, 8, " ") <>
      String.pad_trailing("#{darwin_name}", 30) <>
      " " <>
      String.pad_leading(status_stuff.length, 2) <>
      if suppress_false_destination do
        ""
      else
        render_false_destination(board_location)
      end <>
      if suppress_formation_loading do
        ""
      else
        render_formation(formation) <> render_formation_loading(formation_loading)
      end
  end

  def render_service_header(%Service{
        schedule: %ScheduleOutline{
          rid: rid,
          ssd: ssd,
          uid: uid,
          signalling_id: signalling_id,
          rsid: rsid,
          operator: operator,
          category: category,
          status: status,
          active?: active?,
          charter?: charter?
        }
      }) do
    flags =
      [active: active?, charter: charter?]
      |> Enum.filter(fn {_k, v} -> v end)
      |> Enum.map_join(", ", fn {k, _v} -> ":#{k}" end)

    "#{rid} (#{uid}/#{Date.to_iso8601(ssd)}) - #{signalling_id} - (#{rsid || "--------"}) " <>
      "#{operator} #{category} #{status} " <>
      "#{flags}"
  end

  def render_board_service(
        service = %Service{
          locations: locations,
          here: here
        }
      ) do
    render_service_header(service) <>
      "\n" <>
      (locations
       |> Enum.filter(fn l ->
         l.schedule_location.type in [:origin, :destination] or l == service.here
       end)
       |> Enum.map_join(
         "\n",
         &render_non_board_service_location(
           &1,
           &1 != here,
           &1 != here,
           &1 != here
         )
       ))
  end

  def render_false_destination(%Location{false_destination_tiploc: nil}), do: ""

  def render_false_destination(%Location{
        false_destination_tiploc: tiploc,
        false_destination_reference: false_dest_ref
      }) do
    case false_dest_ref do
      %DarwinRefLocation{
        tiploc: tiploc,
        names: {_, darwin_name, _local_names}
      } ->
        "\n^                        (FD)      #{String.pad_trailing(tiploc, 8, " ")}#{darwin_name}"

      nil ->
        "\n^                        (FD)      #{tiploc}"
    end
  end

  def render_toilet(nil) do
    ""
  end

  def render_toilet(%Formation.Coach.Toilet{
        type: type,
        status: status
      }) do
    case type do
      "Standard" -> "L"
      "Accessible" -> "A"
      "Unknown" -> "?"
      "None" -> " "
    end <>
      case status do
        :in_service -> "."
        :not_in_service -> "*"
        :unknown when type != "None" -> "?"
        :unknown when type == "None" -> " "
      end
  end

  def render_formation(nil) do
    ""
  end

  def render_formation(%Formation{coaches: coaches}) do
    coaches
    |> Enum.map(fn %Formation.Coach{
                     coach_number: coach_number,
                     coach_class: coach_class,
                     toilet: toilet
                   } ->
      {coach_number,
       String.pad_leading("#{String.first(coach_class || "-") <> render_toilet(toilet)}", 3)}
    end)
    |> render_unit_groups()
  end

  def render_formation_loading(nil) do
    ""
  end

  def render_formation_loading(%FormationLoading{loading: loadings}) do
    loadings
    |> Enum.map(fn %FormationLoading.CoachLoading{
                     coach_number: coach_number,
                     loading: loading
                   } ->
      {coach_number, String.pad_leading("#{loading}", 3)}
    end)
    |> render_unit_groups()
  end

  def render_unit_groups(coaches) do
    {_, out} =
      Enum.reduce(coaches, {nil, ""}, fn {coach_number, coach_text}, {last_coach_num, so_far} ->
        entry =
          case {last_coach_num, coach_number} do
            {nil, _} ->
              ""

            {<<last_unit::integer, _::binary>>, <<current_unit::integer, _::binary>>}
            when last_unit in ?A..?Z and current_unit in ?A..?Z and last_unit != current_unit ->
              ","

            _ ->
              " "
          end <> coach_text

        {coach_number, so_far <> entry}
      end)

    "\n^ [" <> out <> "]"
  end

  def get_status_stuff(nil),
    do: %{
      platform: "     ",
      length: ""
    }

  def get_status_stuff(status),
    do: %{
      platform: get_platform(status.platform),
      length:
        if is_nil(status.length) do
          ""
        else
          Integer.to_string(status.length)
        end
    }

  def get_platform(nil), do: "     "

  def get_platform(%TrainStatus.Platform{
        suppressed: suppressed,
        confirmed?: confirmed?,
        platform: platform
      }) do
    prefix = if suppressed, do: "*", else: " "
    suffix = if confirmed?, do: ".", else: " "

    String.pad_leading(prefix <> platform, 4, " ") <> suffix
  end

  def format_status_times(nil) do
    ["            "]
  end

  def format_status_times(time_map) do
    cond do
      # Bugged case (has pass but also either arrive/depart)
      Map.has_key?(time_map, :pass) and
          (Map.has_key?(time_map, :arrive) or Map.has_key?(time_map, :depart)) ->
        format_status_times(time_map, [:arrive, :pass, :depart])

      # Normal pass
      Map.has_key?(time_map, :pass) ->
        format_status_times(time_map, [:pass])

      # Anything else (including empty)
      true ->
        format_status_times(time_map, [:arrive, :depart])
    end
  end

  def format_status_times(time_map, keys) do
    Enum.map(keys, fn key -> Map.get(time_map, key) end)
    |> Enum.map(fn
      nil ->
        "      "

      %StatusEstimatedTime{time: time} ->
        "~" <> (DateTime.to_time(time) |> time_to_string(:truncated)) <> " "

      %StatusActualTime{time: time} ->
        " " <> (DateTime.to_time(time) |> time_to_string(:truncated)) <> "."
    end)
  end

  def format_times(time_map) do
    cond do
      # Bugged case (has pass but also either arrive/depart)
      Map.has_key?(time_map, :pass) and
          (Map.has_key?(time_map, :arrive) or Map.has_key?(time_map, :depart)) ->
        format_times(time_map, [:arrive, :pass, :depart])

      # Normal pass
      Map.has_key?(time_map, :pass) ->
        format_times(time_map, [:pass])

      # Anything else (including empty)
      true ->
        format_times(time_map, [:arrive, :depart])
    end
  end

  def format_times(time_map, keys) do
    Enum.map(keys, fn key -> Map.get(time_map, key) end)
    |> Enum.map(fn
      nil -> "     "
      time -> DateTime.to_time(time) |> time_to_string()
    end)
  end

  defp padded_str_digits(number) do
    number |> Integer.to_string() |> String.pad_leading(2, "0")
  end

  def time_to_string(%Time{hour: hour, minute: minute, second: second}, opt \\ :fractional) do
    [hour_pad, minute_pad, second_pad] = [hour, minute, second] |> Enum.map(&padded_str_digits/1)

    hour_pad <>
      minute_pad <>
      case {opt, second} do
        {:truncated, 0} -> ""
        {:fractional, 0} -> " "
        {:fractional, 30} -> "½"
        _ -> ":" <> second_pad
      end
  end

  def get_tiploc_departures(tiploc, %Range{first: first_ut, last: last_ut}) do
    locations =
      Table.DarwinRefLocation.match_all()
      |> Enum.map(fn row -> {row.tiploc, row} end)
      |> Map.new()

    Table.DarwinTimeLookup.rows_for_value_time(tiploc, first_ut, last_ut)
    |> Enum.filter(fn
      %Table.DarwinTimeLookup{key: {:schedule, :depart}} -> true
      _ -> false
    end)
    |> Enum.sort(&(&1.value <= &2.value))
    |> Enum.map(
      &Service.from_records(
        Table.DarwinRealTime.rows_for_rid(&1.rid),
        tiploc,
        &1.wt_match,
        locations
      )
    )
  end

  def get_service(rid) do
    locations =
      Table.DarwinRefLocation.match_all()
      |> Enum.map(fn row -> {row.tiploc, row} end)
      |> Map.new()

    rid |> Table.DarwinRealTime.rows_for_rid() |> Service.from_records(locations)
  end
end
