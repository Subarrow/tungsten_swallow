defmodule Cobalt.SSH.AccessList do
  @moduledoc false
  use Sshd.AccessList

  def permit?({_address, _port}) do
    # Access list is only used for password auth, and since that's disabled anyway,
    false
  end
end
