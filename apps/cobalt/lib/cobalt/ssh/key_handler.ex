defmodule Cobalt.SSH.KeyHandler do
  use Sshd.PublicKeyAuthenticator
  require Logger

  def authenticate(username, authenticating_key, _opts) do
    :ok = Logger.info("User #{inspect(username)}, authentication starting")

    stored_key = ssh_decode_line(Application.get_env(:cobalt, :ssh_line))

    authenticating_key == stored_key
  end

  defp ssh_decode_line(line) do
    [{key, _meta}] = :ssh_file.decode(line, :auth_keys)
    key
  end
end
