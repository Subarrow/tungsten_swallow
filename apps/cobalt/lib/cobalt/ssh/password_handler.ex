defmodule Cobalt.SSH.PasswordHandler do
  @moduledoc false

  use Sshd.PasswordAuthenticator

  def authenticate(_username, _password) do
    # Password authentication isn't supported, so we'll always return false here
    false
  end
end
