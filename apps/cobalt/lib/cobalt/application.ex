defmodule Cobalt.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  require Logger

  use Application

  alias NREFeeds.Darwin.Util.DarwinXml

  @impl true
  def start(_type, _args) do
    schema_record_definitions = DarwinXml.load_schema_record_definitions!()

    schema_model = DarwinXml.load_schema_model!()

    ref_schema_record_definitions = DarwinXml.load_reference_schema_record_definitions!()

    ref_schema_model = DarwinXml.load_reference_schema_model!()

    relay_host = String.to_charlist(Application.fetch_env!(:cobalt, :relay_host))
    relay_port = Application.fetch_env!(:cobalt, :relay_port)
    relay_username = Application.fetch_env!(:cobalt, :relay_username)
    relay_password = Application.fetch_env!(:cobalt, :relay_password)
    relay_queue = Application.fetch_env!(:cobalt, :relay_queue)

    s3_credentials = Application.fetch_env!(:cobalt, :s3_credentials)

    Logger.info("Waiting for mnesia tables...")

    :ok =
      :mnesia.wait_for_tables(
        [
          :real_time,
          :darwin_real_time,
          :darwin_time_lookup,
          :darwin_date_lookup,
          :darwin_ref_disruption_code,
          :darwin_ref_cis_source,
          :darwin_ref_location
        ],
        100_000_000
      )

    Logger.info("Tables loaded.")

    subscribe_params =
      case Application.fetch_env!(:cobalt, :stream_mode) do
        :first -> [{"x-stream-offset", "first"}]
        :last -> [{"x-stream-offset", "last"}]
      end

    children = [
      {TungstenSwallow.Util.BackoffManager, opts: %{}},
      {Cobalt.Darwin.StoreReference,
       [
         schema_record_definitions: ref_schema_record_definitions,
         schema_model: ref_schema_model,
         s3_credentials: s3_credentials,
         link_opts: [name: Cobalt.Darwin.StoreReference]
       ]},
      {Cobalt.Darwin.Store, [name: Cobalt.Darwin.Store]},
      {NREFeeds.Darwin.RealTimeParser,
       [
         schema_record_definitions: schema_record_definitions,
         schema_model: schema_model,
         sink: Cobalt.Darwin.Store,
         link_opts: [name: Cobalt.Darwin.RealTimeParser]
       ]},
      Supervisor.child_spec(
        {NREFeeds.Darwin.MessageHandler,
         %{
           host: relay_host,
           port: relay_port,
           user: relay_username,
           pass: relay_password,
           queue: relay_queue,
           id: "darwin_intake",
           name: Cobalt.DarwinIntake,
           subscribe_params: subscribe_params,
           sink: Cobalt.Darwin.RealTimeParser
         }},
        id: :darwin_intake
      ),
      Cobalt.Darwin.Cleaner,
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Cobalt.Darwin.Endpoint,
        options: [port: String.to_integer(System.get_env("APP_PORT") || "14650")]
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Cobalt.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
