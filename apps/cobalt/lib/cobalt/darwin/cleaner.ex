defmodule Cobalt.Darwin.Cleaner do
  @moduledoc """
  Cleans up old schedules
  """

  alias Cobalt.Darwin.Store.Table.{DarwinDateLookup, DarwinRealTime, DarwinTimeLookup}

  use GenServer

  require Logger

  @old_threshold 1
  @targets_limit 2000

  def start_link(link_opts \\ []) do
    GenServer.start_link(
      __MODULE__,
      %{},
      link_opts
    )
  end

  def init(state) do
    Process.send_after(self(), :clean, 1_000)
    {:ok, state}
  end

  def handle_info(:clean, state) do
    {:atomic, _} = :mnesia.transaction(&clean/0)
    Process.send_after(self(), :clean, 20_000)
    {:noreply, state}
  end

  def clean() do
    get_targets()
    |> Enum.slice(0, @targets_limit)
    |> Enum.map(&prefetch/1)
    |> Enum.map(&destroy/1)
  end

  def get_targets do
    DarwinDateLookup.rids_older_than(Date.to_gregorian_days(Date.utc_today()) - @old_threshold)
  end

  def prefetch(rid) do
    {rid, DarwinRealTime.rows_for_rid(rid), DarwinTimeLookup.rows_for_rid(rid)}
  end

  def destroy({rid, real_time_rows, time_lookup_rows}) do
    DarwinDateLookup.delete_row(rid)
    real_time_rows |> DarwinRealTime.delete_rows()
    time_lookup_rows |> DarwinTimeLookup.delete_rows()
  end
end
