defmodule Cobalt.Darwin.Board.Service do
  @moduledoc """
  Representation of entire service retrieved from store

  """

  alias Cobalt.Darwin.Board.Location

  alias Cobalt.Darwin.Store.Table.DarwinRealTime
  alias NREFeeds.Darwin.Schema.Types

  @enforce_keys [:rid, :schedule, :status, :formations, :here, :locations]
  defstruct [:rid, :schedule, :status, :formations, :here, :locations]

  @spec from_records([DarwinRealTime.t()], Types.tiploc(), any(), map()) :: map()

  def from_records(service_rows, tiploc, owt, ref_locations) do
    form_service(service_rows, {tiploc, owt}, ref_locations)
  end

  @spec from_records([DarwinRealTime.t()], map()) :: map()

  def from_records(service_rows, ref_locations) do
    form_service(service_rows, nil, ref_locations)
  end

  defp form_service(service_rows, wt_match, ref_locations) do
    head =
      service_rows
      |> Enum.map(fn %DarwinRealTime{key: key, value: value} -> {key, value} end)
      |> Map.new()

    head =
      Map.merge(
        %{
          status_locations: %{},
          formation_loading_locations: %{},
          formations: %{},
          association_locations: %{}
        },
        head
      )

    index_non_lookup =
      head.schedule.index
      |> Enum.sort(fn {_k1, v1}, {_k2, v2} -> v1 <= v2 end)
      |> Enum.map(fn {k, _v} -> k end)

    locations =
      index_non_lookup |> Enum.map(fn tpl_owt -> Location.new(head, tpl_owt, ref_locations) end)

    here =
      unless is_nil(wt_match) do
        Location.new(head, wt_match, ref_locations)
      end

    form_struct(head, locations, here)
  end

  defp form_struct(head, locations, nil) do
    %__MODULE__{
      rid: head.schedule.rid,
      schedule: head.schedule,
      formations: head.formations,
      status: Map.get(head, :status),
      here: nil,
      locations: locations
    }
  end

  defp form_struct(head, locations, here) do
    %__MODULE__{
      rid: head.schedule.rid,
      schedule: head.schedule,
      formations: head.formations,
      status: Map.get(head, :status),
      here: here,
      locations: locations
    }
  end
end
