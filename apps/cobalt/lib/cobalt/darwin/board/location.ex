defmodule Cobalt.Darwin.Board.Location do
  @moduledoc """

  Representation of service location information retrieved from store

  """

  alias Cobalt.Darwin.Store.Struct.{
    StatusLocation
  }

  defstruct [
    :tiploc,
    :reference,
    :schedule_location,
    :status,
    :formation_loading,
    :formation,
    :working_times_utc,
    :status_times_utc,
    :false_destination_tiploc,
    :false_destination_reference,
    :associations
  ]

  def form_status_times(nil), do: nil

  def form_status_times(%StatusLocation{
        working_time_running_utc: wt_run_utc
      }) do
    wt_run_utc
  end

  def new(head_map, full_match, reference_locations) do
    schedule_location = head_map.schedule_locations[full_match]

    %__MODULE__{
      reference: reference_locations[schedule_location.tiploc],
      tiploc: schedule_location.tiploc,
      schedule_location: schedule_location,
      status: head_map.status_locations[full_match],
      formation: head_map.formations[schedule_location.formation_id],
      formation_loading: head_map.formation_loading_locations[full_match],
      working_times_utc: schedule_location.working_times_utc,
      status_times_utc: form_status_times(head_map.status_locations[full_match]),
      false_destination_tiploc: schedule_location.false_destination,
      false_destination_reference: reference_locations[schedule_location.false_destination],
      associations: head_map.association_locations[full_match]
    }
  end
end
