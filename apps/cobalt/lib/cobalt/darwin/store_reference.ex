defmodule Cobalt.Darwin.StoreReference do
  @moduledoc """
  Polls for, retrieves, and stores reference data
  """

  @poll_interval 3_600_000

  use GenServer

  alias Cobalt.Darwin.Store.Table.DarwinRefCISSource
  alias NREFeeds.Darwin.Util.{DarwinS3, DarwinXml}

  alias NREFeeds.Darwin.Data

  alias NREFeeds.Darwin.Schema.Reference

  alias Cobalt.Darwin.Store.Table.DarwinRefLocation
  alias Cobalt.Darwin.Store.Table.DarwinRefDisruptionCode

  def init(
        state = %{
          schema_record_definitions: schema_record_definitions,
          schema_model: schema_model,
          s3_credentials: s3_credentials
        }
      ) do
    fetch_and_store(schema_record_definitions, schema_model, s3_credentials)

    Process.send_after(self(), :poll, @poll_interval)

    {:ok, state}
  end

  def start_link(
        schema_record_definitions: schema_record_definitions,
        schema_model: schema_model,
        s3_credentials: s3_credentials,
        link_opts: link_opts
      ) do
    GenServer.start_link(
      __MODULE__,
      %{
        schema_record_definitions: schema_record_definitions,
        schema_model: schema_model,
        s3_credentials: s3_credentials
      },
      link_opts
    )
  end

  def handle_info(
        :poll,
        state = %{
          schema_record_definitions: schema_record_definitions,
          schema_model: schema_model,
          s3_credentials: s3_credentials
        }
      ) do
    fetch_and_store(schema_record_definitions, schema_model, s3_credentials)

    Process.send_after(self(), :poll, @poll_interval)
    {:noreply, state}
  end

  def fetch_and_store(schema_record_definitions, schema_model, s3_credentials) do
    {:ok, file_contents} = DarwinS3.get_latest_reference_file(s3_credentials)

    {:ok, refdata_parsed} =
      DarwinXml.parse_darwin_binary(file_contents, schema_model, schema_record_definitions)

    reference = Reference.from_xml(refdata_parsed)

    {:atomic, _rets} =
      :mnesia.transaction(fn ->
        [
          reference.locations |> Enum.map(&store/1),
          reference.cancel_reasons |> Enum.map(&store(:cancel, &1)),
          reference.delay_reasons |> Enum.map(&store(:delay, &1)),
          reference.cis_sources |> Enum.map(&store/1)
        ]
      end)
  end

  def store(%Reference.Location{tiploc: tiploc, crs: crs, operator: operator, name: name}) do
    time_zone = Map.get(Data.LocationTimezones.location_timezones(), tiploc, "Europe/London")

    localised_names = Map.get(Data.LocationNames.location_names(), tiploc, %{})

    formed_names = {:darwin, name, localised_names}

    DarwinRefLocation.store_row(%DarwinRefLocation{
      tiploc: tiploc,
      crs: crs,
      operator: operator,
      time_zone: time_zone,
      names: formed_names
    })
  end

  def store(%Reference.CISSource{code: code, name: name}) do
    DarwinRefCISSource.store_row(%DarwinRefCISSource{
      code: code,
      name: name
    })
  end

  def store(type, %Reference.DisruptionReason{code: code, reason_text: reason_text}) do
    DarwinRefDisruptionCode.store_row(%DarwinRefDisruptionCode{
      type: type,
      code: code,
      text: reason_text
    })
  end
end
