defmodule Cobalt.Darwin.Store.Struct.StatusLocation do
  @moduledoc """
  Representation of TS-derived information, see `NREFeeds.Darwin.Schema.RealTime.TrainStatus`

  UTC times are marked, actual and estimated working times are merged into working_time_running_utc,
  with actual taking precedence over estimated whenever the time is non-nil
  """

  alias NREFeeds.Darwin.Schema.Types

  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus
  alias Cobalt.Darwin.Store.Struct.ScheduleLocation

  alias Cobalt.Darwin.Store.Struct.{StatusActualTime, StatusEstimatedTime}

  @type t :: %__MODULE__{
          tiploc: Types.tiploc(),
          platform: TrainStatus.Platform.t(),
          suppressed?: boolean(),
          length: pos_integer() | nil,
          detach_front?: boolean(),
          wt_match: tuple(),
          working_time_running_utc: map()
        }

  @enforce_keys [
    :tiploc,
    :platform,
    :suppressed?,
    :length,
    :detach_front?,
    :wt_match,
    :working_time_running_utc
  ]
  defstruct @enforce_keys

  def from_schema(
        %ScheduleLocation{working_times_naive: working_times_naive},
        ts_location = %TrainStatus.Location{
          tiploc: tiploc,
          arrive: _arrive,
          depart: _depart,
          pass: _pass,
          platform: platform,
          suppressed?: suppressed?,
          length: length,
          detach_front?: detach_front?,
          working_times: _working_times,
          wt_match: wt_match
        }
      ) do
    working_time_estimates_utc = StatusEstimatedTime.from_schema(working_times_naive, ts_location)
    actual_times_utc = StatusActualTime.from_schema(working_times_naive, ts_location)

    %__MODULE__{
      tiploc: tiploc,
      platform: platform,
      suppressed?: suppressed?,
      length: length,
      detach_front?: detach_front?,
      wt_match: wt_match,
      working_time_running_utc: Map.merge(working_time_estimates_utc, actual_times_utc)
    }
  end
end
