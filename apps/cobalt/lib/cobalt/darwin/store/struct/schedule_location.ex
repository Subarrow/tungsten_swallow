defmodule Cobalt.Darwin.Store.Struct.ScheduleLocation do
  @moduledoc """
  Representation of schedule location, fields mostly as in `NREFeeds.Darwin.Schema.RealTime.Schedule.Location`

  Main difference is that times have different fields depending on tz-awareness

  so instead of working_times and public_times, there's now:
  * public_times_utc %{:arrive/:pass/:depart => DateTime}
  * working_times_utc %{:arrive/:pass/:depart => DateTime}
  * working_times_naive %{:arrive/:pass/:depart => NaiveDateTime}

  """

  alias NREFeeds.Darwin.Schema.Types
  alias NREFeeds.Darwin.Schema.RealTime.Schedule.Location

  import NREFeeds.Darwin.Util.DarwinTime

  @type t :: %__MODULE__{
          type: Location.schedule_location_type(),
          operational?: boolean(),
          tiploc: Types.tiploc(),
          activity: Types.activity(),
          planned_activity: Types.activity() | nil,
          cancelled?: boolean(),
          formation_id: Types.formation_id() | nil,
          average_loading: 0 | pos_integer() | nil,
          route_delay: 0 | pos_integer() | nil,
          false_destination: Types.tiploc() | nil,
          public_times_utc: map(),
          working_times_utc: map(),
          working_times_naive: map(),
          wt_match: tuple()
        }

  @enforce_keys [
    :type,
    :operational?,
    :tiploc,
    :activity,
    :planned_activity,
    :cancelled?,
    :formation_id,
    :public_times_utc,
    :average_loading,
    :route_delay,
    :false_destination,
    :working_times_utc,
    :working_times_naive,
    :wt_match
  ]
  defstruct @enforce_keys

  def from_schema(%Location{
        type: type,
        operational?: operational?,
        tiploc: tiploc,
        activity: activity,
        planned_activity: planned_activity,
        cancelled?: cancelled?,
        formation_id: formation_id,
        public_times: public_times_naive,
        average_loading: average_loading,
        route_delay: route_delay,
        false_destination: false_destination,
        working_times: working_times_naive,
        wt_match: wt_match,
        index: _index
      }) do
    %__MODULE__{
      type: type,
      operational?: operational?,
      tiploc: tiploc,
      activity: activity,
      planned_activity: planned_activity,
      cancelled?: cancelled?,
      formation_id: formation_id,
      average_loading: average_loading,
      route_delay: route_delay,
      false_destination: false_destination,
      wt_match: wt_match,
      working_times_utc: wt_to_utc!(tiploc, working_times_naive),
      working_times_naive: working_times_naive,
      public_times_utc: wt_to_utc!(tiploc, public_times_naive)
    }
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias Cobalt.Darwin.Store.Struct.ScheduleLocation
    import TungstenSwallow.Util.Linked

    defp str_wt(owt = {_, _, _}) do
      owt
      |> Tuple.to_list()
      |> Enum.map_join("_", &Integer.to_string/1)
    end

    def activity_to_act_refs(nil) do
      []
    end

    def activity_to_act_refs(activities) do
      Regex.scan(~r/../, activities)
      |> List.flatten()
      |> Enum.map(&String.trim/1)
      |> Enum.reject(&(&1 == ""))
      |> Enum.map(&inline([:bplan, :activity, &1]))
    end

    def to_linked_map(
          %ScheduleLocation{
            type: _type,
            operational?: operational?,
            tiploc: tiploc,
            activity: activity,
            planned_activity: planned_activity,
            cancelled?: cancelled?,
            formation_id: formation_id,
            public_times_utc: _public_times_utc,
            average_loading: average_loading,
            route_delay: route_delay,
            false_destination: false_destination,
            working_times_utc: _working_times_utc,
            working_times_naive: _working_times_naive,
            wt_match: wt_match
          },
          %{rid: rid}
        ) do
      %{
        "type" => "ScheduleLocation",
        "service" => ref([:darwin, :service, rid]),
        "@id" => self_ref([:darwin, :service, rid, :location, tiploc <> "_" <> str_wt(wt_match)]),
        "rid" => rid,
        "tiploc" => tiploc,
        "false_destination_tiploc" => false_destination,
        "is_operational" => operational?,
        "activity" => activity_to_act_refs(activity),
        "planned_activity" => activity_to_act_refs(planned_activity),
        "is_cancelled" => cancelled?,
        "formation_id" => formation_id,
        "formation" => inline_or_nil([:darwin, :service, rid, :formation, formation_id]),
        "route_delay" => route_delay,
        "average_loading" => average_loading
      }
    end
  end
end
