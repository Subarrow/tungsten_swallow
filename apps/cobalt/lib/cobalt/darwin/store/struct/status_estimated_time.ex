defmodule Cobalt.Darwin.Store.Struct.StatusEstimatedTime do
  @moduledoc """
  An 'estimated' status time. See `NREFeeds.Darwin.Schema.RealTime.TrainStatus.Time`

  Times are transformed to UTC
  """

  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus

  alias NREFeeds.Darwin.Util.DarwinTime
  import NREFeeds.Darwin.Util.DarwinTime

  @enforce_keys [:time, :minimum, :delayed?, :unknown?]
  defstruct @enforce_keys

  @type t :: %__MODULE__{
          time: DateTime.t() | nil,
          minimum: DateTime.t() | nil,
          delayed?: boolean(),
          unknown?: boolean()
        }

  @spec from_schema(DarwinTime.normalised_working_times(), TrainStatus.Location.t()) :: %{
          optional(:arrive) => t() | nil,
          optional(:pass) => t() | nil,
          optional(:depart) => t() | nil
        }

  def from_schema(
        working_times,
        schema_location = %TrainStatus.Location{
          tiploc: tiploc,
          arrive: _arrive,
          pass: _pass,
          depart: _depart
        }
      ) do
    Enum.map(working_times, fn {key, base_wt} ->
      case Map.get(schema_location, key) do
        nil ->
          {key, nil}

        %TrainStatus.Time{
          working_estimated_time: working_estimated_time,
          estimated_time: estimated_time,
          estimated_minimum: estimated_minimum,
          delayed?: delayed?,
          estimated_time_unknown?: unknown?,
          source: _source,
          source_instance: _source_instance
        } ->
          {_ldt, wet_combined} = combine_time!(base_wt, working_estimated_time || estimated_time)
          {_ldt, minimum_combined} = combine_time!(base_wt, estimated_minimum)

          {key,
           %__MODULE__{
             time: to_utc!(tiploc, wet_combined),
             minimum: to_utc!(tiploc, minimum_combined),
             delayed?: delayed?,
             unknown?: unknown?
           }}
      end
    end)
    |> Map.new()
  end
end
