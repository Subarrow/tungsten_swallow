defmodule Cobalt.Darwin.Store.Struct.ScheduleOutline do
  @moduledoc """
  Largely an analogue of `NREFeeds.Darwin.Schema.RealTime.Schedule`,
  but without location-specific information.

  Drops the passenger? key, since this can be derived from category

  Adds origins and destinations. Note that these are not derived from
  associations, an origin or destination derived from a join/divide
  won't be expressed here.

  Adds index, a {tiploc, wt_match} => index map
  """

  alias Cobalt.Darwin.Store.Struct.ScheduleLocationOutline

  alias NREFeeds.Darwin.Schema.RealTime.DisruptionReason
  alias NREFeeds.Darwin.Schema.RealTime.Schedule
  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{
          rid: Types.rid(),
          ssd: Date.t(),
          uid: Types.uid(),
          rsid: Types.rsid(),
          signalling_id: Types.signalling_id(),
          operator: binary(),
          status: binary(),
          category: binary(),
          active?: boolean(),
          charter?: boolean(),
          cancel_reason: DisruptionReason.t(),
          index: map()
        }

  @enforce_keys [
    :rid,
    :ssd,
    :uid,
    :rsid,
    :signalling_id,
    :operator,
    :status,
    :category,
    :active?,
    :charter?,
    :cancel_reason,
    :origins,
    :destinations,
    :index
  ]
  defstruct @enforce_keys

  def from_schema(
        _schedule = %Schedule{
          rid: rid,
          ssd: ssd,
          uid: uid,
          rsid: rsid,
          signalling_id: signalling_id,
          operator: operator,
          status: status,
          category: category,
          active?: active?,
          charter?: charter?,
          cancel_reason: cancel_reason,
          passenger?: _passenger?,
          locations: locations
        }
      ) do
    %__MODULE__{
      rid: rid,
      ssd: ssd,
      uid: uid,
      rsid: rsid,
      signalling_id: signalling_id,
      operator: operator,
      status: status,
      category: category,
      active?: active?,
      charter?: charter?,
      cancel_reason: cancel_reason,
      origins:
        Enum.filter(locations, fn %{type: type} -> type == :origin end)
        |> Enum.map(&ScheduleLocationOutline.from_schema/1),
      destinations:
        Enum.filter(locations, fn %{type: type} -> type == :destination end)
        |> Enum.map(&ScheduleLocationOutline.from_schema/1),
      index:
        Enum.map(locations, fn %Schedule.Location{
                                 wt_match: wt_match,
                                 tiploc: tiploc,
                                 index: index
                               } ->
          {{tiploc, wt_match}, index}
        end)
        |> Map.new()
    }
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias Cobalt.Darwin.Store.Struct.ScheduleOutline
    import TungstenSwallow.Util.Linked
    alias TungstenSwallow.Util.Linked.LinkedStructs

    defp str_wt(owt = {_, _, _}) do
      owt
      |> Tuple.to_list()
      |> Enum.map_join("_", &Integer.to_string/1)
    end

    def to_linked_map(
          %ScheduleOutline{
            rid: rid,
            ssd: ssd,
            uid: uid,
            rsid: rsid,
            signalling_id: signalling_id,
            operator: operator,
            status: status,
            category: category,
            active?: active?,
            charter?: charter?,
            index: index,
            origins: _origins,
            destinations: _destinations,
            cancel_reason: disruption_reason
          },
          _context
        ) do
      %{
        "type" => "Schedule",
        "@id" => self_ref([:darwin, :service, rid]),
        "rid" => rid,
        "ssd" => Date.to_iso8601(ssd),
        "uid" => uid,
        "rsid" => rsid,
        "signalling_id" => signalling_id,
        "operator" => ref([:darwin, :operator, operator]),
        "status" => inline([:bplan, :status, status]),
        "category" => inline_or_nil([:bplan, :category, category]),
        "is_active" => active?,
        "is_charter" => charter?,
        "locations" =>
          index
          |> Enum.sort(fn {_k1, v1}, {_k2, v2} -> v1 <= v2 end)
          |> Enum.map(fn {{tiploc, wt_match}, _} ->
            ref([:darwin, :service, rid, :location, "#{tiploc}_#{str_wt(wt_match)}"])
          end),
        "cancel_reason" =>
          unless is_nil(disruption_reason) do
            LinkedStructs.to_linked_map(disruption_reason)
          end
      }
    end
  end
end
