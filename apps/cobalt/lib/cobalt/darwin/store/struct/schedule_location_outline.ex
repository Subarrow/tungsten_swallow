defmodule Cobalt.Darwin.Store.Struct.ScheduleLocationOutline do
  @moduledoc """
  Representation of schedule location, fields mostly as in `NREFeeds.Darwin.Schema.RealTime.Schedule.Location`

  However, most information has been stripped, except:
  * type
  * operational?
  * tiploc
  * cancelled?
  * wt_match
  """

  alias NREFeeds.Darwin.Schema.Types
  alias NREFeeds.Darwin.Schema.RealTime.Schedule.Location

  @type t :: %__MODULE__{
          type: Location.schedule_location_type(),
          operational?: boolean(),
          tiploc: Types.tiploc(),
          cancelled?: boolean(),
          wt_match: tuple()
        }

  @enforce_keys [
    :type,
    :operational?,
    :tiploc,
    :cancelled?,
    :wt_match
  ]
  defstruct @enforce_keys

  def from_schema(%Location{
        type: type,
        operational?: operational?,
        tiploc: tiploc,
        cancelled?: cancelled?,
        wt_match: wt_match
      }) do
    %__MODULE__{
      type: type,
      operational?: operational?,
      tiploc: tiploc,
      cancelled?: cancelled?,
      wt_match: wt_match
    }
  end
end
