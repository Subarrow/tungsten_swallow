defmodule Cobalt.Darwin.Store.Struct.StatusActualTime do
  @moduledoc """
  An 'actual' status time. See `NREFeeds.Darwin.Schema.RealTime.TrainStatus.Time`

  Times are transformed to UTC
  """

  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus

  alias NREFeeds.Darwin.Util.DarwinTime
  import NREFeeds.Darwin.Util.DarwinTime

  @type t :: %__MODULE__{time: DateTime.t(), class: binary() | nil}

  @enforce_keys [:time, :class]
  defstruct @enforce_keys

  @spec from_schema(DarwinTime.normalised_working_times(), TrainStatus.Location.t()) :: %{
          optional(:arrive) => t() | nil,
          optional(:pass) => t() | nil,
          optional(:depart) => t() | nil
        }

  def from_schema(
        working_times,
        schema_location = %TrainStatus.Location{
          tiploc: tiploc,
          arrive: _arrive,
          pass: _pass,
          depart: _depart
        }
      ) do
    Enum.map(working_times, fn {key, base_wt} ->
      case Map.get(schema_location, key) do
        nil ->
          {key, nil}

        %TrainStatus.Time{
          actual_time: nil
        } ->
          {key, nil}

        %TrainStatus.Time{
          actual_time: actual_time,
          actual_time_class: actual_time_class,
          source: _source,
          source_instance: _source_instance
        } ->
          {_ldt, actual_combined} = combine_time!(base_wt, actual_time)

          {key,
           %__MODULE__{
             time: to_utc!(tiploc, actual_combined),
             class: actual_time_class
           }}
      end
    end)
    |> Enum.reject(fn {_key, value} -> is_nil(value) end)
    |> Map.new()
  end
end
