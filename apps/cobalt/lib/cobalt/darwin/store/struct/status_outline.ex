defmodule Cobalt.Darwin.Store.Struct.StatusOutline do
  @moduledoc """
  Analogue of `NREFeeds.Darwin.Schema.RealTime.TrainStatus` without
  locational information.
  """

  alias NREFeeds.Darwin.Schema.Types
  alias NREFeeds.Darwin.Schema.RealTime.DisruptionReason
  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus

  @type t :: %__MODULE__{
          rid: Types.rid(),
          reverse_formation?: boolean(),
          delay_reason: DisruptionReason.t()
        }

  @enforce_keys [:rid, :reverse_formation?, :delay_reason]
  defstruct @enforce_keys

  def from_schema(%TrainStatus{
        rid: rid,
        reverse_formation?: reverse_formation?,
        delay_reason: delay_reason,
        locations: _locations
      }) do
    %__MODULE__{
      rid: rid,
      reverse_formation?: reverse_formation?,
      delay_reason: delay_reason
    }
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs,
    for: Cobalt.Darwin.Store.Struct.StatusOutline do
    alias Cobalt.Darwin.Store.Struct.StatusOutline
    alias NREFeeds.Darwin.Schema.RealTime.DisruptionReason

    import TungstenSwallow.Util.Linked
    alias TungstenSwallow.Util.Linked.LinkedStructs

    def to_linked_map(
          %StatusOutline{rid: rid, reverse_formation?: reverse_formation?, delay_reason: nil},
          _
        ) do
      %{
        "@id" => self_ref([:darwin, :service, rid, :status]),
        "type" => "ScheduleStatus",
        "is_reverse_formation" => reverse_formation?,
        "delay_reason" => nil
      }
    end

    def to_linked_map(
          %StatusOutline{
            rid: rid,
            reverse_formation?: reverse_formation?,
            delay_reason: disruption_reason = %DisruptionReason{}
          },
          context
        ) do
      %{
        "@id" => self_ref([:darwin, :service, rid, :status]),
        "type" => "ScheduleStatus",
        "is_reverse_formation" => reverse_formation?,
        "delay_reason" => LinkedStructs.to_linked_map(disruption_reason, context)
      }
    end
  end
end
