defmodule Cobalt.Darwin.Store.Table.DarwinRefDisruptionCode do
  use TungstenSwallow.Util.MnesiaTable

  @table_name :darwin_ref_disruption_code

  fields([:type, :code], [:text])

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias Cobalt.Darwin.Store.Table.DarwinRefDisruptionCode
    import TungstenSwallow.Util.Linked

    def to_linked_map(
          %DarwinRefDisruptionCode{
            type: disruption_type,
            code: code,
            text: text
          },
          _
        ) do
      %{
        "type" => "DisruptionReason",
        "@id" => self_ref([:darwin, :reason, disruption_type, code]),
        "text" => text
      }
    end
  end
end
