defmodule Cobalt.Darwin.Store.Table.DarwinDateLookup do
  use TungstenSwallow.Util.MnesiaTable

  @table_name :darwin_date_lookup

  fields(:rid, [:ssd])

  def rids_older_than(ssd_end) do
    :mnesia.select(
      @table_name,
      [
        {{@table_name, :"$1", :"$2"}, [{:<, :"$2", ssd_end}], [:"$1"]}
      ]
    )
  end
end
