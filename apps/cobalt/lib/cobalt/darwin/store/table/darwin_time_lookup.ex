defmodule Cobalt.Darwin.Store.Table.DarwinTimeLookup do
  use TungstenSwallow.Util.MnesiaTable

  @table_name :darwin_time_lookup

  fields([:rid, :tiploc, :wt_match, :key], [:rid, :tiploc, :value])

  def rows_for_value_time(tiploc, unix_time_start, unix_time_end) do
    :mnesia.select(
      @table_name,
      [
        {{@table_name, :"$1", :"$2", :"$3", :"$4"},
         [{:"=:=", :"$3", tiploc}, {:>=, :"$4", unix_time_start}, {:<, :"$4", unix_time_end}],
         [:"$$"]}
      ]
    )
    |> from_records()
  end

  def rows_for_location(tiploc) do
    :mnesia.select(
      @table_name,
      [
        {{@table_name, :"$1", :"$2", :"$3", :"$4"}, [{:"=:=", :"$3", tiploc}], [:"$$"]}
      ]
    )
    |> from_records()
  end

  def rows_for_rid(rid) do
    :mnesia.index_read(@table_name, rid, 3) |> from_records()
  end
end
