defmodule Cobalt.Darwin.Store.Table.DarwinRefCISSource do
  use TungstenSwallow.Util.MnesiaTable

  @table_name :darwin_ref_cis_source

  fields(:code, [:name])
end
