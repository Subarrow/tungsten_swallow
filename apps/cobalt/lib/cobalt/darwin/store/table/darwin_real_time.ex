defmodule Cobalt.Darwin.Store.Table.DarwinRealTime do
  use TungstenSwallow.Util.MnesiaTable

  alias NREFeeds.Darwin.Schema.Types

  @type t :: %__MODULE__{rid: Types.rid(), key: atom(), value: struct()}

  @table_name :darwin_real_time

  fields([:rid, :key], [
    :rid,
    :value
  ])

  def rows_for_rid(rid) do
    :mnesia.index_read(:darwin_real_time, rid, 3) |> from_records()
  end

  def rows_for_primary_key(rid, key) do
    read_rows({rid, key})
  end

  def get_one(rid, key) do
    case read_rows({rid, key}) do
      [] -> nil
      [%__MODULE__{value: value}] -> value
    end
  end

  def get_schedule(rid) do
    case rows_for_primary_key(rid, :schedule) do
      [] -> nil
      [%__MODULE__{value: value}] -> value
      _ -> raise "too many returned rows"
    end
  end
end
