defmodule Cobalt.Darwin.Store.Table.DarwinRefLocation do
  use TungstenSwallow.Util.MnesiaTable

  @table_name :darwin_ref_location

  fields(:tiploc, [:crs, :operator, :time_zone, :names])

  def get_location(tiploc) do
    case read_rows(tiploc) do
      [] -> nil
      [location] -> location
      _ -> raise "too many returned rows"
    end
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias Cobalt.Darwin.Store.Table.DarwinRefLocation
    import TungstenSwallow.Util.Linked

    def to_linked_map(
          %DarwinRefLocation{
            tiploc: tiploc,
            crs: crs,
            operator: operator,
            time_zone: time_zone,
            names: {:darwin, darwin_name, _}
          },
          _
        ) do
      %{
        "type" => "Location",
        "@id" => self_ref([:darwin, :location, tiploc]),
        "operator" =>
          unless is_nil(operator) do
            ref([:darwin, :operator, operator])
          end,
        "tiploc" => tiploc,
        "crs" => crs,
        "time_zone" => time_zone,
        "name" => darwin_name
      }
    end
  end
end
