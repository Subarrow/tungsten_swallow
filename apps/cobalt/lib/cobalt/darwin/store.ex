defmodule Cobalt.Darwin.Store do
  use GenServer

  alias Cobalt.Darwin.Store.Table.DarwinDateLookup

  alias NREFeeds.Darwin.Schema.RealTime
  alias NREFeeds.Darwin.Schema.RealTime.DeactivatedSchedule
  alias NREFeeds.Darwin.Schema.RealTime.Schedule
  alias NREFeeds.Darwin.Schema.RealTime.TrainStatus
  alias NREFeeds.Darwin.Schema.RealTime.ScheduleFormations
  alias NREFeeds.Darwin.Schema.RealTime.FormationLoading
  alias NREFeeds.Darwin.Schema.RealTime.Association
  alias NREFeeds.Darwin.Schema.RealTime.Misc.ServiceRIDWithCircularTimes

  alias Cobalt.Darwin.Store.Struct.ScheduleOutline
  alias Cobalt.Darwin.Store.Struct.ScheduleLocation
  alias Cobalt.Darwin.Store.Struct.StatusOutline
  alias Cobalt.Darwin.Store.Struct.StatusLocation

  alias Cobalt.Darwin.Store.Table.DarwinRealTime
  alias Cobalt.Darwin.Store.Table.DarwinTimeLookup

  def start_link(link_opts) do
    GenServer.start_link(__MODULE__, nil, link_opts)
  end

  def init(_) do
    {:ok, %{}}
  end

  def submit_realtime_messages(messages, store_pid) do
    GenServer.cast(store_pid, messages)
  end

  def handle_cast(real_time, state) do
    store_realtime_messages(real_time)

    {:noreply, state}
  end

  defp unix_time(utc_datetime = %DateTime{time_zone: "Etc/UTC"}) do
    DateTime.to_unix(utc_datetime)
  end

  @spec store_realtime_messages([RealTime.t()] | []) :: nil

  def store_realtime_messages([]) do
  end

  def store_realtime_messages([real_time = %RealTime{messages: messages} | rest]) do
    {:atomic, _} =
      :mnesia.transaction(fn ->
        case real_time.type do
          Schedule ->
            messages |> Enum.map(&prefetch/1) |> Enum.map(&store(real_time, &1))

          DeactivatedSchedule ->
            messages |> Enum.map(&prefetch/1) |> Enum.map(&store(real_time, &1))

          TrainStatus ->
            messages |> Enum.map(&prefetch/1) |> Enum.map(&store(real_time, &1))

          ScheduleFormations ->
            messages |> Enum.map(&store(real_time, &1))

          FormationLoading ->
            messages |> Enum.map(&prefetch/1) |> Enum.map(&store(real_time, &1))

          Association ->
            messages |> Enum.map(&prefetch/1) |> Enum.map(&store(real_time, &1))

          _ ->
            nil
        end
      end)

    store_realtime_messages(rest)
  end

  defp prefetch(deactivated_schedule = %DeactivatedSchedule{rid: rid}) do
    {deactivated_schedule, DarwinRealTime.get_one(rid, :schedule)}
  end

  defp prefetch(schedule = %Schedule{rid: rid, deleted?: true}) do
    {schedule, DarwinTimeLookup.rows_for_rid(rid), DarwinRealTime.rows_for_rid(rid)}
  end

  defp prefetch(schedule = %Schedule{rid: rid, deleted?: false}) do
    {schedule, DarwinTimeLookup.rows_for_rid(rid), DarwinRealTime.get_one(rid, :schedule),
     [
       DarwinRealTime.rows_for_primary_key(rid, :schedule_locations),
       DarwinRealTime.rows_for_primary_key(rid, :association_locations),
       DarwinRealTime.rows_for_primary_key(rid, :status_locations),
       DarwinRealTime.rows_for_primary_key(rid, :formation_loading_locations)
     ]}
  end

  defp prefetch(status = %TrainStatus{rid: rid}) do
    {status, DarwinRealTime.get_one(rid, :schedule_locations),
     DarwinRealTime.get_one(rid, :status_locations)}
  end

  defp prefetch(loading = %FormationLoading{rid: rid}) do
    {loading, DarwinRealTime.get_one(rid, :formation_loading_locations) || %{}}
  end

  defp prefetch(
         association = %Association{
           main: %ServiceRIDWithCircularTimes{rid: main_rid},
           associated: %ServiceRIDWithCircularTimes{rid: assoc_rid}
         }
       ) do
    {association, DarwinRealTime.get_one(main_rid, :association_locations),
     DarwinRealTime.get_one(assoc_rid, :association_locations)}
  end

  defp invalidate_wt_tl_map_row(_new_index, []), do: nil

  defp invalidate_wt_tl_map_row(new_index, [
         %DarwinRealTime{
           rid: rid,
           key: key,
           value: old_wt_tiploc_map
         }
       ]) do
    new_wt_tiploc_map =
      old_wt_tiploc_map |> Map.filter(fn {key, _value} -> Map.has_key?(new_index, key) end)

    if new_wt_tiploc_map != old_wt_tiploc_map do
      DarwinRealTime.store_row(%DarwinRealTime{
        rid: rid,
        key: key,
        value: new_wt_tiploc_map
      })
    end
  end

  defp store(
         _darwin_real_time,
         {%Schedule{deleted?: true, rid: rid}, old_lut_entries, all_schedule_rows}
       ) do
    old_lut_entries |> DarwinTimeLookup.delete_rows()
    all_schedule_rows |> DarwinRealTime.delete_rows()
    DarwinDateLookup.delete_row(rid)
  end

  defp store(
         _darwin_real_time,
         {schedule = %Schedule{rid: rid, ssd: ssd, locations: locations}, old_lut_entries,
          _old_s_outline, tiploc_wt_rows_for_invalidation}
       ) do
    DarwinDateLookup.store_row(%DarwinDateLookup{rid: rid, ssd: Date.to_gregorian_days(ssd)})

    new_schedule_outline = ScheduleOutline.from_schema(schedule)

    # Get rid of everything referencing old tiploc/wt pairs no longer in schedule

    old_lut_entries
    |> Enum.reject(fn %DarwinTimeLookup{tiploc: tiploc, wt_match: wt_match} ->
      Map.has_key?(new_schedule_outline.index, {tiploc, wt_match})
    end)
    |> DarwinTimeLookup.delete_rows()

    tiploc_wt_rows_for_invalidation
    |> Enum.each(&invalidate_wt_tl_map_row(new_schedule_outline.index, &1))

    # New schedule rows

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :schedule,
      value: new_schedule_outline
    })

    locations_processed = locations |> Enum.map(&ScheduleLocation.from_schema/1)

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :schedule_locations,
      value:
        locations_processed
        |> Enum.map(fn location = %ScheduleLocation{tiploc: tiploc, wt_match: wt_match} ->
          {{tiploc, wt_match}, location}
        end)
        |> Map.new()
    })

    # New LUT entries

    locations_processed
    |> Enum.map(fn %ScheduleLocation{
                     working_times_utc: loc_working_times_utc,
                     wt_match: wt_match,
                     tiploc: tiploc
                   } ->
      Enum.map([:arrive, :pass, :depart], fn key ->
        case loc_working_times_utc[key] do
          nil ->
            nil

          wt_utc ->
            DarwinTimeLookup.store_row(%DarwinTimeLookup{
              rid: rid,
              tiploc: tiploc,
              wt_match: wt_match,
              key: {:schedule, key},
              value: unix_time(wt_utc)
            })
        end
      end)
    end)
  end

  defp store(_darwin_real_time, {%DeactivatedSchedule{}, nil}), do: nil

  defp store(
         _darwin_real_time,
         {%DeactivatedSchedule{rid: rid}, schedule_outline = %ScheduleOutline{}}
       ) do
    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :schedule,
      value: %ScheduleOutline{schedule_outline | active?: false}
    })
  end

  defp store(_darwin_real_time, {%TrainStatus{}, nil, _}), do: nil

  defp store(
         _darwin_real_time,
         {train_status = %TrainStatus{
            rid: rid,
            locations: ts_locations
          }, schedule_locations, old_status_locations}
       ) do
    old_status_locations = old_status_locations || %{}

    ts_locations =
      ts_locations |> Enum.filter(&Map.has_key?(schedule_locations, {&1.tiploc, &1.wt_match}))

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :status,
      value: StatusOutline.from_schema(train_status)
    })

    ts_locations_processed =
      ts_locations
      |> Enum.map(fn ts_location = %TrainStatus.Location{wt_match: wt_match, tiploc: tiploc} ->
        StatusLocation.from_schema(schedule_locations[{tiploc, wt_match}], ts_location)
      end)

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :status_locations,
      value:
        Map.merge(
          old_status_locations,
          ts_locations_processed
          |> Enum.map(fn ts_location = %StatusLocation{wt_match: wt_match, tiploc: tiploc} ->
            {{tiploc, wt_match}, ts_location}
          end)
          |> Map.new()
        )
    })

    ts_locations_processed
    |> Enum.map(fn %StatusLocation{
                     working_time_running_utc: loc_working_time_running_utc,
                     wt_match: wt_match,
                     tiploc: tiploc
                   } ->
      Enum.map([:arrive, :pass, :depart], fn key ->
        case loc_working_time_running_utc[key] do
          nil ->
            DarwinTimeLookup.delete_row({rid, tiploc, wt_match, {:status, key}})

          %{time: nil} ->
            DarwinTimeLookup.delete_row({rid, tiploc, wt_match, {:status, key}})

          %{time: estimated_utc} ->
            DarwinTimeLookup.store_row(%DarwinTimeLookup{
              rid: rid,
              tiploc: tiploc,
              wt_match: wt_match,
              key: {:status, key},
              value: unix_time(estimated_utc)
            })
        end
      end)
    end)
  end

  defp store(_darwin_real_time, %ScheduleFormations{rid: rid, formations: formations}) do
    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :formations,
      value:
        formations
        |> Enum.map(fn formation = %ScheduleFormations.Formation{formation_id: fid} ->
          {fid, formation}
        end)
        |> Map.new()
    })
  end

  defp store(
         _darwin_real_time,
         {loading = %FormationLoading{
            rid: rid,
            loading: _loading_only,
            tiploc: tiploc,
            wt_match: wt_match
          }, formation_loading_old}
       ) do
    DarwinRealTime.store_row(%DarwinRealTime{
      rid: rid,
      key: :formation_loading_locations,
      value: Map.put(formation_loading_old, {tiploc, wt_match}, loading)
    })
  end

  defp store(
         _darwin_real_time,
         {_association = %Association{
            tiploc: tiploc,
            category: _category,
            deleted?: true,
            main: %ServiceRIDWithCircularTimes{wt_match: main_wt_match, rid: main_rid},
            associated: %ServiceRIDWithCircularTimes{wt_match: assoc_wt_match, rid: assoc_rid}
          }, main_old_association_locations, assoc_old_association_locations}
       ) do
    main_old_association_locations = main_old_association_locations || %{}
    assoc_old_association_locations = assoc_old_association_locations || %{}

    main_old_associations = main_old_association_locations[{tiploc, main_wt_match}] || %{}
    assoc_old_associations = assoc_old_association_locations[{tiploc, assoc_wt_match}] || %{}

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: main_rid,
      key: :association_locations,
      value:
        Map.put(
          main_old_association_locations,
          {tiploc, main_wt_match},
          Map.delete(main_old_associations, assoc_rid)
        )
    })

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: assoc_rid,
      key: :association_locations,
      value:
        Map.put(
          assoc_old_association_locations,
          {tiploc, assoc_wt_match},
          Map.delete(assoc_old_associations, main_rid)
        )
    })
  end

  defp store(
         _darwin_real_time,
         {association = %Association{
            tiploc: tiploc,
            category: _category,
            deleted?: false,
            main: %ServiceRIDWithCircularTimes{wt_match: main_wt_match, rid: main_rid},
            associated: %ServiceRIDWithCircularTimes{wt_match: assoc_wt_match, rid: assoc_rid}
          }, main_old_association_locations, assoc_old_association_locations}
       ) do
    main_old_association_locations = main_old_association_locations || %{}
    assoc_old_association_locations = assoc_old_association_locations || %{}

    main_old_associations = main_old_association_locations[{tiploc, main_wt_match}] || %{}
    assoc_old_associations = assoc_old_association_locations[{tiploc, assoc_wt_match}] || %{}

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: main_rid,
      key: :association_locations,
      value:
        Map.put(
          main_old_association_locations,
          {tiploc, main_wt_match},
          Map.put(main_old_associations, assoc_rid, association)
        )
    })

    DarwinRealTime.store_row(%DarwinRealTime{
      rid: assoc_rid,
      key: :association_locations,
      value:
        Map.put(
          assoc_old_association_locations,
          {tiploc, assoc_wt_match},
          Map.put(assoc_old_associations, main_rid, association)
        )
    })
  end
end
