defmodule Cobalt.Darwin.Endpoint do
  import TungstenSwallow.Util.Linked.LinkedStructs
  import TungstenSwallow.Util.Linked

  alias Cobalt.Darwin.Store.Table.DarwinRealTime
  alias Cobalt.Darwin.Store.Table.DarwinRefDisruptionCode
  alias Cobalt.Darwin.Store.Table.DarwinRefLocation

  alias BPLAN.Data

  use Plug.Router
  plug(Plug.Logger)
  plug(:match)
  plug(Plug.Parsers, parsers: [:json], json_decoder: Jason)
  plug(:dispatch)

  @prefix Application.compile_env(:cobalt, :main_prefix, "http://127.0.0.1:14650/")

  @bplan (Data.Category.get() ++ Data.Status.get() ++ Data.Activity.get())
         |> Enum.map(&to_linked_map/1)
         |> Enum.map(&extract_id/1)
         |> Map.new()

  defp respond_200_json(encoded_json, conn) when is_binary(encoded_json) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(
      200,
      encoded_json
    )
  end

  def respond_bplan(reference = {:inline, _}, conn) do
    reference
    |> resolve_references(prefix: @prefix, inline: @bplan)
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "darwin/service/:rid" do
    {:atomic, service_rows} =
      :mnesia.transaction(fn ->
        DarwinRealTime.rows_for_rid(rid)
      end)

    rows =
      Enum.reject(service_rows, fn %DarwinRealTime{key: key} ->
        key in [:status_locations, :formation_loading_locations, :association_locations]
      end)
      |> Enum.map(& &1.value)
      |> Enum.map(fn
        entry when is_struct(entry) -> entry
        entry when is_map(entry) -> Map.values(entry)
      end)
      |> List.flatten()
      |> Enum.map(&to_linked_map(&1, %{rid: rid}))
      |> Enum.map(fn value = %{"@id" => {_, key}} -> {key, value} end)
      |> Map.new()

    Jason.encode!(
      resolve_references({:inline, [:darwin, :service, rid]},
        prefix: @prefix,
        inline: Map.merge(@bplan, rows)
      )
    )
    |> respond_200_json(conn)
  end

  get "darwin/service" do
    # 3_652_424: ~D[9999-12-31]
    {:atomic, services} =
      :mnesia.transaction(fn ->
        Cobalt.Darwin.Store.Table.DarwinDateLookup.rids_older_than(3_652_424)
        |> Enum.map(&DarwinRealTime.get_schedule/1)
      end)

    services
    |> Enum.map(&to_linked_map(&1, %{rid: &1.rid}))
    |> resolve_references(prefix: @prefix, inline: @bplan)
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "darwin/location" do
    # 3_652_424: ~D[9999-12-31]
    {:atomic, locations} =
      :mnesia.transaction(fn ->
        DarwinRefLocation.match_all()
      end)

    locations
    |> Enum.map(&to_linked_map(&1, %{}))
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "darwin/location/:location_code" do
    {:atomic, location} =
      :mnesia.transaction(fn ->
        DarwinRefLocation.get_location(location_code)
      end)

    location
    |> to_linked_map(%{})
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "bplan/activity/:activity_code" do
    {:inline, [:bplan, :activity, activity_code]}
    |> resolve_references(prefix: @prefix, inline: @bplan)
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "bplan/category/:category_code" do
    {:inline, [:bplan, :category, category_code]}
    |> resolve_references(prefix: @prefix, inline: @bplan)
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "bplan/status/:status_code" do
    {:inline, [:bplan, :status, status_code]}
    |> resolve_references(prefix: @prefix, inline: @bplan)
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "bplan/activity" do
    Data.Activity.get()
    |> Enum.map(&to_linked_map(&1, %{}))
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "bplan/category" do
    Data.Category.get()
    |> Enum.map(&to_linked_map(&1, %{}))
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "bplan/status" do
    Data.Status.get()
    |> Enum.map(&to_linked_map(&1, %{}))
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "darwin/reason/:disruption_type/:reason_code" do
    disruption_type =
      case disruption_type do
        "delay" -> :delay
        "cancel" -> :cancel
      end

    {:atomic, [result]} =
      :mnesia.transaction(fn ->
        DarwinRefDisruptionCode.read_rows({disruption_type, reason_code})
      end)

    result
    |> to_linked_map()
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "darwin/reason/:disruption_type" do
    disruption_type =
      case disruption_type do
        "delay" -> :delay
        "cancel" -> :cancel
      end

    {:atomic, results} = :mnesia.transaction(fn -> DarwinRefDisruptionCode.match_all() end)

    results
    |> Enum.filter(fn
      %DarwinRefDisruptionCode{type: ^disruption_type} -> true
      _ -> false
    end)
    |> Enum.map(&to_linked_map(&1, %{}))
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get "darwin/reason" do
    [ref([:darwin, :reason, :delay]), ref([:darwin, :reason, :cancel])]
    |> resolve_references(prefix: @prefix, inline: %{})
    |> Jason.encode!()
    |> respond_200_json(conn)
  end

  get _ do
    send_resp(conn, 404, "404")
  end
end
