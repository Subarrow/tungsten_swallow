defmodule Mix.Tasks.Cobalt.CheckInternational do
  @moduledoc "Uses BPLAN (the filename of which is assumed to be the sole argument) to find any missing international locations"
  use Mix.Task

  alias BPLAN
  alias BPLAN.Schema.{Location, TimingLink}

  alias NREFeeds.Darwin.Data.LocationTimezones

  @shortdoc "Uses BPLAN to find any missing international locations"
  def run(args) do
    {:ok, file_handle} = File.open(args)
    IO.puts("Reading BPLAN")

    bplan = BPLAN.from_file_handle(file_handle)

    IO.puts("Filtering BPLAN")

    # LOC records have the names
    bplan_names =
      bplan
      |> Enum.filter(fn
        %Location{} -> true
        _ -> false
      end)
      |> Enum.map(&{&1.tiploc, &1.name})
      |> Map.new()

    # TLKs have the timing links
    bplan_tlk =
      bplan
      |> Enum.filter(fn
        %TimingLink{} -> true
        _ -> false
      end)

    timezones = LocationTimezones.location_timezones()

    # If either the origin or destination in the timing link is CHUNEOB or CHUNCTR (i.e. the Eurotunnel boundaries)
    # and we haven't noted either timezone down, print it out
    bplan_tlk
    |> Enum.filter(
      &((&1.origin_tiploc in ["CHUNEOB", "CHUNCTR"] or
           &1.destination_tiploc in ["CHUNEOB", "CHUNCTR"]) and
          (!Map.has_key?(timezones, &1.origin_tiploc) or
             !Map.has_key?(timezones, &1.destination_tiploc)))
    )
    |> Enum.map(fn tlk ->
      String.pad_leading(tlk.origin_tiploc, 7) <>
        " " <>
        String.pad_leading(Map.get(bplan_names, tlk.origin_tiploc), 32) <>
        "- #{tlk.running_time} #{tlk.running_line_code} -" <>
        String.pad_leading(tlk.destination_tiploc, 7) <>
        " " <> String.pad_leading(Map.get(bplan_names, tlk.destination_tiploc), 32)
    end)
    |> Enum.each(&IO.puts/1)
  end
end
