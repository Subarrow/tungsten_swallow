defmodule Mix.Tasks.Cobalt.DropMnesiaTables do
  @moduledoc "Deletes all cobalt mnesia tables"
  use Mix.Task

  @tables [
    :real_time,
    :darwin_real_time,
    :darwin_time_lookup,
    :darwin_ref_location,
    :darwin_ref_disruption_code,
    :darwin_ref_cis_source,
    :darwin_date_lookup,
    :darwin_ref_cis_source
  ]

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(label <> ": " <> inspect(value))
  end

  @shortdoc "Deletes all cobalt mnesia tables"
  def run(_args) do
    :application.start(:mnesia) |> print_inspect("Mnesia start")

    IO.puts("Waiting for: " <> Enum.join(@tables, ", "))

    :ok =
      :mnesia.wait_for_tables(
        @tables,
        100_000_000
      )
      |> print_inspect("finished waiting")

    Enum.each(@tables, fn table ->
      :mnesia.delete_table(table) |> print_inspect("deletion for #{table}")
    end)

    :application.stop(:mnesia) |> print_inspect("Mnesia stop")
  end
end
