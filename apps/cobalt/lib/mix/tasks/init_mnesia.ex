defmodule Mix.Tasks.Cobalt.InitMnesia do
  @moduledoc "Basic single-node mnesia setup"
  use Mix.Task

  # Functionally very similar to IO.inspect, but this simplifies dealing with credo
  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(label <> ": " <> inspect(value))
  end

  @shortdoc "Basic single-node mnesia setup"
  def run(_args) do
    :mnesia.create_schema([node()]) |> print_inspect("Create schema")
    :application.start(:mnesia) |> print_inspect("Mnesia start")

    :mnesia.create_table(:real_time,
      attributes: [:key, :value, :meta],
      index: [:value],
      disc_copies: [node()]
    )
    |> print_inspect("real_time table create")

    :mnesia.create_table(:darwin_real_time,
      attributes: [:pkey, :rid, :value],
      index: [:rid],
      disc_copies: [node()],
      type: :ordered_set
    )
    |> print_inspect("darwin_real_time table create")

    :mnesia.create_table(:darwin_time_lookup,
      attributes: [:pkey, :rid, :tiploc, :value],
      index: [:rid, :tiploc, :value],
      disc_copies: [node()]
    )
    |> print_inspect("darwin_time_lookup table create")

    :mnesia.create_table(:darwin_ref_location,
      attributes: [:tiploc, :crs, :operator, :time_zone, :names],
      index: [:crs, :operator, :time_zone],
      disc_copies: [node()]
    )
    |> print_inspect("darwin_ref_location table create")

    :mnesia.create_table(:darwin_date_lookup,
      attributes: [:rid, :ssd],
      index: [:ssd],
      disc_copies: [node()],
      type: :ordered_set
    )
    |> print_inspect("darwin_date_lookup table create")

    :mnesia.create_table(:darwin_ref_disruption_code,
      attributes: [:pkey, :text],
      disc_copies: [node()],
      type: :ordered_set
    )
    |> print_inspect("darwin_ref_disruption_code table create")

    :mnesia.create_table(:darwin_ref_cis_source,
      attributes: [:code, :name],
      disc_copies: [node()],
      type: :ordered_set
    )
    |> print_inspect("darwin_ref_cis_source table create")

    :application.stop(:mnesia) |> print_inspect("Mnesia stop")
  end
end
