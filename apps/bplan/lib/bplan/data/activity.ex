defmodule BPLAN.Data.Activity do
  @moduledoc """
  Static information from BPLAN's activity reference.

  Note that these are stripped (i.e. trailing spaces are removed), so
  this will have to be replicated when using these to look up activity
  codes.
  """

  defstruct [:code, :description]

  @activities %{
    "W" => "Stops for watering of coaches",
    "X" => "Passes another train at crossing point on single line",
    "KC" => "Ticket collection and examination point",
    "UX" => "Stops to pick up Red Star parcels",
    "-T" => "Stops to attach and detach vehicles",
    "G" => "GBPRTT Data to add",
    "D" => "Stops to set down passengers (shows 's' in GBTT)",
    "VV" => "Do not show diversion (not sent to TSDB)",
    "K" => "Passenger count point",
    "C" => "Stops to change trainmen ONLY",
    "T" => "Stops to Take Up and Set Down passengers",
    "V" => "Train diverted VIA (not sent to TSDB)",
    "KE" => "Ticket examination point",
    "U" => "Stops to take up passengers (shows 'u' in GBTT)",
    "{" => "Force running line indication",
    "x{" => "Suppress running line indication",
    "PR" => "Propelling between points shown",
    "L" => "Stops to change locomotive",
    "S" => "Stops for Railway Personnel Only",
    "TS" => "Activity requested for TOPS reporting purposes",
    "R" => "Stops when required (shows 'x' in GBTT)",
    "TF" => "Train Finishes (Destination)",
    "H" => "Notional activity to prevent WTT columns merge",
    "RM" => "Stops for reversing move or driver changes ends",
    "E" => "Stops for examination",
    "*" => "Supression of traffic stop indicator",
    "AE" => "Attach/Detach assisting locomotive",
    "{}" => "Force path & line indications",
    "TB" => "Train Begins (Origin)",
    "RR" => "Stops for locomotive to run round train",
    "HH" => "As H, to prevent WTT column merge where 3rd Column",
    "KF" => "Ticket examination point - first class only",
    "AX" => "Shows as 'X' on arrival",
    "BL" => "Stops for banking locomotive",
    "KS" => "Selective Ticket Examination Point",
    "-U" => "Stops to attach vehicles",
    "OR" => "Train Locomotive on rear",
    "N" => "Stop not advertised",
    "A" => "Stops or shunts for other trains to pass",
    "TW" => "Stops or passes for tablet, staff or token",
    "NX" => "Red Star Parcels not set down or picked up",
    "OP" => "Stops for other operating reasons",
    "DX" => "Stops to set down Red Star parcels",
    "-D" => "Stops to detach vehicles",
    "}" => "Force path indication"
  }

  def get,
    do:
      @activities
      |> Enum.map(fn {code, description} -> %__MODULE__{code: code, description: description} end)

  def get(code) do
    case Map.get(@activities, code) do
      nil -> nil
      description -> %__MODULE__{code: code, description: description}
    end
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias BPLAN.Data.Activity
    import TungstenSwallow.Util.Linked

    def to_linked_map(
          %Activity{
            code: code,
            description: description
          },
          _context
        ) do
      %{
        "type" => "Activity",
        "@id" => self_ref([:bplan, :activity, code]),
        "code" => code,
        "description" => description
      }
    end
  end
end
