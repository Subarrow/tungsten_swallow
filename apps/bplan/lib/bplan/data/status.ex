defmodule BPLAN.Data.Status do
  @moduledoc """
  Static information from BPLAN's status reference
  """

  defstruct [:code, :description, :permanent?]

  @statuses %{
    "1" => %{desc: "STP Passenger/Mail", permanent?: false},
    "2" => %{desc: "STP Freight", permanent?: false},
    "3" => %{desc: "STP Trip", permanent?: false},
    "4" => %{desc: "STP Ship", permanent?: false},
    "5" => %{desc: "STP Bus", permanent?: false},
    "B" => %{desc: "Bus (Permanent)", permanent?: true},
    "F" => %{desc: "Freight (Permanent - WTT)", permanent?: true},
    "P" => %{desc: "Passenger/Mail (Permanent - WTT)", permanent?: true},
    "S" => %{desc: "Ship (Permanent)", permanent?: true},
    "T" => %{desc: "Trip (Permanent)", permanent?: true}
  }

  defp from_entry({code, %{desc: description, permanent?: permanent?}}) do
    %__MODULE__{code: code, description: description, permanent?: permanent?}
  end

  def get(), do: Enum.map(@statuses, &from_entry/1)

  def get(code) do
    case Map.get(@statuses, code) do
      nil -> nil
      entry -> from_entry({code, entry})
    end
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias BPLAN.Data.Status
    import TungstenSwallow.Util.Linked

    def to_linked_map(
          %Status{
            code: code,
            description: description,
            permanent?: permanent?
          },
          _context
        ) do
      %{
        "type" => "Status",
        "@id" => self_ref([:bplan, :status, code]),
        "is_permanent" => permanent?,
        "code" => code,
        "description" => description
      }
    end
  end
end
