defmodule BPLAN.Data.Category do
  @moduledoc """
  Static information from BPLAN's category reference
  """

  defstruct [:code, :description, :train?, :bus?, :ship?, :passenger?, :express?]

  @categories %{
    # Passenger (ordinary)
    "OO" => {"Ordinary Passenger", [:passenger, :advertised]},
    "OU" => {"Unadvertised Ordinary Passenger", [:passenger]},
    "OL" => {"London Underground/Metro Service", [:passenger, :metro]},

    # Passenger (mixed)
    "OW" => {"Mixed", [:passenger]},

    # Passenger (staff)
    "OS" => "Staff Train",

    # Passenger (express)
    "XX" => {"Express Passenger", [:passenger, :express, :advertised]},
    "XU" => {"Unadvertised Express", [:passenger, :express]},
    "XZ" => {"Domestic Sleeper Service", [:passenger, :advertised]},
    "XC" => {"Channel Tunnel Passenger", [:passenger, :express]},

    # ---
    "XI" => {"International", [:passenger, :international]},
    "XR" => {"Motorail", [:passenger]},
    "XD" => {"European Night Service", [:passenger, :international]},

    # ECS
    "EE" => {"Empty Coaching Stock", [:empty]},
    "ES" => {"E.C.S. - Staff", [:empty]},
    "EL" => {"E.C.S. - London Underground/Metro Service", [:empty, :metro]},

    # Parcels / postal
    "PP" => "Parcels",
    "JJ" => "Postal",
    "PV" => {"Empty N.P.C.C.S.", [:empty]},
    "PM" => "PO Controlled Parcels",

    # Bus
    "BR" => {"Bus Replacement", [:bus, :passenger]},
    "BS" => {"Bus Service", [:bus, :passenger]},

    # Ship
    "SS" => {"Ship", [:ship, :passenger]},

    # Locomotive
    "ZB" => "Light Locomotive and Brake Van",
    "ZZ" => "Light Locomotive",

    # Departmental
    "DD" => "Departmental",
    "DH" => "Civil Engineer",
    "DI" => "Mechanical & Electrical Engineers",
    "DQ" => "Stores",
    "DT" => "Test",
    "DY" => "Signal & Telecomms Engineer",

    # Freight

    "A0" => {"Coal (Distributive)", [:freight]},
    "B0" => {"Coal (Other) & Nuclear", [:freight]},
    "B1" => {"Metals", [:freight]},
    "B4" => {"Aggregates", [:freight]},
    "B5" => {"Refuse", [:freight]},
    "B6" => {"Building Materials", [:freight]},
    "B7" => {"Petroleum Products", [:freight]},
    "E0" => {"Coal (Electricity) - Merry-Go-Round", [:freight]},
    "H0" => {"RfD European Channel Tunnel (mixed business)", [:freight]},
    "H1" => {"RfD European Channel Tunnel Intermodal", [:freight]},
    "H2" => {"RfD Automotive (Vehicles)", [:freight]},
    "H3" => {"RfD European Channel Tunnel Automotive", [:freight]},
    "H4" => {"RfD European Channel Tunnel Contract Services", [:freight]},
    "H5" => {"RfD European Channel Tunnel Haulmark", [:freight]},
    "H6" => {"RfD European Channel Tunnel Joint Venture", [:freight]},
    "H8" => {"RfD European", [:freight]},
    "H9" => {"RfD Freightliner (Other)", [:freight]},
    "J2" => {"RfD Automotive (Components)", [:freight]},
    "J3" => {"RfD Edible Products (UK Contracts)", [:freight]},
    "J4" => {"RfD Industrial Minerals (UK Contracts)", [:freight]},
    "J5" => {"RfD Chemicals (UK Contracts)", [:freight]},
    "J6" => {"RfD Building Materials (UK Contracts)", [:freight]},
    "J8" => {"RfD General Merchandise (UK Contracts)", [:freight]},
    "J9" => {"RfD Freightliner (Contracts)", [:freight]}
  }

  defp from_entry(code, {description, attributes}) do
    train? = :bus not in attributes and :ship not in attributes

    %__MODULE__{
      code: code,
      description: description,
      train?: train?,
      bus?: :bus in attributes,
      ship?: :ship in attributes,
      passenger?: :passenger in attributes,
      express?: :express in attributes
    }
  end

  defp from_entry(code, entry) when is_binary(entry) do
    %__MODULE__{
      code: code,
      description: entry,
      train?: true,
      bus?: false,
      ship?: false,
      passenger?: false,
      express?: false
    }
  end

  def get, do: Enum.map(@categories, fn {code, entry} -> from_entry(code, entry) end)

  def get(code) do
    case Map.get(@categories, code) do
      nil -> nil
      entry -> from_entry(code, entry)
    end
  end

  defimpl TungstenSwallow.Util.Linked.LinkedStructs, for: __MODULE__ do
    alias BPLAN.Data.Category
    import TungstenSwallow.Util.Linked

    def to_linked_map(
          %Category{
            code: code,
            description: description,
            passenger?: passenger?,
            express?: express?,
            ship?: ship?,
            bus?: bus?
          },
          _context
        ) do
      %{
        "type" => "Category",
        "@id" => self_ref([:bplan, :category, code]),
        "code" => code,
        "is_passenger" => passenger?,
        "is_express" => express?,
        "is_bus" => bus?,
        "is_ship" => ship?,
        "description" => description
      }
    end
  end
end
