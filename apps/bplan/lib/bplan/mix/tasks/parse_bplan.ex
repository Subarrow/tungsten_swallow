defmodule Mix.Tasks.BPLAN.ParseAll do
  @moduledoc """
  Pretty-prints all supported structs
  """
  use Mix.Task

  alias BPLAN

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(
      label <> ": " <> inspect(value, pretty: true, limit: :infinity, printable_limit: :infinity)
    )
  end

  @shortdoc "Pretty-prints all supported structs"
  def run(args) do
    {:ok, file_handle} = File.open(args)
    IO.puts("Reading BPLAN")

    BPLAN.from_file_handle(file_handle)
    |> Enum.each(&print_inspect(&1, ""))
  end
end
