defmodule Mix.Tasks.BPLAN.DumpReference do
  @moduledoc """
  Pretty-prints all structs from REF records
  """
  use Mix.Task

  alias BPLAN
  alias BPLAN.Schema.Reference

  @spec print_inspect(any(), binary()) :: any()
  defp print_inspect(value, label) do
    IO.puts(
      label <> ": " <> inspect(value, pretty: true, limit: :infinity, printable_limit: :infinity)
    )
  end

  @shortdoc "Pretty-prints all structs from REF records"
  def run(args) do
    {:ok, file_handle} = File.open(args)
    IO.puts("Reading BPLAN")

    bplan = BPLAN.from_file_handle(file_handle)

    IO.puts("Filtering BPLAN")

    bplan_ref =
      bplan
      |> Enum.filter(fn
        %Reference{} -> true
        _ -> false
      end)
      |> Enum.group_by(fn %Reference{code_type: code_type} -> code_type end, fn %Reference{
                                                                                  code: code,
                                                                                  description:
                                                                                    description
                                                                                } ->
        {code, description}
      end)
      |> Enum.map(fn {key, value} -> {key, Map.new(value)} end)
      |> Map.new()

    print_inspect(bplan_ref, "")
  end
end
