defmodule BPLAN do
  @moduledoc """
  BPLAN contains information on the meaning of various codes in both Network Rail
  data and Darwin (e.g. categories and activities), as well as information on the
  names and codes of locations, the physical layout of the rail network, and the
  allowances and times expected between locations.

  Note that this file contains a lot of out-of-date information.

  https://wiki.openraildata.com/index.php?title=BPLAN_Geography
  """

  @behaviour TungstenSwallow.Util.StaticFileDefinition

  alias BPLAN.Schema.Location
  alias BPLAN.Schema.NetworkLink
  alias BPLAN.Schema.Reference
  alias BPLAN.Schema.TimingLink
  alias BPLAN.Schema.Footer

  defp split_strip_line(line) do
    line
    |> String.split("\t")
    |> Enum.map(&String.trim/1)
    |> Enum.map(fn
      "" -> nil
      field -> field
    end)
  end

  def from_file_handle(file_handle, accumulator, opts \\ %{}) do
    line = IO.read(file_handle, :line)

    no_timing_link? = opts[:no_timing_link?] || false

    case parse_line(line) do
      nil ->
        from_file_handle(file_handle, accumulator, opts)

      %TimingLink{} when no_timing_link? ->
        Enum.reverse([footer_from_file_handle(file_handle) | accumulator])

      record = %Footer{} ->
        Enum.reverse([record | accumulator])

      record ->
        from_file_handle(file_handle, [record | accumulator], opts)
    end
  end

  def from_file_handle(file_handle) do
    file_contents = IO.read(file_handle, :eof)
    file_contents |> String.split("\n") |> Enum.map(&parse_line/1) |> Enum.reject(&is_nil/1)
  end

  def parse_line(line) do
    line_split = split_strip_line(line)

    case line_split do
      ["REF", "A" | _rest] -> Reference.from_fields(line_split)
      ["LOC", "A" | _rest] -> Location.from_fields(line_split)
      ["NWK", "A" | _rest] -> NetworkLink.from_fields(line_split)
      ["TLK", "A" | _rest] -> TimingLink.from_fields(line_split)
      ["PIT" | _rest] -> Footer.from_fields(line_split)
      _ -> nil
    end
  end

  def footer_from_file_handle(file_handle) do
    {:ok, _newpos} = :file.position(file_handle, {:eof, -300})

    last_contents = IO.read(file_handle, :eof)

    {:ok, _newpos} = :file.position(file_handle, :bof)

    last_contents
    |> String.split("\n")
    |> Enum.filter(fn
      <<"PIT", _rest::binary>> -> true
      _ -> false
    end)
    |> List.first()
    |> split_strip_line()
    |> Footer.from_fields()
  end

  def header_raw_from_file_handle(file_handle) do
    {:ok, _newpos} = :file.position(file_handle, :bof)
    first_line = IO.read(file_handle, :line)
    {:ok, _newpos} = :file.position(file_handle, :bof)
    first_line
  end

  def get_version(file_handle) do
    case {
      :file.position(file_handle, :bof),
      IO.read(file_handle, :line),
      :file.position(file_handle, :bof)
    } do
      {{:ok, _newpos_1}, first_line, {:ok, _newpos_2}} when is_binary(first_line) ->
        {:ok, first_line}

      {a, b, c} ->
        [a, b, c]
        |> Enum.filter(fn
          {:error, _reason} -> true
          :eof -> true
          _ -> false
        end)
        |> Enum.map(fn
          {:error, reason} -> {:error, reason}
          :eof -> {:error, :eof}
        end)
        |> List.first()
    end
  end

  def read!(file_handle, args) do
    from_file_handle(file_handle,[], args)
  end
end
