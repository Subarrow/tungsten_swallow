defmodule BPLAN.Util.Normalise do
  def parse_date!(date_text) do
    date_text |> String.split("-") |> Enum.reverse() |> Enum.join("-") |> Date.from_iso8601!()
  end

  def normalise_date!(date_text) do
    [date_text, _time] = :binary.split(date_text, " ")
    parse_date!(date_text)
  end

  def normalise_nullable_date!(nil) do
    nil
  end

  def normalise_nullable_date!(date_text) do
    normalise_date!(date_text)
  end

  def normalise_os_reference!(os_ref_text) do
    parse_integer!(os_ref_text)
  end

  def parse_boolean!(bool_raw) do
    case bool_raw do
      "Y" -> true
      "N" -> false
    end
  end

  def parse_nullable_integer!(nil) do
    nil
  end

  def parse_nullable_integer!(int_text) do
    parse_integer!(int_text)
  end

  def parse_integer!(int_text) do
    {int_int, ""} = Integer.parse(int_text)

    int_int
  end
end
