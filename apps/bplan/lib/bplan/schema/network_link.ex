defmodule BPLAN.Schema.NetworkLink do
  @moduledoc """
  * (not mapped)              R   text  (3) record type ("NWK")
  * (not mapped)              R   text  (1) action code ("A")
  * origin_location             R/0 text  (7)
  * destination_location        R/0 text  (7)
  * running_line_code           R/0 text  (3)
  * running_line_description    O   text (20)
  * start_date                  R   date (19)
  * end_date                    O   date (19)
  * initial_direction           R   text  (1) U/D -> :up/:down
  * final_direction             O   text  (1) U/D -> :up/:down/nil
  * distance                    O    int  (5)
  * doo_passenger?              R*  bool  (1) Y/N
  * doo_non_passenger?          R*  bool  (1) Y/N
  * radio_electric_token_block? R*  bool  (1) Y/N
  * zone                        R   text  (1)
  * reversible_line             R   text  (1) B/R/N -> :bidirectional/:reversible/:neither
  * power_supply_type           R   text  (1)
  * route_availability          R/0 text  (2)
  * maximum_train_length        O    int  (4)
  """

  import BPLAN.Util.Normalise

  @type direction :: :up | :down
  @type reversible_line :: :bidirectional | :reversible | :neither

  @type t :: %__MODULE__{
          origin_tiploc: binary(),
          destination_tiploc: binary(),
          running_line_code: binary(),
          running_line_description: binary() | nil,
          start_date: Date.t(),
          end_date: Date.t() | nil,
          initial_direction: direction(),
          final_direction: direction() | nil,
          distance: pos_integer() | nil,
          doo_passenger?: boolean(),
          doo_non_passenger?: boolean(),
          radio_electric_token_block?: boolean(),
          zone: binary(),
          reversible_line: reversible_line(),
          power_supply_type: binary(),
          route_availability: binary(),
          maximum_train_length: pos_integer()
        }

  @enforce_keys [
    :origin_tiploc,
    :destination_tiploc,
    :running_line_code,
    :running_line_description,
    :start_date,
    :end_date,
    :initial_direction,
    :final_direction,
    :distance,
    :doo_passenger?,
    :doo_non_passenger?,
    :radio_electric_token_block?,
    :zone,
    :reversible_line,
    :power_supply_type,
    :route_availability,
    :maximum_train_length
  ]

  defstruct @enforce_keys

  defp normalise_reversible(reversible_raw) do
    case reversible_raw do
      "B" -> :bidirectional
      "R" -> :reversible
      "N" -> :neither
    end
  end

  defp normalise_nullable_direction(nil), do: nil

  defp normalise_nullable_direction(direction_raw), do: normalise_direction(direction_raw)

  defp normalise_direction(direction_raw) do
    case direction_raw do
      "U" -> :up
      "D" -> :down
    end
  end

  def from_fields([
        "NWK",
        "A",
        origin_tiploc,
        destination_tiploc,
        running_line_code,
        running_line_description,
        start_date_raw,
        end_date_raw,
        initial_direction_raw,
        final_direction_raw,
        distance_raw,
        doo_passenger_raw,
        doo_non_passenger_raw,
        retb_raw,
        zone,
        reversible_line_raw,
        power_supply_type,
        route_availability,
        maximum_train_length_raw
      ]) do
    %__MODULE__{
      origin_tiploc: origin_tiploc,
      destination_tiploc: destination_tiploc,
      running_line_code: running_line_code,
      running_line_description: running_line_description,
      start_date: normalise_date!(start_date_raw),
      end_date: normalise_nullable_date!(end_date_raw),
      initial_direction: normalise_direction(initial_direction_raw),
      final_direction: normalise_nullable_direction(final_direction_raw),
      distance: parse_nullable_integer!(distance_raw),
      doo_passenger?: parse_boolean!(doo_passenger_raw),
      doo_non_passenger?: parse_boolean!(doo_non_passenger_raw),
      radio_electric_token_block?: parse_boolean!(retb_raw),
      zone: zone,
      reversible_line: normalise_reversible(reversible_line_raw),
      power_supply_type: power_supply_type,
      route_availability: route_availability,
      maximum_train_length: parse_nullable_integer!(maximum_train_length_raw)
    }
  end
end
