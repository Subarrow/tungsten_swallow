defmodule BPLAN.Schema.Footer do
  @moduledoc """
  File footer. Establishes addition/change/delete counts for each record type.
  Only snapshots are available to the public, and so these only contain additions.

  The records are listed in order of appearance in the file, and so with a little
  effort and seeking can be used to create an offset table.

  * (not mapped) R binary(3) record type - "PIT"
  """

  @enforce_keys [:counts]
  defstruct @enforce_keys

  def from_fields(["PIT" | counts]) do
    %__MODULE__{
      counts: from_other_fields(counts, [])
    }
  end

  def from_other_fields([], acc), do: Enum.reverse(acc)

  def from_other_fields([field_name, added, "0", "0" | rest], acc) do
    from_other_fields(rest, [{field_name, added} | acc])
  end
end
