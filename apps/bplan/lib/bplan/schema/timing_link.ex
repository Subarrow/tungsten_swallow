defmodule BPLAN.Schema.TimingLink do
  @moduledoc """
    * (not mapped)       R   binary   (3)                 record type ("TLK")
    * (not mapped)       R   binary   (1)                 action code ("A")
    * origin_tiploc      R/0 binary   (7)                 origin tiploc
    * destination_tiploc R/0 binary   (7)                 destination tiploc
    * running_line_code  O/0 binary   (3)                 e.g. FL, SL, UFL, etc
    * traction_type      R/0 binary   (6)                 Type of traction, e.g. 92, 373, EMU, BUS, 2x20
    * trailing_load      O/0 binary   (5)                 tonnes (4), E/L empty/loaded (1)
    * speed              O/0 binary   (3) -> integer      Maximum permitted speed
    * route_allowance    O/0 binary   (3)                 Route allowance (1) and gauge (2)
    * entry_speed        O/0 binary   (3) -> integer/nil  0 for starting, -1 for maximum appropriate/permitted
    * exit_speed         O/0 binary   (3) -> integer/nil  0 for starting, -1 for maximum appropriate/permitted
    * start_date         R/0 binary  (19) -> DateTime     Start date
    * end_date           O/0 binary  (19) -> DateTime/nil End date
    * running_time       R   binary   (7) -> integer      (sign)mmm'ss e.g. +100'00 -> +6000
    * description        O   binary  (64)                 description

  """

  import BPLAN.Util.Normalise

  @type t :: %__MODULE__{
          origin_tiploc: binary(),
          destination_tiploc: binary(),
          running_line_code: binary(),
          traction_type: binary(),
          trailing_load: binary() | nil,
          speed: integer() | nil,
          route_allowance: binary() | nil,
          entry_speed: integer() | nil,
          exit_speed: integer() | nil,
          start_date: Date.t(),
          end_date: Date.t() | nil,
          running_time: integer(),
          description: binary() | nil
        }

  @enforce_keys [
    :origin_tiploc,
    :destination_tiploc,
    :running_line_code,
    :traction_type,
    :trailing_load,
    :speed,
    :route_allowance,
    :entry_speed,
    :exit_speed,
    :start_date,
    :end_date,
    :running_time,
    :description
  ]

  defstruct [
    :origin_tiploc,
    :destination_tiploc,
    :running_line_code,
    :traction_type,
    :trailing_load,
    :speed,
    :route_allowance,
    :entry_speed,
    :exit_speed,
    :start_date,
    :end_date,
    :running_time,
    :description
  ]

  defp normalise_running_time("+" <> running_time) do
    [minutes_raw, seconds_raw] = :binary.split(running_time, "'")
    minutes = parse_integer!(minutes_raw)
    seconds = parse_integer!(seconds_raw)

    minutes * 60 + seconds
  end

  def from_fields([
        "TLK",
        "A",
        origin_tiploc,
        destination_tiploc,
        running_line_code,
        traction_type,
        trailing_load,
        speed_raw,
        route_allowance,
        entry_speed_raw,
        exit_speed_raw,
        start_date_raw,
        end_date_raw,
        running_time_raw,
        description
      ]) do
    %__MODULE__{
      origin_tiploc: origin_tiploc,
      destination_tiploc: destination_tiploc,
      running_line_code: running_line_code,
      traction_type: traction_type,
      trailing_load: trailing_load,
      speed: parse_nullable_integer!(speed_raw),
      route_allowance: route_allowance,
      entry_speed: parse_nullable_integer!(entry_speed_raw),
      exit_speed: parse_nullable_integer!(exit_speed_raw),
      start_date: normalise_date!(start_date_raw),
      end_date: normalise_nullable_date!(end_date_raw),
      running_time: normalise_running_time(running_time_raw),
      description: description
    }
  end
end
