defmodule BPLAN.Schema.Location do
  @moduledoc """
    * (not mapped)      R binary  (3)                  record type ("LOC")
    * (not mapped)      R binary  (1)                  action code ("A")
    * tiploc            R binary  (7)                  location code
    * name              R binary (32)                  location name
    * start_date        R binary (19) -> Date          start date
    * end_date          O binary (19) -> Date/nil      end date
    * os_easting        O binary  (6) -> integer/nil   OS grid reference
    * os_northing       O binary  (6) -> integer/nil   OS grid reference
    * timing_point_type R binary  (1) -> atom          timing point type; T->trust: TRUST, M->mandatory: Mandatory, O->optional: Optional
    * zone              R binary  (1)                  zone responsible for maintaining record
    * stanox            O binary  (5) -> binary/nil    STANOX code
    * off_network?      R binary  (1) -> boolean       Y/true if location is off network, N/false if on-network.
    * force_lpb         O binary  (1) -> atom/nil      LPB; L if running line should appear in schedule when approaching location, P if when departing, B for both, nil for none

    Note that 'off network' is seemingly very loosely defined, PARISND (i.e. France) is supposedly not off the network, same for some LT stations.
  """

  import BPLAN.Util.Normalise

  @type timing_point_type :: :mandatory | :trust | :optional

  @type t :: %__MODULE__{
          tiploc: binary(),
          name: binary(),
          start_date: Date.t(),
          end_date: Date.t() | nil,
          os_easting: integer() | nil,
          os_northing: integer() | nil,
          timing_point_type: timing_point_type(),
          zone: binary(),
          stanox: binary(),
          off_network?: boolean(),
          force_lpb: binary()
        }

  @enforce_keys [
    :tiploc,
    :name,
    :start_date,
    :end_date,
    :os_easting,
    :os_northing,
    :timing_point_type,
    :zone,
    :stanox,
    :off_network?,
    :force_lpb
  ]
  defstruct [
    :tiploc,
    :name,
    :start_date,
    :end_date,
    :os_easting,
    :os_northing,
    :timing_point_type,
    :zone,
    :stanox,
    :off_network?,
    :force_lpb
  ]

  defp normalise_timing_point_type(timing_point_type_text) do
    case timing_point_type_text do
      "M" -> :mandatory
      "T" -> :trust
      "O" -> :optional
    end
  end

  defp normalise_force_lpb(force_lpb_raw) do
    force_lpb_raw
  end

  def from_fields([
        "LOC",
        "A",
        tiploc,
        name,
        start_date_raw,
        end_date_raw,
        os_easting_raw,
        os_northing_raw,
        timing_point_type_raw,
        zone,
        stanox,
        off_network_raw,
        force_lpb
      ]) do
    %__MODULE__{
      tiploc: tiploc,
      name: name,
      start_date: normalise_date!(start_date_raw),
      end_date: normalise_nullable_date!(end_date_raw),
      os_easting: normalise_os_reference!(os_easting_raw),
      os_northing: normalise_os_reference!(os_northing_raw),
      timing_point_type: normalise_timing_point_type(timing_point_type_raw),
      zone: zone,
      stanox: stanox,
      off_network?: parse_boolean!(off_network_raw),
      force_lpb: normalise_force_lpb(force_lpb)
    }
  end
end
