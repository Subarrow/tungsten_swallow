defmodule BPLAN.Schema.Reference do
  @moduledoc """
    * (not mapped)      R binary  (3)                  record type ("REF")
    * (not mapped)      R binary  (1)                  action code ("A")
    * code_type         R binary  (3)                  reference code type
    * code              O binary  (3)                  reference code
    * description       R binary (32)                  description of field
  """

  @type t :: %__MODULE__{
          code_type: binary(),
          code: binary() | nil,
          description: binary()
        }

  @enforce_keys [:code_type, :code, :description]
  defstruct @enforce_keys

  def from_fields([
        "REF",
        "A",
        code_type,
        code,
        description
      ]) do
    %__MODULE__{code_type: code_type, code: code, description: description}
  end
end
