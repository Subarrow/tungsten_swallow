defmodule BPLAN.ParsesFileTest do
  use ExUnit.Case

  doctest BPLAN

  alias BPLAN
  alias BPLAN.Schema.TimingLink
  alias BPLAN.Schema.Location
  alias BPLAN.Schema.Reference

  @expected_output [
    %BPLAN.Schema.Reference{code_type: "PWR", code: nil, description: "Undefined"},
    %BPLAN.Schema.Reference{code_type: "PWR", code: "A", description: "25kV overhead"},
    %BPLAN.Schema.Reference{code_type: "PWR", code: "B", description: "3rd Rail DC"},
    %BPLAN.Schema.Reference{code_type: "PWR", code: "C", description: "4 Rail DC"},
    %BPLAN.Schema.Reference{
      code_type: "PWR",
      code: "D",
      description: "25kV overhead & 3rd Rail DC"
    },
    %BPLAN.Schema.Reference{
      code_type: "PWR",
      code: "E",
      description: "25kV overhead & 4 Rail DC"
    },
    %BPLAN.Schema.Reference{code_type: "RES", code: nil, description: "Undefined"},
    %Location{
      end_date: nil,
      force_lpb: nil,
      name: "Sizewell BNFL (MAGNOX)",
      off_network?: false,
      os_easting: 645_500,
      os_northing: 262_400,
      stanox: "49112",
      start_date: ~D[1995-01-01],
      timing_point_type: :optional,
      tiploc: "SIZCEGB",
      zone: "3"
    },
    %BPLAN.Schema.NetworkLink{
      destination_tiploc: "SXMNDHJ",
      distance: 7081,
      doo_non_passenger?: false,
      doo_passenger?: false,
      end_date: nil,
      final_direction: :up,
      initial_direction: :up,
      maximum_train_length: nil,
      origin_tiploc: "SIZCEGB",
      power_supply_type: nil,
      radio_electric_token_block?: false,
      reversible_line: :neither,
      route_availability: "0",
      running_line_code: nil,
      running_line_description: nil,
      start_date: ~D[1995-01-01],
      zone: "3"
    },
    %BPLAN.Schema.NetworkLink{
      destination_tiploc: "SIZCEGB",
      distance: 7242,
      doo_non_passenger?: false,
      doo_passenger?: false,
      end_date: nil,
      final_direction: :down,
      initial_direction: :down,
      maximum_train_length: nil,
      origin_tiploc: "SXMNDHJ",
      power_supply_type: nil,
      radio_electric_token_block?: false,
      reversible_line: :neither,
      route_availability: "0",
      running_line_code: nil,
      running_line_description: nil,
      start_date: ~D[1995-01-01],
      zone: "3"
    },
    %BPLAN.Schema.NetworkLink{
      destination_tiploc: "EIGG",
      distance: 0,
      doo_non_passenger?: false,
      doo_passenger?: false,
      end_date: nil,
      final_direction: :up,
      initial_direction: :up,
      maximum_train_length: nil,
      origin_tiploc: "MLAIG",
      power_supply_type: nil,
      radio_electric_token_block?: false,
      reversible_line: :neither,
      route_availability: "0",
      running_line_code: "SHP",
      running_line_description: nil,
      start_date: ~D[2016-12-11],
      zone: "9"
    },
    %BPLAN.Schema.NetworkLink{
      destination_tiploc: "MORAR",
      distance: 4426,
      doo_non_passenger?: true,
      doo_passenger?: false,
      end_date: nil,
      final_direction: :up,
      initial_direction: :up,
      maximum_train_length: nil,
      origin_tiploc: "MLAIG",
      power_supply_type: nil,
      radio_electric_token_block?: false,
      reversible_line: :neither,
      route_availability: "0",
      running_line_code: nil,
      running_line_description: nil,
      start_date: ~D[1995-01-01],
      zone: "9"
    },
    %BPLAN.Schema.NetworkLink{
      destination_tiploc: "MORAR",
      distance: 4426,
      doo_non_passenger?: false,
      doo_passenger?: false,
      end_date: nil,
      final_direction: :up,
      initial_direction: :up,
      maximum_train_length: nil,
      origin_tiploc: "MLAIG",
      power_supply_type: nil,
      radio_electric_token_block?: false,
      reversible_line: :neither,
      route_availability: "0",
      running_line_code: "BUS",
      running_line_description: nil,
      start_date: ~D[2004-12-12],
      zone: "9"
    },
    %TimingLink{
      description: nil,
      destination_tiploc: "SXMNDHJ",
      end_date: nil,
      entry_speed: 0,
      exit_speed: 0,
      origin_tiploc: "SIZCEGB",
      route_allowance: nil,
      running_line_code: nil,
      running_time: 2580,
      speed: 40,
      start_date: ~D[2006-06-11],
      traction_type: "40-OTM",
      trailing_load: nil
    },
    %TimingLink{
      description: nil,
      destination_tiploc: "SXMNDHJ",
      end_date: nil,
      entry_speed: 0,
      exit_speed: 0,
      origin_tiploc: "SIZCEGB",
      route_allowance: nil,
      running_line_code: nil,
      running_time: 2580,
      speed: 75,
      start_date: ~D[2005-12-11],
      traction_type: "SRO",
      trailing_load: nil
    },
    %TimingLink{
      description: nil,
      destination_tiploc: "SXMNDHJ",
      end_date: nil,
      entry_speed: 0,
      exit_speed: -1,
      origin_tiploc: "SIZCEGB",
      route_allowance: nil,
      running_line_code: nil,
      running_time: 2580,
      speed: 75,
      start_date: ~D[2013-12-08],
      traction_type: "UTU-T",
      trailing_load: nil
    },
    %TimingLink{
      description: nil,
      destination_tiploc: "SIZCEGB",
      end_date: nil,
      entry_speed: 0,
      exit_speed: 0,
      origin_tiploc: "SXMNDHJ",
      route_allowance: nil,
      running_line_code: nil,
      running_time: 2580,
      speed: 75,
      start_date: ~D[2005-12-11],
      traction_type: "SRO",
      trailing_load: nil
    },
    %TimingLink{
      description: nil,
      destination_tiploc: "SIZCEGB",
      end_date: nil,
      entry_speed: -1,
      exit_speed: 0,
      origin_tiploc: "SXMNDHJ",
      route_allowance: nil,
      running_line_code: nil,
      running_time: 2580,
      speed: 30,
      start_date: ~D[2013-12-08],
      traction_type: "UTU-R",
      trailing_load: nil
    },
    %BPLAN.Schema.Footer{counts: [{"REF", "7"}, {"LOC", "1"}, {"NWK", "5"}, {"TLK", "5"}]}
  ]

  test "Parses a file O.K." do
    {:ok, file_handle} = File.open("test/bplan/test_input.txt")

    assert BPLAN.from_file_handle(file_handle, []) == @expected_output
  end
end
