# TungstenSwallow

Port-mapping daemon must be started
`epmd -daemon`

config/(env).secret.exs
```
import Config

config :tungsten_swallow,
  node_name: :"tungsten_swallow@127.0.0.1",
  relay_host: "example.com",
  relay_port: 61613,
  relay_username: "arsenicCatnip",
  relay_password: "hunter2",
  relay_queue: "/amq/queue/darwin-v16-xml",
  stream_mode: :last
```

To init tables and run:
```
elixir --name "tungsten_swallow_test@127.0.0.1" -S mix tungsten_swallow.init_mnesia

elixir --name "tungsten_swallow_test@127.0.0.1" -S mix run --no-halt
```

Note that you may need to install erlang-ssh, erlang-ftp and erlang-xmerl packages or similar.

